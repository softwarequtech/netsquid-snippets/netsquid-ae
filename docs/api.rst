API Documentation
-----------------

Below are the modules of this package.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/ae_chain_setup.rst
   modules/ae_classes.rst
   modules/ae_component_setup.rst
   modules/ae_magic_distributor.rst
   modules/ae_state_delivery_samplers.rst
   modules/forced_magic_state_generator.rst
   modules/ae_parameter_set.rst
   modules/ae_protocols.rst
   modules/application_protocols.rst
   modules/datacollectors.rst
   modules/multi_photon_detectors.rst
   modules/multi_photon_povms.rst
   modules/multi_photon_tools.rst
   modules/parameter_set_collection.rst
   modules/protocol_event_types.rst
   modules/qlossmodel.rst
   modules/spdcsource.rst
   modules/utility.rst


