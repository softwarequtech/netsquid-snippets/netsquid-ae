Multi-photon POVMs
-------------------------

Collection of QuAlg functions to calculate symbolic multi-photon POVMs for arbitrary photon numbers.

.. automodule:: netsquid_ae.multi_photon_povms
    :members: