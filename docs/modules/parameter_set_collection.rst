Parameter Set Collection
-------------------------

Collection of ParameterSets for use with atomic ensemble simulations.

.. automodule:: netsquid_ae.parameter_set_collection
    :members: