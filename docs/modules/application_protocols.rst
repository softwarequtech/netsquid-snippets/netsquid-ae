Application Protocols
-------------------------
Collection of Protocols used for an application of the entanglement instead of the operation of a repeater chain.
For example end node detection to use the repeater chain for a Quantum Key Distribution Experiment.

.. automodule:: netsquid_ae.application_protocols
    :members: