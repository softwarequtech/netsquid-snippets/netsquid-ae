Multi-photon Tools
-------------------------

Collection of Tools needed to convert the symbolic QuAlg POVMS to NetSquid operators with variable visibility.

.. automodule:: netsquid_ae.multi_photon_tools
    :members: