AE MagicDistributor
-------------------------
MagicDistributor for atomic ensembles.

.. automodule:: netsquid_ae.ae_magic_distributor
    :members: