Atomic Ensemble Protocols
-------------------------
Collection of Protocols necessary for the operation of a repeater chain using atomic ensembles.

.. automodule:: netsquid_ae.ae_protocols
    :members:
