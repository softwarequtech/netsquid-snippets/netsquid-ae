Protocol Event Types
-------------------------
Event Types for Protocols.

.. automodule:: netsquid_ae.protocol_event_types
    :members: