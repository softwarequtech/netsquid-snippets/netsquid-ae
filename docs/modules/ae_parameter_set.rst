Parameter Set
-------------------------

Class defining the set of Parameters for an Atomic Ensemble bases simulation.

.. automodule:: netsquid_ae.ae_parameter_set
    :members: