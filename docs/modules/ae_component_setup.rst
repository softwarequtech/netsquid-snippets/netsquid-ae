Component Setup
-------------------------
Collection of scripts to set up the components necessary for a simulation of a repeater chain.

.. automodule:: netsquid_ae.ae_component_setup
    :members: