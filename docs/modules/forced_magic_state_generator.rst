AE Forced Magic State Generator
-------------------------
Generator for atomic ensemble forced magic states.

.. automodule:: netsquid_ae.forced_magic_state_generator
    :members:
forced_magic_state_generator