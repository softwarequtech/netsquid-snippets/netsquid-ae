Repeater Chain Setup
-------------------------

Collection of Scripts to set up a simulation of a repeater chain experiment.

.. automodule:: netsquid_ae.ae_chain_setup
    :members: