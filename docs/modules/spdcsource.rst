SPDC Source
-------------------------
Single Parametric DownConversion source producing entangled photon pairs.

.. automodule:: netsquid_ae.spdcsource
    :members: