Multi-photon Detectors
-------------------------
Subclasses of the BSM and QKD detector of `netsquid-physlayer`,
which have the same functionality but then modified to work
with our binary-encoded multi-photon states.

.. automodule:: netsquid_ae.multi_photon_detectors
    :members: