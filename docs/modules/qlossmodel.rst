Loss Models
-------------------------
Loss models for various components.

.. automodule:: netsquid_ae.qlossmodel
    :members: