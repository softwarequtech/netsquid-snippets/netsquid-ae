Classes
-------------------------
Helper classes that use the functions of `ae_component_setup.py`
in order to setup classes for the Nodes and Connections
that are used in this snippet.

.. automodule:: netsquid_ae.ae_classes
    :members: