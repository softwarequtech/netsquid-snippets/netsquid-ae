import unittest
import itertools

import netsquid as ns
from pydynaa.core import Entity
import netsquid.qubits.qubitapi as qapi
from netsquid.components.qsource import SourceStatus

from netsquid_ae.spdcsource import SPDCSource


class TestSPDCSource(unittest.TestCase):
    """Unit tests for the SPDCSource module."""

    def setUp(self):
        """Sets up a new simulation environment for each test."""
        ns.sim_reset()
        self.god = Entity()  # helper entity

    def get_output_mem(self, message):
        self.mem_received.append(message)

    def get_output_con(self, message):
        self.con_received.append(message)

    def create_SPDC(self, spectral_modes=1, encoding="presence_absence", mu=None, status=SourceStatus.INTERNAL,
                    frequency=1e9, multi_photon=True, emission_probability=None, Rate=None, g2=None):
        self.con_received = []
        self.mem_received = []
        name = "SPDC_test"
        spdc_test = SPDCSource(name=name, frequency=frequency, spectral_modes=spectral_modes, encoding=encoding,
                               mu=mu, status=status, multi_photon=multi_photon,
                               emission_probability=emission_probability, Rate=Rate, g2=g2)
        spdc_test.ports["qout0"].bind_output_handler(self.get_output_mem)
        spdc_test.ports["qout1"].bind_output_handler(self.get_output_con)
        return spdc_test

    def test_encodings_operation(self):
        """Test if the SPDC source works with all encodings and regular thermal distribution."""
        for encoding, multi_photon in itertools.product(["time_bin", "presence_absence"], [True, False]):
            if multi_photon:
                self.create_SPDC(encoding=encoding, mu=0.1, multi_photon=multi_photon)
            else:
                self.create_SPDC(encoding=encoding, emission_probability=[0.5, 0.5, 0, 0], multi_photon=multi_photon)
            ns.sim_run(1)
            qubits_mem = self.mem_received[0].items
            qubits_con = self.mem_received[0].items
            self.assertEqual(len(qubits_mem), len(qubits_con))
            if multi_photon:
                if encoding == "presence_absence":
                    self.assertEqual(len(qubits_mem), 2)
                else:
                    self.assertEqual(len(qubits_mem), 4)
            else:
                self.assertTrue(len(qubits_mem) == 1)
            ns.sim_reset()

    def test_multiplexed_generation(self):
        """Test the generation of frequency multiplexed qubit pairs"""
        spectral_modes = 100
        self.create_SPDC(spectral_modes=spectral_modes, mu=0.9)
        ns.sim_run(1)
        qubits_mem = self.mem_received[0].items
        qubits_con = self.con_received[0].items
        self.assertTrue(len(qubits_mem) == len(qubits_con))
        self.assertTrue(len(qubits_mem) == 2 * spectral_modes)
        # Check whether correct qubits are entangled
        for m in range(0, spectral_modes + 1, 2):
            self.assertEqual(qapi.measure(qubits_mem[m])[0], qapi.measure(qubits_con[m])[0])
            self.assertEqual(qapi.measure(qubits_mem[m + 1])[0], qapi.measure(qubits_con[m + 1])[0])

    def test_number_of_modes_calculation(self):
        """Tests the computation of the number of modes the generation of frequency multiplexed qubit pairs"""
        spectral_modes = None
        Rate = 4.2 * 3.5e3
        g2 = 0.12
        frequency = 5e4
        source = self.create_SPDC(spectral_modes=spectral_modes, mu=0.9, Rate=Rate, g2=g2, frequency=frequency)
        ns.sim_run(1)
        self.assertTrue(int(4/g2*Rate/frequency) == source.spectral_modes)

    def test_given_input_state(self):
        """Test the generation of a given input state."""
        emission_probability = [0, 1, 0, 0]  # Create perfect photon pair / Bell state
        for encoding in ["presence_absence", "time_bin"]:
            self.create_SPDC(emission_probability=emission_probability, encoding=encoding)
            ns.sim_run(1)
            qubits_mem = self.mem_received[0].items
            qubits_con = self.con_received[0].items
            if encoding == "presence_absence":
                # Expect perfect photon pair
                self.assertEqual(qapi.measure(qubits_mem[0])[0], 0)
                self.assertEqual(qapi.measure(qubits_mem[0])[0], 0)
                self.assertEqual(qapi.measure(qubits_con[0])[1], 1)
                self.assertEqual(qapi.measure(qubits_con[0])[1], 1)
            else:
                # Expect perfect Bell state
                if qapi.measure(qubits_mem[1])[0] == 1:
                    # Single photon in the early time window from both ports
                    self.assertEqual(qapi.measure(qubits_con[1])[0], 1)
                else:
                    # Single photon in the late time window from both ports
                    self.assertEqual(qapi.measure(qubits_con[3])[0], 1)
            ns.sim_reset()


if __name__ == "__main__":
    ns.logger.setLevel(0)
    unittest.main()
