import os
import pandas
import logging
import pathlib
import unittest
import itertools
import numpy as np
import pickle

import netsquid as ns
from netsquid.nodes import Node
from netsquid.util.simlog import logger
import netsquid.qubits.qubitapi as qapi
from netsquid.util.simtools import sim_time
from netsquid.components.component import Message
from netsquid.qubits.ketstates import BellIndex, b01, b11

from netsquid_ae.spdcsource import SPDCSource, SourceStatus
from netsquid_ae.application_protocols import DetectionProtocol
from netsquid_ae.ae_classes import EndNode, RepeaterNode, QKDNode, MessagingConnection
from netsquid_ae.protocol_event_types import EVTYPE_SUCCESS, EVTYPE_FAIL
from netsquid_ae.ae_protocols import EmissionProtocol, SwapProtocol, ExtractionProtocol, MessagingProtocol
from netsquid_ae.ae_chain_setup import create_repeater_chain, create_qkd_application, create_elementary_link

from netsquid_physlayer.detectors import BSMOutcome, QKDOutcome

from netsquid_simulationtools.parameter_set import ParameterSet
from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder


class TestAtomicEnsembleProtocols(unittest.TestCase):
    """Unittests for the different Protocols used for the atomic ensemble simulation."""

    def setUp(self):
        """Sets up a new simulation environment for each test."""
        ns.sim_reset()
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)
        self.sim_params = {
            # multi-photon encoding: 'presence_absence', 'time_bin' or None
            "num_repeaters": 1,
            "multi_photon": True,
            "encoding": 'presence_absence',  # needs to be unified, this is only used for source end end node detector
            # channel
            "length": 50,                   # channel length [km]
            "channel_length_l": 12.5,       # channel length left of the detectors [km]
            "channel_length_r": 12.5,       # channel length right of the detectors [km]
            "coupling_loss_fibre": 0.,      # initial loss on channel to midpoint detectors
            "attenuation_l": 0.25,          # channel attenuation left of the detectors [dB/km]
            "attenuation_r": 0.25,          # channel attenuation right of the detectors [dB/km]
            "fibre_phase_stdv_l": np.pi / 4,
            "fibre_phase_stdv_r": np.pi / 4,
            # source
            "source_frequency": 80e6,       # frequency of the photon pair source [Hz]
            "source_num_modes": 10,         # Number of modes for the source
            "g2": None,                     # Measured heralded g2. Only used if source_num_modes = None
            "Rate": None,                   # Number of photons pr. second.  Only used if source_num_modes = None
            "mean_photon_number": 0.05,     # mean photon pair number
            "emission_probabilities": [0.5, 0.5, 0, 0],  # emission probability
            # midpoint detector
            "det_dark_count_prob": 1e-6,  # probability per detection
            "det_efficiency": 1.,       # detector efficiency
            "det_visibility": 1.,       # photon indistinguishably, should be 1 for multi-photon
            "det_num_resolving": True,  # using number or non_number resolving detectors
            # swap & end node detector
            "swap_det_dark_count_prob": 1e-6,  # probability of dark count per detection
            "swap_det_efficiency": 1.,      # detector efficiency
            "swap_det_visibility": 1.,      # photon indistinguishability (must be 1 for multi_photon=True)
            "swap_det_num_resolving": True,  # using number or non_number resolving detectors
            # memory
            "memory_coherence_time": 1e6,   # coherence time of the AFC memory
            "max_memory_efficiency": .9,    # maximum efficiency of the AFC memory
            "memory_time_dependence": "exponential",    # time-dependence of efficiency
            "mem_num_modes": 100,
            # clock
            "time_step": 1,                 # time step between clicks of the clock
            "num_attempts": 1,              # number of clicks after which the clock stops (thus stopping the protocols)
            "num_attempts_proto": -1,       # number of clicks after which the emission protocol stops
            "magic": None,
            "state_files_directory": str(pathlib.Path(__file__).parent.absolute()) + "/magic_state_data",
            "multiple_link_successes": False
        }

    def test_emission_protocol(self):
        """Tests functionality of the emission protocol."""
        # debug
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("{:.2e} -- Testing emission protocol. --".format(
                sim_time()))

        for self.sim_params["multi_photon"], self.sim_params["encoding"] in \
                itertools.product([True, False], ['presence_absence', 'time_bin']):
            self.setUp()
            self.sim_params["spectral_modes"] = 10
            # debug
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug("{:.2e} -- Encoding {}, --- Multiphoton {} --".format(
                    sim_time(), self.sim_params["encoding"], self.sim_params["multi_photon"]))

            rep_node = RepeaterNode("Emission_Test_Node", **self.sim_params)

            # create node to connect to for is_connected to succeed
            dummy = Node("Dummy Node")
            dummy.add_ports(["Dummy_port"])
            rep_node.ports["IO_Connection_L"].connect(dummy.ports["Dummy_port"])

            qubit_msg = []

            def get_qubit_message(msg):
                qubit_msg.append(msg.items)
                # discard qubits in the message so memory can reset
                for q in msg.items:
                    if q is not None:
                        qapi.discard(q)
                # suppress warning
                emission_proto._is_waiting = False

            # bind handler to dummy port
            dummy.ports["Dummy_port"].bind_input_handler(get_qubit_message)

            emission_proto = EmissionProtocol(rep_node, source=rep_node.subcomponents["PPS_L"],
                                              memory=rep_node.subcomponents["QMem_L"])
            emission_proto.start()
            for port_name in rep_node.subcomponents["QMem_L"].ports:
                rep_node.subcomponents["QMem_L"].ports[port_name].disconnect()

            rep_node.subcomponents["Clock"]._max_ticks = 1
            rep_node.subcomponents["Clock"].start()
            # run protocol and see if photons arrive at port
            ns.sim_run(10, magnitude=ns.NANOSECOND)

            self.assertEqual(1, len(qubit_msg))
            # determine correct number of qubits that should be in the message
            if self.sim_params["multi_photon"]:
                if self.sim_params["encoding"] == "presence_absence":
                    mode_multiplier = 2
                else:
                    mode_multiplier = 4
            else:
                mode_multiplier = 1

            self.assertEqual(len(qubit_msg[0]),
                             self.sim_params["source_num_modes"] * mode_multiplier)
            # send back message and see if it is stored correctly
            # check if data is still empty
            self.assertTrue(rep_node.cdata["successful_modes_L"] == [])
            self.assertTrue(rep_node.cdata["midpoint_outcomes_L"] == [])
            bsm_outcomes = []
            modes = []
            for mode in range(self.sim_params["spectral_modes"]):
                bsm_outcomes.append(BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS))
                modes.append(mode)
            message = Message(bsm_outcomes, successful_modes=modes, detection_time=ns.sim_time())

            rep_node.ports["IO_Connection_L"].tx_input(message)

            # check if data was appended correctly
            for i in range(self.sim_params["spectral_modes"]):
                self.assertEqual(rep_node.cdata["successful_modes_L"][-1][i], i)
                self.assertEqual(rep_node.cdata["midpoint_outcomes_L"][-1][i].bell_index, BellIndex.PSI_PLUS)
            assert len(rep_node.cdata["successful_modes_L"]) == 1
            assert len(rep_node.cdata["midpoint_outcomes_L"]) == 1

            # check if failure is handled correctly
            message = Message([BSMOutcome(success=False)], successful_modes=[None], detection_time=ns.sim_time())

            rep_node.ports["IO_Connection_L"].tx_input(message)
            assert len(rep_node.cdata["successful_modes_L"]) == 2
            assert len(rep_node.cdata["midpoint_outcomes_L"]) == 2
            assert rep_node.cdata["successful_modes_L"][-1] == [None]
            assert rep_node.cdata["midpoint_outcomes_L"][-1][0].success is False

            # check if functionality repeats correctly
            # debug
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug("{:.2e} Checking repeated functionality with clock {}".format(
                    sim_time(), rep_node.subcomponents["Clock"].name))
            # reset clock and at 2 more clicks to make a total of 3
            rep_node.subcomponents["Clock"].reset()
            rep_node.subcomponents["Clock"]._max_ticks = 2
            rep_node.subcomponents["Clock"].start()

            ns.sim_run(duration=5, magnitude=ns.MICROSECOND)

            self.assertEqual(3, emission_proto.num_attempts)
            # no messaged were sent back so information on node should still be the same as before
            assert len(rep_node.cdata["successful_modes_L"]) == 2
            assert len(rep_node.cdata["midpoint_outcomes_L"]) == 2
            assert rep_node.cdata["successful_modes_L"][-1] == [None]
            assert rep_node.cdata["midpoint_outcomes_L"][-1][0].success is False

            # test stopping
            rep_node.subcomponents["PPS_L"].ports["qout1"].connect(rep_node.subcomponents["QMem_L"].ports["qin"])
            emission_proto.stop()

    def test_delay_in_emission_protocol(self):
        """Test that emission protocol can trigger source with a specified delay."""

        class DummyEmissionProtocol(EmissionProtocol):
            """Dummy emission protocol that keeps track of when source is triggered but is not connected."""

            def __init__(self, node, memory, source, delay=0):
                self.trigger_times = []
                super().__init__(node, memory, source, delay=delay)

            def _trigger(self, event):
                """Instead of triggering the source, just record when this method is called."""
                self.trigger_times.append(ns.sim_time())

            def is_connected(self):
                return True

        for delay in [0, 2, 4]:
            ns.sim_reset()
            rep_node = RepeaterNode("Emission_Test_Node", **self.sim_params)
            emission_proto = DummyEmissionProtocol(rep_node, source=rep_node.subcomponents["PPS_L"],
                                                   memory=rep_node.subcomponents["QMem_L"], delay=delay)

            emission_proto.start()

            rep_node.subcomponents["Clock"]._max_ticks = 3
            rep_node.subcomponents["Clock"].start()
            ns.sim_run(200, magnitude=ns.NANOSECOND)

            # Note: the start_time of the node is assumed to be it default value 0 here.
            assert emission_proto.trigger_times == [k * self.sim_params["time_step"] + delay for k in [0, 1, 2]]

    def test_swap_protocol(self):
        """Tests functionality of the swap protocol."""
        self.setUp()
        rep_node = RepeaterNode("Swap_Test_Node", **self.sim_params)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("{:.2e} -- Testing swap protocol on {} --".format(
                sim_time(), rep_node.name))

        # create node to connect to for is_connected to succeed
        dummy = Node("Dummy Node")
        dummy.add_ports(["Dummy_port_L", "Dummy_port_R"])
        rep_node.ports["IO_Connection_L"].connect(dummy.ports["Dummy_port_L"])
        rep_node.ports["IO_Connection_R"].connect(dummy.ports["Dummy_port_R"])

        # set up emission protocols with qubits in memory
        ep1 = EmissionProtocol(rep_node, source=rep_node.subcomponents["PPS_L"],
                               memory=rep_node.subcomponents["QMem_L"])
        ep2 = EmissionProtocol(rep_node, source=rep_node.subcomponents["PPS_R"],
                               memory=rep_node.subcomponents["QMem_R"])
        ep1.start()
        ep2.start()
        rep_node.cdata["successful_modes_L"] = [[0]]
        rep_node.cdata["successful_modes_R"] = [[None], [0]]
        rep_node.cdata["detector_outcome_L"] = [[BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)]]
        rep_node.cdata["detector_outcome_R"] = [[BSMOutcome(success=False)]]
        rep_node.subcomponents["Clock"]._max_ticks = 1
        rep_node.subcomponents["Clock"].start()
        # run sim to make sure there are qubits in the memory
        ns.sim_run(duration=1, magnitude=ns.SECOND)
        # set up swap protocol
        swap_proto = SwapProtocol(rep_node, ep1, ep2)
        swap_proto.start()

        # schedule arbitrary events
        ep1._schedule_now(EVTYPE_SUCCESS)
        ep2._schedule_now(EVTYPE_FAIL)
        # check swap data is still empty
        self.assertTrue(len(rep_node.cdata["swap_outcomes"]) == 0)
        ns.sim_run(duration=1, magnitude=ns.SECOND)
        # check swap data has been recorded
        self.assertTrue(len(rep_node.cdata["swap_outcomes"]) == 1)
        # test stopping
        swap_proto.stop()
        self.setUp()
        # TODO add more tests to investigate different cases (e.g. encodings , what happens to qubit = None)

    def test_single_swap(self):
        """Tests if swap protocol correctly handles single and multiple swaps."""
        class DummySwapProtocol(SwapProtocol):
            def _select_modes_to_swap(self, successful_modes_l, successful_modes_r):
                return [1, 2, 3, 4], [5, 6, 7, 8], None, None

        self.setUp()
        self.sim_params["multi_photon"] = False
        rep_node = RepeaterNode("Swap_Test_Node", **self.sim_params)
        ep1 = EmissionProtocol(rep_node, source=rep_node.subcomponents["PPS_L"],
                               memory=rep_node.subcomponents["QMem_L"])
        ep2 = EmissionProtocol(rep_node, source=rep_node.subcomponents["PPS_R"],
                               memory=rep_node.subcomponents["QMem_R"])

        single_swap_protocol = DummySwapProtocol(node=rep_node, proto_l=ep1, proto_r=ep2, swap_single_pair_only=True)
        multi_swap_protocol = DummySwapProtocol(node=rep_node, proto_l=ep1, proto_r=ep2, swap_single_pair_only=False)

        rep_node.subcomponents["QMem_L"].put(qapi.create_qubits(4), [1, 2, 3, 4])
        rep_node.subcomponents["QMem_R"].put(qapi.create_qubits(4), [5, 6, 7, 8])

        rep_node.cdata["successful_modes_L"].append([1])
        rep_node.cdata["successful_modes_R"].append([0])

        # Test single mode is selected
        single_swap_protocol._trigger(self)
        assert rep_node.cdata["swapped_modes_L"] == [[1]]
        assert rep_node.cdata["swapped_modes_R"] == [[5]]

        rep_node.cdata["swapped_modes_L"] = []
        rep_node.cdata["swapped_modes_R"] = []

        rep_node.subcomponents["QMem_L"].put(qapi.create_qubits(4), [1, 2, 3, 4])
        rep_node.subcomponents["QMem_R"].put(qapi.create_qubits(4), [5, 6, 7, 8])
        # Test all modes are selected
        multi_swap_protocol._trigger(self)
        assert rep_node.cdata["swapped_modes_L"] == [[1, 2, 3, 4]]
        assert rep_node.cdata["swapped_modes_R"] == [[5, 6, 7, 8]]

    def test_extraction_protocol(self):
        """Tests functionality of the extraction protocol."""
        self.setUp()

        end_node = EndNode("Extraction_Test_Node", **self.sim_params)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("{:.2e} -- Testing extraction protocol on {} --".format(
                sim_time(), end_node.name))

        # create node to connect to for is_connected to succeed
        dummy = Node("Dummy Node")
        dummy.add_ports(["Dummy_port_L", "Dummy_port_R"])
        end_node.ports["IO_Connection_E"].connect(dummy.ports["Dummy_port_L"])
        end_node.ports["Qbit_out"].connect(dummy.ports["Dummy_port_R"])

        # set up emission protocol
        ep = EmissionProtocol(end_node, source=end_node.subcomponents["PPS"], memory=end_node.subcomponents["QMem"])
        ep.start()
        end_node.cdata["successful_modes_E"] = [[0]]
        end_node.cdata["midpoint_outcomes_E"] = [[BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)]]
        end_node.subcomponents["Clock"]._max_ticks = 1
        end_node.subcomponents["Clock"].start()
        # run sim to make sure there are qubits in the memory
        ns.sim_run(duration=5, magnitude=ns.SECOND)

        # bind handler to output port
        qubit_msg = []

        def get_qubit_message(message):
            qubit_msg.append(message.items)

        # end_node.ports["Qbit_out"].bind_output_handler(get_qubit_message)
        dummy.ports["Dummy_port_R"].bind_input_handler(get_qubit_message)

        # set up extraction protocol
        ex_proto = ExtractionProtocol(end_node, ep)
        ex_proto.start()

        # schedule emission protocol event
        ep._schedule_now(EVTYPE_SUCCESS)

        # check port is empty
        self.assertEqual(0, len(qubit_msg))

        ns.sim_run(duration=5, magnitude=ns.SECOND)

        # check if qubit arrived at port
        self.assertEqual(1, len(qubit_msg))

        # determine correct number of qubits that should be in the message
        if not self.sim_params["multi_photon"]:
            mode_multiplier = 1
        elif self.sim_params["encoding"] == 'presence_absence':
            mode_multiplier = 2
        else:
            mode_multiplier = 4
        self.assertEqual(len(qubit_msg[0]), mode_multiplier)

        # test stopping
        ex_proto.stop()

    def test_correct_extraction(self):
        """Test whether the extraction protocol extracts the correct qubits for all encodings"""
        self.setUp()

        end_node = EndNode("Extraction_Test_Node", **self.sim_params)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("{:.2e} -- Testing correct qubit extraction on {} --".format(
                sim_time(), end_node.name))

        # create node to connect to for is_connected to succeed
        dummy = Node("Dummy Node")
        dummy.add_ports(["Dummy_port_L", "Dummy_port_R"])
        end_node.ports["IO_Connection_E"].connect(dummy.ports["Dummy_port_L"])
        end_node.ports["Qbit_out"].connect(dummy.ports["Dummy_port_R"])

        # set up emission protocol
        ep = EmissionProtocol(end_node, source=end_node.subcomponents["PPS"], memory=end_node.subcomponents["QMem"])

        end_node.cdata["round_E"] = [1]
        end_node.cdata["successful_modes_E"] = [[3]]
        end_node.cdata["midpoint_outcome_E"] = [[BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)]]

        # set up extraction protocol
        ex_proto = ExtractionProtocol(end_node, ep)
        ex_proto.start()

        # create qubits in the memory in mode 3
        memory = end_node.subcomponents["QMem"]
        positions = list(range(ex_proto.grouping * 3, (ex_proto.grouping * 3) + ex_proto.grouping))
        qubits = qapi.create_qubits(ex_proto.grouping)
        memory.put(qubits, positions)

        # bind handler to output port
        qubit_msg = []

        def get_qubit_message(message):
            qubit_msg.append(message.items)

        # end_node.ports["Qbit_out"].bind_output_handler(get_qubit_message)
        dummy.ports["Dummy_port_R"].bind_input_handler(get_qubit_message)

        # schedule emission protocol event
        ep._schedule_now(EVTYPE_SUCCESS)

        # check port is empty
        self.assertEqual(0, len(qubit_msg))

        ns.sim_run(duration=1, magnitude=ns.NANOSECOND)

        # check if qubit arrived at port
        self.assertEqual(1, len(qubit_msg))

        # determine correct number of qubits that should be in the message
        if not self.sim_params["multi_photon"]:
            mode_multiplier = 1
        elif self.sim_params["encoding"] == 'presence_absence':
            mode_multiplier = 2
        else:
            mode_multiplier = 4
        self.assertEqual(len(qubit_msg[0]), mode_multiplier)

    def test_messaging_protocol(self):
        """Tests functionality of the Messaging protocol."""
        self.setUp()
        # create simple repeater chain (3-5 nodes)
        self.sim_params["time_step"] = 2e5  # set time_step that is larger than cycle_time of chain
        self.sim_params["num_repeaters"] = 1
        # test two and one rails of messages
        for self.sim_params["encoding"] in ['time_bin', 'presence_absence']:
            protocols, network1, _ = create_repeater_chain(id_str="1", **self.sim_params)
            if self.sim_params["encoding"] == 'presence_absence':
                protocols2, network2, _ = create_repeater_chain(id_str="2", **self.sim_params)
                protocols.extend(protocols2)
                network1.combine(network2)
            det_protos = create_qkd_application(network=network1, sim_params=self.sim_params)
            protocols.extend(det_protos)

            # fill data storages
            messaging_protocols = []
            for proto in protocols:
                proto.start()
                if proto.protocol_type == "Emission":
                    proto.node.cdata["successful_modes_{}".format(proto.source.name[-1])] = [[10]]
                    proto.node.cdata["midpoint_outcomes_{}".format(proto.source.name[-1])] = [[1]]
                    proto.node.cdata["round_{}".format(proto.source.name[-1])] = [0]
                elif proto.protocol_type == "Swap":
                    proto.node.cdata["swap_outcomes"] = [[2]]
                    proto.node.cdata["swapped_modes_L"] = [[10]]
                    proto.node.cdata["swapped_modes_R"] = [[10]]
                elif proto.protocol_type == "Detection":
                    proto.node.cdata["qkd_outcomes"] = [[QKDOutcome(success=True, outcome=1, measurement_basis="X")]]
                    proto.node.cdata["round"] = [0]
                    proto.node.cdata["successful_modes"] = [[10]]
                    proto.node.cdata["measured_modes"] = [[10]]
                elif proto.protocol_type == "Messaging":
                    messaging_protocols.append(proto)

            # test data collection
            # TODO: pretty useless now that data is stored on node
            test_evt = ns.EventType("TEST", "TestEvent")
            for proto in messaging_protocols:
                proto._collect_protocol_data(test_evt)
                if proto.node_type == proto.node_type_detection:
                    self.assertTrue(proto.node.cdata["round"] == [0])
                    self.assertEqual(proto.node.cdata["qkd_outcomes"][0][0].measurement_basis, "X")
                    self.assertEqual(proto.node.cdata["qkd_outcomes"][0][0].outcome, 1)
                elif proto.node_type == proto.node_type_end:
                    self.assertTrue(proto.node.cdata["round_E"] == [0])
                    self.assertTrue(proto.node.cdata["midpoint_outcomes_E"] == [[1]])
                else:  # repeater node
                    self.assertTrue(proto.node.cdata["round_L"] == [0])
                    self.assertTrue(proto.node.cdata["round_R"] == [0])
                    self.assertTrue(proto.node.cdata["midpoint_outcomes_L"] == [[1]])
                    self.assertTrue(proto.node.cdata["midpoint_outcomes_R"] == [[1]])
                    self.assertTrue(proto.node.cdata["swap_outcomes"] == [[2]])

            # message started in above step, check correct propagation
            ns.sim_run(duration=1, magnitude=ns.SECOND)

            # calculate expected message
            exp_message_a = ['det_alice', [QKDOutcome(success=True, outcome=1, measurement_basis="X")], [1]] + \
                (self.sim_params["num_repeaters"] * [[2], [1]]) + \
                [[QKDOutcome(success=True, outcome=1, measurement_basis="X")], 'det_bob']
            exp_message_b = ['det_bob', [QKDOutcome(success=True, outcome=1, measurement_basis="X")], [1]] + \
                (self.sim_params["num_repeaters"] * [[2], [1]]) + \
                [[QKDOutcome(success=True, outcome=1, measurement_basis="X")], 'det_alice']
            if self.sim_params["encoding"] == 'presence_absence':
                exp_message_a = 2 * exp_message_a
                exp_message_b = 2 * exp_message_b

            for proto in messaging_protocols:
                if proto.node_type == proto.node_type_detection:
                    final_msg = proto.node.cdata["final_message"][0]
                    self.assertTrue(final_msg.items in [exp_message_a, exp_message_b])
                    self.assertTrue(final_msg.meta["round"] == 0)
            # test stopping
            for prot in messaging_protocols:
                prot.stop()

            ns.sim_reset()

    def test_messaging_protocol_schedules_events_correctly(self):
        """Test that messaging protocol checks all QKDOutcomes and BSMOutcomes before scheduling a SUCCESS."""

        class DummyMessagingProtocol(MessagingProtocol):
            """Messaging protocol that keeps track of scheduled failures and successes."""

            def __init__(self, node):
                self.failures = 0
                self.successes = 0
                super().__init__(node)

            def _schedule_after(self, delay, event):
                if event == EVTYPE_FAIL:
                    self.failures += 1
                elif event == EVTYPE_SUCCESS:
                    self.successes += 1

        end_node = EndNode("Test_End_Node", **self.sim_params)  # arbitrary node on which to run the protocol
        msg_protocol = DummyMessagingProtocol(end_node)
        msg_protocol.node.cdata["final_message"] = ["fake final message"]  # for when protocol schedules a success

        assert msg_protocol.failures == 0
        assert msg_protocol.successes == 0

        success_message = ['det_alice', QKDOutcome(success=True, measurement_basis='X', outcome=0),
                           [BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)],
                           QKDOutcome(success=True, measurement_basis='X', outcome=0), 'det_bob']

        msg_protocol._schedule_events(Message(items=success_message, round=0))
        assert msg_protocol.failures == 0
        assert msg_protocol.successes == 1

        failed_qkd_message = ['det_alice', QKDOutcome(success=False, measurement_basis='X', outcome=-1),
                              [BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)],
                              QKDOutcome(success=True, measurement_basis='X', outcome=0), 'det_bob']

        msg_protocol._schedule_events(Message(items=failed_qkd_message, round=0))
        assert msg_protocol.failures == 1
        assert msg_protocol.successes == 1

        failed_bsm_message = ['det_alice', QKDOutcome(success=True, measurement_basis='X', outcome=0),
                              [BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS),
                               BSMOutcome(success=False)],
                              QKDOutcome(success=True, measurement_basis='X', outcome=0), 'det_bob']

        msg_protocol._schedule_events(Message(items=failed_bsm_message, round=0))
        assert msg_protocol.failures == 2
        assert msg_protocol.successes == 1

    def test_messaging_protocol_multiple_successes(self):
        """Test messaging protocol correctly handling messages on different nodes with multiple successes."""
        ns.sim_reset()
        test_end_node = EndNode(name="A_1", **self.sim_params)
        test_rep_node = RepeaterNode(name="message_test_repeater", **self.sim_params)

        # Test End node
        end_node_ep = EmissionProtocol(node=test_end_node,
                                       source=test_end_node.subcomponents["PPS"],
                                       memory=test_end_node.subcomponents["QMem"])
        end_node_mp = MessagingProtocol(node=test_end_node, emission_protocol_1=end_node_ep)
        # fill storage, Note: labeling bell_index with unique integers for test
        test_end_node.cdata["successful_modes_E"] = [[0], [1, 3, 5]]
        test_end_node.cdata["midpoint_outcomes_E"] = [[BSMOutcome(success=False)],
                                                      [BSMOutcome(success=True, bell_index=0),
                                                       BSMOutcome(success=True, bell_index=1),
                                                       BSMOutcome(success=True, bell_index=2)]]
        test_end_node.cdata["round_E"] = [1, 2]
        test_end_node.cdata["sent_LR"] = [0, 1]

        # set up node and protocols
        end_node_mp.app_connected = True
        dummy_connection = MessagingConnection("test_con", **self.sim_params)
        dummy_connection.ports["A"].connect(test_end_node.ports["Messaging_IO_L"])
        dummy_connection.ports["B"].connect(test_end_node.ports["Messaging_IO_R"])
        end_node_mp.start()

        # collect output message
        message_storage = []

        def get_message(message):
            message_storage.append(message)

        # disconnect ports that were needed to start protocol again, so we can bind output handler
        test_end_node.ports["Messaging_IO_L"].disconnect()
        test_end_node.ports["Messaging_IO_R"].disconnect()
        test_end_node.ports["Messaging_IO_R"].bind_output_handler(get_message)

        # put message on port
        end_node_test_message = Message(items=['det_alice',
                                               [QKDOutcome(success=True, measurement_basis="X", outcome=1),
                                                QKDOutcome(success=True, measurement_basis="X", outcome=2),
                                                QKDOutcome(success=True, measurement_basis="X", outcome=3)]],
                                        round=2, successful_modes=[3, 4, 5], chain="1")
        test_end_node.ports["Messaging_IO_L"].tx_input(end_node_test_message)

        # run
        ns.sim_run()

        assert len(message_storage) == 1
        assert message_storage[0].items == ['det_alice',
                                            [QKDOutcome(success=True, measurement_basis='X', outcome=1),
                                             QKDOutcome(success=True, measurement_basis='X', outcome=3)],
                                            [BSMOutcome(success=True, bell_index=1),
                                             BSMOutcome(success=True, bell_index=2)]]
        assert message_storage[0].meta["round"] == 2
        assert message_storage[0].meta["chain"] == "1"
        assert message_storage[0].meta["successful_modes"] == [3, 5]
        # check used up data has been deleted (note: this would normally only happen when the second message was sent)
        for key in test_end_node.cdata.keys():
            assert test_end_node.cdata[key] == []

        message_storage = []

        # Test repeater node
        rep_node_ep1 = EmissionProtocol(node=test_rep_node,
                                        source=test_rep_node.subcomponents["PPS_L"],
                                        memory=test_rep_node.subcomponents["QMem_L"])
        rep_node_ep2 = EmissionProtocol(node=test_rep_node,
                                        source=test_rep_node.subcomponents["PPS_R"],
                                        memory=test_rep_node.subcomponents["QMem_R"])
        rep_node_sp = SwapProtocol(node=test_rep_node, proto_l=rep_node_ep1, proto_r=rep_node_ep2)
        rep_node_mp = MessagingProtocol(node=test_rep_node, emission_protocol_1=rep_node_ep1,
                                        emission_protocol_2=rep_node_ep2, swap_protocol=rep_node_sp)
        # fill storage
        test_rep_node.cdata["successful_modes_L"] = [[0], [1, 3, 5]]
        test_rep_node.cdata["midpoint_outcomes_L"] = [[BSMOutcome(success=False)],
                                                      [BSMOutcome(success=True, bell_index=0),
                                                       BSMOutcome(success=True, bell_index=1),
                                                       BSMOutcome(success=True, bell_index=2)]]
        test_rep_node.cdata["successful_modes_R"] = [[0], [0, 1, 2, 3, 7]]
        test_rep_node.cdata["midpoint_outcomes_R"] = [[BSMOutcome(success=False)],
                                                      [BSMOutcome(success=True, bell_index=3),
                                                       BSMOutcome(success=True, bell_index=4),
                                                       BSMOutcome(success=True, bell_index=5),
                                                       BSMOutcome(success=True, bell_index=6),
                                                       BSMOutcome(success=True, bell_index=7)]]
        test_rep_node.cdata["swapped_modes_L"] = [[], [1, 5]]
        test_rep_node.cdata["swapped_modes_R"] = [[], [0, 7]]
        test_rep_node.cdata["swap_outcomes"] = [[],
                                                [BSMOutcome(success=True, bell_index=8),
                                                 BSMOutcome(success=True, bell_index=9)]]
        test_rep_node.cdata["round_L"] = [1, 2]
        test_rep_node.cdata["round_R"] = [1, 2]
        test_rep_node.cdata["sent_LR"] = [1, 1]

        # set up node and protocols
        dummy_connection = MessagingConnection("test_con", **self.sim_params)
        dummy_connection.ports["A"].connect(test_rep_node.ports["Messaging_IO_L"])
        dummy_connection.ports["B"].connect(test_rep_node.ports["Messaging_IO_R"])
        rep_node_mp.start()

        # disconnect ports that were needed to start protocol again, so we can bind output handler
        test_rep_node.ports["Messaging_IO_L"].disconnect()
        test_rep_node.ports["Messaging_IO_R"].disconnect()
        test_rep_node.ports["Messaging_IO_R"].bind_output_handler(get_message)

        # put message on port
        rep_node_test_message = Message(items=['det_alice',
                                               [QKDOutcome(success=True, measurement_basis='X', outcome=1),
                                                QKDOutcome(success=True, measurement_basis='X', outcome=3)],
                                               [BSMOutcome(success=True, bell_index=1),
                                                BSMOutcome(success=True, bell_index=2)]],
                                        round=2, successful_modes=[3, 5], chain="1")
        test_rep_node.ports["Messaging_IO_L"].tx_input(rep_node_test_message)

        # run
        ns.sim_run()

        assert len(message_storage) == 1
        assert message_storage[0].items == [
            'det_alice',
            [QKDOutcome(success=True, measurement_basis='X', outcome=3)],   # detector node
            [BSMOutcome(success=True, bell_index=2)],                       # 1st el link
            [BSMOutcome(success=True, bell_index=9)],                       # swap
            [BSMOutcome(success=True, bell_index=7)]                        # 2nd el link
        ]
        assert message_storage[0].meta["round"] == 2
        assert message_storage[0].meta["chain"] == "1"
        assert message_storage[0].meta["successful_modes"] == [7]

    def test_detection_protocol(self):
        """Tests functionality of the detection protocol."""
        self.setUp()

        def create_adapter(node):
            """Creates an adapter node that can be put on a port to manipulate the message passing through."""
            node.add_ports(["IN", "OUT"])

            def manipulate(message):
                """Manipulate message."""
                message.meta["round"] = 0
                node.ports["OUT"].tx_output(message)

            node.ports["IN"].bind_input_handler(manipulate)

        for self.sim_params["multi_photon"], self.sim_params["encoding"], meas_basis in \
                itertools.product([True, False], ['presence_absence', 'time_bin'], ["X", "Z"]):

            det_node = QKDNode("Detection_Test_Node", self.sim_params)

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug("{:.2e} ------- Testing detection protocol on {} for sim params mp: {}, enc: {}, basis {}."
                             .format(sim_time(), det_node.name, self.sim_params["multi_photon"],
                                     self.sim_params["encoding"], meas_basis))

            # test if dual_chain is detected correctly
            node1, node2 = Node("Node1"), Node("Node2")
            node1.add_ports(["Message_IO_1", "OUT1"])
            node2.add_ports(["Message_IO_2", "OUT2"])

            # test if all encodings are being handled correctly
            adapter_1, adapter_2 = Node("A1"), Node("A2")
            create_adapter(adapter_1)
            create_adapter(adapter_2)

            if self.sim_params["encoding"] == 'time_bin':
                # Change the measurement basis of the detector explicitly
                det_node.subcomponents["QDet"].measurement_basis = meas_basis
                det_proto = DetectionProtocol(det_node)
                # test setting measurement basis
                det_proto.qkd_det.measurement_basis = meas_basis
                self.assertTrue(det_proto.qkd_det.measurement_basis == meas_basis)
                # testing if attribute dual_chain is detected correctly
                self.assertFalse(det_proto.dual_chain)
                # testing if state is accepted correctly
                test_source_1 = SPDCSource("Test_source_1", encoding=self.sim_params["encoding"],
                                           multi_photon=self.sim_params["multi_photon"],
                                           emission_probability=[1., 0., 0., 0.],
                                           status=SourceStatus.EXTERNAL, frequency=1e9)
                det_node.cdata["measured_modes"] = [[0]]
                det_node.ports["IN_Chain_1"].disconnect()
                det_node.ports["IN_Chain_1"].connect(adapter_1.ports["OUT"])
                adapter_1.ports["IN"].connect(test_source_1.ports["qout0"])
                det_proto.start()
                test_source_1.trigger()
                ns.sim_run(duration=1, magnitude=ns.SECOND)

            else:
                # 'presence_absence':
                # testing if attribute dual_chain is detected correctly
                det_node.ports["IN_Chain_2"].connect(node2.ports["OUT2"])
                det_node.ports["Message_IO_Chain_2"].connect(node2.ports["Message_IO_2"])
                det_proto = DetectionProtocol(det_node, measurement_basis=meas_basis)
                self.assertTrue(det_proto.dual_chain)
                # testing if state is accepted correctly
                test_source_2 = SPDCSource("Test_source_2", encoding=self.sim_params["encoding"],
                                           multi_photon=self.sim_params["multi_photon"],
                                           emission_probability=[1., 0., 0., 0.],
                                           status=SourceStatus.EXTERNAL)
                det_node.cdata["measured_modes"] = [[0]]
                det_node.ports["IN_Chain_1"].disconnect()
                det_node.ports["IN_Chain_2"].disconnect()
                det_node.ports["IN_Chain_1"].connect(adapter_1.ports["OUT"])
                adapter_1.ports["IN"].connect(test_source_2.ports["qout1"])
                det_node.ports["IN_Chain_2"].connect(adapter_2.ports["OUT"])
                adapter_2.ports["IN"].connect(test_source_2.ports["qout0"])
                det_proto.start()
                test_source_2.trigger()
                ns.sim_run(duration=1, magnitude=ns.SECOND)

            self.assertEqual(det_node.cdata["qkd_outcomes"][-1], [])
            self.assertTrue(det_node.cdata["round"][-1] == 1)

            # test stopping
            det_proto.stop()

            self.setUp()

    def test_magic_protocol(self):
        """Test the magic protocol."""
        self.setUp()
        for self.sim_params["multiple_link_successes"] in [False, True]:
            self.sim_params["magic"] = "analytical"
            self.sim_params["mean_photon_number"] = ParameterSet._NOT_SPECIFIED

            for self.sim_params["multi_photon"], self.sim_params["encoding"] in \
                    itertools.product([True, False], ['presence_absence', 'time_bin']):
                # set emission probabilities such that we have near perfect success probability
                if self.sim_params["encoding"] == 'presence_absence':
                    self.sim_params["emission_probabilities"] = [.5, .5, 0, 0]

                else:
                    self.sim_params["emission_probabilities"] = [0, 1., 0, 0]

                # set up elementary link
                self.sim_params["channel_length_l"] = 0.1
                self.sim_params["channel_length_r"] = 0.1
                self.sim_params["length"] = 0.2
                self.sim_params["time_step"] = 1.5e9 * self.sim_params["length"] / 2e5  # pick arbitrary, small step
                self.sim_params["num_repeaters"] = 0
                if self.sim_params["multiple_link_successes"] and self.sim_params["encoding"] == 'presence_absence':
                    with self.assertRaises(NotImplementedError):
                        [alice_proto, bob_proto], network, magic = create_elementary_link(**self.sim_params)
                else:
                    [alice_proto, bob_proto], network, magic = create_elementary_link(**self.sim_params)
                [alice, bob] = list(network.nodes.values())
                alice_proto.start()
                bob_proto.start()

                magic[0].start_auto_delivery()

                ns.sim_run(duration=self.sim_params["time_step"] * 1.5, magnitude=ns.NANOSECOND)
                self.assertEqual(alice.cdata["midpoint_outcomes_E"], bob.cdata["midpoint_outcomes_E"])
                self.assertEqual(alice.cdata["round_E"], bob.cdata["round_E"])
                if not self.sim_params["multiple_link_successes"]:
                    self.assertEqual(alice.cdata["successful_modes_E"][0], [0])  # Successful mode is fixed at 0
                    # Qstates in memories of Alice and Bob should be equal
                    if alice.subcomponents["QMem"].peek(0, skip_noise=True)[0] is not None\
                            and bob.subcomponents["QMem"].peek(0, skip_noise=True)[0] is not None:
                        assert np.allclose(alice.subcomponents["QMem"].peek(0, skip_noise=True)[0].qstate.qrepr.ket,
                                           bob.subcomponents["QMem"].peek(0, skip_noise=True)[0].qstate.qrepr.ket)
                self.assertTrue(alice.cdata["midpoint_outcomes_E"][0][0].success)  # outcome should always be success
                # test stopping
                alice_proto.stop()
                bob_proto.stop()

                ns.sim_reset()

    def test_magic_detection_override(self):
        """Test if the magic protocol correctly overrides the rounds in the detection protocol"""
        self.setUp()
        self.sim_params["magic"] = "forced"
        self.sim_params["mean_photon_number"] = None
        self.sim_params["time_step"] = 1e6

        for self.sim_params["encoding"] in ["time_bin", "presence_absence"]:
            b11dm = np.outer(b11, b11)  # RCDFH required DMs
            b01dm = np.outer(b01, b01)
            if self.sim_params["encoding"] == "time_bin":
                data = {"forced_label": ["0110", "1001", "1010", "0101"],
                        "success_probability": [0.25] * 4,
                        "state": [b11dm, b11dm, b01dm, b01dm],
                        "midpoint_outcome_0": [BellIndex.PSI_PLUS, BellIndex.PSI_PLUS,
                                               BellIndex.PSI_MINUS, BellIndex.PSI_MINUS]}
            else:
                data = {"forced_label": ["10", "01"],
                        "success_probability": [.5, .5],
                        "state": [b11dm, b01dm],
                        "midpoint_outcome_0": [BellIndex.PSI_PLUS, BellIndex.PSI_MINUS]}
            forced_df = pandas.DataFrame(data=data)

            for self.sim_params["multi_photon"] in [True, False]:
                self.sim_params["emission_probabilities"] = [0., 1., 0., 0.]

                # set up elementary link
                self.sim_params["num_repeaters"] = 0
                self.sim_params["length"] = 25
                self.sim_params["mem_num_modes"] = 1
                self.sim_params["source_num_modes"] = 1
                filename = (self.sim_params['state_files_directory'] + '/forced_magic_state' +
                            '_ENC_' + self.sim_params['encoding'] +
                            '_MPN_' + str(self.sim_params['mean_photon_number']) + '_' +
                            str(self.sim_params['mean_photon_number']) +
                            '_EP_' + str(self.sim_params['emission_probabilities']) + '_' +
                            str(self.sim_params['emission_probabilities']) +
                            '_CL_' + str(self.sim_params['channel_length_l']) + '_' +
                            str(self.sim_params['channel_length_r']) +
                            '_ATT_' + str(self.sim_params['attenuation_l']) + '_' +
                            str(self.sim_params['attenuation_r']) +
                            '_LO_' + str(self.sim_params['coupling_loss_fibre']) +
                            '_DC_' + str(self.sim_params['det_dark_count_prob']) +
                            '_EF_' + str(self.sim_params['det_efficiency']) +
                            '_V_' + str(self.sim_params['det_visibility']) +
                            '_NR_' + str(self.sim_params['det_num_resolving']) +
                            '.pickle')
                # make all lists (required by safe netconf loader) into tuples (required by RDFHolder)
                saved_params = {}
                for key in self.sim_params.keys():
                    if isinstance(self.sim_params[key], list):
                        saved_params[key] = tuple(self.sim_params[key])
                container = RepchainDataFrameHolder(data=forced_df, number_of_nodes=2,
                                                    baseline_parameters=saved_params)
                self.sim_params["spectral_modes"] = 1000
                if not os.path.exists(self.sim_params["state_files_directory"]):
                    os.mkdir(self.sim_params["state_files_directory"])
                with open(filename, 'wb') as handle:
                    pickle.dump(container, handle, protocol=pickle.HIGHEST_PROTOCOL)

                protos, network, [magic_dist, magic_prot] = create_repeater_chain(id_str="1", **self.sim_params)
                if self.sim_params["encoding"] == "presence_absence":
                    # for presence_absence, create second chain
                    protocols2, network2, [magic_dist2, magic_prot2] = \
                        create_repeater_chain(id_str="2", **self.sim_params)

                    # combine networks
                    network.combine(network2)
                    protos.extend(protocols2)
                    magic_dist.merge_magic_distributor(magic_dist2)
                    magic_prot.extend(magic_prot2)

                # add detector nodes
                det_prots = create_qkd_application(network, sim_params=self.sim_params)
                nodes = list(network.nodes.values())
                # pick out detector nodes to interact with ports
                qkd_nodes = [node for node in nodes if isinstance(node, QKDNode)]
                # pick out end node to trigger override
                end_nodes = [node for node in nodes if isinstance(node, EndNode)]
                # pick out the ports
                messaging_port = qkd_nodes[0].ports["Message_IO_Chain_1"].connected_port
                end_node_io = end_nodes[0].ports["IO_Connection_E"]

                # start protocols
                for prot in protos:
                    # start all protos except the det protos
                    prot.start()

                # disconnect the port to intervene
                end_node_io.disconnect()

                # set handler to listen to messaging port
                test_msg = []

                def get_test_message(msg):
                    test_msg.append(msg)

                messaging_port.bind_input_handler(get_test_message)

                # put message on IO port
                message = Message(items=[BSMOutcome(True, BellIndex.PSI_PLUS)], successful_modes=[0])
                message.meta["round"] = 42
                end_node_io.tx_input(message)

                # run simulation
                ns.sim_run(duration=self.sim_params["time_step"], magnitude=ns.NANOSECOND)
                # check round has been written
                self.assertTrue(qkd_nodes[0].cdata["round"] == [42])
                self.assertTrue(qkd_nodes[0].cdata['magic_override'] == [True])
                self.assertTrue(qkd_nodes[0].cdata['qkd_outcomes'] == [])
                self.assertTrue(qkd_nodes[0].cdata['final_message'] == [])

                # trigger success on data protocol an run again
                for p in det_prots:
                    # start up messaging and detection protocols
                    p.start()

                det_prots[0]._handle_qkd_outcome(message=Message(QKDOutcome(True, "X", 1)))

                ns.sim_run(duration=self.sim_params["time_step"], magnitude=ns.NANOSECOND)
                # check node cdata
                self.assertTrue(qkd_nodes[0].cdata["round"] == [42])
                self.assertTrue(qkd_nodes[0].cdata['magic_override'] == [True])
                self.assertTrue(qkd_nodes[0].cdata['qkd_outcomes'] == [[QKDOutcome(True, "X", 1)]])
                self.assertTrue(qkd_nodes[0].cdata['final_message'] == [])
                # check message
                self.assertTrue(len(test_msg) == 1)
                self.assertTrue(test_msg[0].items == [qkd_nodes[0].name, [QKDOutcome(True, "X", 1)]])
                self.assertTrue(test_msg[0].meta["round"] == 42)

                # reset simulation for next loop
                ns.sim_reset()

        for name in os.listdir(self.sim_params['state_files_directory']):
            os.remove(self.sim_params['state_files_directory'] + '/' + name)
        os.rmdir(self.sim_params['state_files_directory'])


if __name__ == "__main__":
    ns.logger.setLevel(logging.NOTSET)
    unittest.main()
