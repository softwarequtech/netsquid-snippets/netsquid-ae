import unittest

import netsquid as ns
from netsquid.util import DataCollector
from pydynaa import EventExpression, ExpressionHandler
from netsquid.qubits.ketstates import BellIndex
from netsquid.components.component import Message

from netsquid_ae.ae_classes import QKDNode
from netsquid_ae.ae_protocols import MessagingProtocol
from netsquid_ae.application_protocols import DetectionProtocol
from netsquid_ae.protocol_event_types import EVTYPE_SUCCESS
from netsquid_ae.datacollectors import MeasurementCollector, DecisionMaker

from netsquid_physlayer.detectors import BSMOutcome, QKDOutcome


class TestDataCollectionSetup(unittest.TestCase):
    """Unittest for datacollectors."""
    def setUp(self):
        """Sets up a new simulation environment for each test."""
        ns.sim_reset()
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)

        # Tunable simulation parameters
        self.sim_params = {
            # SIMULATION
            "multi_photon": False,          # bool: whether to use multi-photon up to n=3 (WARNING: very slow)
            "num_repeaters": 0,             # number of repeaters
            "encoding": "time_bin",         # encoding of the entangled photons
            # end of simulation             Note: only use one and set others to -1
            "num_attempts": 10,             # number of clicks after which the clock stops (thus stopping the protocols)
            "num_attempts_proto": -1,       # number of clicks after which the emission protocols stop
            "num_successes": -1,            # number of successes after which to stop the simulation
            # magic
            "magic": None,                  # whether we want to use magic (should be "analytical", "sampled" or None)
            "state_files_directory": None,
            # clock                         Note: only used if magic = None, otherwise cycle time is used as time_step
            "time_step": 1e6,               # time step for the clock [ns]
            "multiple_link_successes": False,  # whether elementary links can have multiple successful modes

            # COMPONENTS
            # channel
            "length": 0,                    # total distance between end node/ total channel length [km]
            "channel_length_l": -1,         # channel length left of the detectors [km]
            "channel_length_r": -1,         # channel length right of the detectors [km]
            "coupling_loss_l": 0.,          # initial loss on channel left of detectors
            "coupling_loss_r": 0.,          # initial loss on channel right of detectors
            "attenuation_l": 0.25,          # channel attenuation left of the detectors [dB/km]
            "attenuation_r": 0.25,          # channel attenuation right of the detectors [dB/km]
            "fibre_phase_stdv_l": .0,
            "fibre_phase_stdv_r": .0,
            # source
            "source_frequency": 5e4,                 # frequency of the photon pair source [Hz]
            "source_num_modes": None,
            "g2": 0.12,                     # Measured heralded g^2
            "Rate": 4*3.5e3,                # Number of photons pr. second.
            "mean_photon_number": None,               # mean photon pair number (only used if multi_photon=True)
            "emission_probabilities": [0., 1., 0., 0.],  # emission probs for photon pair sources
            # midpoint detector
            "mid_det_dark_count_prob": 0,   # probability of dark count per detection
            "mid_det_efficiency": 1.,       # detector efficiency
            "mid_det_visibility": 1.,       # photon indistinguishability (must be 1 for multi_photon=True)
            "mid_det_num_resolving": True,  # using number or non_number resolving detectors
            # swap & end node detector
            "swap_det_dark_count_prob": 0,   # probability of dark count per detection
            "swap_det_efficiency": 1.,       # detector efficiency
            "swap_det_visibility": 1.,       # photon indistinguishability (must be 1 for multi_photon=True)
            "swap_det_num_resolving": True,  # using number or non_number resolving detectors
            # memory
            "memory_coherence_time": 1e6,               # coherence time of the quantum memory [ns]
            "mem_num_modes": 100,
            "max_memory_efficiency": 1.,                # maximum efficiency of the quantum memory
            "memory_time_dependence": "exponential",    # time-dependence of efficiency
        }

    def test_datacollection_dual_chain(self):
        alice = QKDNode(name="det_alice", params=self.sim_params)
        bob = QKDNode(name="det_bob", params=self.sim_params)
        det_prot = DetectionProtocol(node=alice)
        messaging_protocol_alice = MessagingProtocol(node=alice, detection_protocol=det_prot)
        messaging_protocol_bob = MessagingProtocol(node=bob, detection_protocol=det_prot)

        evexpr_succ_a = EventExpression(event_type=EVTYPE_SUCCESS, source=messaging_protocol_alice)
        evexpr_succ_b = EventExpression(event_type=EVTYPE_SUCCESS, source=messaging_protocol_bob)
        evexpr_success = evexpr_succ_a and evexpr_succ_b

        measurement_collector = DataCollector(MeasurementCollector(messaging_protocol_alice=messaging_protocol_alice,
                                                                   messaging_protocol_bob=messaging_protocol_bob))

        decision_maker = DecisionMaker(min_successes=10, datacollector=measurement_collector,
                                       messaging_protocol_alice=messaging_protocol_alice,
                                       messaging_protocol_bob=messaging_protocol_bob)
        # set up Handler for EventExpression
        decision_handler = ExpressionHandler(decision_maker.decide)
        decision_maker._wait(decision_handler, expression=evexpr_success)
        # wait for EventExpression with DataCollector
        measurement_collector.collect_on(evexpr_success)

        for _ in range(10):
            # fill node cdata
            bob.cdata["final_message"].append(Message([alice.name, [QKDOutcome(success=True, measurement_basis="X",
                                                                               outcome=1)],
                                                       [BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)],
                                                       [QKDOutcome(success=True, measurement_basis="X", outcome=1)],
                                                       bob.name], **{"round": 1}))
            alice.cdata["final_message"].append(Message([bob.name, [QKDOutcome(success=True, measurement_basis="X",
                                                                               outcome=1)],
                                                        [BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)],
                                                        [QKDOutcome(success=True, measurement_basis="X", outcome=1)],
                                                         alice.name], **{"round": 1}))
            for node in [alice, bob]:
                node.cdata["round"] = []
                node.cdata["round"].append(1)

            messaging_protocol_alice._schedule_now(EVTYPE_SUCCESS)
            messaging_protocol_bob._schedule_now(EVTYPE_SUCCESS)

            ns.sim_run()

        assert len(measurement_collector.dataframe) == 10
        assert decision_maker.num_successes == 10

    def test_datacollection_single_chain(self):
        alice = QKDNode(name="det_alice", params=self.sim_params)
        bob = QKDNode(name="det_bob", params=self.sim_params)
        det_prot = DetectionProtocol(node=alice)
        det_prot.dual_chain = True
        messaging_protocol_alice = MessagingProtocol(node=alice, detection_protocol=det_prot)
        messaging_protocol_bob = MessagingProtocol(node=bob, detection_protocol=det_prot)

        evexpr_succ_a = EventExpression(event_type=EVTYPE_SUCCESS, source=messaging_protocol_alice)
        evexpr_succ_b = EventExpression(event_type=EVTYPE_SUCCESS, source=messaging_protocol_bob)
        evexpr_success = evexpr_succ_a and evexpr_succ_b

        measurement_collector = DataCollector(MeasurementCollector(messaging_protocol_alice=messaging_protocol_alice,
                                                                   messaging_protocol_bob=messaging_protocol_bob))

        decision_maker = DecisionMaker(min_successes=10, datacollector=measurement_collector,
                                       messaging_protocol_alice=messaging_protocol_alice,
                                       messaging_protocol_bob=messaging_protocol_bob)
        # set up Handler for EventExpression
        decision_handler = ExpressionHandler(decision_maker.decide)
        decision_maker._wait(decision_handler, expression=evexpr_success)
        # wait for EventExpression with DataCollector
        measurement_collector.collect_on(evexpr_success)

        for _ in range(10):
            # fill node cdata
            bob.cdata["final_message"].append(Message([alice.name, [QKDOutcome(success=True, measurement_basis="X",
                                                                               outcome=1)],
                                                       [BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)],
                                                       [QKDOutcome(success=True, measurement_basis="X", outcome=1)],
                                                       bob.name, alice.name,
                                                       [QKDOutcome(success=True, measurement_basis="X", outcome=1)],
                                                       [BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)],
                                                       [QKDOutcome(success=True, measurement_basis="X", outcome=1)],
                                                       bob.name], **{"round": 1}))
            alice.cdata["final_message"].append(Message([bob.name, [QKDOutcome(success=True, measurement_basis="X",
                                                                               outcome=1)],
                                                         [BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)],
                                                         [QKDOutcome(success=True, measurement_basis="X", outcome=1)],
                                                         alice.name, bob.name,
                                                         [QKDOutcome(success=True, measurement_basis="X", outcome=1)],
                                                         [BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)],
                                                         [QKDOutcome(success=True, measurement_basis="X", outcome=1)],
                                                         alice.name], **{"round": 1}))
            for node in [alice, bob]:
                node.cdata["round"] = []
                node.cdata["round"].append(1)

            messaging_protocol_alice._schedule_now(EVTYPE_SUCCESS)
            messaging_protocol_bob._schedule_now(EVTYPE_SUCCESS)

            ns.sim_run()

        assert len(measurement_collector.dataframe) == 10
        assert decision_maker.num_successes == 10
