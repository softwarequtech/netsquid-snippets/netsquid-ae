import unittest
import numpy as np

import netsquid as ns
import netsquid.qubits.qubitapi as qapi
from netsquid.components.component import Component


from netsquid_ae.multi_photon_beam_splitter import MultiPhotonBeamSplitter


def _setup_beam_splitter(encoding, dual_input=False):
    """Set up the beam splitter for the input-output test."""
    ns.sim_reset()
    ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.DM)
    # set up beam splitter and connect to dummy component
    test_beam_splitter = MultiPhotonBeamSplitter("TestBeamSplitter", encoding=encoding)
    input_ports = ["0", "1"] if dual_input else ["0"]
    dummy_component = Component("dummy", port_names=input_ports)
    for port_name in input_ports:
        dummy_component.ports[port_name].connect(test_beam_splitter.ports["qin" + port_name])
    assert test_beam_splitter._single_input is True

    return test_beam_splitter


def perform_input_output_check(beam_splitter_component, expected_output_state, input_dm0, input_dm1=None):
    """Perform a multiplexed input/output check of the beam_splitter_component between input_dm0, input_dm1 and
    expected_output_state."""
    # bind output handler to collect output qubit state
    output = []

    def collect(message):
        output.append(message.items)
    beam_splitter_component.ports["qout0"].bind_output_handler(collect)

    # create multi-photon encoded input states
    qubit_list_0, qubit_list_1 = [], []
    for _ in range(10):
        if not isinstance(input_dm0, str):
            qubits_0 = qapi.create_qubits(int(np.log2(input_dm0.shape[0])), system_name="0", no_state=True)
            qapi.assign_qstate(qubits_0, qrepr=input_dm0)
        elif input_dm0 == "0":
            # TODO: something goes wrong when netsquid assigns zeros as qstate
            qubits_0 = qapi.create_qubits(int(np.log2(input_dm1.shape[0])), system_name="0")
        else:
            raise ValueError()
        qubit_list_0.extend(qubits_0)

        if input_dm1 is not None:
            if not isinstance(input_dm1, str):
                qubits_1 = qapi.create_qubits(int(np.log2(input_dm1.shape[0])), system_name="1", no_state=True)
                qapi.assign_qstate(qubits_1, qrepr=input_dm1)
            elif input_dm1 == "0":
                # TODO: something goes wrong when netsquid assigns zeros as qstate
                qubits_1 = qapi.create_qubits(int(np.log2(input_dm0.shape[0])), system_name="1")
            else:
                raise ValueError()
            qubit_list_1.extend(qubits_1)

    # transmit through connected input port
    beam_splitter_component.ports["qin0"].tx_input(qubit_list_0)
    if input_dm1 is not None:
        beam_splitter_component.ports["qin1"].tx_input(qubit_list_1)

    ns.sim_run()

    for q in output[0]:
        assert np.allclose(q.qstate.qrepr.reduced_dm(), expected_output_state)


class TestMultiPhotonBeamSplitter(unittest.TestCase):

    def test_setting_up_bs(self):
        """Test whether beam splitter can be (in-)correctly set up."""
        ns.sim_reset()
        with self.assertRaises(ValueError):
            MultiPhotonBeamSplitter("Wrong_Encoding_BS", encoding="wrong_encoding")

        correct_bs = MultiPhotonBeamSplitter("Correct_Encoding_BS", encoding="presence_absence")
        assert correct_bs.encoding == "presence_absence"
        correct_bs.encoding = "time_bin"
        assert correct_bs.encoding == "time_bin"

    def test_multiplexed_superposition_single_input_presence_absence(self):
        """Test that single input 1 maps onto superposition of 01 + 10 in multiplexed operation."""
        test_beam_splitter = _setup_beam_splitter(encoding="presence_absence", dual_input=False)

        # create multi-photon presence_absence encoded single photon |1>
        dm = np.zeros(shape=(4, 4))
        dm[1, 1] = 1
        # we expect output state (01 + 10)/sqrt(2)
        expected_output_state = np.zeros(shape=(16, 16))
        expected_output_state[1, 1] = .5
        expected_output_state[1, 4] = .5
        expected_output_state[4, 1] = .5
        expected_output_state[4, 4] = .5

        perform_input_output_check(test_beam_splitter, expected_output_state, input_dm0=dm, input_dm1=None)

    def test_multiplexed_superposition_dual_input_presence_absence(self):
        """Test that dual input |1>, |0> maps onto superposition of 01 + 10 in multiplexed operation."""
        test_beam_splitter = _setup_beam_splitter(encoding="presence_absence", dual_input=True)

        # input |1>, |0>
        dm = np.zeros(shape=(4, 4))
        dm[1, 1] = 1
        # we expect output state (01 + 10)/sqrt(2)
        expected_output_state = np.zeros(shape=(16, 16))
        expected_output_state[1, 1] = .5
        expected_output_state[4, 4] = .5
        expected_output_state[1, 4] = .5
        expected_output_state[4, 1] = .5

        perform_input_output_check(test_beam_splitter, expected_output_state, input_dm0=dm, input_dm1="0")

        # input |0>, |1>
        # we expect output state (01 - 10)/sqrt(2)
        expected_output_state = np.zeros(shape=(16, 16))
        expected_output_state[1, 1] = .5
        expected_output_state[4, 4] = .5
        expected_output_state[1, 4] = -.5
        expected_output_state[4, 1] = -.5

        perform_input_output_check(test_beam_splitter, expected_output_state, input_dm0="0", input_dm1=dm)

        assert test_beam_splitter._single_input is False

    def test_multiplexed_superposition_single_input_time_bin(self):
        """Test that single input 1 maps onto superposition of 01 + 10 in multiplexed operation in
        time_bin encoding."""
        test_beam_splitter = _setup_beam_splitter(encoding="time_bin", dual_input=False)

        # we input state |0>_e +|1>_l
        dm = np.zeros(shape=(16, 16))
        dm[1, 1] = 1

        # we expect output state |0>_e + |(01 + 10)/sqrt(2)>_l
        expected_output_state = np.zeros(shape=(256, 256))
        # TODO: double check that this state is indeed correct
        expected_output_state[4, 4] = .5
        expected_output_state[4, 16] = .5
        expected_output_state[16, 4] = .5
        expected_output_state[16, 16] = .5

        perform_input_output_check(test_beam_splitter, expected_output_state, input_dm0=dm, input_dm1=None)

        assert test_beam_splitter._single_input is True


if __name__ == "__main__":
    unittest.main()
