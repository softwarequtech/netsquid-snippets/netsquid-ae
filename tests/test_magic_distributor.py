import logging
import unittest
import itertools
import numpy as np


import netsquid as ns
from netsquid.nodes import Network
from netsquid.qubits import qformalism
import netsquid.qubits.qubitapi as qapi
from netsquid.qubits.state_sampler import StateSampler

from netsquid_physlayer.detectors import BSMOutcome

from netsquid_ae.qlossmodel import FibrePhaseModel
from netsquid_ae.ae_magic_distributor import AEMagicDistributor
from netsquid_ae.ae_classes import EndNode, HeraldedConnection, RepeaterNode
from netsquid_ae.ae_state_delivery_samplers import AEStateDeliverySamplerFactory


class MD(AEMagicDistributor):

    states_added = 0
    state_delivery_times = {}
    label_delivery_times = {}

    def _handle_state_delivery(self, node_delivery, event):
        if self.states_added < 6:
            super()._handle_state_delivery(
                node_delivery=node_delivery,
                event=event,
            )
            self.states_added += 1
            if self.state_delivery_times.get(node_delivery.node_id, None) is None:
                self.state_delivery_times[node_delivery.node_id] = []
            self.state_delivery_times[node_delivery.node_id].append(ns.sim_time())

    def _handle_label_delivery(self, node_delivery, event):
        super()._handle_label_delivery(
            node_delivery=node_delivery,
            event=event,
        )
        if self.label_delivery_times.get(node_delivery.node_id, None) is None:
            self.label_delivery_times[node_delivery.node_id] = []
        self.label_delivery_times[node_delivery.node_id].append(ns.sim_time())


class TestMagicDistributor(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()

        self.sim_params = {
            # SIMULATION
            "multi_photon": False,  # bool: whether to use multi-photon up to n=3 (WARNING: very slow)
            "num_repeaters": 0,  # number of repeaters
            "encoding": "time_bin",  # encoding of the entangled photons
            # end of simulation             Note: only use one and set others to -1
            "num_attempts": 10,  # number of clicks after which the clock stops (thus stopping the protocols)
            "num_attempts_proto": -1,  # number of clicks after which the emission protocols stop
            "num_successes": -1,  # number of successes after which to stop the simulation
            # magic
            "magic": "analytical",  # whether we want to use magic (should be "analytical", "sampled" or None)
            "state_files_directory": None,
            # clock                         Note: only used if magic = None, otherwise cycle time is used as time_step
            "time_step": 1000,  # time step for the clock [ns]

            # COMPONENTS
            # channel
            "length": 4,                # total distance between end node/ total channel length [km]
            "channel_length_l": 1,      # channel length left of the detectors [km]
            "channel_length_r": 1,      # channel length right of the detectors [km]
            "coupling_loss_fibre": 0.,  # initial loss on channel to midpoint detectors
            "attenuation_l": 0.,        # channel attenuation left of the detectors [dB/km]
            "attenuation_r": 0.,        # channel attenuation right of the detectors [dB/km]
            "fibre_phase_stdv_l": None,   # stdev of random phase picked up on the fibre in presence_absence encoding
            "fibre_phase_stdv_r": None,   # stdev of random phase picked up on the fibre in presence_absence encoding
            # source
            "source_frequency": 5e4,   # frequency of the photon pair source [Hz]
            "source_num_modes": None,       # number of multiplexing modes of the photon pair source
            "mean_photon_number": None,  # mean photon pair number (only used if multi_photon=True)
            "emission_probabilities": [0, 1., 0, 0],  # emission probs for photon pair sources (for multi_photon=False)
            "g2": 0.12,                 # Measured heralded g^2
            "Rate": 4*3.5e3,            # Number of photons pr. second.
            # midpoint detector
            "det_dark_count_prob": 0,   # probability of dark count per detection
            "det_efficiency": 1.,       # detector efficiency
            "det_visibility": 1.,       # photon indistinguishability
            "det_num_resolving": True,  # using number or non_number resolving detectors
            # swap & end node detector
            "swap_det_dark_count_prob": 0,      # probability of dark count per detection
            "swap_det_efficiency": 1.,          # detector efficiency
            "swap_det_visibility": 1.,          # photon indistinguishability
            "swap_det_num_resolving": True,     # using number or non_number resolving detectors
            # memory
            "memory_coherence_time": 1e6,               # coherence time of the quantum memory [ns]
            "mem_num_modes": 100,
            "max_memory_efficiency": 1.,                # maximum efficiency of the quantum memory
            "memory_time_dependence": "exponential",    # time-dependence of efficiency

            "multiple_link_successes": False,
        }

    @staticmethod
    def create_md(sim_params, node_l="Alice", node_r="Bob"):
        # setup nodes and connection
        network = Network(name="Elementary Link Setup")
        alice = EndNode(node_l, **sim_params) if node_l == "Alice" else RepeaterNode(node_l, **sim_params)
        bob = EndNode(node_r, **sim_params) if node_r == "Bob" else RepeaterNode(node_r, **sim_params)
        port_a = "IO_Connection_E" if node_l == "Alice" else "IO_Connection_R"
        port_b = "IO_Connection_E" if node_r == "Bob" else "IO_Connection_L"
        network.add_nodes([alice, bob])
        # Create a heralded connection and add it to the Network by connecting it to Alice and Bob
        heralded_connection = HeraldedConnection("HeraldedConnection", **sim_params)
        network.add_connection(node1=alice, node2=bob, connection=heralded_connection, label="heralding",
                               port_name_node1=port_a, port_name_node2=port_b)

        return MD(nodes=[alice, bob], heralded_connection=heralded_connection)

    def test_states_are_added(self):
        """Check that states are continuously added when auto delivery is started."""
        for self.sim_params["multi_photon"], self.sim_params["encoding"] in \
                itertools.product([True, False], ['presence_absence', 'time_bin']):

            # set emission probabilities such that we have near perfect success probability
            if self.sim_params["encoding"] == 'presence_absence':
                self.sim_params["emission_probabilities"] = [.5, .5, 0, 0]
            else:
                self.sim_params["emission_probabilities"] = [0, 1., 0, 0]

            ns.sim_reset()
            md = self.create_md(self.sim_params)
            assert md.states_added == 0
            md.start_auto_delivery()
            time_step = md.repetition_duration
            ns.sim_run(duration=3.1 * time_step)  # 3 chain deliveries
            md.stop_auto_delivery()
            ns.sim_run(duration=10000)  # allow all deliveries to happen
            # assert md.states_added == 6  # each delivery add one state on each of the two nodes
            if md.states_added != 6:
                raise ValueError("States added is {} not 6 when multi_photon is {} and encoding is {}".format(
                    md.states_added, self.sim_params["multi_photon"], self.sim_params["encoding"]
                ))

    def test_time_states_spend_on_memory_symmetric_link(self):
        """
        Check that state and label deliveries happen at the correct time for symmetric elementary link.

        In this case, the link is symmetric and the cycle times on both sides are equal. Therefore also the state
        and label delays are equal for both sides.
        See below a schematic of the link and timeline of the deliveries.

        .. code-block:: text

                +-------+ length_l = 1                  length_r = 1 +-----+
                | Alice | ---------------- midpoint ---------------- | Bob |
                +-------+ cycle_time_l = 10000  cycle_time_r = 10000 +-----+

            t   ----------------------- start ------------------------
            i         state delay (0)             state delay (0)
            m   ----- state delivery -----  ----- state delivery ----- t = 0
            e                |                           |
            l                |                           |
            i       label delay (10000)         label delay (10000)    states are at midpoint when t = 5000
            n                |                           |
            e                |                           |
                ------------------- label delivery ------------------- max_chain_cycle_time (t = 10000)

        Note that in the simulation, deliveries are scheduled after the time step (i.e. the value of time step would be
        added to all values in the timeline).
        """
        for self.sim_params["multi_photon"], self.sim_params["encoding"] in \
                itertools.product([True, False], ['presence_absence', 'time_bin']):

            # set emission probabilities such that we have near perfect success probability
            if self.sim_params["encoding"] == 'presence_absence':
                self.sim_params["emission_probabilities"] = [.5, .5, 0, 0]
            else:
                self.sim_params["emission_probabilities"] = [0, 1., 0, 0]

            ns.sim_reset()
            md = self.create_md(self.sim_params)

            md.start_auto_delivery()
            time_step = md.repetition_duration
            ns.sim_run(duration=3.1 * time_step)  # 3 chain deliveries
            md.stop_auto_delivery()
            ns.sim_run(duration=13000)

            node_ids = [node.ID for node in md.nodes[0]]

            # state delivery times are the same for both nodes (0)
            assert md.state_delivery_times[node_ids[0]] == md.state_delivery_times[node_ids[1]]
            assert md.state_delivery_times[node_ids[0]] == [k * time_step for k in [1, 2, 3]]

            # label delivery times happen at the same time for both nodes
            assert md.label_delivery_times[node_ids[0]] == [k * time_step + md._max_chain_cycle_time for k in [1, 2, 3]]
            assert md.label_delivery_times[node_ids[1]] == [k * time_step + md._max_chain_cycle_time for k in [1, 2, 3]]

    def test_time_states_spend_on_memory_asymmetric_link(self):
        """
        Check that state and label deliveries happen at the correct time for asymmetric elementary link.

        See below a schematic of the link and timeline of the deliveries.

        .. code-block:: text

                +-------+ length_l = 0.5              length_r = 1.5 +-----+
                | Alice | -------- midpoint ------------------------ | Bob |
                +-------+ cycle_time_l = 5000   cycle_time_l = 15000 +-----+

            t   ----------------------- start ------------------------ t = 0
            i                |                    state delay (0)
            m       state delay (5000)      ----- state delivery -----
            e                |                           |
            l   ----- state delivery -----               |
            i                |                  label delay (15000)    states are at midpoint when t = 7500
            n       label delay (10000)                  |
            e                |                           |
                ------------------- label delivery ------------------- max_chain_cycle_time (t = 15000)

        Note that in the simulation, deliveries are scheduled after the time step (i.e. the value of time step would be
        added to all values in the timeline).
        """
        for self.sim_params["multi_photon"], self.sim_params["encoding"] in \
                itertools.product([True, False], ['presence_absence', 'time_bin']):

            # set emission probabilities such that we have near perfect success probability
            if self.sim_params["encoding"] == 'presence_absence':
                self.sim_params["emission_probabilities"] = [.5, .5, 0, 0]
            else:
                self.sim_params["emission_probabilities"] = [0, 1., 0, 0]

            ns.sim_reset()
            self.sim_params["channel_length_l"] = 0.5
            self.sim_params["channel_length_r"] = 1.5
            md = self.create_md(self.sim_params)

            md.start_auto_delivery()
            time_step = md.repetition_duration
            ns.sim_run(duration=3.1 * time_step)  # 3 chain deliveries
            md.stop_auto_delivery()
            ns.sim_run(duration=15000)

            node_ids = [node.ID for node in md.nodes[0]]

            # first node has longer state delay (5000) because it has shorter cycle time
            assert md.state_delivery_times[node_ids[0]] == [k * time_step + 5000 for k in [1, 2, 3]]
            # second node has the max cycle time so there is no state delay
            assert md.state_delivery_times[node_ids[1]] == [k * time_step for k in [1, 2, 3]]

            # label delivery times happen at the same time for both nodes
            assert md.label_delivery_times[node_ids[0]] == [k * time_step + md._max_chain_cycle_time for k in [1, 2, 3]]
            assert md.label_delivery_times[node_ids[1]] == [k * time_step + md._max_chain_cycle_time for k in [1, 2, 3]]

    def test_time_states_spend_on_memory_asymmetric_chain(self):
        """
        Check that state and label deliveries happen at the correct time in an asymmetric chain.

        See below a schematic of the chain setup and the timeline for deliveries along with expected values for delays.

        .. code-block:: text

                +-------+ length = 1                  length = 1 +-----+ length = 0.5              length = 1.5 +-----+
                | Alice | -------------- midpoint -------------- | Rep | ------- midpoint --------------------- | Bob |
                +-------+ cycle_time = 10000  cycle_time = 10000 +-----+ cycle_time = 5000   cycle_time = 15000 +-----+

            t   -------------------------------------------- start --------------------------------------------- t = 0
            i               |                         |                         |                  state delay (0)
            m       state delay (5000)        state delay (5000)       state delay (5000)     ---- state delivery ----
            e               |                         |                        |                          |
            l   ---- state delivery ----  ---- state delivery ----  ---- state delivery ----              |
            i               |                         |                         |                label delay (7500)
            n      label delay (10000)       label delay (10000)       label delay (10000)                |
            e               |                         |                         |                         |
                -------------------------- label delivery ---------------------------- max_chain_cycle_time (t = 15000)

        Here, states in the left connection (between Alice and Repeater) arrive at midpoint at t = 10000. States in
        the right connection (between Repeater and Bob) arrive at midpoint at t = 7500.
        """
        for self.sim_params["multi_photon"], self.sim_params["encoding"] in \
                itertools.product([True, False], ['presence_absence', 'time_bin']):

            # set emission probabilities such that we have near perfect success probability
            if self.sim_params["encoding"] == 'presence_absence':
                self.sim_params["emission_probabilities"] = [.5, .5, 0, 0]
            else:
                self.sim_params["emission_probabilities"] = [0, 1., 0, 0]

            ns.sim_reset()
            self.sim_params["channel_length_l"] = 1
            self.sim_params["channel_length_r"] = 1
            md_l = self.create_md(self.sim_params, node_r="Repeater")

            self.sim_params["channel_length_l"] = 0.5
            self.sim_params["channel_length_r"] = 1.5
            md_r = self.create_md(self.sim_params, node_l="Repeater")

            md_l.merge_magic_distributor(md_r)

            md_l.start_auto_delivery()
            time_step = md_l.repetition_duration

            ns.sim_run(duration=1.1 * time_step)  # one chain delivery
            md_l.stop_auto_delivery()
            ns.sim_run(duration=15000)

            node_ids_l = [node.ID for node in md_l.nodes[0]]
            node_ids_r = [node.ID for node in md_l.nodes[1]]
            # nodes in left connection are the same and both have state delay of 5000
            assert md_l.state_delivery_times[node_ids_l[0]] == md_l.state_delivery_times[node_ids_l[1]]
            assert md_l.state_delivery_times[node_ids_l[0]] == [5000. + time_step]

            # first node in the right connection has longer state delay (5000) because it has shorter cycle time
            assert md_l.state_delivery_times[node_ids_r[0]] == [time_step + 5000.]
            # second node in the right connection has the max cycle time so there is no state delay
            assert md_l.state_delivery_times[node_ids_r[1]] == [time_step]

            # label delivery times happen at the same time for all nodes
            for node_id in node_ids_l + node_ids_r:
                assert md_l.label_delivery_times[node_id] == [time_step + md_l._max_chain_cycle_time]

    def test_magic_phase_presence_absence(self):
        """Test if phase is correctly added in a delivery by the MD."""
        ns.sim_reset()
        # set channel length to 0 so we only have phase not loss
        self.sim_params["channel_length_l"] = 0
        self.sim_params["channel_length_r"] = 0
        self.sim_params["multi_photon"] = True
        self.sim_params["encoding"] = "presence_absence"

        # check if heralded connection is correctly set up with and without magic
        self.sim_params["magic"] = None
        heralded_connection = HeraldedConnection("HeraldedConnection", **self.sim_params)
        # no magic, therefore noise_model is concatenated loss_model + phase_model
        assert heralded_connection.subcomponents["QCh_L"].models["quantum_noise_model"].is_concatenated
        self.sim_params["magic"] = "analytical"
        heralded_connection = HeraldedConnection("HeraldedConnection", **self.sim_params)
        # magic, therefore noise_model is not concatenated just loss_model and phase is handled by MD
        assert not heralded_connection.subcomponents["QCh_L"].models["quantum_noise_model"].is_concatenated

        # create MD
        phase_md = self.create_md(self.sim_params)
        # change delivered state
        input_dm = np.zeros((16, 16))
        input_dm.fill(.25)
        input_dm /= np.trace(input_dm)

        class MockDelFactory(AEStateDeliverySamplerFactory):
            def __init__(self, input_dm):
                super().__init__(func_delivery=self._create_state_sampler)
                self.input_dm = input_dm
                self._success_probability = 1

            def _create_state_sampler(self, **kwargs):
                return StateSampler([self.input_dm], probabilities=[1.], labels=[BSMOutcome(success=True)]), 1

        phase_md.delivery_sampler_factory[0] = MockDelFactory(input_dm=input_dm)
        # change phase_model to fixed phase so test is deterministic instead of random
        assert not phase_md._apply_phase
        assert phase_md._fibre_phase_model_l is None
        assert phase_md._fibre_phase_model_r is None
        fixed_phase = np.pi/4
        phase_md._fibre_phase_model_l = FibrePhaseModel(multi_photon=self.sim_params["multi_photon"],
                                                        standard_deviation=0., fixed_phase=fixed_phase)
        phase_md._fibre_phase_model_r = FibrePhaseModel(multi_photon=self.sim_params["multi_photon"],
                                                        standard_deviation=0., fixed_phase=fixed_phase)
        phase_md._apply_phase = True

        # deliver single state
        phase_md.start_auto_delivery()
        time_step = phase_md.repetition_duration
        ns.sim_run(duration=time_step * 1.1)
        # check delivered state
        output_state = qapi.create_qubits(4, no_state=True)
        qapi.assign_qstate(output_state, qrepr=input_dm)
        phase_md._fibre_phase_model_l.error_operation(output_state[0:2])
        phase_md._fibre_phase_model_r.error_operation(output_state[2:4])

        for node in [0, 1]:
            assert np.allclose(phase_md.nodes[0][node].subcomponents["QMem"].peek([0, 1])[0].qstate.qrepr.reduced_dm(),
                               output_state[0].qstate.qrepr.reduced_dm())

    def test_multiple_states_delivered(self):
        """Test if multiple states per link are delivered correctly."""
        ns.sim_reset()
        # set formalism to ket
        qformalism.set_qstate_formalism(qformalism.KetRepr)

        self.sim_params["multiple_link_successes"] = True
        if self.sim_params["source_num_modes"] is None:
            if None in [self.sim_params["g2"], self.sim_params["Rate"], self.sim_params["source_frequency"]]:
                raise ValueError("Either num_multiplexing_modes has to be specified or the parameters g2, Rate "
                                 "and frequency has to be specified.")
            else:
                self.sim_params["source_num_modes"] = int(
                    4/self.sim_params["g2"]*self.sim_params["Rate"]/self.sim_params["source_frequency"]
                )
        num_modes = min(self.sim_params["source_num_modes"], self.sim_params["mem_num_modes"])
        md = self.create_md(self.sim_params)

        assert len(md.delivery_sampler_factory) == 1
        assert md.delivery_sampler_factory[0].single_mode_success_prob == .5  # perfect params, lin. opt. BSM

        for node in md.nodes[0]:
            # check if correct number of memory positions were created
            assert node.subcomponents["QMem"].num_positions == self.sim_params["mem_num_modes"]
            assert not node.subcomponents["QMem"].used_positions

        md.start_auto_delivery()
        time_step = md.repetition_duration
        ns.sim_run(duration=1.1 * time_step)  # 1 chain delivery
        for node in md.nodes[0]:
            # check if with the imperfect probability (50%) at least 1 and at most num_modes modes were delivered
            assert 0 < len(node.subcomponents["QMem"].used_positions) <= num_modes

            # reset memory

            node.subcomponents["QMem"].reset()
            assert not node.subcomponents["QMem"].used_positions

        # set single mode probability to perfect
        class MockPerfectDelFactory(AEStateDeliverySamplerFactory):
            def __init__(self):
                super().__init__(func_delivery=self._create_state_sampler)
                self._single_mode_success_prob = 1.

            @staticmethod
            def _create_state_sampler(**kwargs):
                return StateSampler([ns.qubits.ketstates.b10, ns.qubits.ketstates.b11],
                                    probabilities=[.5, .5], labels=[BSMOutcome(success=True),
                                                                    BSMOutcome(success=True)]), 1

        md.delivery_sampler_factory[0] = MockPerfectDelFactory()

        ns.sim_run(duration=1.1 * time_step)  # 1 chain delivery
        assert md.delivery_sampler_factory[0].single_mode_success_prob == 1.
        for node in md.nodes[0]:
            # check if with the perfect probability all modes were delivered
            assert node.subcomponents["QMem"].used_positions == list(range(num_modes))

        # check if different states were delivered
        states_one_memory = []
        for mem_position in md.nodes[0][0].subcomponents["QMem"].used_positions:
            state = md.nodes[0][0].subcomponents["QMem"].peek(mem_position, skip_noise=True)[0]
            states_one_memory.append(state)
        assert len(set(states_one_memory)) == len(md.nodes[0][0].subcomponents["QMem"].used_positions)
        qstates_on_mem = [qubit.qstate.qrepr.ket for qubit in states_one_memory]
        unique_arrays = [qstates_on_mem[0]]
        for array in qstates_on_mem:
            already_exists = 0
            for unique_array in unique_arrays:
                if np.array_equal(array, unique_array):
                    already_exists += 1
            if already_exists == 0:
                unique_arrays.append(array)
        assert len(unique_arrays) == 2  # MockPerfectDelFactory contains two states

        # TODO: check labels?


if __name__ == "__main__":
    ns.logger.setLevel(logging.DEBUG)
    unittest.main()
