import logging
import unittest

import netsquid as ns
from netsquid.nodes import Connection
from netsquid_physlayer.detectors import BSMDetector

from netsquid_ae.ae_component_setup import setup_heralded_connection, setup_source
from netsquid_ae.multi_photon_detectors import MultiPhotonBSMDetector


class TestComponentSetup(unittest.TestCase):

    def setUp(self):
        self.sim_params = {
            # COMPONENTS

            # channel
            "channel_length_l": 10,
            "channel_length_r": 10,
            "attenuation_l": 0.,
            "attenuation_r": 0.,
            "coupling_loss_fibre": 0.,
            "fibre_phase_stdv_l": 0.0,
            "fibre_phase_stdv_r": 0.0,

            # midpoint detector
            "det_dark_count_prob": 0,   # probability of dark count per detection
            "det_efficiency": 1.,       # detector efficiency
            "det_visibility": 1.,       # photon indistinguishability (must be 1 for multi_photon=True)
            "det_num_resolving": True,  # using number or non_number resolving detectors

            # SIMULATION
            "multi_photon": False,      # bool: whether to use multi-photon up to n=3 (WARNING: very slow)
            "encoding": "time_bin",     # encoding of the entangled photons
            "magic": None,
            "state_files_directory": None,
            "multiple_link_successes": False,
        }

    @staticmethod
    def assert_correct_heralded_connection_params(connection, sim_params):
        """Check that the connection was set up with the right parameters."""
        # check length
        assert connection.subcomponents["QCh_L"].properties["length"] == sim_params["channel_length_l"]
        assert connection.subcomponents["QCh_R"].properties["length"] == sim_params["channel_length_r"]
        assert connection.subcomponents["CCh_L"].properties["length"] == sim_params["channel_length_l"]
        assert connection.subcomponents["CCh_R"].properties["length"] == sim_params["channel_length_r"]

        # check loss models
        loss_model_l = connection.subcomponents["QCh_L"].models["quantum_noise_model"]
        loss_model_r = connection.subcomponents["QCh_R"].models["quantum_noise_model"]
        if sim_params["encoding"] == "time_bin" or sim_params["magic"] is not None:
            assert not connection.subcomponents["QCh_L"].models["quantum_noise_model"].is_concatenated
            assert not connection.subcomponents["QCh_R"].models["quantum_noise_model"].is_concatenated
            assert loss_model_l.properties["p_loss_init"] == sim_params["coupling_loss_fibre"]
            assert loss_model_l.properties["p_loss_length"] == sim_params["attenuation_l"]
            assert loss_model_r.properties["p_loss_init"] == sim_params["coupling_loss_fibre"]
            assert loss_model_r.properties["p_loss_length"] == sim_params["attenuation_r"]
        else:
            assert connection.subcomponents["QCh_L"].models["quantum_noise_model"].is_concatenated
            assert connection.subcomponents["QCh_R"].models["quantum_noise_model"].is_concatenated
            assert loss_model_l._models[0].properties["p_loss_init"] == sim_params["coupling_loss_fibre"]
            assert loss_model_l._models[0].properties["p_loss_length"] == sim_params["attenuation_l"]
            assert loss_model_r._models[0].properties["p_loss_init"] == sim_params["coupling_loss_fibre"]
            assert loss_model_r._models[0].properties["p_loss_length"] == sim_params["attenuation_r"]
            assert loss_model_l._models[1].properties["standard_deviation"] == sim_params["fibre_phase_stdv_l"]
            assert loss_model_r._models[1].properties["standard_deviation"] == sim_params["fibre_phase_stdv_r"]

        # check properties of midpoint detector
        qdetector = connection.subcomponents["QDet"]
        assert qdetector.p_dark == sim_params["det_dark_count_prob"]
        assert qdetector.det_eff == sim_params["det_efficiency"]
        assert qdetector.visibility == sim_params["det_visibility"]
        assert qdetector.num_resolving == sim_params["det_num_resolving"]

        # check detector class and encoding
        if sim_params["multi_photon"]:
            assert isinstance(qdetector, MultiPhotonBSMDetector)
            assert connection.subcomponents["QCh_L"].properties["mp_encoding"] == sim_params["encoding"]
            assert connection.subcomponents["QCh_R"].properties["mp_encoding"] == sim_params["encoding"]
            assert qdetector.encoding == sim_params["encoding"]
        else:
            assert isinstance(qdetector, BSMDetector)

        # check ports
        assert connection.subcomponents["QCh_L"].ports["recv"].is_connected
        assert connection.subcomponents["QCh_R"].ports["recv"].is_connected
        assert connection.subcomponents["QCh_L"].ports["send"].forwarded_ports is not {}
        assert connection.subcomponents["QCh_R"].ports["send"].forwarded_ports is not {}
        assert connection.subcomponents["CCh_L"].ports["recv"].forwarded_ports is not {}
        assert connection.subcomponents["CCh_R"].ports["recv"].forwarded_ports is not {}
        assert connection.subcomponents["CCh_L"].ports["send"].is_connected
        assert connection.subcomponents["CCh_R"].ports["send"].is_connected

    def test_setup_symmetric_heralded_connection(self):
        """Check that heralded connection is set up correctly."""
        # no changes to sim_params necessary
        connection = setup_heralded_connection(Connection(name="Test Connection"), **self.sim_params)
        self.assert_correct_heralded_connection_params(connection, self.sim_params)

    def test_setup_asymmetric_heralded_connection(self):
        """Check that heralded connection is set up correctly in case of asymmetry."""
        # change left side of connection, the connection is now asymmetric
        self.sim_params["channel_length_l"] *= 2
        self.sim_params["attenuation_l"] = 0.2

        connection = setup_heralded_connection(Connection(name="Test Connection"), **self.sim_params)
        self.assert_correct_heralded_connection_params(connection, self.sim_params)

        # change to presence_absence
        self.sim_params["encoding"] = "presence_absence"
        self.sim_params["fibre_phase_stdv_l"] = 0.1
        connection = setup_heralded_connection(Connection(name="Test Connection"), **self.sim_params)
        self.assert_correct_heralded_connection_params(connection, self.sim_params)
        # change to presence_absence and magic
        self.sim_params["magic"] = "analytical"
        connection = setup_heralded_connection(Connection(name="Test Connection"), **self.sim_params)
        self.assert_correct_heralded_connection_params(connection, self.sim_params)

    def test_setup_source_with_different_mpns(self):
        """Check that source is set up correctly when there are multiple mpn values for the chain.

        Source in this example is placed on the rightmost node (Bob) in a single repeater setup.
        """
        sim_params = {
            "multi_photon": True,
            "mean_photon_number": [0.2, 0.3, 0.4, 0.5],
            "emission_probabilities": None,
            "source_frequency": 10e6,
            "num_multiplexing_modes": None,
            "g2": 0.12,
            "Rate": 4*3.5e3,
            "encoding": "presence_absence"
        }

        source = setup_source("SPDCSource_Test", **sim_params, mpn_index=3)

        assert source.spectral_modes is not None
        assert source.encoding == "presence_absence"
        assert source.mu == 0.5


if __name__ == "__main__":
    ns.logger.setLevel(logging.NOTSET)
    unittest.main()
