import unittest
import numpy as np

import netsquid as ns
import netsquid.qubits.qubitapi as qapi
import netsquid.qubits.operators as ops
from netsquid.qubits import ketstates
from netsquid.components import QuantumMemory, QuantumChannel

from netsquid_ae.qlossmodel import MemoryLossModel, FibrePhaseModel, FibreLossModel, FixedLossModel


class TestQuantumLossModel(unittest.TestCase):
    """ Unittest for the QuantumLossModel used for the AFC Model."""

    def setUp(self):
        """Sets up a new simulation environment for each test."""
        ns.sim_reset()

    @staticmethod
    def memory_factory(time_dependence="exponential"):
        """setting up an efficient and an unefficient memory"""
        ineff_properties = {
            'coherence_time': 1e6,
            'max_efficiency': 0,
            'mp_encoding': None,
            'delta_time': None,
        }
        eff_properties = {
            'coherence_time': 1e6,
            'max_efficiency': 1,
            'mp_encoding': None,
            'delta_time': None,
        }

        ineff_memory = QuantumMemory("Inefficient_TestMemory", memory_noise_models=None, num_positions=2,
                                     properties=ineff_properties)
        eff_memory = QuantumMemory("Efficient_TestMemory", memory_noise_models=None, num_positions=2,
                                   properties=eff_properties)
        eff_memory_on_memory_noise = QuantumMemory("Efficient_TestMemory_OnMemory", memory_noise_models=None,
                                                   num_positions=2, properties=eff_properties)
        ineff_memory.models['qout_noise_model'] = MemoryLossModel(time_dependence=time_dependence)
        eff_memory.models['qout_noise_model'] = MemoryLossModel(time_dependence=time_dependence)

        return ineff_memory, eff_memory, eff_memory_on_memory_noise

    @staticmethod
    def ref_state_factory(encoding):
        """ producing reference states of n <=3 photons in time-bin or number state encoding."""
        if encoding == 'presence_absence':
            # initializing states with n <= 3 photons
            num_state_3 = np.array([[0] * 4] * 4)
            num_state_3[3][3] = 1
            num_state_2 = np.array([[0] * 4] * 4)
            num_state_2[2][2] = 1
            num_state_1 = np.array([[0] * 4] * 4)
            num_state_1[1][1] = 1
            num_state_0 = np.array([[0] * 4] * 4)
            num_state_0[0][0] = 1
            return num_state_0, num_state_1, num_state_2, num_state_3
        elif encoding == 'time_bin':
            # initializing states with n <= 3 photons in the same time-bin
            tb_state_3 = np.array([[0] * 16] * 16)
            tb_state_3[12][12] = 1
            tb_state_3[12][3] = 1
            tb_state_3[3][12] = 1
            tb_state_3[3][3] = 1
            tb_state_3 = 1 / 2 * tb_state_3
            tb_state_2 = np.array([[0] * 16] * 16)
            tb_state_2[8][8] = 1
            tb_state_2[8][2] = 1
            tb_state_2[2][8] = 1
            tb_state_2[2][2] = 1
            tb_state_2 = 1 / 2 * tb_state_2
            tb_state_1 = np.array([[0] * 16] * 16)
            tb_state_1[4][4] = 1
            tb_state_1[4][1] = 1
            tb_state_1[1][4] = 1
            tb_state_1[1][1] = 1
            tb_state_1 = 1 / 2 * tb_state_1
            tb_state_0 = np.array([[0] * 16] * 16)
            tb_state_0[0][0] = 1
            return tb_state_0, tb_state_1, tb_state_2, tb_state_3
        else:
            raise ValueError("wrong encoding specified for ref_state_factory.")

    def test_MemoryLossModel_qubits_erasure(self):
        """Test QuantumLossModel example on standard qubits"""
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)
        q1, q2, q3, q4, q5, q6 = qapi.create_qubits(6)
        qapi.operate(q1, ops.H)
        qapi.operate([q1, q2], ops.CNOT)  # b00 state
        qapi.operate(q3, ops.H)
        qapi.operate([q3, q4], ops.CNOT)  # b00 state
        qapi.operate(q5, ops.H)
        qapi.operate([q5, q6], ops.CNOT)  # b00 state

        # all qubits not number states -> erasure
        self.assertFalse(q1.is_number_state)
        self.assertFalse(q2.is_number_state)
        self.assertFalse(q3.is_number_state)
        self.assertFalse(q4.is_number_state)
        self.assertFalse(q5.is_number_state)
        self.assertFalse(q6.is_number_state)

        ineff_memory, eff_memory, on_mem_memory = self.memory_factory()

        ineff_memory.ports["qin"].tx_input([q1, q2])
        eff_memory.ports["qin"].tx_input([q3, q4])
        on_mem_memory.ports["qin"].tx_input([q5, q6])

        ns.sim_run(5, magnitude=ns.NANOSECOND)

        # test whether inefficient memory has discarded state
        delta_time_a, = ineff_memory.delta_time(positions=0)
        delta_time_b, = ineff_memory.delta_time(positions=1)
        if delta_time_a != delta_time_b:
            raise RuntimeError("Qubits spent different time on memory.")
        else:
            ineff_memory.models["qout_noise_model"].properties.update({"delta_time": delta_time_a})
        qa, = ineff_memory.pop(positions=0)
        qb, = ineff_memory.pop(positions=1)
        self.assertEqual(qa.qstate, None)
        self.assertEqual(qb.qstate, None)

        # test whether efficient memory restores state
        delta_time_a, = eff_memory.delta_time(positions=0)
        delta_time_b, = eff_memory.delta_time(positions=1)
        if delta_time_a != delta_time_b:
            raise RuntimeError("Qubits spent different time on memory.")
        else:
            eff_memory.models["qout_noise_model"].properties.update({"delta_time": delta_time_a})
        qc, = eff_memory.pop(positions=0)
        qd, = eff_memory.pop(positions=1)
        f_eff = qapi.fidelity([qc, qd], ketstates.b00)
        self.assertAlmostEqual(f_eff, 1)
        # same for efficient memory with noise on memory
        qe, = on_mem_memory.pop(positions=0)
        qf, = on_mem_memory.pop(positions=1)
        f_eff = qapi.fidelity([qe, qf], ketstates.b00)
        self.assertAlmostEqual(f_eff, 1)

        # put qubits back in memories for long run
        eff_memory.ports["qin"].tx_input([qc, qd])

        ns.sim_run(5e10, magnitude=ns.SECOND)

        # test whether efficient memory has now discarded the state as well
        delta_time_a, = eff_memory.delta_time(positions=0)
        delta_time_b, = eff_memory.delta_time(positions=1)
        if delta_time_a != delta_time_b:
            raise RuntimeError("Qubits spent different time on memory.")
        else:
            eff_memory.models["qout_noise_model"].properties.update({"delta_time": delta_time_a})
        qc, = eff_memory.pop(positions=0)
        qd, = eff_memory.pop(positions=1)
        self.assertEqual(qc.qstate, None)
        self.assertEqual(qd.qstate, None)
        # same for efficient memory with noise on memory
        qe, = on_mem_memory.pop(positions=0)
        qf, = on_mem_memory.pop(positions=1)
        self.assertEqual(qe, None)
        self.assertEqual(qf, None)

    def test_MemoryLossModel_qubits_damping(self):
        """Test QuantumLossModel example on number state qubits"""
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)
        qubits = qapi.create_qubits(6)
        for qubit in qubits:
            qubit.is_number_state = True
        for i in range(0, len(qubits), 2):
            # Create pair-wise b00 states
            qapi.operate(qubits[i], ops.H)
            qapi.operate(qubits[i:i + 2], ops.CNOT)

        # all qubits number states -> amplitude damping
        for qubit in qubits:
            self.assertTrue(qubit.is_number_state)

        ineff_memory, eff_memory, _ = self.memory_factory()

        ineff_memory.ports["qin"].tx_input(qubits[:2])
        eff_memory.ports["qin"].tx_input(qubits[2:4])
        ns.sim_run(duration=5, magnitude=ns.NANOSECOND)

        # test whether inefficient memory has damped b00 to |00> state
        delta_time_a, delta_time_b = ineff_memory.delta_time(positions=[0, 1])
        if delta_time_a != delta_time_b:
            raise RuntimeError("Qubits spent different time on memory.")
        ineff_memory.models["qout_noise_model"].properties.update({"delta_time": delta_time_a})
        qa, qb = ineff_memory.pop(positions=[0, 1])
        state_00 = np.array([[1, 0, 0, 0],
                             [0, 0, 0, 0],
                             [0, 0, 0, 0],
                             [0, 0, 0, 0]])
        self.assertTrue(np.allclose(qapi.reduced_dm([qa, qb]), state_00))

        # test whether efficient memory restores state
        delta_time_a, delta_time_b = eff_memory.delta_time(positions=[0, 1])
        if delta_time_a != delta_time_b:
            raise RuntimeError("Qubits spent different time on memory.")
        eff_memory.models["qout_noise_model"].properties.update({"delta_time": delta_time_a})
        qc, qd = eff_memory.pop(positions=[0, 1])
        f_eff = qapi.fidelity([qc, qd], ketstates.b00)
        self.assertAlmostEqual(f_eff, 1)

        # transmit last two qubits for long simulation run
        eff_memory.ports["qin"].tx_input(qubits[4:])
        ns.sim_run(duration=1e3, magnitude=ns.SECOND)

        # test whether efficient memory has now damped the state to |00> as well
        delta_time_a, delta_time_b = eff_memory.delta_time(positions=[0, 1])
        if delta_time_a != delta_time_b:
            raise RuntimeError("Qubits spent different time on memory.")
        eff_memory.models["qout_noise_model"].properties.update({"delta_time": delta_time_a})
        qe, qf = eff_memory.pop(positions=[0, 1])
        self.assertTrue(np.allclose(qapi.reduced_dm([qe, qf]), state_00))

    def test_MemoryLossModel_multi_photon_damping_number_state(self):
        """test if multi-photon generalized amplitude damping is working as intended for number-state encoding"""
        for time_dependence in ["exponential", "gaussian"]:

            # create reference states
            num_state_0, num_state_1, num_state_2, num_state_3 = self.ref_state_factory(encoding='presence_absence')

            # create 3 and 1 photon state
            qa, qb = qapi.create_qubits(2, "mp_numstate_3")
            qc, qd = qapi.create_qubits(2, "mp_numstate_1")
            qapi.assign_qstate([qa, qb], num_state_3)
            qapi.assign_qstate([qc, qd], num_state_1)
            for q in [qa, qb, qc, qd]:
                q.is_number_state = True

            # set time on memory
            mem_time = 1

            ineff_memory, eff_memory, _ = self.memory_factory(time_dependence)

            # number state encoding
            ineff_memory.properties['mp_encoding'] = 'presence_absence'
            eff_memory.properties['mp_encoding'] = 'presence_absence'

            # not damping
            eff_memory.models["qout_noise_model"].properties.update({"delta_time": mem_time})
            eff_memory.models["qout_noise_model"].error_operation([qa, qb], **eff_memory.properties)
            eff_memory.models["qout_noise_model"].error_operation([qc, qd], **eff_memory.properties)

            self.assertTrue(np.allclose(qa.qstate.qrepr.reduced_dm(), num_state_3, atol=1.e-5))
            self.assertTrue(np.allclose(qb.qstate.qrepr.reduced_dm(), num_state_3, atol=1.e-5))
            self.assertTrue(np.allclose(qc.qstate.qrepr.reduced_dm(), num_state_1, atol=1.e-5))
            self.assertTrue(np.allclose(qd.qstate.qrepr.reduced_dm(), num_state_1, atol=1.e-5))

            # damping a bit
            eff_memory.models["qout_noise_model"].properties.update({"delta_time": 1e6 * mem_time})
            eff_memory.models["qout_noise_model"].error_operation([qa, qb], **eff_memory.properties)
            eff_memory.models["qout_noise_model"].error_operation([qc, qd], **eff_memory.properties)
            self.assertTrue((qa.qstate.qrepr.reduced_dm() == num_state_3).all or
                            (qa.qstate.qrepr.reduced_dm() == num_state_2).all or
                            (qa.qstate.qrepr.reduced_dm() == num_state_1).all or
                            (qa.qstate.qrepr.reduced_dm() == num_state_0).all)
            self.assertTrue((qc.qstate.qrepr.reduced_dm() == num_state_1).all or
                            (qc.qstate.qrepr.reduced_dm() == num_state_0).all)
            self.assertTrue(qc.qstate.qrepr.reduced_dm()[2][2] == 0 and qc.qstate.qrepr.reduced_dm()[3][3] == 0)
            # damping completely
            ineff_memory.models["qout_noise_model"].properties.update({"delta_time": mem_time})
            ineff_memory.models["qout_noise_model"].error_operation([qa, qb], **ineff_memory.properties)
            ineff_memory.models["qout_noise_model"].error_operation([qc, qd], **ineff_memory.properties)
            self.assertTrue((qa.qstate.qrepr.reduced_dm() == num_state_0).all())
            self.assertTrue((qb.qstate.qrepr.reduced_dm() == num_state_0).all())
            self.assertTrue((qc.qstate.qrepr.reduced_dm() == num_state_0).all())
            self.assertTrue((qd.qstate.qrepr.reduced_dm() == num_state_0).all())
            self.assertFalse((qa.qstate.qrepr.reduced_dm() == num_state_1).all())  # sanity check

    def test_MemoryLossModel_multiphoton_damping_time_bin(self):
        """test if multi-photon generalized amplitude damping is working as intended for time-bin encoding"""
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)

        for time_dependence in ["exponential", "gaussian"]:

            # produce reference states
            tb_state_0, tb_state_1, tb_state_2, tb_state_3 = self.ref_state_factory(encoding='time_bin')

            # create 3 and 1 photon state
            q1, q2, q3, q4 = qapi.create_qubits(4, "mp_timebin_3")
            q5, q6, q7, q8 = qapi.create_qubits(4, "mp_timebin_1")
            tb3 = [q1, q2, q3, q4]
            tb1 = [q5, q6, q7, q8]
            qapi.assign_qstate(tb3, tb_state_3)
            qapi.assign_qstate(tb1, tb_state_1)
            for q in [q1, q2, q3, q4, q5, q6, q7, q8]:
                q.is_number_state = True

            # set time on memory
            mem_time = 1

            ineff_memory, eff_memory, _ = self.memory_factory(time_dependence)

            # time-bin encoding
            ineff_memory.properties['mp_encoding'] = 'time_bin'
            eff_memory.properties['mp_encoding'] = 'time_bin'
            # not damping
            eff_memory.models["qout_noise_model"].properties.update({"delta_time": mem_time})
            eff_memory.models["qout_noise_model"].error_operation(tb3, **eff_memory.properties)
            eff_memory.models["qout_noise_model"].error_operation(tb1, **eff_memory.properties)

            self.assertTrue(np.allclose(q1.qstate.qrepr.reduced_dm(), tb_state_3, atol=1.e-5))
            self.assertTrue(np.allclose(q3.qstate.qrepr.reduced_dm(), tb_state_3, atol=1.e-5))
            self.assertTrue(np.allclose(q5.qstate.qrepr.reduced_dm(), tb_state_1, atol=1.e-5))
            self.assertTrue(np.allclose(q7.qstate.qrepr.reduced_dm(), tb_state_1, atol=1.e-5))
            # damping a bit
            eff_memory.models["qout_noise_model"].properties.update({"delta_time": 1e6 * mem_time})
            eff_memory.models["qout_noise_model"].error_operation(tb3, **eff_memory.properties)
            eff_memory.models["qout_noise_model"].error_operation(tb1, **eff_memory.properties)
            self.assertTrue(q1.qstate.qrepr.ket[0] == 1 or
                            q1.qstate.qrepr.ket[1] != 0 or
                            q1.qstate.qrepr.ket[2] != 0 or
                            q1.qstate.qrepr.ket[3] != 0 or
                            q1.qstate.qrepr.ket[4] != 0 or
                            q1.qstate.qrepr.ket[8] != 0 or
                            q1.qstate.qrepr.ket[12] != 0)
            self.assertTrue(q5.qstate.qrepr.ket[0] == 1 or
                            q5.qstate.qrepr.ket[1] != 0 or
                            q5.qstate.qrepr.ket[4] != 0)
            self.assertTrue(q5.qstate.qrepr.ket[2] == 0 and
                            q5.qstate.qrepr.ket[3] == 0 and
                            q5.qstate.qrepr.ket[8] == 0 and
                            q5.qstate.qrepr.ket[12] == 0)
            # damping completely
            ineff_memory.models["qout_noise_model"].properties.update({"delta_time": mem_time})
            ineff_memory.models["qout_noise_model"].error_operation(tb3, **ineff_memory.properties)
            ineff_memory.models["qout_noise_model"].error_operation(tb1, **ineff_memory.properties)
            self.assertTrue((q1.qstate.qrepr.reduced_dm() == tb_state_0).all())
            self.assertTrue((q3.qstate.qrepr.reduced_dm() == tb_state_0).all())
            self.assertTrue((q5.qstate.qrepr.reduced_dm() == tb_state_0).all())
            self.assertTrue((q7.qstate.qrepr.reduced_dm() == tb_state_0).all())
            self.assertFalse((q1.qstate.qrepr.reduced_dm() == tb_state_1).all())  # sanity check


class TestFibreLossModel(unittest.TestCase):
    """Unittest for the FibreLossModel."""
    def setUp(self):
        """Sets up a new simulation environment for each test."""
    ns.sim_reset()

    def test_FibreLossModel_multiphoton_damping_time_bin(self):
        """Test if multi-photon generalized amplitude damping is working as intended for time-bin encoding for the
         Fibre."""
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)

        # produce reference states
        tb_state_0, tb_state_1, tb_state_2, tb_state_3 = TestQuantumLossModel.ref_state_factory(encoding='time_bin')

        # create 3 and 1 photon state
        q1, q2, q3, q4 = qapi.create_qubits(4, "mp_timebin_3")
        q5, q6, q7, q8 = qapi.create_qubits(4, "mp_timebin_1")
        tb3 = [q1, q2, q3, q4]
        tb1 = [q5, q6, q7, q8]
        qapi.assign_qstate(tb3, tb_state_3)
        qapi.assign_qstate(tb1, tb_state_1)
        for q in [q1, q2, q3, q4, q5, q6, q7, q8]:
            q.is_number_state = True

        # Loss model with initial loss and loss over distance
        bad_loss_model = FibreLossModel(p_loss_init=.5, p_loss_length=0.2)
        no_loss_model = FibreLossModel(p_loss_init=0., p_loss_length=0.2)
        short_good_fibre = QuantumChannel("short_good_fibre", length=0, models={"quantum_noise_model": no_loss_model},
                                          transmit_empty_items=True, properties={'mp_encoding': None})
        short_bad_fibre = QuantumChannel("short_bad_fibre", length=0, models={"quantum_noise_model": bad_loss_model},
                                         transmit_empty_items=True, properties={'mp_encoding': None})
        long_fibre = QuantumChannel("moon_fibre", length=400000, models={"quantum_noise_model": no_loss_model},
                                    transmit_empty_items=True, properties={'mp_encoding': None})

        # time-bin encoding
        long_fibre.properties['mp_encoding'] = 'time_bin'
        short_good_fibre.properties['mp_encoding'] = 'time_bin'
        short_bad_fibre.properties['mp_encoding'] = 'time_bin'
        # not damping
        short_good_fibre.models["quantum_noise_model"].error_operation(tb3, **short_good_fibre.properties)
        short_good_fibre.models["quantum_noise_model"].error_operation(tb1, **short_good_fibre.properties)

        self.assertTrue(np.allclose(q1.qstate.qrepr.reduced_dm(), tb_state_3, atol=1.e-5))
        self.assertTrue(np.allclose(q3.qstate.qrepr.reduced_dm(), tb_state_3, atol=1.e-5))
        self.assertTrue(np.allclose(q5.qstate.qrepr.reduced_dm(), tb_state_1, atol=1.e-5))
        self.assertTrue(np.allclose(q7.qstate.qrepr.reduced_dm(), tb_state_1, atol=1.e-5))
        # damping a bit
        short_bad_fibre.models["quantum_noise_model"].error_operation(tb3, **short_bad_fibre.properties)
        short_bad_fibre.models["quantum_noise_model"].error_operation(tb1, **short_bad_fibre.properties)
        self.assertTrue(q1.qstate.qrepr.ket[0] == 1 or
                        q1.qstate.qrepr.ket[1] != 0 or
                        q1.qstate.qrepr.ket[2] != 0 or
                        q1.qstate.qrepr.ket[3] != 0 or
                        q1.qstate.qrepr.ket[4] != 0 or
                        q1.qstate.qrepr.ket[8] != 0 or
                        q1.qstate.qrepr.ket[12] != 0)
        self.assertTrue(q5.qstate.qrepr.ket[0] == 1 or
                        q5.qstate.qrepr.ket[1] != 0 or
                        q5.qstate.qrepr.ket[4] != 0)
        self.assertTrue(q5.qstate.qrepr.ket[2] == 0 and
                        q5.qstate.qrepr.ket[3] == 0 and
                        q5.qstate.qrepr.ket[8] == 0 and
                        q5.qstate.qrepr.ket[12] == 0)
        # damping completely
        long_fibre.models["quantum_noise_model"].error_operation(tb3, **long_fibre.properties)
        long_fibre.models["quantum_noise_model"].error_operation(tb1, **long_fibre.properties)
        self.assertTrue((q1.qstate.qrepr.reduced_dm() == tb_state_0).all())
        self.assertTrue((q3.qstate.qrepr.reduced_dm() == tb_state_0).all())
        self.assertTrue((q5.qstate.qrepr.reduced_dm() == tb_state_0).all())
        self.assertTrue((q7.qstate.qrepr.reduced_dm() == tb_state_0).all())
        self.assertFalse((q1.qstate.qrepr.reduced_dm() == tb_state_1).all())  # sanity check


class TestFixedLossModel(unittest.TestCase):
    """Unittest for the FixedLossModel for presence-absence encoding."""
    def setUp(self):
        """Sets up a new simulation environment for each test."""
        ns.sim_reset()

    def test_FibreLossModel_multiphoton_damping_time_bin(self):
        """Test if multi-photon generalized amplitude damping is working as intended for time-bin encoding."""
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)

        # produce reference states
        tb_state_0, tb_state_1, tb_state_2, tb_state_3 = TestQuantumLossModel.ref_state_factory(encoding='time_bin')

        # create 3 and 1 photon state
        q1, q2, q3, q4 = qapi.create_qubits(4, "mp_timebin_3")
        q5, q6, q7, q8 = qapi.create_qubits(4, "mp_timebin_1")
        tb3 = [q1, q2, q3, q4]
        tb1 = [q5, q6, q7, q8]
        qapi.assign_qstate(tb3, tb_state_3)
        qapi.assign_qstate(tb1, tb_state_1)
        for q in [q1, q2, q3, q4, q5, q6, q7, q8]:
            q.is_number_state = True

        # Loss model with initial loss and loss over distance
        no_loss_model = FixedLossModel(max_efficiency=1., mp_encoding='time_bin')
        bad_loss_model = FixedLossModel(max_efficiency=0.5, mp_encoding='time_bin')
        all_loss_model = FixedLossModel(max_efficiency=0., mp_encoding='time_bin')

        # not damping
        no_loss_model.error_operation(tb3, **no_loss_model.properties)
        no_loss_model.error_operation(tb1, **no_loss_model.properties)

        self.assertTrue(np.allclose(q1.qstate.qrepr.reduced_dm(), tb_state_3, atol=1.e-5))
        self.assertTrue(np.allclose(q3.qstate.qrepr.reduced_dm(), tb_state_3, atol=1.e-5))
        self.assertTrue(np.allclose(q5.qstate.qrepr.reduced_dm(), tb_state_1, atol=1.e-5))
        self.assertTrue(np.allclose(q7.qstate.qrepr.reduced_dm(), tb_state_1, atol=1.e-5))
        # damping a bit
        bad_loss_model.error_operation(tb3, **bad_loss_model.properties)
        bad_loss_model.error_operation(tb1, **bad_loss_model.properties)
        self.assertTrue(q1.qstate.qrepr.ket[0] == 1 or
                        q1.qstate.qrepr.ket[1] != 0 or
                        q1.qstate.qrepr.ket[2] != 0 or
                        q1.qstate.qrepr.ket[3] != 0 or
                        q1.qstate.qrepr.ket[4] != 0 or
                        q1.qstate.qrepr.ket[8] != 0 or
                        q1.qstate.qrepr.ket[12] != 0)
        self.assertTrue(q5.qstate.qrepr.ket[0] == 1 or
                        q5.qstate.qrepr.ket[1] != 0 or
                        q5.qstate.qrepr.ket[4] != 0)
        self.assertTrue(q5.qstate.qrepr.ket[2] == 0 and
                        q5.qstate.qrepr.ket[3] == 0 and
                        q5.qstate.qrepr.ket[8] == 0 and
                        q5.qstate.qrepr.ket[12] == 0)
        # damping completely
        all_loss_model.error_operation(tb3, **all_loss_model.properties)
        all_loss_model.error_operation(tb1, **all_loss_model.properties)
        self.assertTrue((q1.qstate.qrepr.reduced_dm() == tb_state_0).all())
        self.assertTrue((q3.qstate.qrepr.reduced_dm() == tb_state_0).all())
        self.assertTrue((q5.qstate.qrepr.reduced_dm() == tb_state_0).all())
        self.assertTrue((q7.qstate.qrepr.reduced_dm() == tb_state_0).all())
        self.assertFalse((q1.qstate.qrepr.reduced_dm() == tb_state_1).all())  # sanity check


class TestFibrePhaseModel(unittest.TestCase):
    """Unittest for the FibrePhaseModel for presence-absence encoding."""

    def setUp(self):
        """Sets up a new simulation environment for each test."""
        ns.sim_reset()

    def test_set_up_with_fibre_no_multi_photon(self):
        """Test if channel can be correcly set up with phase model and phase is applied correctly without using the
        multi-photon simulation."""
        # set DM formalism to analyse correctness  of DMs
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.DM)
        fixed_phase = np.pi/4
        phase_model = FibrePhaseModel(multi_photon=False, fixed_phase=fixed_phase, standard_deviation=0)
        # create qchannel with phase model
        test_qchannel_0 = QuantumChannel("Phasetest_QChannel", length=0,
                                         models={"quantum_loss_model": phase_model},
                                         transmit_empty_items=True, properties={'mp_encoding': None})
        # input single photon presence absence state.
        qubits = qapi.create_qubits(10)
        for q in qubits:
            qapi.operate(q, ops.H)
            q.is_number_state = True
        self.output = []

        def recv_channel_output(message):
            self.output.append(message)

        test_qchannel_0.ports['recv'].bind_output_handler(recv_channel_output)
        test_qchannel_0.ports['send'].tx_input(qubits)
        ns.sim_run()
        # check dm
        for qubit in self.output[0].items:
            assert np.allclose(qubit.qstate.qrepr.reduced_dm(), np.array([[.5, 0.5 * np.exp(-1j * fixed_phase)],
                                                                         [.5 * np.exp(+1j * fixed_phase), .5]]))

    def test_fibre_multi_photon_presence_absence(self):
        """Test if channel can be correctly set up with phase model and phase is applied correctly when using the
        multi-photon simulation in presence-absence encoding."""
        # set DM formalism to analyse correctness  of DMs
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.DM)
        fixed_phase = np.pi/4
        phase_model = FibrePhaseModel(multi_photon=True, fixed_phase=fixed_phase, standard_deviation=0)
        # create qchannel with phase model
        test_qchannel_0 = QuantumChannel("Phasetest_QChannel", length=0,
                                         models={"quantum_loss_model": phase_model},
                                         transmit_empty_items=True, properties={'mp_encoding': "presence_absence"})
        # input multi-photon presence absence state.

        qubits = []
        for _ in range(10):
            qs = qapi.create_qubits(2, no_state=True)
            qapi.assign_qstate(qs, qrepr=np.array([[.25, .25, .25, .25],
                                                   [.25, .25, .25, .25],
                                                   [.25, .25, .25, .25],
                                                   [.25, .25, .25, .25]]))
            qubits.extend(qs)

        self.output = []

        def recv_channel_output(message):
            self.output.append(message)

        test_qchannel_0.ports['recv'].bind_output_handler(recv_channel_output)
        test_qchannel_0.ports['send'].tx_input(qubits)
        ns.sim_run()
        # check dm
        for qubit in self.output[0].items:
            p = .25 * np.exp(+1j * fixed_phase)
            q = .25 * np.exp(-1j * fixed_phase)
            assert np.allclose(qubit.qstate.qrepr.reduced_dm(), np.array([[.25, q, q, q],
                                                                          [p, .25, .25, .25],
                                                                          [p, .25, .25, .25],
                                                                          [p, .25, .25, .25]]))

    def test_properties(self):
        """Test properties work correctly"""
        phase_model = FibrePhaseModel(multi_photon=False, fixed_phase=np.pi, standard_deviation=0)
        assert phase_model.multi_photon is False
        assert phase_model.fixed_phase == np.pi
        assert phase_model.standard_deviation == 0
        phase_model.multi_photon = True
        phase_model.fixed_phase = 0
        phase_model.standard_deviation = np.pi / 2
        assert phase_model.multi_photon is True
        assert phase_model.fixed_phase == 0
        assert phase_model.standard_deviation == np.pi / 2


if __name__ == "__main__":
    unittest.main()
