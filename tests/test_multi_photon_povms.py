import pytest
import unittest
import numpy as np
from os import remove
import os.path as path
import pickle
from itertools import product
from scipy.linalg import sqrtm

from netsquid.qubits import operators as ops

from qualg.integrate import integrate
from qualg.toolbox import simplify, get_variables

from netsquid_ae.multi_photon_tools import read_from_txt, write_to_txt
from netsquid_ae.multi_photon_povms import generate_fock_states, construct_projector, generate_effective_povms, \
    convert_scalars


def set_old_operators():
    """Computes the Kraus operators for both threshold and number resolving photon detectors.

    Also computes a dictionary which translates a click pattern to a measurement outcome and vice versa.
    Assumes perfect photon indistinguishibility, i.e. a mid_det_visibility of 1.

    """
    # Matrix for a creation operator for up to six photons
    creation_op = np.array([[0, 0, 0, 0, 0, 0, 0],
                            [1, 0, 0, 0, 0, 0, 0],
                            [0, np.sqrt(2), 0, 0, 0, 0, 0],
                            [0, 0, np.sqrt(3), 0, 0, 0, 0],
                            [0, 0, 0, 2, 0, 0, 0],
                            [0, 0, 0, 0, np.sqrt(5), 0, 0],
                            [0, 0, 0, 0, 0, np.sqrt(6), 0]])
    # Creation operators for the subspaces of the two outgoing arms of the beam splitter
    c = np.kron(creation_op, np.eye(7))
    d = np.kron(np.eye(7), creation_op)
    # Creation operators for the beam splitter
    a = (1 / np.sqrt(2)) * (c + d)
    b = (1 / np.sqrt(2)) * (c - d)
    # Start with the state |00>
    zero_state = np.zeros([1, 49])[0]
    zero_state[0] = 1
    OutIn = np.zeros([49, 49])
    # Consider all 49 states with 0 up to 6 photons in each outgoing arm
    for n in range(0, 7):
        an = np.linalg.matrix_power(a, n)
        for m in range(0, 7 - n):
            idx = 7 * m + n
            bm = np.linalg.matrix_power(b, m)
            OutIn[idx, :] = (an @ bm @ zero_state) / (np.sqrt(np.math.factorial(n)) * np.sqrt(np.math.factorial(m)))
    InOut = OutIn.transpose()
    ProjList = []
    for n in range(0, 7):
        for m in range(0, 7):
            if m + n < 7:
                idx = 7 * m + n
                # Create a projector for each state
                P = np.outer(InOut[idx, :], InOut[idx, :])
                for k in range(6, -1, -1):
                    for l in range(6, -1, -1):
                        if k > 3 or l > 3:
                            # Delete all columns and rows that have more than 3 photons as input state
                            P = np.delete(P, 7 * k + l, 0)
                            P = np.delete(P, 7 * k + l, 1)
                ProjList.append((n, m, P))
    kraus_operators_num_res = []
    outcome_dict = {}
    for idx, p in enumerate(ProjList):
        # Take the matrix square root of the POVM to get the Kraus operator
        kraus_operators_num_res.append(ops.Operator("n_{}{}".format(p[0], p[1]), sqrtm(p[2])))
        # The ordering is 00, 01, 02, 03, 04, 05, 06, 10, 11, 12, 13, 14, 15, 20, 21, 22, 23, 24, 30, 31, 32, 33
        #                 40, 41, 42, 50, 51, 60 (51, 33 and 51 are all zeros due to photon bunching)
        # Store a dictionary that translates measurement outcomes to tuples of clicks in the detectors
        outcome_dict[idx] = (p[0], p[1])
    P_00 = ProjList[0][2]
    P_01 = np.zeros([16, 16])
    P_10 = np.zeros([16, 16])
    P_11 = np.zeros([16, 16])
    for p in ProjList:
        n = p[0]
        m = p[1]
        # Group all POVMs with certain click patterns
        if n == 0 and m > 0:
            P_01 += p[2]
        elif n > 0 and m == 0:
            P_10 += p[2]
        elif n > 0 and m > 0:
            P_11 += p[2]
    # Take the matrix square root of the POVM to get the Kraus operator
    n_00 = ops.Operator("n_00", sqrtm(P_00))
    n_01 = ops.Operator("n_01", sqrtm(P_01))
    n_10 = ops.Operator("n_10", sqrtm(P_10))
    n_11 = ops.Operator("n_11", sqrtm(P_11))
    kraus_operators = [n_00, n_01, n_10, n_11]

    # TODO: store these ~32 16 x 16 matrices explicitly?

    return kraus_operators, kraus_operators_num_res, outcome_dict


class TestMultiPhoton(unittest.TestCase):
    """Test the correct implementation of multi photon states."""

    @pytest.mark.slow
    def test_state_generation(self):
        """Test the correct generation of Fock states."""
        n = 2
        states, states_dict = generate_fock_states(n, n)

        for name in states_dict:
            state = states_dict[name]
            # calculate inner product
            inner_prod = state.inner_product(state)
            ip = simplify(inner_prod)
            # integrate
            norm = integrate(ip)
            # must be equal to 1
            # currently only works for phi_i = phi_j m psi_i = psi_j
            self.assertAlmostEqual(norm, 1)

    @pytest.mark.slow
    def test_projectors(self):
        """Tests that projectors project on correct state."""
        pnum = 2
        lst = range(pnum+1)
        states, states_dict = generate_fock_states(pnum, pnum)
        for name in states_dict:
            for n, m in product(lst, lst):
                projector = construct_projector(n, m)
                state = states_dict[name]
                projection = projector * state
                # simplify
                if not isinstance(projection, int):
                    for base_op, scalar in projection._terms.items():
                        scalar_variables = get_variables(scalar) - get_variables(base_op)
                        projection._terms[base_op] = integrate(scalar, scalar_variables)
                inner_prod = projection.inner_product(state)
                norm = integrate(simplify(inner_prod))
                (k1, k2) = name
                if k1 + k2 != n + m:
                    self.assertAlmostEqual(norm, 0)
                else:
                    self.assertNotEqual(norm, 0)

    def test_povms(self):
        """Tests the correct generation of POVM operators."""
        # read from pre-generated text file containing the operators.
        ops_dict = read_from_txt(path.dirname(path.abspath(__file__)) + "/../netsquid_ae/full_multiphoton_povms.txt")
        # test read and write
        with open(path.dirname(path.abspath(__file__)) + '/test.pkl', 'wb') as output:
            pickle.dump(ops_dict, output)
        write_to_txt(path.dirname(path.abspath(__file__)) + '/test.pkl',
                     path.dirname(path.abspath(__file__)) + '/test')
        remove(path.dirname(path.abspath(__file__)) + '/test.pkl')
        remove(path.dirname(path.abspath(__file__)) + '/test.txt')

        for visibility in np.linspace(0, 1, 10):
            sum_povms = np.zeros(shape=(16, 16))
            for op in ops_dict.values():
                mat = op.to_numpy_matrix(convert_scalars, visibility=visibility)
                sum_povms += mat
            # check if povms sum up to identity
            idnt = np.identity(16)
            self.assertTrue(np.testing.assert_array_almost_equal(sum_povms, idnt) is None)

    def test_against_ll(self):
        """Tests if the generation script can reproduce the link layer povms."""
        # generate link layer arrays
        def set_ll_operators_num_resolving(visibility):
            """Sets up the relevant operators used for the measurement, depending on the state formalism used.

            Note: These are the operators for a number resolving detector.

            """
            # Assuming mu is real
            mu = np.sqrt(visibility)
            s_plus = (np.sqrt(1 + mu) + np.sqrt(1 - mu)) / (2. * np.sqrt(2))
            s_min = (np.sqrt(1 + mu) - np.sqrt(1 - mu)) / (2. * np.sqrt(2))
            # the Kraus operator for measuring zero photons
            M_00 = np.array([[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])
            # the Kraus operator for measuring one photon from station A
            M_10 = \
                np.array([[0, 0, 0, 0],
                          [0, s_plus, s_min, 0],
                          [0, s_min, s_plus, 0],
                          [0, 0, 0, 0]])
            # the Kraus operator for measuring one photon from station B
            M_01 = \
                np.array([[0, 0, 0, 0],
                          [0, s_plus, -1. * s_min, 0],
                          [0, -1. * s_min, s_plus, 0],
                          [0, 0, 0, 0]])
            # the Kraus operator for measuring two photons, one on each side
            M_11 = \
                np.array([[0, 0, 0, 0],
                          [0, 0, 0, 0],
                          [0, 0, 0, 0],
                          [0, 0, 0, np.sqrt(1 - mu * mu) / np.sqrt(2)]])
            # the Kraus operator for measuring two photons from station A
            M_20 = \
                np.array([[0, 0, 0, 0],
                          [0, 0, 0, 0],
                          [0, 0, 0, 0],
                          [0, 0, 0, np.sqrt(1 + mu * mu) / 2]])
            # the Kraus operator for measuring two photons from station B
            M_02 = M_20

            dict = {(0, 0): M_00,
                    (0, 1): M_01,
                    (0, 2): M_02,
                    (1, 0): M_10,
                    (1, 1): M_11,
                    (2, 0): M_20}

            return dict

        # generate povms
        ops_dict = generate_effective_povms(1, 1, multi_processing=False)
        for visibility in np.linspace(0, 1, 10):
            # generate ll povms with current visibility
            ll_ops_dict = set_ll_operators_num_resolving(visibility)
            # translate new povms to arrays with current visibility
            array_dict = {}
            for k in ops_dict.keys():
                array_dict[k] = sqrtm(ops_dict[k].to_numpy_matrix(convert_scalars, visibility=visibility))
            # compare arrays
            for key in array_dict.keys():
                self.assertTrue(np.testing.assert_array_almost_equal(array_dict[key], ll_ops_dict[key]) is None)

    def test_against_old_povms(self):
        """Test if POVMs agree with the old ones for mid_det_visibility = 1."""
        # import new povms from txt file
        ops_dict = read_from_txt(path.dirname(path.abspath(__file__)) + "/../netsquid_ae/full_multiphoton_povms.txt")
        # import old povms from qdetector
        kraus_ops, kraus_ops_num_res, outcome_dict = set_old_operators()

        def generate_dict(total_photon_number, list):
            key = []
            dict = {}
            for n in range(total_photon_number + 1):
                for m in range(total_photon_number + 1):
                    if n + m <= total_photon_number:
                        key.append((n, m))
            for i in range(len(list)):
                dict[key[i]] = list[i]
            return dict
        kraus_dict = generate_dict(6, kraus_ops_num_res)
        array_dict = {}
        square_array_dict = {}
        # convert POVM operators to Kraus matrices with visibility = 1
        visibility = 1.
        for k in ops_dict.keys():
            if k in [(0, 2), (2, 0)]:
                square_array_dict[k] = (ops_dict[k].to_numpy_matrix(convert_scalars, visibility=visibility))
            array_dict[k] = sqrtm(ops_dict[k].to_numpy_matrix(convert_scalars, visibility=visibility))

        for key in array_dict.keys():
            # old POVMs seem to have reverse numbering
            (n, m) = key
            rev_key = (m, n)
            if key not in [(2, 0), (0, 2)]:
                self.assertTrue(np.testing.assert_array_almost_equal(array_dict[key],
                                                                     kraus_dict[rev_key].arr.real) is None)
            else:
                print(f"For {key} the Kraus operators do not agree! But the old 'Kraus' operator seems to be the POVM "
                      f"operator instead (therefore missing a sqrtm).")
                self.assertTrue(np.testing.assert_array_almost_equal(square_array_dict[key],
                                                                     kraus_dict[rev_key].arr.real) is None)
                print("POVM operators agree.")


if __name__ == "__main__":
    unittest.main()
