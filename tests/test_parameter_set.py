import unittest
import numpy as np
import netsquid as ns

from netsquid_ae.ae_chain_setup import create_repeater_chain, create_qkd_application
from netsquid_ae.ae_parameter_set import AEParameterSet, convert_parameter_set, _inverse_coherence_time_prob_fn
from netsquid_ae.parameter_set_collection import PerfectPhotonPairTest, Guha2015p000, Guha2015p035, Guha2015p041

from netsquid_simulationtools.parameter_set import rootbased_improvement_fn, ParameterSet


class TestAEParameterSet(unittest.TestCase):
    """Test the AEParameterSet class."""

    def setUp(self):
        """Sets up a new simulation environment for each test."""
        ns.sim_reset()

    def test_errors(self):
        """Test errors in parameter checks."""
        sim_params = {"length": 0,
                      "channel_length_l": 0,
                      "channel_length_r": 0,
                      "num_repeaters": 0}
        # test wrong input type
        with self.assertRaises(ValueError):
            convert_parameter_set(sim_params, [])
        # test wrong input distribution
        params = {'photon_num_distribution': 'test',
                  'mean_photon_number': 0.1}
        with self.assertRaises(ValueError):
            convert_parameter_set(sim_params, params)
        # test wrong mean photon - distribution combination
        params = {'photon_num_distribution': 'thermal',
                  'mean_photon_number': ParameterSet._NOT_SPECIFIED}
        with self.assertRaises(ValueError):
            convert_parameter_set(sim_params, params)
        # test different source-memory encodings
        params = {'photon_num_distribution': 'thermal',
                  'emission_probabilities': [0, 1, 0, 0],
                  'mean_photon_number': 0.1,
                  'source_encoding': 'time_bin',
                  'memory_encoding': 'presence_absence'}
        with self.assertRaises(ValueError):
            convert_parameter_set(sim_params, params)
        # test mode_limit
        params = {'photon_num_distribution': 'thermal',
                  'emission_probabilities': [0, 1, 0, 0],
                  'mean_photon_number': 0.1,
                  'source_encoding': 'time_bin',
                  'memory_encoding': 'time_bin',
                  "source_spectral_modes": 1,
                  "memory_spectral_modes": 1,
                  "source_temporal_modes": 1,
                  "memory_temporal_modes": 1,
                  "source_spatial_modes": 10,
                  "memory_spatial_modes": 100,
                  'mid_det_mode_limit': 1,
                  }
        with self.assertRaises(KeyError):
            convert_parameter_set(sim_params, params)

    def test_coherence_time(self):
        """Test conversion for coherence time."""
        assert _inverse_coherence_time_prob_fn(1) == np.inf
        assert _inverse_coherence_time_prob_fn(0) == 0
        assert _inverse_coherence_time_prob_fn(.5) == - 1. / np.log(.5)

    def test_initialization(self):
        """Test that class can be properly initialized."""

        NA = ParameterSet._NOT_SPECIFIED

        class TestParameterSet(AEParameterSet):
            source_encoding = "time_bin"
            source_frequency = 20e8
            source_spectral_modes = 1000
            source_spatial_modes = 10
            source_temporal_modes = 100
            photon_num_distribution = "thermal"
            mean_photon_number = 0.05
            emission_probabilities = [.1, .8, .05, .05]
            Rate = NA                                   # Not needed as modes are specified
            g2 = NA                                     # Not needed as modes are specified

            max_memory_efficiency = .8
            memory_T1 = 1e-6
            memory_T2 = 1e-8
            memory_time_dependence = "exponential"
            memory_spectral_modes = 1000
            memory_spatial_modes = 10
            memory_temporal_modes = 10
            memory_additional_noise = "multiphoton_emission"
            memory_encoding = "time_bin"

            coupling_loss_fibre = 0.01

            # midpoint detectors
            det_visibility = .9
            det_time_window = 1e3
            det_dead_time = 1e2
            det_after_pulse = 0.1
            det_dark_count_rate = 1e-5
            det_efficiency = .9
            det_num_resolving = False
            det_mode_limit = False
            det_lin_opt = True

            swap_det_visibility = .9
            swap_det_time_window = 1e3
            swap_det_dead_time = 1e2
            swap_det_after_pulse = 0.1
            swap_det_dark_count_rate = 1e-5
            swap_det_efficiency = .9
            swap_det_num_resolving = True
            swap_det_mode_limit = True
            swap_det_lin_opt = True

            el_link_fidelity = 0.8
            el_link_succ_prob = 1.
            swap_quality = 0.01
            swap_probability = 0.49

        test_param_set = TestParameterSet()

        # test arbitrary parameters
        self.assertEqual(test_param_set.source_encoding, "time_bin")
        self.assertEqual(test_param_set.emission_probabilities, [.1, .8, .05, .05])

        # test improving
        test_param_set.to_improved_dict(param_dict=test_param_set.to_dict(),
                                        param_improvement_dict={"memory_T1": 0.2},
                                        improvement_fn=rootbased_improvement_fn)

    def test_conversion(self):
        """Test if ParameterSet is correctly converted and can be used for simulation setup."""

        NA = ParameterSet._NOT_SPECIFIED

        class TestParamSet(AEParameterSet):
            # SOURCE
            source_encoding = "time_bin"
            source_frequency = 10e3
            source_spectral_modes = 3
            source_spatial_modes = 1
            source_temporal_modes = 2
            photon_num_distribution = "thermal"
            mean_photon_number = 0.05
            emission_probabilities = NA
            Rate = NA                   # Not needed as modes are specified
            g2 = NA                     # Not needed as modes are specified

            # MEMORY
            max_memory_efficiency = 0.5
            # the claim is they dont have this
            memory_T1 = np.inf
            memory_T2 = 1e5
            memory_time_dependence = "gaussian"
            memory_spectral_modes = 20
            memory_spatial_modes = 1
            memory_temporal_modes = 1
            memory_additional_noise = NA
            memory_encoding = "time_bin"

            # FIBER (telecom)
            coupling_loss_fibre = 0.01

            # MIDPOINT DETECTORS
            det_visibility = 1.
            det_time_window = 10.
            det_dead_time = 100.
            det_after_pulse = NA
            det_dark_count_rate = 10.
            det_efficiency = 0.9
            det_num_resolving = True
            det_mode_limit = 10
            det_lin_opt = True

            # SWAP DETECTORS
            swap_det_visibility = .9
            swap_det_time_window = 10.
            swap_det_dead_time = 0.
            swap_det_after_pulse = NA
            swap_det_dark_count_rate = 100.
            swap_det_efficiency = 0.8
            swap_det_num_resolving = False
            swap_det_mode_limit = False
            swap_det_lin_opt = True

            # ABSTRACT PARAMS
            el_link_fidelity = NA
            el_link_succ_prob = NA
            swap_quality = NA
            swap_probability = NA

        # Set up other sim_params
        sim_params = {
            "num_repeaters": 1,             # number of repeaters in the chain (number of el. links = num_repeaters + 1)
            "multi_photon": True,           # bool: whether to use multi-photon up to n=3
            "num_attempts": -1,             # number of clicks after which the clock stops (thus stopping the protocols)
            "num_attempts_proto": -1,       # number of clicks after which the emission protocol stops
            "num_successes": 1000,             # number of successes after which the DC should stop the simulation
            "mem_num_modes": 100,            # Number of modes in memory
            "source_num_modes": 100,         # Number of modes in source
            "Rate": None,                   # Not neede due to Source_num_modes
            "g2": None,                     # Not neede due to Source_num_modes

            # fibre
            "attenuation_l": 0.2,           # channel attenuation left of the detectors [dB/km]
            "attenuation_r": 0.2,           # channel attenuation right of the detectors [dB/km]
            "length": 20,                   # total distance between end node/ total channel length [km]
            "channel_length_l": 5.,       # channel length left of the detectors [km]
            "channel_length_r": 5.,       # channel length right of the detectors [km]
            "fibre_phase_stdv_l": .0,
            "fibre_phase_stdv_r": .0,
            # clock
            "time_step": 1e6,               # time step for the clock in ns
            # magic
            "magic": None,                  # whether we want to use magic (should be "analytical", "sampled" or None)
            "state_dataframe_holder": None,  # file name for sampled magic
            "multiple_link_successes": False,
            "state_files_directory": None,
        }

        # test is chain can be set up without errors
        param_set = TestParamSet()
        sim_params = convert_parameter_set(sim_params, param_set.to_dict())

        protocols, network, _ = create_repeater_chain(**sim_params)
        create_qkd_application(network, sim_params, measure_directly=True, initial_measurement_basis="X")

        self.assertTrue(sim_params["num_multiplexing_modes"] <= param_set.to_dict()["det_mode_limit"])

        # test if NA is processed correctly
        param_set_2 = param_set.to_perfect_dict()
        self.assertEqual(param_set_2["swap_det_after_pulse"], NA)

    @staticmethod
    def check_test_parameter_sets():
        """Test if TestParameterSets are valid AEParameterSets"""
        _ = PerfectPhotonPairTest()
        _ = Guha2015p035()
        _ = Guha2015p041()
        _ = Guha2015p000()


if __name__ == "__main__":
    unittest.main()
