import os
import pathlib
import random
import logging
import unittest
import numpy as np
import pickle
from copy import deepcopy

import netsquid as ns
from netsquid.util import DataCollector
import netsquid.qubits.qubitapi as qapi
from netsquid.qubits.ketstates import b00, b01, b10, b11, BellIndex

from netsquid_ae.ae_classes import QKDNode
from netsquid_ae.datacollectors import ChainStateCollector
from netsquid_ae.protocol_event_types import EVTYPE_SUCCESS
from netsquid_ae.ae_magic_distributor import AEMagicDistributor
from netsquid_ae.forced_magic_state_generator import set_magic_file_name
from netsquid_ae.ae_protocols import EmissionProtocol, ExtractionProtocol
from netsquid_ae.ae_chain_setup import create_elementary_link, _check_sim_params, create_single_repeater, \
    create_qkd_application, create_repeater_chain, _check_and_process_length_params, _get_mpn_indices

from netsquid_simulationtools.repchain_data_functions import _expected_target_state
from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder


class TestChainSetup(unittest.TestCase):
    """Unittest for ae chain setup functions."""

    def setUp(self):
        """Sets up a new simulation environment for each test."""
        ns.sim_reset()
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)

        # Tunable simulation parameters
        self.sim_params = {
            # SIMULATION
            "multi_photon": False,          # bool: whether to use multi-photon up to n=3 (WARNING: very slow)
            "num_repeaters": 0,             # number of repeaters
            "encoding": "time_bin",         # encoding of the entangled photons
            # end of simulation             Note: only use one and set others to -1
            "num_attempts": 10,             # number of clicks after which the clock stops (thus stopping the protocols)
            "num_attempts_proto": -1,       # number of clicks after which the emission protocols stop
            "num_successes": -1,            # number of successes after which to stop the simulation
            # magic
            "magic": None,                  # whether we want to use magic (should be "analytical", "sampled" or None)
            "state_files_directory": str(pathlib.Path(__file__).parent.absolute()) + "/magic_state_data",
            # clock                         Note: only used if magic = None, otherwise cycle time is used as time_step
            "time_step": 1e6,               # time step for the clock [ns]
            "multiple_link_successes": False,  # whether elementary links can have multiple successful modes

            # COMPONENTS
            # channel
            "length": 0,                    # total distance between end node/ total channel length [km]
            "channel_length_l": -1,         # channel length left of the detectors [km]
            "channel_length_r": -1,         # channel length right of the detectors [km]
            "coupling_loss_fibre": 0.,      # initial loss on channel to midpoint detectors
            "attenuation_l": 0.25,          # channel attenuation left of the detectors [dB/km]
            "attenuation_r": 0.25,          # channel attenuation right of the detectors [dB/km]
            "fibre_phase_stdv_l": .0,
            "fibre_phase_stdv_r": .0,
            # source
            "source_frequency": 5e4,                 # frequency of the photon pair source [Hz]
            "source_num_modes": None,               # Number of modes for source. If None: Calculated by g2 and Rate.
            "g2": 0.12,                         # Measured heralded g^2
            "Rate": 4.2*3.5e3,                    # Number of photon pairs pr. second.
            "mean_photon_number": None,               # mean photon pair number (only used if multi_photon=True)
            "emission_probabilities": [0., 1., 0., 0.],  # emission probs for photon pair sources
            # midpoint detector
            "det_dark_count_prob": 0,   # probability of dark count per detection
            "det_efficiency": 1.,       # detector efficiency
            "det_visibility": 1.,       # photon indistinguishability (must be 1 for multi_photon=True)
            "det_num_resolving": True,  # using number or non_number resolving detectors
            # swap & end node detector
            "swap_det_dark_count_prob": 0,   # probability of dark count per detection
            "swap_det_efficiency": 1.,       # detector efficiency
            "swap_det_visibility": 1.,       # photon indistinguishability (must be 1 for multi_photon=True)
            "swap_det_num_resolving": True,  # using number or non_number resolving detectors
            # memory
            "memory_coherence_time": 1e6,               # coherence time of the quantum memory [ns]
            "mem_num_modes": 100,
            "max_memory_efficiency": 1.,                # maximum efficiency of the quantum memory
            "memory_time_dependence": "exponential",    # time-dependence of efficiency
        }

    @staticmethod
    def create_mock_repchain_dataframe(baseline_parameters=None, varied_parameters=None,
                                       number_of_results=10):
        baseline_params = _check_sim_params(**baseline_parameters)
        baseline_parameters = deepcopy(baseline_params)
        del baseline_parameters["mean_photon_number"]
        baseline_parameters["mean_photon_number_l"] = baseline_params["mean_photon_number"]
        baseline_parameters["mean_photon_number_r"] = baseline_params["mean_photon_number"]
        del baseline_parameters["emission_probabilities"]
        baseline_parameters["emission_probabilities_l"] = baseline_params["emission_probabilities"]
        baseline_parameters["emission_probabilities_r"] = baseline_params["emission_probabilities"]
        if varied_parameters is None:
            varied_parameters = ["length", "channel_length_l", "channel_length_r"]

        number_of_nodes = baseline_parameters["num_repeaters"] + 2

        # set modes to 1 for sample dataframe
        baseline_parameters["num_multiplexing_modes"] = 1

        bell_states = [np.outer(b00, b00), np.outer(b01, b01),
                       np.outer(b10, b10), np.outer(b11, b11)]

        # set generation_duration_unit to rounds
        baseline_parameters["generation_duration_unit"] = "rounds"

        data = {"state": random.choices(population=bell_states, k=number_of_results),
                "basis_A": random.choices(population=["X", "Y", "Z"], k=number_of_results),
                "basis_B": random.choices(population=["X", "Y", "Z"], k=number_of_results),
                "outcome_A": random.choices(population=[0, 1], k=number_of_results),
                "outcome_B": random.choices(population=[0, 1], k=number_of_results),
                "generation_duration": random.choices(population=range(1, 100), k=number_of_results),
                }

        for i in range(number_of_nodes - 2):
            data.update({"midpoint_outcome_{}".format(i): random.choices(population=[0, 1, 2, 3], k=number_of_results),
                         "swap_outcome_{}".format(i): random.choices(population=[0, 1, 2, 3], k=number_of_results)})
        data.update({"midpoint_outcome_{}".format(number_of_nodes - 2): random.choices(population=[0, 1, 2, 3],
                                                                                       k=number_of_results)})

        for param in varied_parameters:
            param_values = []
            for i in range(number_of_results):
                if param == "length":
                    param_values.append(baseline_parameters["length"])
                elif param == "channel_length_l":
                    param_values.append(baseline_parameters["channel_length_l"])
                else:
                    param_values.append(baseline_parameters["channel_length_r"])
            data.update({param: param_values})
        for param in varied_parameters:
            del baseline_parameters[param]

        baseline_parameters_dict = baseline_parameters

        # make all lists (required by safe netconf loader) into tuples (required by RDFHolder)
        for key in baseline_parameters_dict.keys():
            if isinstance(baseline_parameters_dict[key], list):
                baseline_parameters_dict[key] = tuple(baseline_parameters_dict[key])

        test_holder = RepchainDataFrameHolder(number_of_nodes=number_of_nodes,
                                              baseline_parameters=baseline_parameters_dict,
                                              data=data)
        baseline_parameters_dict.update({'channel_length_l': data['channel_length_l'][0],
                                         'channel_length_r': data['channel_length_r'][0]})
        filename = set_magic_file_name(**baseline_parameters_dict)

        if not os.path.exists(baseline_parameters_dict['state_files_directory']):
            os.mkdir(baseline_parameters_dict['state_files_directory'])

        with open(filename, 'wb') as handle:
            pickle.dump(test_holder, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def test_setup(self):
        """Tests elementary link setup."""

        self.sim_params["length"] = 100
        self.sim_params = _check_sim_params(**self.sim_params)
        self.assertTrue(self.sim_params["channel_length_l"] == 50)
        self.assertTrue(self.sim_params["channel_length_r"] == 50)

        protocols, network, magic = create_elementary_link(**self.sim_params)

        self.assertTrue(len(protocols) == 2)
        self.assertTrue(len(network.nodes) == 2)
        self.assertTrue(len(magic) == 0)       # magic = None , no MagicDistributor should be created

    def test_states_elementary_link(self):
        """Tests states generated by elementary link."""

        # create Link
        protocols, network, _ = create_elementary_link(**self.sim_params)

        # Start Protocols
        for proto in protocols:
            proto.start()
        # Start Clocks
        for node in network.nodes.values():
            node.subcomponents["Clock"].start()
        # Set up data collection
        link_state_collector = DataCollector(ChainStateCollector(nodes_1=list(network.nodes.values())))
        link_state_collector.collect_on([(protocols[0], EVTYPE_SUCCESS), (protocols[1], EVTYPE_SUCCESS)], "AND")

        # run simulation
        ns.sim_run()

        df = link_state_collector.dataframe
        # check length of dataframe
        self.assertTrue(len(df) == self.sim_params["num_attempts"])
        # check that all tries succeeded in 1 round
        for rnd in df["number_of_rounds"]:
            self.assertTrue(rnd == 1.)
        # check all heralding signals were successful
        for out in df["midpoint_outcome_0"]:
            self.assertTrue(out == BellIndex.PSI_PLUS or out == BellIndex.PSI_MINUS)
        # check that correct states were collected
        for index, row in df.iterrows():
            q1, q2 = qapi.create_qubits(num_qubits=2)
            qapi.assign_qstate([q1, q2], row["state"])
            if row["midpoint_outcome_0"] == BellIndex.B01:
                self.assertTrue(np.isclose(qapi.fidelity([q1, q2], b01), 1))
            else:
                self.assertTrue(np.isclose(qapi.fidelity([q1, q2], b11), 1))

    def test_states_single_repeater(self):
        """Tests states generated by single repeater."""

        # create repeater
        self.sim_params["num_repeaters"] = 1
        self.sim_params["num_attempts"] = 20

        protocols, network, _ = create_single_repeater(**self.sim_params)

        # Start Protocols
        for proto in protocols:
            proto.start()
        # Start Clocks
        non_qkd_nodes = [node for node in network.nodes.values() if not isinstance(node, QKDNode)]
        for node in non_qkd_nodes:
            # Note: QKD nodes do not have clocks
            node.subcomponents["Clock"].start()
        # Set up data collection, (excluding qkd nodes here)
        rep_state_collector = DataCollector(ChainStateCollector(nodes_1=non_qkd_nodes))
        ex_protos = [protocol for protocol in protocols if protocol.protocol_type == "Extraction"]
        rep_state_collector.collect_on([(ex_protos[0], ExtractionProtocol.evtype_extract),
                                        (ex_protos[1], ExtractionProtocol.evtype_extract)], "AND")

        # run simulation
        ns.sim_run()

        df = rep_state_collector.dataframe
        # check length of dataframe
        self.assertTrue(len(df) <= self.sim_params["num_attempts"])
        # check all heralding signals were successful
        for out in df["midpoint_outcome_0"]:
            self.assertTrue(out == BellIndex.PSI_PLUS or out == BellIndex.PSI_MINUS)
        for out in df["swap_outcome_0"]:
            self.assertTrue(out == BellIndex.PSI_PLUS or out == BellIndex.PSI_MINUS)
        for out in df["midpoint_outcome_1"]:
            self.assertTrue(out == BellIndex.PSI_PLUS or out == BellIndex.PSI_MINUS)
        # check that correct states were collected
        for index, row in df.iterrows():
            q1, q2 = qapi.create_qubits(num_qubits=2)
            qapi.assign_qstate([q1, q2], row["state"])
            if _expected_target_state(row) == BellIndex.B01:
                self.assertTrue(np.isclose(qapi.fidelity([q1, q2], b01), 1))
            else:
                self.assertTrue(np.isclose(qapi.fidelity([q1, q2], b11), 1))

    def test_states_repeater_chain(self):
        """Tests states generated by single repeater."""

        # create repeater
        self.sim_params["num_repeaters"] = 2
        self.sim_params["num_attempts"] = 20

        protocols, network, _ = create_repeater_chain(**self.sim_params)

        # Start Protocols
        for proto in protocols:
            proto.start()
        # Start Clocks
        non_qkd_nodes = [node for node in network.nodes.values() if not isinstance(node, QKDNode)]
        for node in non_qkd_nodes:
            # Note: QKD nodes do not have clocks
            node.subcomponents["Clock"].start()
        # Set up data collection, (excluding qkd nodes here)
        rep_state_collector = DataCollector(ChainStateCollector(nodes_1=non_qkd_nodes))
        ex_protos = [protocol for protocol in protocols if protocol.protocol_type == "Extraction"]
        rep_state_collector.collect_on([(ex_protos[0], ExtractionProtocol.evtype_extract),
                                        (ex_protos[1], ExtractionProtocol.evtype_extract)], "AND")

        # run simulation
        ns.sim_run()

        df = rep_state_collector.dataframe
        # check length of dataframe
        self.assertTrue(len(df) <= self.sim_params["num_attempts"])
        # check all heralding signals were successful
        for out in df["midpoint_outcome_0"]:
            self.assertTrue(out == BellIndex.PSI_PLUS or out == BellIndex.PSI_MINUS)
        for out in df["swap_outcome_0"]:
            self.assertTrue(out == BellIndex.PSI_PLUS or out == BellIndex.PSI_MINUS)
        for out in df["midpoint_outcome_1"]:
            self.assertTrue(out == BellIndex.PSI_PLUS or out == BellIndex.PSI_MINUS)
        # check that correct states were collected
        for index, row in df.iterrows():
            q1, q2 = qapi.create_qubits(num_qubits=2)
            qapi.assign_qstate([q1, q2], row["state"])
            if _expected_target_state(row) == BellIndex.B01:
                self.assertTrue(np.isclose(qapi.fidelity([q1, q2], b01), 1))
            else:
                self.assertTrue(np.isclose(qapi.fidelity([q1, q2], b11), 1))

    def test_magic_setup(self):
        """Test if everything can be set up with magic and without."""

        for self.sim_params["magic"] in [None, "analytical", "sampled"]:
            # test if chain can be set up
            self.sim_params["num_repeaters"] = 0
            self.sim_params["length"] = 1
            if self.sim_params["magic"] == "sampled":
                self.create_mock_repchain_dataframe(baseline_parameters=self.sim_params,
                                                    varied_parameters=None, number_of_results=10)
            self.sim_params["num_repeaters"] = 5
            self.sim_params["length"] = 6
            protocols, network, _ = create_repeater_chain(**self.sim_params)
            nodes = list(network.nodes.values())
            create_qkd_application(network=network, sim_params=self.sim_params)

            self.assertEqual(len(nodes), self.sim_params["num_repeaters"] + 2)
            self.assertEqual(len(network.nodes) - len(nodes), 2)

            # test if single repeater can be set up
            self.sim_params["num_repeaters"] = 0
            self.sim_params["length"] = 1
            if self.sim_params["magic"] == "sampled":
                self.create_mock_repchain_dataframe(baseline_parameters=self.sim_params,
                                                    varied_parameters=None, number_of_results=10)
            self.sim_params["num_repeaters"] = 1
            self.sim_params["length"] = 2
            _, network, magic = create_single_repeater(**self.sim_params)
            # single repeater already includes the qkd application therefore 2 nodes "longer"
            self.assertEqual(len(network.nodes), self.sim_params["num_repeaters"] + 4)
            if self.sim_params["magic"] is None:
                self.assertEqual(len(magic), 0)
            else:
                self.assertEqual(len(magic), 2)
                self.assertTrue(isinstance(magic[0], AEMagicDistributor))
                self.assertEqual(len(magic[1]), 4)

        for name in os.listdir(self.sim_params['state_files_directory']):
            os.remove(self.sim_params['state_files_directory'] + '/' + name)
        os.rmdir(self.sim_params['state_files_directory'])

    def test_check_and_convert_length_params(self):
        # none are defined
        self.sim_params["length"] = - 1
        self.sim_params["channel_length_r"] = -1
        self.sim_params["channel_length_l"] = -1

        with self.assertRaises(ValueError):
            _check_and_process_length_params(**self.sim_params)

        # incorrectly defined
        self.sim_params["num_repeaters"] = 1
        self.sim_params["length"] = 20
        self.sim_params["channel_length_l"] = 10
        self.sim_params["channel_length_r"] = 10

        with self.assertRaises(ValueError):
            _check_and_process_length_params(**self.sim_params)

        # channel lengths are defined based on length
        self.sim_params["channel_length_l"] = -1
        self.sim_params["channel_length_r"] = -1

        defined_channels = _check_and_process_length_params(**self.sim_params)
        assert defined_channels["channel_length_l"] == 5
        assert defined_channels["channel_length_r"] == 5

        # length is defined based on asymmetric channel lengths
        self.sim_params["length"] = -1
        self.sim_params["channel_length_l"] = 3
        self.sim_params["channel_length_r"] = 7

        defined_length = _check_and_process_length_params(**self.sim_params)
        assert defined_length["length"] == 20

    def test_setup_different_values_of_mpn(self):
        """Check that when mean photon numbers are defined individually, they are correctly assigned to the sources."""
        # check elementary link setup
        self.sim_params["multi_photon"] = True
        self.sim_params["mean_photon_number"] = [0.1, 0.2]

        _, elementary_link_network, _ = create_elementary_link(**self.sim_params)
        assert elementary_link_network.subcomponents["Alice"].subcomponents["PPS"].mu == 0.1
        assert elementary_link_network.subcomponents["Bob"].subcomponents["PPS"].mu == 0.2

        # check single repeater setup
        self.sim_params["multi_photon"] = True
        self.sim_params["mean_photon_number"] = [0.1, 0.2, 0.3, 0.4]
        self.sim_params["num_repeaters"] = 1

        _, single_rep_network, _ = create_single_repeater(**self.sim_params)
        assert single_rep_network.subcomponents["A_1"].subcomponents["PPS"].mu == 0.1
        assert single_rep_network.subcomponents["R_1_1"].subcomponents["PPS_L"].mu == 0.2
        assert single_rep_network.subcomponents["R_1_1"].subcomponents["PPS_R"].mu == 0.3
        assert single_rep_network.subcomponents["B_1"].subcomponents["PPS"].mu == 0.4

        # check repeater chain setup (with 2 repeaters)
        self.sim_params["multi_photon"] = True
        self.sim_params["mean_photon_number"] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
        self.sim_params["num_repeaters"] = 2

        _, rep_chain_network, _ = create_repeater_chain(**self.sim_params)
        assert rep_chain_network.subcomponents["A_1"].subcomponents["PPS"].mu == 0.1
        assert rep_chain_network.subcomponents["R_1_1"].subcomponents["PPS_L"].mu == 0.2
        assert rep_chain_network.subcomponents["R_1_1"].subcomponents["PPS_R"].mu == 0.3
        assert rep_chain_network.subcomponents["R_1_2"].subcomponents["PPS_L"].mu == 0.4
        assert rep_chain_network.subcomponents["R_1_2"].subcomponents["PPS_R"].mu == 0.5
        assert rep_chain_network.subcomponents["B_1"].subcomponents["PPS"].mu == 0.6

    def test_get_mpn_indices(self):
        """Check that correct mean photon indices are calculated."""
        el_link_asymmetric = {
            "multi_photon": True,
            "num_repeaters": 0,
            "mean_photon_number": [0.1, 0.2]
        }
        assert _get_mpn_indices(el_link_asymmetric) == [0, 1]

        el_link_symmetric = {
            "multi_photon": True,
            "num_repeaters": 0,
            "mean_photon_number": 0.1
        }
        assert _get_mpn_indices(el_link_symmetric) == [None, None]

        rep_chain_asymmetric = {
            "multi_photon": True,
            "num_repeaters": 2,
            "mean_photon_number": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
        }
        assert _get_mpn_indices(rep_chain_asymmetric) == [0, 1, 3, 5]

    def test_emission_protocol_delay_setup(self):
        """Check that delays for emission protocols in elementary links with midpoint asymmetry are set up correctly."""
        # check symmetric elementary link has no delay
        self.sim_params["length"] = 100
        self.sim_params["channel_length_l"] = 50
        self.sim_params["channel_length_r"] = 50

        repchain_protocols, _, _ = create_repeater_chain(**self.sim_params)
        ellink_protocols, _, _ = create_elementary_link(**self.sim_params)

        for proto in repchain_protocols + ellink_protocols:
            if isinstance(proto, EmissionProtocol):
                assert proto.delay == 0

        # check asymmetric elementary link without magic where Alice is closer to midpoint
        self.sim_params["length"] = 100
        self.sim_params["channel_length_l"] = 25
        self.sim_params["channel_length_r"] = 75

        delay_alice = 50 * 1e9 / 2e5  # difference in length * factor for ns / speed of photons in fibre

        repchain_protocols, _, _ = create_repeater_chain(**self.sim_params)
        ellink_protocols, _, _ = create_elementary_link(**self.sim_params)

        for proto in repchain_protocols + ellink_protocols:
            if isinstance(proto, EmissionProtocol):
                if "A" in proto.node.name:
                    assert proto.delay == delay_alice  # Alice should have delay
                else:
                    assert proto.delay == 0  # Bob should not have any delay

        # check asymmetric elementary link without magic where Bob is closer to midpoint
        self.sim_params["length"] = 100
        self.sim_params["channel_length_l"] = 60
        self.sim_params["channel_length_r"] = 40

        delay_bob = 20 * 1e9 / 2e5  # difference in length * factor for ns / speed of photons in fibre

        repchain_protocols, _, _ = create_repeater_chain(**self.sim_params)
        ellink_protocols, _, _ = create_elementary_link(**self.sim_params)

        for proto in repchain_protocols + ellink_protocols:
            if isinstance(proto, EmissionProtocol):
                if "A" in proto.node.name:
                    assert proto.delay == 0  # Alice should not have any delay
                else:
                    assert proto.delay == delay_bob  # Bob should have delay

    def test_repeater_chains_do_not_support_asymmetry_without_magic(self):
        """Check that repeater chains that should be set up without magic do not support midpoint asymmetry."""
        self.sim_params["length"] = 200
        self.sim_params["channel_length_l"] = 25
        self.sim_params["channel_length_r"] = 75
        self.sim_params["num_repeaters"] = 1

        with self.assertRaises(NotImplementedError):
            _, _, _ = create_repeater_chain(**self.sim_params)


if __name__ == "__main__":
    ns.logger.setLevel(logging.NOTSET)
    unittest.main()
