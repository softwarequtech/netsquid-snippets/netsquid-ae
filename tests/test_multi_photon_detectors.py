"""Unit tests for two MultiPhotonDetectors."""
import pytest
import unittest
import itertools
import numpy as np

from netsquid.util.simlog import logger
import netsquid.qubits.qubitapi as qapi
from netsquid.qubits import operators as ops
from netsquid.qubits.ketstates import BellIndex
from netsquid.util.simtools import sim_run, sim_reset, get_random_state
from netsquid.qubits.qformalism import set_qstate_formalism, QFormalism

from netsquid_ae.multi_photon_detectors import MultiPhotonBSMDetector, MultiPhotonQKDDetector

from netsquid_physlayer.detectors import QKDOutcome, BSMOutcome


class TestMultiPhotonBSMDetector(unittest.TestCase):

    def setUp(self):
        """Sets up a new simulation environment for each test."""
        sim_reset()

    def _get_output_a(self, message):
        """Callback handler for storing the classical message returned by the detector."""
        self.outputs_a.append((message.items, message.meta["successful_modes"]))

    def _get_output_b(self, message):
        """Callback handler for storing the classical message returned by the detector."""
        self.outputs_b.append((message.items, message.meta["successful_modes"]))

    def setup_detector(self, name, num_resolving=False, p_dark=0., det_eff=1., visibility=1.,
                       encoding="presence_absence", force_povm_outcome=None):
        """Setup a MultiPhotonBSMDetector, bind its output ports and reset the message storage."""
        bsm_det = MultiPhotonBSMDetector(name, num_resolving=num_resolving, p_dark=p_dark, det_eff=det_eff,
                                         visibility=visibility, encoding=encoding,
                                         force_povm_outcome=force_povm_outcome)
        bsm_det.ports["cout0"].bind_output_handler(self._get_output_a)
        bsm_det.ports["cout1"].bind_output_handler(self._get_output_b)
        self.outputs_a, self.outputs_b = [], []
        return bsm_det

    def test_meas_operators(self):
        """Setup a detector with random valid values for the parameter that affect the measurement operators
        with a beam splitter included, and check whether the Kraus operators add up to the identity matrix."""
        rng = get_random_state()
        p_dark, det_eff, visibility = rng.rand(3, )
        bsm_det_non_num_res = self.setup_detector("non_num_res", p_dark=p_dark, det_eff=det_eff, visibility=visibility,
                                                  num_resolving=False)
        bsm_det_num_res = self.setup_detector("num_res", p_dark=p_dark, det_eff=det_eff, visibility=visibility,
                                              num_resolving=True)
        for bsm_det in [bsm_det_non_num_res, bsm_det_num_res]:
            # First verify that the measurement operators with a beamsplitter add up to identity
            bsm_det._set_meas_operators_with_beamsplitter()
            # Note that the measurement operators are Kraus operators, so that we should take the matrix product
            # of the Hermitian with itself
            matrix_prod_meas_ops = [op.arr.conj().T @ op.arr for op in bsm_det._meas_operators]
            self.assertTrue(np.allclose(sum(matrix_prod_meas_ops), np.identity(16, dtype=np.complex_)))

    def test_regular_operation(self):
        """Test whether the expected input leads to the expected output."""
        for encoding in ['presence_absence', 'time_bin']:
            bsm_det = self.setup_detector("bsm_det", encoding=encoding)
            if encoding == 'presence_absence':
                qubits = qapi.create_qubits(4)
                # Create state |01>
                ampl = [0] * (2 ** 4)
                ampl[1] = 1
            else:
                qubits = qapi.create_qubits(8)
                # Create an equal superposition of early and late
                ampl = [0] * (2 ** 8)
                ampl[65] = 1  # 1 lE, 1 rL
            qapi.assign_qstate(qubits, np.array(ampl).T)
            bsm_det.ports["qin0"].tx_input(qubits[:int(len(qubits) / 2)])
            bsm_det.ports["qin1"].tx_input(qubits[int(len(qubits) / 2):])
            sim_run()
            self.assertEqual(self.outputs_a, self.outputs_b)
            self.assertTrue(self.outputs_a[0][0][0].success)
            self.assertEqual(self.outputs_a[0][1], [0])
            # check that inputting vacuum results in no success
            if encoding == 'presence_absence':
                qubits = qapi.create_qubits(4)
                # Create state |00>
                ampl = [0] * (2 ** 4)
                ampl[0] = 1
            else:
                qubits = qapi.create_qubits(8)
                # Create 00
                ampl = [0] * (2 ** 8)
                ampl[0] = 1
            qapi.assign_qstate(qubits, np.array(ampl).T)
            bsm_det.ports["qin0"].tx_input(qubits[:int(len(qubits) / 2)])
            bsm_det.ports["qin1"].tx_input(qubits[int(len(qubits) / 2):])
            sim_run()
            assert self.outputs_a == self.outputs_b
            assert len(self.outputs_a) == 2
            assert isinstance(self.outputs_a[1][0][0], BSMOutcome)
            assert self.outputs_a[1][0][0].success is False
            assert self.outputs_a[1][0][0].bell_index == -1
            assert self.outputs_a[1][1][0] is None
            sim_reset()

    def test_multiplexed_detection(self):
        """Test multiplexed detection in case of multi-qubit states."""
        qdet = self.setup_detector("perfect_det")
        # Create qubits in 100 spectral modes in pairs of four (since we are using presence-absence encoding)
        spectral_modes = 100
        qubits = []
        for _ in range(spectral_modes):
            # Note that all qubits will be in the state |0000>
            qubits.extend(qapi.create_qubits(4))
        rng = get_random_state()
        successful_mode = rng.randint(0, spectral_modes)
        # Set successful mode to |0100>, leave the rest as |0000>
        ampl = [0] * (2 ** 4)
        ampl[1] = 1
        qapi.assign_qstate(qubits[4 * successful_mode:4 * (successful_mode + 1)], np.array(ampl).T)
        # Split the qubits to be sent to the left and right input of the detector
        qubits_left, qubits_right = [], []
        for m in range(spectral_modes):
            qubits_left.extend(qubits[4 * m: 4 * m + 2])
            qubits_right.extend(qubits[4 * m + 2: 4 * (m + 1)])
        qdet.ports["qin0"].tx_input(qubits_left)
        qdet.ports["qin1"].tx_input(qubits_right)
        sim_run()
        # We expect there to be a success in the single successful mode
        self.assertEqual(self.outputs_a, self.outputs_b)
        assert isinstance(self.outputs_a[0][0][0], BSMOutcome)
        self.assertTrue(self.outputs_a[0][0][0].success)
        self.assertEqual(self.outputs_a[0][1], [successful_mode])

    def test_num_resolving_detection(self):
        """Test the difference between threshold and photon-number-resolving detectors."""
        bsm_det_non_num_res = self.setup_detector("non_num_res", num_resolving=False)
        bsm_det_num_res = self.setup_detector("num_res", num_resolving=True)
        # Set the probability amplitudes for the state |0101>, i.e. one photon on both arms
        ampl = [0] * (2 ** 4)
        ampl[5] = 1
        ampl = np.array(ampl).T
        qubits = qapi.create_qubits(4)
        qapi.assign_qstate(qubits, ampl)
        bsm_det_non_num_res.ports["qin0"].tx_input(qubits[:2])
        bsm_det_non_num_res.ports["qin1"].tx_input(qubits[2:])
        sim_run()
        qubits = qapi.create_qubits(4)
        qapi.assign_qstate(qubits, ampl)
        bsm_det_num_res.ports["qin0"].tx_input(qubits[:2])
        bsm_det_num_res.ports["qin1"].tx_input(qubits[2:])
        sim_run()
        # Due to photon bunching, the threshold detector records a success while the number resolving detector records
        # 2 photons in one of the arms and therefore outputs a failure
        self.assertEqual(self.outputs_a, self.outputs_b)
        assert isinstance(self.outputs_a[0][0][0], BSMOutcome)
        assert isinstance(self.outputs_a[1][0][0], BSMOutcome)
        self.assertTrue(self.outputs_a[0][0][0].success)
        self.assertFalse(self.outputs_a[1][0][0].success)
        # The successful mode of the number resolving detector should be [None]
        self.assertIsNone(self.outputs_a[1][1][0])

    @pytest.mark.slow
    def test_visibility(self):
        """Test the correct implementation of visibility by considering two extreme cases.
        Note: This test is marked as slow, since it is a probabilistic test needing a large number `total_number_runs`.
        """
        # Set up detectors with perfect photon (in-)distinguishability
        bsm_det_indist = MultiPhotonBSMDetector("indist_bsm_det", visibility=1.)
        bsm_det_dist = MultiPhotonBSMDetector("fully_dist_bsm_det", visibility=0.)
        bsm_det_indist.ports["cout0"].bind_output_handler(self._get_output_a)
        bsm_det_dist.ports["cout0"].bind_output_handler(self._get_output_b)
        # Reference to Fock states |1>, |2> and |3>
        state1 = np.array([0, 1, 0, 0])
        state2 = np.array([0, 0, 1, 0])
        state3 = np.array([0, 0, 0, 1])
        # Number that is used as `delta` in the assertions
        total_number_runs = 5000
        number_allowed_deviations = 0.05 * total_number_runs  # allow 5% deviation

        # Helper function for parsing a list of BSMOutcomes
        def parse_outcomes(outcomes):
            num_fails, num_psi_plus, num_psi_minus = 0, 0, 0
            for outcome in outcomes:
                if not outcome.success:
                    num_fails += 1
                else:
                    if outcome.bell_index == BellIndex.PSI_PLUS:
                        num_psi_plus += 1
                    else:
                        num_psi_minus += 1

            return num_fails, num_psi_plus, num_psi_minus

        for state in [state1, state2, state3]:
            self.outputs_a, self.outputs_b = [], []
            for _ in range(total_number_runs):
                # Create states to be put on both detectors
                [a1, a2, b1, b2, c1, c2, d1, d2] = qapi.create_qubits(8)
                for photon in [[a1, a2], [b1, b2], [c1, c2], [d1, d2]]:
                    qapi.assign_qstate(photon, state)
                # Transmit photons in correct grouping
                bsm_det_indist.ports["qin0"].tx_input([a1, a2])
                bsm_det_indist.ports["qin1"].tx_input([b1, b2])
                bsm_det_dist.ports["qin0"].tx_input([c1, c2])
                bsm_det_dist.ports["qin1"].tx_input([d1, d2])
                sim_run()
            indist_outcomes = [output[0][0] for output in self.outputs_a]
            dist_outcomes = [output[0][0] for output in self.outputs_b]
            if (state == state1).all():
                # Indistinguishable photons should have bunched, so that the state |11> = 1/sqrt(2) (|20> + |02>)
                # should be measured
                for outcome in indist_outcomes:
                    self.assertTrue(outcome.success)
                num_fails, num_psi_plus, num_psi_minus = parse_outcomes(dist_outcomes)
                # Distinguishable photons should not have bunched, 2 ** 2 possible outcomes of which only two are
                # successful
                self.assertAlmostEqual(num_fails, 2 * len(dist_outcomes) / 4, delta=number_allowed_deviations)
                self.assertAlmostEqual(num_psi_plus, len(dist_outcomes) / 4, delta=number_allowed_deviations)
                self.assertAlmostEqual(num_psi_minus, len(dist_outcomes) / 4, delta=number_allowed_deviations)
            elif (state == state2).all():
                # Measured state must be |22> = 1/4 (sqrt(6)*|40> - 2*|22> + sqrt(6)*|04>)
                num_fails, num_psi_plus, num_psi_minus = parse_outcomes(indist_outcomes)
                self.assertAlmostEqual(num_fails, len(indist_outcomes) / 4, delta=number_allowed_deviations)
                self.assertAlmostEqual(num_psi_plus, 3 * len(indist_outcomes) / 8, delta=number_allowed_deviations)
                self.assertAlmostEqual(num_psi_minus, 3 * len(indist_outcomes) / 8, delta=number_allowed_deviations)
                # Distinguishable photons should not have bunched, 2 ** 4 possible outcomes of which only two are
                # successful
                num_fails, num_psi_plus, num_psi_minus = parse_outcomes(dist_outcomes)
                self.assertAlmostEqual(num_fails, 14 * len(dist_outcomes) / 16, delta=number_allowed_deviations)
                self.assertAlmostEqual(num_psi_plus, len(dist_outcomes) / 16, delta=number_allowed_deviations)
                self.assertAlmostEqual(num_psi_minus, len(dist_outcomes) / 16, delta=number_allowed_deviations)
            else:
                # Measured state must be |33> = 1/4 (sqrt(5)*|60> - sqrt(3)*|42> + sqrt(3)*|24> - sqrt(5)*|06>)
                num_fails, num_psi_plus, num_psi_minus = parse_outcomes(indist_outcomes)
                self.assertAlmostEqual(num_fails, 6 * len(indist_outcomes) / 16, delta=number_allowed_deviations)
                self.assertAlmostEqual(num_psi_plus, 5 * len(indist_outcomes) / 16, delta=number_allowed_deviations)
                self.assertAlmostEqual(num_psi_minus, 5 * len(indist_outcomes) / 16, delta=number_allowed_deviations)
                # Distinguishable photons should not have bunched, 2 ** 6 possible outcomes of which only two are
                # successful
                num_fails, num_psi_plus, num_psi_minus = parse_outcomes(dist_outcomes)
                self.assertAlmostEqual(num_fails, 62 * len(dist_outcomes) / 64, delta=number_allowed_deviations)
                self.assertAlmostEqual(num_psi_plus, len(dist_outcomes) / 64, delta=number_allowed_deviations)
                self.assertAlmostEqual(num_psi_minus, len(dist_outcomes) / 64, delta=number_allowed_deviations)

    def test_biased_detection(self):
        """Test biased detection for forced magic."""
        # create detector with wrong label and check correct error
        with self.assertRaises(ValueError):
            self.setup_detector(name="error_det", force_povm_outcome="0")

        for encoding, num_resolving in itertools.product(["presence_absence", "time_bin"], [True, False]):
            sim_reset()
            # create detector
            forced_qdet = self.setup_detector(name="forced_detector", encoding=encoding, num_resolving=num_resolving)
            # force DM formalism
            set_qstate_formalism(QFormalism.DM)

            # determine labels to loop over
            if num_resolving:
                # number resolving: 6 ops ( no-clicks , 2x exactly-one, 2x more-than-1, at-least-one-in-each)
                labels = ["00", "01", "10", "11", "02", "20"]
            else:
                # non-number = 4 operators
                labels = ["00", "01", "10", "11"]
            if encoding == "time_bin":
                # compute all combinations of labels between the two bins
                dual_labels = []
                for l1 in labels:
                    for l2 in labels:
                        dual_labels.append(l1 + l2)
                labels = dual_labels

            # (iterate over both encodings once sparseDM is ready)
            # iterate over all labels
            for forced_label in labels:
                forced_qdet.force_povm_outcome = forced_label
                # create qubits
                if encoding == "presence_absence":
                    qubits = qapi.create_qubits(4)
                    # Create state |01>
                    ampl = [0] * (2 ** 4)
                    ampl[1] = 1
                else:
                    qubits = qapi.create_qubits(8)
                    # Create an equal superposition of early and late
                    ampl = [0] * (2 ** 8)
                    ampl[65] = 1  # 1 lE, 1 rL
                qapi.assign_qstate(qubits, np.array(ampl).T)

                # put qubits on detector
                forced_qdet.ports["qin0"].tx_input(qubits[:int(len(qubits) / 2)])
                forced_qdet.ports["qin1"].tx_input(qubits[int(len(qubits) / 2):])
                sim_run()
                # check outcome and probability
                self.assertEqual(self.outputs_a, self.outputs_b)

                self.outputs_a = []
                self.outputs_b = []
            # check if qdet storage has correct length
            self.assertEqual(len(forced_qdet._forced_probabilities), len(labels))
            probability_sum = 0
            for n in range(len(labels)):
                # check if correct labels are in qdet storage
                self.assertEqual(labels[n], forced_qdet._forced_probabilities[n][0])
                probability_sum += forced_qdet._forced_probabilities[n][1]
            # check if probabilities sum to 1
            self.assertAlmostEqual(probability_sum, 1.)


class TestMultiPhotonQKDDetector(unittest.TestCase):

    def setUp(self):
        """Sets up a new simulation environment for each test."""
        sim_reset()

    def _get_measurement_output(self, message):
        """Callback handler for storing the classical message returned by the detector."""
        self.outputs.append(message.items)

    def setup_detector(self, name, num_resolving=False, p_dark=0., det_eff=1., visibility=1.,
                       measurement_basis="Z", encoding="presence_absence"):
        """Setup a MultiPhotonQKDDetector, bind its output ports and reset the message storage."""
        qkd_det = MultiPhotonQKDDetector(name, num_resolving=num_resolving, p_dark=p_dark, det_eff=det_eff,
                                         visibility=visibility, measurement_basis=measurement_basis,
                                         encoding=encoding)
        qkd_det.ports["cout0"].bind_output_handler(self._get_measurement_output)
        self.outputs = []
        return qkd_det

    def test_meas_operators(self):
        """Setup a detector with random valid values for the parameter that affect the measurement operators
        without a beam splitter included, and check whether the Kraus operators add up to the identity matrix.
        Note that the the operators for the setup with a beam splitter included is verified in the other test class."""
        rng = get_random_state()
        p_dark, det_eff, visibility = rng.rand(3, )
        qkd_det_non_num_res = self.setup_detector("non_num_res", p_dark=p_dark, det_eff=det_eff, visibility=visibility,
                                                  num_resolving=False)
        qkd_det_num_res = self.setup_detector("num_res", p_dark=p_dark, det_eff=det_eff, visibility=visibility,
                                              num_resolving=True)
        for qkd_det in [qkd_det_non_num_res, qkd_det_num_res]:
            # First verify that the measurement operators with a beamsplitter add up to identity
            qkd_det._set_meas_operators_without_beamsplitter()
            # Note that the measurement operators are Kraus operators, so that we should take the matrix product
            # of the Hermitian with itself
            matrix_prod_meas_ops = [op.arr.conj().T @ op.arr for op in qkd_det._meas_operators]
            self.assertTrue(np.allclose(sum(matrix_prod_meas_ops), np.identity(16, dtype=np.complex_)))

    def test_measurements(self):
        """Test the regular measurement of presence-absence and time-bin encoded multi-qubit states."""
        for encoding in ["presence_absence", "time_bin"]:
            qkd_det = self.setup_detector(name="qkd_det_presence_absence", num_resolving=True, encoding=encoding)
            for measurement_basis in ["X", "Y", "Z"]:
                qkd_det.measurement_basis = measurement_basis
                # First transmit qubits in the |01> state (|0001> in binary)
                qubits1 = []
                qubits2 = []
                for _ in range(10):
                    # test multiplexed operation
                    qubits = qapi.create_qubits(4)
                    qapi.operate(qubits[-1], ops.X)
                    if encoding == "presence_absence":
                        qubits1.extend(qubits[:2])
                        qubits2.extend(qubits[2:])
                    else:
                        qubits1.extend(qubits)
                if encoding == "presence_absence":
                    qkd_det.ports["qin0"].tx_input(qubits1)
                    qkd_det.ports["qin1"].tx_input(qubits2)
                else:
                    qkd_det.ports["qin0"].tx_input(qubits1)
                sim_run()
                assert len(self.outputs[0]) == 10
                for output in self.outputs[0]:
                    assert isinstance(output, QKDOutcome)
                    self.assertTrue(output.success)
                    self.assertIn(output.outcome, [0, 1])
                    self.assertEqual(output.measurement_basis, measurement_basis)
                # Now transmit qubit in the |10> state (|0100> in binary)
                qubits = qapi.create_qubits(4)
                qapi.operate(qubits[1], ops.X)
                if encoding == "presence_absence":
                    qkd_det.ports["qin0"].tx_input(qubits[:2])
                    qkd_det.ports["qin1"].tx_input(qubits[2:])
                else:
                    qkd_det.ports["qin0"].tx_input(qubits)
                sim_run()
                assert isinstance(self.outputs[1][0], QKDOutcome)
                self.assertTrue(self.outputs[1][0].success)
                self.assertIn(self.outputs[1][0].outcome, [0, 1])
                # Next, transmit two qubits in the |11> state (|0101> in binary)
                qubits = qapi.create_qubits(4)
                qapi.operate(qubits[1], ops.X)
                qapi.operate(qubits[-1], ops.X)
                if encoding == "presence_absence":
                    qkd_det.ports["qin0"].tx_input(qubits[:2])
                    qkd_det.ports["qin1"].tx_input(qubits[2:])
                else:
                    qkd_det.ports["qin0"].tx_input(qubits)
                sim_run()
                assert isinstance(self.outputs[2][0], QKDOutcome)
                self.assertFalse(self.outputs[2][0].success)
                # Finally, transmit two qubits in the |00> state (|0000> in binary)
                qubits = qapi.create_qubits(4)
                if encoding == "presence_absence":
                    qkd_det.ports["qin0"].tx_input(qubits[:2])
                    qkd_det.ports["qin1"].tx_input(qubits[2:])
                else:
                    qkd_det.ports["qin0"].tx_input(qubits)
                sim_run()
                assert isinstance(self.outputs[3][0], QKDOutcome)
                self.assertFalse(self.outputs[3][0].success)
                self.outputs = []

    def test_time_bin_correlations(self):
        """Test if entangled multi-qubit states cause the correct correlations in measurements."""
        qkd_det_left = self.setup_detector(name="qkd_det_left", num_resolving=True, encoding="time_bin")
        qkd_det_right = self.setup_detector(name="qkd_det_right", num_resolving=True, encoding="time_bin")
        for measurement_basis in ["X", "Y", "Z"]:
            qkd_det_left.measurement_basis = measurement_basis
            qkd_det_right.measurement_basis = measurement_basis
            for phase in [-1, 1]:
                qubits = qapi.create_qubits(8)
                # Create the state (|01,00;01,00> +- |00,01;01,00>)/sqrt(2)
                ket = np.zeros([1, 2 ** 8])[0]
                ket[20] = 1 / np.sqrt(2)
                ket[65] = phase / np.sqrt(2)
                qapi.assign_qstate(qubits, ket)
                qkd_det_left.ports["qin0"].tx_input(qubits[:4])
                qkd_det_right.ports["qin0"].tx_input(qubits[4:])
                sim_run()
                assert len(self.outputs) == 2
                self.assertEqual(self.outputs[0][0].measurement_basis, measurement_basis)
                self.assertEqual(self.outputs[1][0].measurement_basis, measurement_basis)
                self.assertEqual(self.outputs[0][0].success, self.outputs[1][0].success)
                assert self.outputs[0][0].success is True
                assert isinstance(self.outputs[0][0], QKDOutcome)
                # In case of p_dark = 0 and efficiency = 1
                # When the incoming state is (|01> + |10>), then both in X and Y the measurements should be equal
                # When the incoming state is (|01> - |10>), then both in X and Y the measurements should be unequal
                # For the Z basis, the measurements should always be unequal independent of the phase
                if measurement_basis == "Z":
                    self.assertNotEqual(self.outputs[0][0].outcome, self.outputs[1][0].outcome)
                else:
                    self.assertNotEqual(self.outputs[0][0].outcome, self.outputs[1][0].outcome) if phase == -1 else \
                        self.assertEqual(self.outputs[0][0].outcome, self.outputs[1][0].outcome)
                self.outputs = []

    def test_presence_absence_correlations(self):
        """Test if entangled multi-qubit states cause the correct correlations in measurements."""
        # For presence absence the correlations should be as follows
        # Z: always anti-correlated
        # X,Y: iff same bell state in both chains: correlations else: anti-correlations
        qkd_det_left = self.setup_detector(name="qkd_det_left", num_resolving=True, encoding="presence_absence")
        qkd_det_right = self.setup_detector(name="qkd_det_right", num_resolving=True, encoding="presence_absence")
        # create (01 + 10)/sqrt(2)
        ket1 = np.zeros([1, 2 ** 4])[0]
        ket1[1] = 1 / np.sqrt(2)
        ket1[4] = 1 / np.sqrt(2)
        # create (01 - 10)/sqrt(2)
        ket2 = np.zeros([1, 2 ** 4])[0]
        ket2[1] = 1 / np.sqrt(2)
        ket2[4] = - 1 / np.sqrt(2)
        self.outputs = []
        for measurement_basis in ["X", "Y", "Z"]:
            qkd_det_left.measurement_basis = measurement_basis
            qkd_det_right.measurement_basis = measurement_basis
            # same bell state on both links
            qubits = qapi.create_qubits(8)
            qubits1 = qubits[4:]
            qubits2 = qubits[:4]
            qapi.assign_qstate(qubits1, ket1)
            qapi.assign_qstate(qubits2, ket1)
            qkd_det_left.ports["qin0"].tx_input(qubits1[2:])
            qkd_det_right.ports["qin0"].tx_input(qubits1[:2])
            qkd_det_left.ports["qin1"].tx_input(qubits2[2:])
            qkd_det_right.ports["qin1"].tx_input(qubits2[:2])
            sim_run()
            # same input state: should be anti-correlated for Z else correlated
            assert len(self.outputs) == 2
            self.assertEqual(self.outputs[0][0].measurement_basis, measurement_basis)
            self.assertEqual(self.outputs[1][0].measurement_basis, measurement_basis)
            self.assertEqual(self.outputs[0][0].success, self.outputs[1][0].success)
            if self.outputs[0][0].success and measurement_basis == "Z":
                assert self.outputs[0][0].outcome != self.outputs[1][0].outcome
            else:
                assert self.outputs[0][0].outcome == self.outputs[1][0].outcome
            for q in qubits:
                qapi.discard(q)
            self.outputs = []
            # different bell state on each link
            qubits = qapi.create_qubits(8)
            qubits1 = qubits[4:]
            qubits2 = qubits[:4]
            qapi.assign_qstate(qubits1, ket1)
            qapi.assign_qstate(qubits2, ket2)
            qkd_det_left.ports["qin0"].tx_input(qubits1[2:])
            qkd_det_right.ports["qin0"].tx_input(qubits1[:2])
            qkd_det_left.ports["qin1"].tx_input(qubits2[2:])
            qkd_det_right.ports["qin1"].tx_input(qubits2[:2])
            sim_run()
            # different bell states: should be anti-correlated in all bases
            assert len(self.outputs) == 2
            self.assertEqual(self.outputs[0][0].measurement_basis, measurement_basis)
            self.assertEqual(self.outputs[1][0].measurement_basis, measurement_basis)
            self.assertEqual(self.outputs[0][0].success, self.outputs[1][0].success)
            if self.outputs[0][0].success:
                assert self.outputs[0][0].outcome != self.outputs[1][0].outcome
            self.outputs = []

    def test_none_input(self):
        """Test that the detector still operates correctly when one/or both of the inputs are None."""
        # check if all None input is handled correctly
        for encoding in ['presence_absence', 'time_bin']:
            for meas_basis in ['X', 'Y', 'Z']:
                qkd_det = self.setup_detector("qkd_det", encoding=encoding, measurement_basis=meas_basis)
                none_inputs = 10 * [None]
                # transmit lists of None
                qkd_det.ports["qin0"].tx_input(none_inputs)
                if encoding == 'presence_absence':
                    qkd_det.ports["qin1"].tx_input(none_inputs)
                sim_run()
                # check if output is list of fails of correct length
                assert len(self.outputs[0]) == 10
                for outcome in self.outputs[0]:
                    assert isinstance(outcome, QKDOutcome)
        # check if half-None input is handled correctly
        for meas_basis in ['X', 'Y', 'Z']:
            qkd_det = self.setup_detector("qkd_det", encoding="presence_absence", measurement_basis=meas_basis)
            # create (01 + 10)/sqrt(2)
            ket1 = np.zeros([1, 2 ** 4])[0]
            ket1[1] = 1 / np.sqrt(2)
            ket1[4] = 1 / np.sqrt(2)
            qubits1, qubits2 = [], []
            for n in range(10):
                # test multiplexed operation
                qubits = qapi.create_qubits(4)
                qapi.assign_qstate(qubits, ket1)
                # make alternating lists
                if n % 2 == 0:
                    qubits1.extend(qubits[:2])
                    qubits2.append(None)
                else:
                    qubits1.append(None)
                    qubits2.extend(qubits[:2])

            qkd_det.ports["qin0"].tx_input(none_inputs)
            qkd_det.ports["qin1"].tx_input(none_inputs)

            sim_run()
            # check if output is list of fails of correct length
            assert len(self.outputs[0]) == 10
            for outcome in self.outputs[0]:
                assert isinstance(outcome, QKDOutcome)


if __name__ == "__main__":
    logger.setLevel("WARNING")
    unittest.main()
