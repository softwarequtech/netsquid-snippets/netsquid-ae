import os
import pytest
import unittest
import numpy as np

import netsquid as ns
import netsquid.qubits.qubitapi as qapi

from netsquid_ae.parameter_set_collection import Guha2015p000
from netsquid_ae.ae_parameter_set import convert_parameter_set
from netsquid_ae.forced_magic_state_generator import generate_forced_magic


class TestForcedQState(unittest.TestCase):

    def setUp(self):
        """Sets up a new simulation environment for each test."""
        ns.sim_reset()

    @staticmethod
    def analytical_state_guha(detection_efficiency, dark_count_prob):
        """Calculate analytical ent to end quantum state according to Appendix A.1 of Guha et al., 2015."""
        P_e = dark_count_prob
        # note that fiber loss is absorbed into detection efficiency
        eta = detection_efficiency
        A_e = 1 - (1 - P_e) * (1 - eta)
        B_e = 1 - (1 - P_e) * (1 - eta)**2

        # calculate coefficients
        a1 = (1 / 8) * (P_e**2 * (1 - A_e)**2 + A_e**2 * (1 - P_e)**2)
        b1 = (1 / 8) * (2 * A_e * P_e * (1 - A_e) * (1 - P_e))
        c1 = (1 / 8) * P_e * (1 - P_e) * (P_e * (1 - B_e) + B_e * (1 - P_e))
        # d1 = 0

        s1 = (1 / 8) * ((A_e + P_e - 2 * A_e * P_e)**2 + P_e * (1 - P_e) * (B_e + P_e - 2 * B_e * P_e))

        # target state: |00,01;00,01>
        state_psi0 = np.array([[0. + 0.j]] * 256)
        state_psi0[68] = 1. + 0.j
        # target state: |10,00;10,00>
        state_psi3 = np.array([[0. + 0.j]] * 256)
        state_psi3[17] = 1. + 0.j
        # target state: (|01,00;00,01> + |00,01;01,00>)/sqrt(2)
        state_m_plus = np.array([[0. + 0.j]] * 256)
        state_m_plus[65] = 1 / np.sqrt(2.) + 0.j
        state_m_plus[20] = 1 / np.sqrt(2.) + 0.j
        # target state: (|01,00;00,01> - |00,01;01,00>)/sqrt(2)
        state_m_minus = np.array([[0. + 0.j]] * 256)
        state_m_minus[65] = 1 / np.sqrt(2.) + 0.j
        state_m_minus[20] = - 1 / np.sqrt(2.) + 0.j

        analytical_qstate = (1 / s1) * (a1 * state_m_plus +
                                        b1 * state_m_minus +
                                        c1 * (state_psi0 + state_psi3))

        return analytical_qstate

    def check_forced_qstate(self, det_eff_list, det_dc_list):
        """Validate elementary link density matrix against analytical result from 'Rate-loss analysis of an efficient
        quantum repeater architecture' , Guha et al. 2015.
        """

        # set up simulation parameters
        sim_params = {
            "num_repeaters": 0,             # number of repeaters in the chain (number of el. links = num_rep + 1 )
            # only implemented for multi_photon = True!!
            "multi_photon": True,           # bool: whether to use multi-photon up to n=3
            "num_attempts": -1,             # number of clicks after which the clock stops (thus stopping the protocols)
            "num_attempts_proto": -1,       # number of clicks after which the emission protocol stops
            "num_successes": 1,             # number of successes after which the DC should stop the simulation
            # channel
            "length": 0,                    # total distance between end node/ total channel length [km]
            "channel_length_l": -1,         # not using asymmetric channel lengths
            "channel_length_r": -1,         # not using asymmetric channel lengths
            "attenuation_l": 0.2,
            "attenuation_r": 0.2,
            "fibre_phase_stdv_l": 0.0,
            "fibre_phase_stdv_r": 0.0,
            # clock
            "time_step": 2e6,               # time step for the clock in ns
            # magic
            "magic": "forced",              # whether we want to use magic (should be "analytical", "sampled" or None)
            "state_files_directory": "test_magic_states",
        }
        # Specify ParameterSet:
        parameter_set = Guha2015p000()
        sim_params = convert_parameter_set(sim_params, parameter_set.to_dict())

        for sim_params["det_efficiency"] in det_eff_list:
            for sim_params["det_dark_count_prob"] in det_dc_list:

                # TODO: investigate why fidelity drops for larger dark count rates
                #  (we neglect the possibility of multiple DCs)

                # run simulation
                states_df = generate_forced_magic(**sim_params).dataframe

                # extract successful end-to-end state
                state_1 = states_df.loc[states_df["forced_label"] == "0110"].state.values[0]
                state_2 = states_df.loc[states_df["forced_label"] == "1001"].state.values[0]
                state_3 = states_df.loc[states_df["forced_label"] == "0101"].state.values[0]
                state_4 = states_df.loc[states_df["forced_label"] == "1010"].state.values[0]
                el_link_state = (state_1 + state_2 + state_3 + state_4) / 2.

                # create qubits with state
                qubits = qapi.create_qubits(num_qubits=8)
                qapi.assign_qstate(qubits, ns.qubits.qformalism.SparseDMRepr(el_link_state))

                # calculate analytical state
                analytical_reference_state =\
                    self.analytical_state_guha(detection_efficiency=sim_params["det_efficiency"],
                                               dark_count_prob=sim_params["det_dark_count_prob"])

                # check fidelity

                f = qapi.fidelity(qubits, analytical_reference_state, squared=True)

                print(f"Fidelity to analytical reference state is {f} at det_eff={sim_params['det_efficiency']},"
                      f" p_dark={sim_params['det_dark_count_prob']}")
                self.assertAlmostEqual(f, 1., places=2)

                ns.sim_reset()

                for name in os.listdir(sim_params['state_files_directory']):
                    os.remove(sim_params['state_files_directory'] + '/' + name)
                os.rmdir(sim_params['state_files_directory'])

    def test_forced_qstate_quick(self):
        self.check_forced_qstate(det_eff_list=[.9], det_dc_list=[1e-5])

    @pytest.mark.slow
    def test_forced_qstate_full(self):
        self.check_forced_qstate(det_eff_list=[.1, .9], det_dc_list=[1e-5, 1e-6])


if __name__ == "__main__":
    unittest.main()
