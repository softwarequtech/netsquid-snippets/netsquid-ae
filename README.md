NetSquid-AE (6.4.0)
================================

Description
-----------

This is a user contributed _snippet_ for the [NetSquid quantum network simulator](https://netsquid.org).

Snippet for various Atomic Ensemble (AE) components used in a quantum network.

Installation
------------

See the [INSTALL file](INSTALL.md) for instruction of how to install this snippet.

Documentation
-------------

How to build the docs:

First build the docs by:

```bash
make build-docs
```

This will first install any required dependencies and build the html files.

To open the built docs (using 'open' or 'xdg-open'), do:

```bash
make open-docs
```

To both build the html files and open them, do:
```bash
make docs
```


Usage
-----

See the examples for demonstration of usage.

Contact
-------

For questions, contact the maintainer of this snippet: David Maier (<d.j.maier[at]tudelft.nl>).

Contributors
------------

David Maier (d.j.maier[at]tudelft.nl), \
Julian Rabbie, \
Hana Jirovska, \
Robert Knegjens, \
Loek Nijsten

License
-------

The NetSquid-AE Snippet has the following license:

> Copyright 2018 QuTech (TUDelft and TNO)
> 
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
> 
>     http://www.apache.org/licenses/LICENSE-2.0
> 
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.
