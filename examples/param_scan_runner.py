import multiprocessing as mp
from itertools import product


class _ParamScanRunner:
    """Helper class for performing parameter scan on multiprocessors. """
    def __init__(self, scan_params, sim_params, run_simulation):
        self.scan_params = scan_params
        self.sim_params = sim_params
        self.run_simulation = run_simulation

    def run_sim_params(self, param_values):
        params = self.sim_params.copy()
        for scan_param, param_value in zip(self.scan_params, list(param_values)):
            params[scan_param] = param_value
        df, state = self.run_simulation(sim_params=params)
        for scan_param, param_value in zip(self.scan_params, list(param_values)):
            df[scan_param] = param_value
        return df, state


def do_parameter_scan(sim_params, scan_params, scan_ranges, run_simulation):
    """Do a parameter scan over simulation parameters.

    Uses multiprocessing.

    Parameters
    ----------
    sim_params : dict
        Simulation parameters.
    scan_params : list of str
        List of simulation parameter names to scan over.
    scan_ranges : list of iterable
        List of simulation parameter ranges to scan over.

    Returns
    -------
    df : :obj:`~pandas.DataFrame`
        Dataframe containing simulation results.
    state : :obj:`~pandas.DataFrame`
        Dataframe containing end-to-end states.

    """
    runner = _ParamScanRunner(scan_params, sim_params, run_simulation)
    # Create grid of inputs in a list
    pool_inputs = [x for x in product(*scan_ranges)]
    # Use multiprocessing
    with mp.Pool() as pool:
        dataframes = pool.map(runner.run_sim_params, pool_inputs)
    df, state = dataframes[0]
    for df2, state2 in dataframes[1:]:
        df = df.append(df2, ignore_index=True)
        state = state.append(state2, ignore_index=True)
    return df, state
