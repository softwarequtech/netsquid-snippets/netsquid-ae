"""Simulation script for an AE single repeater simulation using a full simulation or elementary link magic."""
import os
import time
import logging
import pickle
from matplotlib import rcParams
import matplotlib.pyplot as plt
from argparse import ArgumentParser

import netsquid as ns
from netsquid.util import DataCollector
from netsquid.util.simlog import logger

from pydynaa import EventExpression, ExpressionHandler

from netsquid_ae.ae_protocols import ExtractionProtocol
from netsquid_ae.protocol_event_types import EVTYPE_SUCCESS
from netsquid_ae.ae_chain_setup import create_single_repeater
from netsquid_ae.datacollectors import DecisionMaker, ChainStateCollector, MeasurementCollector

from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder
from netsquid_simulationtools.repchain_data_process import process_repchain_dataframe_holder, process_data_bb84

from analytical_models import analytical_skr
from param_scan_runner import do_parameter_scan


def run_simulation(sim_params):
    """Set up and run a simulation for fixed set of parameters.

    Parameters
    ----------
    sim_params : dict
        Simulation parameters.

    Returns
    -------
    df_meas: instance of :class:`~pandas.DataFrame`
        DataFrame containing simulation/measurement results collected with the `measurement_collector`.
    df_state: instance of :class:`~pandas.DataFrame`
        DataFrame containing end-to-end state and relevant data collected with the `chain_collector`.

    """
    ns.sim_reset()

    # set up protocols and nodes (non-zero extraction_delay to be able to collect post-swap end-to-end state)
    protocols, network, _ = create_single_repeater(extraction_delay=sim_params["time_step"]/10, **sim_params)

    # Start Protocols
    for proto in protocols:
        proto.start()
    # For Magic clocks don't matter, the Wizard triggers the protocols
    if not sim_params["magic"]:
        # Start Clocks
        for node in list(network.nodes.values())[:-2]:
            # Note: Detection nodes do not have clocks
            node.subcomponents["Clock"].start()

    # Setup data collection
    # =====================
    # setting up EventExpressions for success at Alice and Bob (triggered by the MessagingProtocols at the detectors)
    evexpr_succ_a = EventExpression(event_type=EVTYPE_SUCCESS, source=protocols[-2])
    evexpr_succ_b = EventExpression(event_type=EVTYPE_SUCCESS, source=protocols[-1])
    evexpr_success = evexpr_succ_a and evexpr_succ_b

    # Using callable classes for data collection
    measurement_collector = DataCollector(MeasurementCollector(messaging_protocol_alice=protocols[-2],
                                                               messaging_protocol_bob=protocols[-1]))
    chain_state_collector = DataCollector(ChainStateCollector(nodes_1=list(network.nodes.values())[:-2]))
    # decision maker (changes measurement basis and stops simulation)
    decision_maker = DecisionMaker(min_successes=sim_params["num_successes"], datacollector=measurement_collector,
                                   messaging_protocol_alice=protocols[-2],
                                   messaging_protocol_bob=protocols[-1])
    # set up Handler for EventExpression
    decision_handler = ExpressionHandler(decision_maker.decide)
    decision_maker._wait(decision_handler, expression=evexpr_success)
    # wait for EventExpression with DataCollector
    measurement_collector.collect_on(evexpr_success)
    # select the two extraction protocols for chain_state_collector
    ex_protos = [protocol for protocol in protocols if isinstance(protocol, ExtractionProtocol)]
    chain_state_collector.collect_on([(ex_protos[0], ExtractionProtocol.evtype_extract),
                                     (ex_protos[1], ExtractionProtocol.evtype_extract)], "AND")

    # Run simulation
    print("starting simulation with sim_parms", sim_params)
    # ns.logger.setLevel(logging.DEBUG)
    ns.sim_run()

    # filter chain_state_collector for non-empty lines
    state_data = chain_state_collector.dataframe

    return measurement_collector.dataframe, state_data


def main():
    """Run example simulation."""
    # parse arguments
    parser = ArgumentParser()
    parser.add_argument('--filebasename', required=False, type=str, help="Name of the file to store results in.")
    parser.add_argument('-mpn', '--mean_photon_number', required=False, type=float,
                        help="Mean photon number of the SPDC Source.")
    parser.add_argument('-sm', '--spectral_modes', required=False, type=float,
                        help="Number of spectral modes of the SPDC Source.")
    parser.add_argument("--num_successes", required=False, type=int,
                        help='Number of entangled pairs to distribute between end nodes.')
    parser.add_argument("--length", required=False, type=float,
                        help="Total distance between end nodes.")
    args = parser.parse_args()

    # Define default simulation parameters
    # Note: fixed-sim_params should not be changed for this simulation but are possible with modified setup
    # Fixed simulation parameters
    fixed_sim_params = {"num_repeaters": 1,         # number of repeaters
                        "encoding": "time_bin",     # encoding of the entangled photons
                        "temporal_modes": 1}        # number of temporal modes
    # Tunable simulation parameters
    sim_params = {
        # SIMULATION
        "multi_photon": False,          # bool: whether to use multi-photon up to n=3 (WARNING: very slow)
        # end of simulation             Note: only use one and set others to -1
        "num_attempts": -1,             # number of clicks after which the clock stops (thus stopping the protocols)
        "num_attempts_proto": -1,       # number of clicks after which the emission protocols stop
        "num_successes": 10,            # number of successes after which the DecisionMaker should stop the simulation
        # magic
        "magic": None,                  # whether we want to use magic (should be "analytical" or None)
        "state_files_directory": None,
        # clock                         Note: only used if magic = None, otherwise cycle time is used as time_step
        "time_step": 1e6,               # time step for the clock in ns , determines time between entanglement attempts
        # protocol
        "multiple_link_successes": False,  # whether the protocol uses multiple successful modes per elementary link

        # COMPONENTS
        # channel                       Note: use either length or node_distance and set the other one to -1
        "length": 1.5,                  # total distance between end node/ total channel length [km]
        "channel_length_l": 0.375,      # length of the channel left of the detectors [km]
        "channel_length_r": 0.375,      # length of the channel right of the detectors [km]
        "coupling_loss_fibre": 0.,      # initial loss on channel to the midpoint detectors
        "attenuation_l": 0.2,           # channel attenuation left of the detectors [dB/km]
        "attenuation_r": 0.2,           # channel attenuation right of the detectors [dB/km]
        "fibre_phase_stdv_l": .0,       # stdev of random phase picked up on the fibre in presence_absence encoding
        "fibre_phase_stdv_r": .0,       # stdev of random phase picked up on the fibre in presence_absence encoding
        # source
        "source_frequency": 20e6,       # frequency of the photon pair source [Hz]
        "source_num_modes": None,       # number of multiplexing modes of the photon pair source
        "mean_photon_number": 0.05,     # mean photon pair number (only used if multi_photon=True)
        "emission_probabilities": [0, 1., 0, 0],   # emission probability for photon pair sources
        "g2": 0.12,                     # Measured heralded g^2
        "Rate": 4 * 3.5e3,              # Number of photons pr. second.

        # detectors
        # midpoint
        "det_dark_count_prob": 3e-5,    # probability of dark count per detection
        "det_efficiency": 0.9,          # detector efficiency
        "det_visibility": 1.,           # photon indistinguishability
        "det_num_resolving": False,     # using number or non_number resolving detectors
        # swap
        "swap_det_dark_count_prob": 3e-5,   # probability of dark count per detection
        "swap_det_efficiency": 0.9,         # detector efficiency
        "swap_det_visibility": 1.,          # photon indistinguishability
        "swap_det_num_resolving": False,    # using number or non_number resolving detectors
        # memory
        "memory_coherence_time": 1e6,       # coherence time of the quantum memory [ns]
        "mem_num_modes": 100,
        "max_memory_efficiency": 0.8,       # maximum efficiency of the quantum memory
        "memory_time_dependence": "exponential"     # time-dependence of efficiency
    }

    sim_params.update(fixed_sim_params)

    # overwrite sim_params with parsed arguments
    if args.spectral_modes is not None:
        sim_params["spectral_modes"] = args.spectral_modes
    if args.mean_photon_number is not None:
        sim_params["mu"] = args.mean_photon_number
    if args.num_successes is not None:
        sim_params["num_successes"] = args.num_successes
    if args.length is not None:
        sim_params["length"] = args.length

    run_single_simulation = True
    store_data = False
    plot_data = False
    compare_versus_analytical = False

    start_time = time.time()

    # set debugging or info
    logger.setLevel(logging.NOTSET)
    # logger.setLevel(logging.DEBUG)

    if run_single_simulation:
        # Run simulation for fixed parameters:
        df_meas, df_state = run_simulation(sim_params)
    else:
        # Run parameter scan:
        if sim_params["magic"] == "sampled":
            # Scan over all available sampled elementary link data
            states_container = pickle.load(open(sim_params["sample_file"], "rb"))
            el_node_dists = sorted(states_container.dataframe.node_distance.unique().tolist())
            scan_param_range = [i * (sim_params["num_repeaters"] + 1) for i in el_node_dists]
        else:
            # set range for parameter scan
            max_value = 150
            min_value = 3
            num_steps = 5
            step_size = int((max_value - min_value) / num_steps)
            scan_param_range = list(range(min_value, max_value + step_size, step_size))
        scan_param_name = "length"
        df_meas, df_state = do_parameter_scan(sim_params, [scan_param_name], [scan_param_range], run_simulation)

    runtime = time.time() - start_time

    print("finished AE Rep Chain simulation, runtime {} seconds for {} successes.\n"
          .format(runtime, sim_params["num_successes"]))

    if plot_data:
        print(df_meas)
        rdfh = RepchainDataFrameHolder(data=df_meas, number_of_nodes=3, baseline_parameters=sim_params)
        df_sk = process_repchain_dataframe_holder(rdfh, processing_functions=[process_data_bb84])
        print(df_sk)
        rcParams["font.size"] = 20
        _, ax = plt.subplots(1, 1, figsize=(10, 8))
        ax.set_yscale('log')
        ax.set_xlabel('End-to-end length [km]')
        ax.set_ylabel('Secret key rate [bits / attempts]')
        # ax = df_sk.plot(x="length", y="sk_rate", yerr="sk_error")
        if compare_versus_analytical:
            R_N, simulated_skr_per_attempt = [], []
            sk_rate, attempts_per_success = list(df_sk.sk_rate), list(df_sk.attempts_per_success)
            for idx, length in enumerate(scan_param_range):
                R_N.append(analytical_skr(length, sim_params)[0] / sim_params["source_frequency"])
                simulated_skr_per_attempt.append(sk_rate[idx] / (2 * attempts_per_success[idx]))
            ax.plot(scan_param_range, simulated_skr_per_attempt, 'o')
            ax.plot(scan_param_range, R_N)
            df_sk.plot(x="length", y="tgw_bound", ax=ax, logy=True)
            plt.legend(["Simulated", "Analytical", "TGW Bound"])
        plt.show()

    if store_data:
        # create directory for data
        if not os.path.exists("raw_data"):
            os.mkdir("raw_data")

        saved = False
        id = 1

        while not saved:
            timestr = time.strftime("%Y%m%d-%H%M%S")
            basename = "{}/mpn_{}_sm_{}_num_successes_{}_".format("raw_data", args.mean_photon_number,
                                                                  args.spectral_modes,
                                                                  sim_params["num_successes"])
            id += 1
            filename = basename + timestr
            if not os.path.isfile(filename):
                if run_single_simulation:
                    with open(filename + "_measurement.pickle", 'wb') as handle:
                        pickle.dump(df_meas, handle, protocol=pickle.HIGHEST_PROTOCOL)
                    with open(filename + "_states.pickle", 'wb') as handle:
                        pickle.dump(df_state, handle, protocol=pickle.HIGHEST_PROTOCOL)
                saved = True


if __name__ == "__main__":
    main()
