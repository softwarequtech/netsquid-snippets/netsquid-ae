import numpy as np


def analytical_skr(length, sim_params):
    """Small script for computing the analytical secret-key rate based on the model described by Guha et al. in
    https://arxiv.org/abs/1404.7183. It is based on a setup with a perfect single-photon-pair source and time-bin
    encoding for the photons, so it is important that these are also used in the simulated setup.

    Parameters
    ----------
    length : float
        Total end-to-end distance in km.
    sim_params : dict
        Dictionary of simulation parameters from which the relevant values are extracted.

    Returns
    -------
    R_N : float
        Analytical secret-key rate in bits per second based on a repeater chain with N elementary links.
    R_max : float
        Maximum secret key rate envelope (for details see paper).
    """

    def P_1(P_d, eta_d):
        A_d = eta_d + (1 - eta_d) * P_d
        q_1 = (1 - P_d) * A_d
        q_2 = (1 - A_d) * P_d
        q_3 = P_d * A_d
        res = (q_1 + q_2 + q_3) ** 2
        return q_1, q_2, q_3, res

    def coeffs_r(P_r, eta_r, lambda_m):
        A_r = eta_r * lambda_m + P_r * (1 - eta_r * lambda_m)
        B_r = 1 - (1 - P_r) * (1 - eta_r * lambda_m) ** 2
        a = (1 / 8) * (P_r ** 2 * (1 - A_r) ** 2 + A_r ** 2 * (1 - P_r) ** 2)
        b = (1 / 8) * (2 * A_r * P_r * (1 - A_r) * (1 - P_r))
        c = (1 / 8) * P_r * (1 - P_r) * (P_r * (1 - B_r) + B_r * (1 - P_r))
        return a, b, c

    def coeffs_e(P_e, eta_e, lambda_t):
        A_e = eta_e * lambda_t + P_e * (1 - eta_e * lambda_t)
        B_e = 1 - (1 - P_e) * (1 - eta_e * lambda_t) ** 2
        a_e = (1 / 8) * (P_e ** 2 * (1 - A_e) ** 2 + A_e ** 2 * (1 - P_e) ** 2)
        b_e = (1 / 8) * (2 * A_e * P_e * (1 - A_e) * (1 - P_e))
        c_e = (1 / 8) * P_e * (1 - P_e) * (P_e * (1 - B_e) + B_e * (1 - P_e))
        return a_e, b_e, c_e

    def Q_N(a, b, c, a_e, b_e, c_e, q_1, q_2, q_3, N):
        w_1 = c_e / (a_e + b_e)
        w_r = c / (a + b)
        t_e = (1 - 2 * w_1) / (1 + 2 * w_1)
        t_r = (1 - 2 * w_r) / (1 + 2 * w_r)
        t_d = ((q_1 - q_2) / (q_1 + q_2 + q_3)) ** 2
        out = 1 / 2 * (1 - (t_d / t_r) * (t_r * t_e) ** N)
        return out, t_r, t_d

    def P_succ(a, b, c, a_e, b_e, c_e, M, n):
        s = a + b + 2 * c
        s_1 = a_e + b_e + 2 * c_e
        out = (1 / (4 * s)) * (4 * s * (1 - (1 - 4 * s_1) ** M)) ** (2 ** n)
        return out

    def R_2(Q):
        h_2 = - Q * np.log2(Q) - (1 - Q) * np.log2(1 - Q)
        return 1 - 2 * h_2

    N = sim_params["num_repeaters"] + 1
    n = np.log2(N)
    T_q = 1 / sim_params["source_frequency"]
    lambda_t = np.exp(- (sim_params["attenuation"] * np.log(10) / 10 * length / (2 * N)))
    q_1, q_2, q_3, P_1_val = P_1(sim_params["mid_det_dark_count_prob"], sim_params["mid_det_efficiency"])
    a, b, c = coeffs_r(sim_params["mid_det_dark_count_prob"], sim_params["mid_det_efficiency"],
                       sim_params["max_memory_efficiency"])
    a_e, b_e, c_e = coeffs_e(sim_params["mid_det_dark_count_prob"], sim_params["mid_det_efficiency"], lambda_t)
    P_succ_val = P_succ(a, b, c, a_e, b_e, c_e, sim_params["spectral_modes"], n)
    Q_N_val, t_r, t_d = Q_N(a, b, c, a_e, b_e, c_e, q_1, q_2, q_3, N)
    R_N = P_1_val * P_succ_val * R_2(Q_N_val) / (2 * T_q)

    A = sim_params["mid_det_efficiency"] ** 2 / \
        (sim_params["mid_det_efficiency"] ** 2 * sim_params["max_memory_efficiency"] ** 2 * T_q)
    R_max = A * (sim_params["mid_det_efficiency"] ** 2 * sim_params["max_memory_efficiency"] ** 2 / 2) ** N

    return R_N, R_max


def analytical_second_order_auto_and_cross_correlation_functions(p_dark, p_emis, det_eff):
    """Analytical second-order cross-correlation and auto-correlation functions with imperfect detectors,
    as specified in Equations (21) and (29) of https://arxiv.org/abs/1109.0194.
    It is defined as the ratio of coincidence counts over the product of the getting a click in either detector, or
    the ratio of the coincidence counts over the square of getting a click in one of the two detectors respectively.
    Furthermore, it is based on a thermal state of the form (see Equation (1))
    :math:(1 - p)exp(sqrt(p) a^\\dagger b^\\dagger) |00><00| exp(sqrt(p) a b)
    as input to a Bell-state measurement station based on linear optics.

    Parameters
    ----------
    p_dark : float
        Dark count probability.
    p_emis : float
        Emission probability (represent the parameter p in the thermal state).
    det_eff : float
        Detection efficiency.

    Returns
    -------
    second_order_cross_correlation_function : float
        Value of the analytical second-order cross-correlation function.
    second_order_auto_correlation_function : float
        Value of the analytical second-order auto-correlation function.
    """
    a_1 = (1 - p_dark) * (1 - p_emis) / (1 - p_emis * (1 - det_eff))
    a_2 = (1 - p_dark) * (1 - p_emis) / (1 - p_emis * (1 - det_eff / 2))
    b_1 = (1 - p_dark) ** 2 * (1 - p_emis) / (1 - p_emis * (1 - det_eff) ** 2)
    b_2 = (1 - p_dark) ** 2 * (1 - p_emis) / (1 - p_emis * (1 - det_eff))
    second_order_cross_correlation_function = (1 - 2 * a_1 + b_1) / (1 - a_1) ** 2
    second_order_auto_correlation_function = (1 - 2 * a_2 + b_2) / (1 - a_2) ** 2
    return second_order_cross_correlation_function, second_order_auto_correlation_function
