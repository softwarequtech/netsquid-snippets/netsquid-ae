import pandas
import numpy as np
import multiprocessing
from matplotlib import rcParams
from matplotlib import pyplot as plt
from numpy.lib import scimath as scm
from time import time, strftime, localtime

from netsquid import sim_reset, sim_run
from netsquid.components.qsource import SourceStatus

from netsquid_ae.spdcsource import SPDCSource
from netsquid_ae.multi_photon_detectors import MultiPhotonQKDDetector

from netsquid_physlayer.detectors import QKDOutcome

from analytical_models import analytical_second_order_auto_and_cross_correlation_functions


class CCDetector(MultiPhotonQKDDetector):
    """Detector for cross correlation simulation.

    This is a single component containing two threshold detectors (a) and (b) that
    can count coincidences and singles in (a), (b).

    """

    def postprocess_outputs(self, dict_port_outcomes):
        """Overriding original function to detect coincidences as outcome = 2."""
        meas_outcome = dict_port_outcomes["cout0"][0]
        qkd_outcome = QKDOutcome(success=True, outcome=meas_outcome - 1, measurement_basis=self._measurement_basis)

        dict_port_outcomes["cout0"] = [qkd_outcome]


def simulate_second_order_cross_correlation_function(mean_photon_number, p_emis_val, num_triggers, p_dark, det_eff):
    """Simulate the second order cross-correlation function with components of this snippet.
    This setup replicates the setup of Figure 6 of https://arxiv.org/abs/1109.0194.

    Parameters
    ----------
    mean_photon_number : float or None
        Mean photon number of the SPDC Source, alternatively to model a different source a tuple of length 4 can be
        given containing the emission probabilities (p(0), p(1), p(2), p(3)).
    det_eff : float
        Detector efficiency.
    p_dark : float
        Dark count probability.
    p_emis_val : float, not used if using mean_photon_number
        Emission probability of the photon-pair source.
    num_triggers : int
        Number of times the source is triggered.

    Returns
    -------
    simulated_auto_correlation_function : float
        Second order autocorrelation of this simulation run.
    """
    # Setup detector once and bind one of the classical output ports to a function that adds the outcome to a list
    sim_reset()
    det = CCDetector(name="det", p_dark=p_dark, det_eff=det_eff, visibility=1.,
                     num_resolving=False, encoding="presence_absence")
    # Bind classical output port to message storage in outcomes list
    outcomes = []
    det.ports["cout0"].bind_output_handler(lambda message: outcomes.append(message.items[0]))
    # Setup photon-pair source
    if mean_photon_number is None:
        photon_pair_source = SPDCSource(name="photon_pair_source", encoding="presence_absence", multi_photon=True,
                                        emission_probability=[(1 - p_emis_val),
                                                              p_emis_val * (1 - p_emis_val),
                                                              p_emis_val ** 2 * (1 - p_emis_val),
                                                              p_emis_val ** 3],
                                        status=SourceStatus.EXTERNAL)
    elif isinstance(mean_photon_number, float):
        photon_pair_source = SPDCSource(name="photon_pair_source", encoding="presence_absence", multi_photon=True,
                                        emission_probability=None,
                                        mu=mean_photon_number,
                                        status=SourceStatus.EXTERNAL)
    elif isinstance(mean_photon_number, list):
        photon_pair_source = SPDCSource(name="photon_pair_source", encoding="presence_absence", multi_photon=True,
                                        emission_probability=mean_photon_number,
                                        mu=None,
                                        status=SourceStatus.EXTERNAL)

    # Connect the outputs of the photon pair source to the detector
    photon_pair_source.ports["qout0"].connect(det.ports["qin0"])
    photon_pair_source.ports["qout1"].connect(det.ports["qin1"])

    print(f"Starting simulation for mpn: {mean_photon_number}, p_emis: {p_emis_val}, p_dark: {p_dark},"
          f" det_eff: {det_eff}. Triggering {num_triggers} times.")
    for _ in range(num_triggers):
        # Trigger the source to emit the photons
        photon_pair_source.trigger()
        sim_run()
    # Based on the outcomes, check how many single clicks there were in one of the detectors and the
    # amount of double clicks
    clicks_a, clicks_b, double_clicks = 0, 0, 0
    for outcome in outcomes:
        if outcome.success:
            if outcome.outcome == 0:
                clicks_a += 1
            elif outcome.outcome == 1:
                clicks_b += 1
            elif outcome.outcome == 2:
                double_clicks += 1
            elif outcome.outcome == -1:
                pass
            else:
                # sanity check for detector
                raise ValueError(f"Detector should not have outcome {outcome.outcome}. Check detector.")

    # Now compute the cross correlation function by taking the average of the number of double clicks
    # divided by the average of the number of single clicks in A times B.
    simulated_cross_correlation_function = np.inf if ((clicks_a + double_clicks) * (clicks_b + double_clicks)) == 0\
        else num_triggers * double_clicks / ((clicks_a + double_clicks) * (clicks_b + double_clicks))
    # Reset components and disconnect detector ports to make sure no memory leakage
    det.reset()
    det.ports["qin0"].disconnect()
    det.ports["qin1"].disconnect()
    photon_pair_source.reset()

    return simulated_cross_correlation_function


def run_simulation(det_eff, p_dark, p_emis, num_triggers, num_simulations, mean_photon_number=None,
                   multiprocessed=True):
    """Simulate the second order cross-correlation function with components of this snippet.
    This setup replicates the setup of Figure 6 of https://arxiv.org/abs/1109.0194.

    Note: This function is mainly a wrapper to run the actual simulation multiple times.

    Parameters
    ----------
    mean_photon_number : list or None if using the state from eq. (20) of the paper
        List of mean photon number of the SPDC Source, alternatively to model a different source a tuple of length 4
        can be given containing the emission probabilities (p(0), p(1), p(2), p(3)).
    det_eff : float
        Detector efficiency.
    p_dark : float
        Dark count probability.
    p_emis : list or None
        List of emission probabilities of the photon-pair source that are meant to be simulated.
    num_triggers : int
        Number of times the source is triggered per simulation.
    num_simulations : int
        Number of independent simulation runs, which are used to determine the mean and standard deviation
        of the second order auto-correlation function.
    multiprocessed : bool
        Whether multiprocessing should be used to speed up the simulation or not.

    Returns
    -------
    mean_sim_cross_corr : list of float
        Mean of the simulated second order cross-correlation function,
        computed over `num_simulations` runs.
    std_sim_cross_corr : list of float
        Standard deviation of the simulated second order cross-correlation function,
        computed over `num_simulations` runs.
    """
    mean_sim_cross_corr, std_sim_cross_corr = [], []
    iterator = 1
    for param in [mean_photon_number, p_emis]:
        # Note: only one out of [mean_photon_number, p_emis] should be a list while the other one should be None
        if isinstance(param, list):
            iterator *= len(param)
    for i in range(iterator):
        # Use a particular emission probability
        if isinstance(p_emis, list):
            p_emis_val = p_emis[i]
        else:
            p_emis_val = p_emis
        if isinstance(mean_photon_number, list):
            mpn_val = mean_photon_number[i]
        else:
            mpn_val = mean_photon_number
        # list arguments for starmap
        arguments = num_simulations * [(mpn_val, p_emis_val, num_triggers, p_dark, det_eff)]

        if multiprocessed:
            # multiprocessing
            with multiprocessing.Pool() as pool:
                # use starmap to pass multiple arguments
                sim_auto_corr = pool.starmap(simulate_second_order_cross_correlation_function, tuple(arguments))
        else:
            # no multiprocessing
            print(f"Running example for p ={p_emis_val} without multi-processing. "
                  f"Set multiprocessed=True for increased speed.")
            sim_auto_corr = []
            for arg in arguments:
                (mean_photon_number, p_emis_val, num_triggers, p_dark, det_eff) = arg
                sim_auto_corr.append(simulate_second_order_cross_correlation_function(mean_photon_number, p_emis_val,
                                                                                      num_triggers, p_dark, det_eff))

        # Store the mean and standard deviation for this particular emission probability based on the simulation runs
        mean_sim_cross_corr.append(np.mean(sim_auto_corr))
        std_sim_cross_corr.append(np.std(sim_auto_corr))

    return mean_sim_cross_corr, std_sim_cross_corr


def plot_cross_correlation_function(det_eff, p_dark, p_emis, mean_sim_cross_corr, std_sim_cross_corr,
                                    num_triggers, num_simulations, save=False):
    """Plot the second-order cross correlation function.

    Parameters
    ----------
    det_eff : float
        Detector efficiency.
    p_dark : float
        Dark count probability.
    p_emis : list
        List of emission probabilities of the photon-pair source.
    mean_sim_cross_corr : list
        List of the mean values of the simulated second-order cross correlation.
    std_sim_cross_corr :
        List of the standard deviations of the simulated second-order cross correlation.
    num_triggers : int
        Number of times the source is triggered per simulation.
    num_simulations : int
        Number of independent simulation runs, which are used to determine the mean and standard deviation
        of the second order auto-correlation function.
    save : bool (optional, default=False)
        Whether or not to save the plot to disk.

    """
    # Set overall font size to 20
    rcParams["font.size"] = 20
    fig, ax = plt.subplots(1, 1, figsize=(15, 9))
    # Compute the analytical cross-correlation function in a smooth fashion over the domain
    ana_cross_corr = []
    p_emis_ana = np.logspace(-5, -0, 250)
    for p_emis_val in p_emis_ana:
        ana_cross_corr.append(analytical_second_order_auto_and_cross_correlation_functions(p_dark=p_dark,
                                                                                           p_emis=p_emis_val,
                                                                                           det_eff=det_eff)[0])
    # Plot the analytical cross-correlation function
    plt.plot(p_emis_ana, ana_cross_corr, color="xkcd:blue")
    plt.ylim(1., 1e5)
    ax.set_yscale('log')
    ax.set_xscale('log')
    # And plot the simulated one with shaded error bars.
    plt.errorbar(p_emis, mean_sim_cross_corr, yerr=std_sim_cross_corr,
                 marker='o', linestyle='', color="xkcd:orange")
    plus_one_std = [mean_sim_cross_corr[i] + std_sim_cross_corr[i] for i in range(len(p_emis))]
    minus_one_std = [mean_sim_cross_corr[i] - std_sim_cross_corr[i] for i in range(len(p_emis))]
    ax.fill_between(p_emis, plus_one_std, minus_one_std, alpha=0.2,
                    edgecolor="xkcd:orange", facecolor="xkcd:orange", linewidth=1, antialiased=True)
    plt.legend(['Analytical', 'Simulated'])
    plt.xlabel("Emission probability")
    plt.ylabel("Second order cross-correlation function $g^{(2)}_{ab}$")
    plt.title(f"p_dark = {p_dark}, det_eff = {det_eff}, num_triggers = {num_triggers}, "
              f"num_simulations = {num_simulations}", fontsize=20)
    # save figure
    if save:
        for fileformat in [".eps", ".svg", ".pdf", ".png"]:
            fig.savefig(f"cross_correlation_eff_{det_eff}_pdark_{p_dark}_trig_{num_triggers}" + fileformat,
                        bbox_inches="tight")
    plt.show()


def single_pair_prob_to_mpn(single_pair_prob):
    """Convert single pair emission probability to mean photon number mu.

    Following the equation: p(1) = (2 * mu) / (mu + 1)^3 .

    Parameters
    ----------
    single_pair_prob : float
        Probability for the SPDC source to emit a single photon pair.

    Returns
    -------
    mean_photon_number : list , length 3
        List of possible solutions for mu. The last entry is the physically correct mean photon number.

    """
    a = single_pair_prob
    # Solution 1:
    # plain text: a!=0,
    # x = (sqrt(3) sqrt(27 a^4 - 8 a^3) - 9 a^2)^(1/3)/(3^(2/3) a)
    # + 2/(3^(1/3) (sqrt(3) sqrt(27 a^4 - 8 a^3) - 9 a^2)^(1/3))
    # - 1
    term = scm.power(scm.sqrt(3) * scm.sqrt(27 * a ** 4 - 8 * a ** 3) - 9 * a ** 2, 1 / 3)
    first_term = term / (scm.power(3, 2 / 3) * a)
    second_term = 2. / (scm.power(3, 1 / 3) * term)
    mean_photon_number_1 = first_term + second_term - 1

    # Solution 2:
    # a!=0,
    # x = -((1 - i sqrt(3)) (sqrt(3) sqrt(27 a^4 - 8 a^3)- 9 a^2)^(1/3))/(2 3^(2/3) a)
    # - (1 + i sqrt(3))/(3^(1/3) (sqrt(3) sqrt(27 a^4 - 8 a^3) - 9 a^2)^(1/3))
    # - 1
    first_term_2 = -1 * (1 - scm.sqrt(3) * 1.j) * term / (2 * scm.power(3, 2 / 3) * a)
    second_term_2 = -1 * (1 + scm.sqrt(3) * 1.j) / (scm.power(3, 1 / 3) * term)
    mean_photon_number_2 = first_term_2 + second_term_2 - 1

    # Solution 3:
    # a!=0,
    # x = -((1 + i sqrt(3)) (sqrt(3) sqrt(27 a^4 - 8 a^3)- 9 a^2)^(1/3))/(2 3^(2/3) a)
    # - (1 - i sqrt(3))/(3^(1/3) (sqrt(3) sqrt(27 a^4 - 8 a^3) - 9 a^2)^(1/3))
    # - 1
    first_term_3 = -1 * (1 + scm.sqrt(3) * 1.j) * term / (2 * scm.power(3, 2 / 3) * a)
    second_term_3 = -1 * (1 - scm.sqrt(3) * 1.j) / (scm.power(3, 1 / 3) * term)
    mean_photon_number_3 = first_term_3 + second_term_3 - 1

    return mean_photon_number_1, mean_photon_number_2, mean_photon_number_3


def plot_against_paper(mean_sim_cross_corr, std_sim_cross_corr, save=False):
    """Plot against Figure 8 of 'Entanglement and nonlocality between disparate solid-state quantum memories
    mediated by photons' , Puigibert et al. , 2020, showing different values of the cross correlation for
    different values of the pump power.

    Parameters
    ----------
    mean_sim_cross_corr : list
        List of the mean values for the simulated cross correlation.
    std_sim_cross_corr : list
        List of the same length containing the corresponding standard deviations.
    save : bool (optional, default=False)
        Whether or not to save the plot to disk.

    """
    fig, ax = plt.subplots(1, 1, figsize=(7, 5))
    # experimental values from figure 8 (taken from original data sent by the authors)
    pump_power = list(np.linspace(10, 100, 10))
    g12_dt = [419, 215, 152, 103, 96, 82, 62, 65, 57, 61]
    g12_err = [20.47, 14.66, 12.33, 10.15, 9.80, 9.06, 7.87, 8.06, 7.55, 7.81]
    # fit to 1/pump_power (taken from original data sent by the authors)
    fit_g = []
    fit_x = np.linspace(1, 105, 250)
    for x in fit_x:
        fit_g.append(4442.73/x)
    # Plot fit and experimental values
    plt.errorbar(pump_power, g12_dt, yerr=g12_err, color="xkcd:blue", marker='o', linestyle='')
    plt.plot(fit_x, fit_g, color="xkcd:blue")
    plt.ylim(0, 600)
    plt.xlim(5, 105)
    # And plot the simulated values with error bars.
    plt.errorbar(pump_power, mean_sim_cross_corr, yerr=std_sim_cross_corr,
                 marker='o', linestyle='', color="xkcd:orange")
    plt.legend(['Experimental', 'Fit to experiment', 'Simulated'], fontsize=17)
    plt.xlabel("Pump Power relative to Maximum [%]", fontsize=20)
    plt.ylabel(r"$g^{(2)}_{12}(\delta t)$", fontsize=24)
    plt.title(r"Second order cross-correlation function", fontsize=24)
    plt.xticks(pump_power, fontsize=18)
    plt.yticks(fontsize=18)
    ax.grid()
    # save figure
    if save:
        for fileformat in [".eps", ".svg", ".pdf", ".png"]:
            fig.savefig("cross_correlation_vs_paper" + fileformat,
                        bbox_inches="tight")
    plt.show()


def compare_against_paper(num_triggers=100000, num_simulations=5, save=False):
    """Compare the source component against the data given in 'Entanglement and nonlocality between disparate
    solid-state quantum memories mediated by photons' Puigibert et al. 2020.

    Parameters
    ----------
    num_triggers : int (optional, default=100000)
        Number of times the source is triggered per simulation.
    num_simulations: int (optional, default=5)
        Number of simulations per datapoint.
    save : bool (optional, default=False)
        Whether or not to save the data to disk.

    """
    # set parameters
    dark_count_rate = 100               # dark count rate in Hz
    # from discussion with Gustavo: detectors are free-running (SNSPDs) thus no time window
    # therefore taking 1 / repetition rate as effective time_window (such that detectors are always on)
    repetition_rate = 80e6          # repetition rate in Hz
    detector_time_window = 1 / repetition_rate    # effective time_window in s
    p_dark = 1 - np.exp(- dark_count_rate * detector_time_window)
    detection_efficiency = 0.7
    single_pair_prob_max = 0.016

    # just like in paper simulate increments of 10%
    single_pair_prob_list = list(np.linspace(.1 * single_pair_prob_max, single_pair_prob_max, 10))
    # convert to mean photon numbers for SPDC source
    mpn_list = []
    for spp in single_pair_prob_list:
        mpn012 = single_pair_prob_to_mpn(spp)
        mpn_list.append(np.real(mpn012[2]))

    start_time = time()

    print(f"Starting time {strftime('%H:%M:%S', localtime())}")
    print("simulating for mean photon numbers: ", mpn_list)
    mean_sim, std_sim = run_simulation(det_eff=detection_efficiency, p_dark=p_dark,
                                       p_emis=None, num_triggers=num_triggers,
                                       num_simulations=num_simulations,
                                       mean_photon_number=mpn_list,
                                       multiprocessed=True)
    print(f"Simulation took {time() - start_time} seconds.")
    if save:
        df = pandas.DataFrame(data={"mean_cross_corr": mean_sim, "std_cross_corr": std_sim})
        df.to_csv(f"cross_corr_vs_paper_{num_simulations}x{num_triggers}.csv")
    return mean_sim, std_sim


def main(num_triggers=100, num_simulations=2):
    """Function to run any arbitrary setup with functions from this file."""
    # Parameter values
    det_eff = .9
    p_dark = 1e-5
    # Either use logspace and linspace
    p_emis = list(np.logspace(-3.5, -0, 3))
    mpn_list = None
    # p_emis = list(np.linspace(0.001, 0.20, 10))
    # p_emis = list(np.linspace(0.001, 0.20, 4))
    start_time = time()
    print(f"Starting time {strftime('%H:%M:%S', localtime())}")
    mean_sim, std_sim = run_simulation(det_eff=det_eff, p_dark=p_dark,
                                       p_emis=p_emis, num_triggers=num_triggers,
                                       num_simulations=num_simulations,
                                       mean_photon_number=mpn_list,
                                       multiprocessed=True)
    print(f"Simulation took {time() - start_time} seconds.")
    print(mean_sim)
    print(std_sim)
    # plot_auto_correlation_function(det_eff, p_dark, p_emis, mean_sim, std_sim, num_triggers, num_simulations)

    mean_sim, std_sim = compare_against_paper(num_triggers=100, num_simulations=2)


if __name__ == "__main__":
    main(num_triggers=100000, num_simulations=5)
    mean_sim, std_sim = compare_against_paper(num_triggers=100000, num_simulations=5)
    # plot_against_paper(mean_sim, std_sim)
