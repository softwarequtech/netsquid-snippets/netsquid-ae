import numpy as np
from time import time, strftime, localtime
from matplotlib import rcParams
from matplotlib import pyplot as plt
import multiprocessing

from netsquid import sim_reset, sim_run
from netsquid.components.qsource import SourceStatus
from netsquid.qubits import qubitapi as qapi
from netsquid.qubits.ketstates import BellIndex

from netsquid_ae.spdcsource import SPDCSource
from netsquid_ae.multi_photon_detectors import MultiPhotonBSMDetector

from netsquid_physlayer.detectors import BSMOutcome

from analytical_models import analytical_second_order_auto_and_cross_correlation_functions


def run_simulation(det_eff, p_dark, p_emis, num_triggers, num_simulations, multiprocessed=True):
    """Simulate the second order auto-correlation function with components of this snippet.
    This setup replicates the setup of Figure 3 of https://arxiv.org/abs/1109.0194.

    Note: This function is mainly a wrapper to run the actual simulation multiple times.

    Parameters
    ----------
    det_eff : float
        Detector efficiency.
    p_dark : float
        Dark count probability.
    p_emis : list
        List of emission probabilities of the photon-pair source that are meant to be simulated.
    num_triggers : int
        Number of times the source is triggered per simulation.
    num_simulations : int
        Number of independent simulation runs, which are used to determine the mean and standard deviation
        of the second order auto-correlation function.
    multiprocessed : bool
        Whether multiprocessing should be used to speed up the simulation or not.

    Returns
    -------
    mean_sim_auto_corr : list of float
        Mean of the simulated second order auto-correlation function,
        computed over `num_simulations` runs.
    std_sim_auto_corr : list of float
        Standard deviation of the simulated second order auto-correlation function,
        computed over `num_simulations` runs.
    """
    mean_sim_auto_corr, std_sim_auto_corr = [], []

    for i in range(len(p_emis)):
        # Use a particular emission probability
        p_emis_val = p_emis[i]
        # list arguments for starmap
        arguments = num_simulations * [(p_emis_val, num_triggers, p_dark, det_eff)]

        if multiprocessed:
            # multiprocessing
            with multiprocessing.Pool() as pool:
                # use starmap to pass multiple arguments
                sim_auto_corr = pool.starmap(simulate_second_order_auto_correlation_function, tuple(arguments))
        else:
            # no multiprocessing
            print(f"Running example for p ={p_emis_val} without multi-processing. "
                  f"Set multiprocessed=True for increased speed.")
            sim_auto_corr = []
            for arg in arguments:
                (p_emis_val, num_triggers, p_dark, det_eff) = arg
                sim_auto_corr.append(simulate_second_order_auto_correlation_function(p_emis_val,
                                                                                     num_triggers, p_dark, det_eff))

        # Store the mean and standard deviation for this particular emission probability based on the simulation runs
        mean_sim_auto_corr.append(np.mean(sim_auto_corr))
        std_sim_auto_corr.append(np.std(sim_auto_corr))

    return mean_sim_auto_corr, std_sim_auto_corr


def simulate_second_order_auto_correlation_function(p_emis_val, num_triggers, p_dark, det_eff):
    """Simulate the second order auto-correlation function with components of this snippet.
    This setup replicates the setup of Figure 3 of https://arxiv.org/abs/1109.0194.

    Parameters
    ----------
    det_eff : float
        Detector efficiency.
    p_dark : float
        Dark count probability.
    p_emis_val : float
        Emission probability of the photon-pair source.
    num_triggers : int
        Number of times the source is triggered.

    Returns
    -------
    simulated_auto_correlation_function : float
        Second order autocorrelation of this simulation run.
    """
    # Setup detector once and bind one of the classical output ports to a function that adds the outcome to a list
    sim_reset()
    bsm_det = MultiPhotonBSMDetector(name="bsm_det", p_dark=p_dark, det_eff=det_eff, visibility=1.,
                                     num_resolving=False, encoding="presence_absence")
    # Override conversion dictionary entry to distinguish double click outcome
    bsm_det._measoutcome2bsmoutcome[3] = BSMOutcome(success=False, bell_index=BellIndex.PHI_MINUS)
    # Bind classical output port to message storage in outcomes list
    outcomes = []
    bsm_det.ports["cout0"].bind_output_handler(lambda message: outcomes.append(message.items[0]))
    bsm_det.ports["cout1"].bind_output_handler(None)
    # Setup photon-pair source for this specific emission probability
    # The distribution of Equation (20) in the paper is used, where the fourth term is adjusted
    # so that the emission probability is normalized to 1.
    photon_pair_source = SPDCSource(name="photon_pair_source", encoding="presence_absence", multi_photon=True,
                                    emission_probability=[(1 - p_emis_val),
                                                          p_emis_val * (1 - p_emis_val),
                                                          p_emis_val ** 2 * (1 - p_emis_val),
                                                          p_emis_val ** 3],
                                    status=SourceStatus.EXTERNAL)
    # Connect the photon pair source to the detector with one side
    photon_pair_source.ports["qout0"].connect(bsm_det.ports["qin0"])
    # Discard all the qubits that are emitted to the unused side
    photon_pair_source.ports["qout1"].bind_output_handler(
        lambda message: [qapi.discard(q) for q in message.items])

    for _ in range(num_triggers):
        # Insert vacuum in the other input port, since the detector expects inputs from both input ports
        # simultaneously.
        bsm_det.ports["qin1"].tx_input(qapi.create_qubits(2))
        # Trigger the source to emit the photons
        photon_pair_source.trigger()
        sim_run()
    # Based on the outcomes, check how many single clicks there were in one of the detectors and the
    # amount of double clicks
    clicks_left, double_clicks = 0, 0
    for outcome in outcomes:
        if outcome.success:
            if outcome.bell_index == BellIndex.PSI_PLUS:
                clicks_left += 1
        else:
            # This is a manual override in BSMDetectorMulti to represent a double click
            if outcome.bell_index == BellIndex.PHI_MINUS:
                double_clicks += 1
    # Now compute the auto correlation function by taking the average of the number of double clicks
    # divided by the average of the number of single clicks squared.
    simulated_auto_correlation_function = 0 if double_clicks == 0 else (num_triggers * double_clicks) / \
                                                                       ((clicks_left + double_clicks) ** 2)
    # Reset components and disconnect detector port to make sure no memory leakage
    bsm_det.reset()
    bsm_det.ports["qin0"].disconnect()
    photon_pair_source.reset()

    return simulated_auto_correlation_function


def plot_auto_correlation_function(det_eff, p_dark, p_emis, mean_sim_auto_corr, std_sim_auto_corr,
                                   num_triggers, num_simulations):
    # Set overall font size to 20
    rcParams["font.size"] = 20
    fig, ax = plt.subplots(1, 1, figsize=(15, 9))
    # Compute the analytical auto-correlation function in a smooth fashion over the domain
    ana_auto_corr = []
    p_emis_ana = np.linspace(p_emis[0], p_emis[-1], 250)
    for p_emis_val in p_emis_ana:
        ana_auto_corr.append(analytical_second_order_auto_and_cross_correlation_functions(p_dark=p_dark,
                                                                                          p_emis=p_emis_val,
                                                                                          det_eff=det_eff)[1])
    # Plot the analytical auto-correlation function
    plt.plot(p_emis_ana, ana_auto_corr, color="xkcd:blue")
    plt.ylim(0., 3.0)
    # And plot the simulated one with shaded errorbars.
    plt.errorbar(p_emis, mean_sim_auto_corr, yerr=std_sim_auto_corr,
                 marker='o', linestyle='', color="xkcd:orange")
    plus_one_std = [mean_sim_auto_corr[i] + std_sim_auto_corr[i] for i in range(len(p_emis))]
    minus_one_std = [mean_sim_auto_corr[i] - std_sim_auto_corr[i] for i in range(len(p_emis))]
    ax.fill_between(p_emis, plus_one_std, minus_one_std, alpha=0.2,
                    edgecolor="xkcd:orange", facecolor="xkcd:orange", linewidth=1, antialiased=True)
    plt.legend(['Analytical', 'Simulated'])
    plt.xlabel("Emission probability")
    plt.ylabel("Second order auto-correlation function $g^{(2)}_a$")
    plt.title(f"p_dark = {p_dark}, det_eff = {det_eff}, num_triggers = {num_triggers}, "
              f"num_simulations = {num_simulations}", fontsize=20)
    # save figure
    # for fileformat in [".eps", ".svg", ".pdf", ".png"]:
    #     fig.savefig(f"autocorrelation_eff_{det_eff}_pdark_{p_dark}" + fileformat, bbox_inches="tight")
    plt.show()


def main(num_triggers=100, num_simulations=2):
    # Parameter values
    det_eff = 0.5
    p_dark = 0.01
    # Either use logspace and linspace
    # p_emis = list(np.logspace(-3, -0, 10))
    # print(p_emis)
    # p_emis = list(np.linspace(0.001, 0.20, 10))
    p_emis = list(np.linspace(0.001, 0.20, 3))

    start_time = time()
    print(f"Starting time {strftime('%H:%M:%S', localtime())}")
    mean_sim, std_sim = run_simulation(det_eff=det_eff, p_dark=p_dark,
                                       p_emis=p_emis, num_triggers=num_triggers,
                                       num_simulations=num_simulations,
                                       multiprocessed=True)
    print(f"Simulation took {time() - start_time} seconds.")
    # plot_auto_correlation_function(det_eff, p_dark, p_emis, mean_sim, std_sim, num_triggers, num_simulations)


if __name__ == "__main__":
    main(num_triggers=100000, num_simulations=5)
