CHANGELOG
=========

2024-03-22 (6.4.0)
------------------
- Added separate number of modes for memory and source, and method of calculating number of modes from these.
- Added number of modes calculation for SPDC source, based on new parameters 'Rate' and 'g2' 

2024-03-22 (6.3.4)
------------------
- Remove pickle5 dependency and replace usages with pickle directly
- Restrict pandas to <1.5.0
- Restrict numpy to <1.21.0

2022-08-31 (6.3.3)
-----------------
- moved data clearing from `datacollectors.MeasurementCollector` to `ae_protocols.MessagingProtocol`

2022-08-19 (6.3.2)
------------------
- fixed data clearing in `datacollectors.MeasurementCollector`

2022-07-26 (6.3.1)
------------------
- removed left over print statement in `application_protocols.py`

2022-07-21 (6.3.0)
------------------
- `ae_protocols.MessagingProtocol` now schedules a single event per successful end-to-end entanglement
- `datacollectors.MeasurementCollector` has been adapted to handle that change
- unified all rng to use `netsquid.util.simtools.get_random_state`

2022-07-05 (6.2.0)
------------------
- adapted `ae_protocols.MessagingProtocol` to be able to handle multiple link successes

2022-05-03 (6.1.0)
------------------
- added `MemoryLossModelOnMemory` to `qlossmodel.py` which has the same functionality as `MemoryLossModel` but can be used as intended by NetSquid on the memory
- Note: This can only be used for non-multiphoton states as we still need a clunky workaround to be able to apply memory-noise to multi-photon states
- updated `AEAnalyticalSDSFactory` to actually take the emission probabilities for 'presence_absence' encoding into account
- small fixes to remove hardcoded delays of clocks

2022-03-22 (6.0.0)
-----------------
- added option to deliver `multiple_link_successes` in a single delivery in `ae_magic_distributor.py`
- updated `numpy` requirements to match `pandas`
- `forced_magic_state_generator.py` now prints wall-time it takes to generate states
- changed `"mid_det_*"` parameter name to `"det_*"`
- combined `"coupling_loss_l"` and `"coupling_loss_r"` to `"coupling_loss_fibre"` as they are typically the same

2022-01-05 (5.1.0)
------------------
- updated requirements to `netsquid-magic>=13.0.0`
- unified the functions in `ae_chain_setup.py`
- fixed bug in `datacollectors.ChainStateCollector` that led to end-to-end states being predicted wrongly
- fixed bug in `ae_protocols.MessagingProtocol` when not having an application connected to the end nodes

2022-01-03 (5.0.5)
-----------------
- unified setup of `ae_classes.MessagingConnection` with `netsquid-physlayer`

2021-12-15 (5.0.4) (Server Migration)
------------------------------------
- moved build and make commands of documentation to common makefile
- updated `.gitlab-ci.yaml` and `README.md` accordingly

2021-11-10 (5.0.3)
------------------
- added `delivery_id` as unique identifier to delivery labels in `ae_magic_distributor.py`
- fixed setting of formalisms in `forced_magic_state_generator.py`
- made sure parameters of type `list` passed to a `RepchainDataFrameHolder` are first converted to tuples and this is handled correctly

2021-09-13 (5.0.2)
-----------------
- small bugfixes related to `repetition_delay`

2021-09-08 (5.0.1)
-----------------
- changed the way how `ae_magic_distributor.py` sends rounds in labels
- added optional `repetition_delay` to `ae_magic_distributor.py` for automatic repetition of deliveries

2021-08-26 (5.0.0)
-----------------
- completely refactored the way that "magic" is set up to unify it with `netsquid-magic`
- split `ae_magic.py` into `ae_state_delivery_samplers.py` and `ae_magic_distributor.py`
- added `forced_magic_state_generator.py` to generate full end-to-end elementary link states for use with magic.
- upgraded pipeline to use docker image with `python3.8` (3.8.2)
- upgraded `multi_photon_detectors.py` to improvements in `netsquid_physlayer==4.2.0` (to include `dead_time`)
- pruned datacollectors and moved them to `datacollectors.py`

2021-07-07 (4.1.0)
-----------------
- removed default parameters in `netsquid_ae.qlossmodel`
- added delay for `EmissionProtocol` so simulations of midpoint asymmetry can be run without magic.
  (this is implemented with event scheduling in `EmissionProtocol`, so protocols listening to `EmissionProtocol` now need to listen to specific events)

2021-07-06 (4.0.2)
-----------------
- fixed check for successful end-to-end messages in `netsquid_ae.ae_protocols.MessagingProtocol` to flatten lists correctly instead of ignoring them from the check.

2021-05-21 (4.0.1)
-----------------
- updated requirements to `netsquid-simulationtools>=1.0.0, <3.0.0` for compatibility with todays release of `netsquid-simulationtools==2.0.0`

2021-05-20 (4.0.0)
-----------------
- extended `netsquid_ae.multi_photon_beam_splitter.MultiPhotonBeamSplitter` to also work with 'time_bin' encoding and
  accept input from two ports. (including tests)
- added code coverage visualization to gitlab CI
- improved test coverage e.g. for `netsquid_ae.qlossmodel`, `netsquid_ae.ae_protocols.SwapProtocol` and `netsquid_ae.ae_parameter_set`
- allow for specifying mean photon number for each source in the link or chain (i.e. mean photon numbers can now be asymmetric)
- added analytical single-click magic for ideal photon sources (multi- and single-photon) in `netsquid_ae.ae_magic.AEAnalyticalSDSFactory`
- added magic implementation of random phase in `netsquid_ae.ae_magic.AEMagicDistributor`
- fixed a bug in `netsquid_ae.multi_photon_detectors.MultiPhotonQKDDetector` that handled the input of `None` elements incorrectly

2021-04-29 (3.0.0)
------------------
- allow for asymmetric elementary links
- `netsquid_ae.ae_magic.AEMagicDistributor` now works for asymmetric links and chains
- removed the `node_distance` parameter
- corrected two-mode squeezing photon number distribution for 'presence_absence' encoding in `netsquid_ae.qlossmodel.spdcsource`
- added multi-photon beam splitter component `netsquid_ae.multi_photon_beam_splitter.MultiPhotonBeamSplitter`
- updated requirements to new `netsquid-magic=10.0.0`

2021-04-09 (2.0.0)
------------------
- allow for multiple successes on elementary link
- added phase drift of photons as `netsquid_ae.qlossmodel.FibrePhaseModel`

2021-03-19 (1.0.1)
------------------
- fixed deprecation warnings in tests and adjusted probabilistic test in `test_multi_photon_detectors.py` to higher number of runs (to increase success probability to near-deterministic regime),
- updated requirements for magic and simulationtools compatible with `netsquid>=1.0.6`

2021-03-12 (1.0.0)
------------------
- First release after making snippet public including update to public release version of `netsquid`.
- updated requirements to `netsquid>=1.0.6` to include necessary bugfix in `sparseutil`.
- added pytest custom marker `slow` to skip slow tests and added `make tests_full` to run all tests including slow tests.

2021-02-19 (0.5.7)
------------------
- `netsquid_ae.ae_chain_setup` now has `extraction_delay` as optional parameter that defaults to 0.

2021-02-12 (0.5.6)
------------------
- `netsquid_ae.ae_chain_setup` can now correctly set up protocols and magic if passes 2 chains.

2021-01-20 (0.5.5)
------------------
- `netsquid_ae.ae_protocols.MessagingProtocol` now removing messages and data on nodes once the message has been sent on 
- adapted `netsquid_ae.spdc_source` to use list instead of tuple for `emission_probabilities` in order to be compatible with netconf
- removed outdated mail addresses from `README.md`

2020-12-09 (0.5.4)
------------------
- updated requirements to netsquid-magic 7.0.0

2020-11-26 (0.5.3)
------------------
- changed `ae_parameter_set.convert_parameter_set` to use dict as input to make it compatible with new stopos workflow

2020-11-03 (0.5.2)
------------------
- updated experimental data in `examples/example_cross_correlation.py`
- changed to `pickle5`
- small bugfix in `netsquid_ae.ae_protocols.MessagingProtocol`

2020-09-16 (0.5.0)
------------------
- added new multi-photon detectors, which subclass from the ones in
netsquid-physlayer, and incorporated these in all the protocols and tests.
- incorporate changes of NetSquid 0.10, use detectors from netsquid-physlayer,
update DataCollectors to use the new BSMOutcomes
- removed Wizard and integrated functionality into MagicDistributor. Restructured MagicProtocol accordingly.
- common interface for StateDeliverySamplerFactories
- added the possibility to force measurement outcomes to BSMDetectorMulti
- added examples to calculate second-order auto- and cross-correlation function
- small bug fixes (e.g. linter in examples, changing examples from runpy to sys and importlib)

2020-07-20 (0.4.0)
-----------------
- minor changes to maintain compatibility with other snippets (e.g. improvements in Magic)
- updated qdetector to general bsmdetector component
- changed physical network configuration


2020-04-07 (0.3.1)
-----------------

- fixed compatibility issue with data sets produced with version < 0.2.0


2020-03-31 (0.3.0)
------------------

- added gaussian decay for memory efficiency and possibility to switch between them
- number of fixes for simulation 
- additions to parameter_set_collection


2020-02-03 (0.2.0)
-----------------

- fixes to magic
- added predefined ParameterSet class
- adjusted simulation setup to ParameterSet class

2020-01-20 (0.1.0)
-----------------
breaking update, now requires NetSquid >= v0.7

- reworked the implementation of magic
- added new multiphoton POVMs with visibility

2019-12-08 (0.0.1)
-----------------

- added better simulation setup scripts in ae_chain_setup.py

2019-11-23 (0.0.0)
------------------

- Created this snippet
