import logging
import copy
from scipy import sparse

from pydynaa import Entity

import netsquid as ns
from netsquid.util.simlog import logger
import netsquid.qubits.qubitapi as qapi
from netsquid.util.simtools import sim_time, get_random_state

from netsquid_ae.ae_classes import EndNode
from netsquid_physlayer.detectors import BSMOutcome, QKDOutcome

__all__ = [
    "ChainStateCollector",
    "MeasurementCollector",
    "DecisionMaker",
]


class ChainStateCollector:
    """Callable class for the DataCollector of a repeater chain collecting the end-to-end state and all midpoint and
    swap outcomes.

    To achieve this, this DataCollector 'cheats' and accesses all nodes instantly (is therefore non-local).

    For atomic ensembles this should be triggered by the ExtractionProtocol (any kind of evtype)
    Note: each chain needs its own ChainStateCollector

    Note: States including more than two qubits (aka. size > 4x4) will be saved as sparse instead of dense matrices.

    Parameters
    ----------
    nodes_1 : list of :obj:`~netsquid.nodes.node`
        All nodes in the repeater chain (End Nodes and RepeaterNodes , not Heralding Stations)
    nodes_2 : list of :obj:`~netsquid.nodes.node` (optional)
        When using presence_absence encoding with two repeater chains , this should be a lost containing all nodes in
        the second repeater chain (while nodes_1 contains all nodes in the first chain).
    magic_protocols : list (optional)
        List of '~netsquid_ae.ae_protocols.MagicProtocol`. When using magic without
        `~netsquid_ae.ae_protocols.MessagingProtocol` it is necessary to un-trigger the
        '~netsquid_ae.ae_protocols.MagicProtocol` and therefore these can be passed to the StateCollector.
    Notes
    -------
    constructs:

    dataframe: instance of :class:`~pandas.DataFrame`
        Dataframe containing all midpoint and swap results along the chain, the end-to-end state, the number of attempts
        necessary to generate that state and the round this state was generated in.
        Note: Since this collects the state it does not contain any end-node measurement results (as these these would
        change the state)
    """
    def __init__(self, nodes_1, nodes_2=None, magic_protocols=None, max_successes=None):
        self.current_round = 0
        self.num_successes = 0
        self.nodes = nodes_1
        self.nodes_2 = nodes_2
        self.magic_protocols = magic_protocols
        self.max_successes = max_successes

    def __call__(self, event):
        """Function that is called by the data collector.

        Collects the end-to-end state from the end nodes.

        "Cheats" and collects all simulation data of this round from all nodes.

        Returns
        -------
        data : dict
            Dictionary containing the end-to-end state and the simulation data for this successful round.

        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"ChainCollector collecting states at {sim_time()}")

        if not self.nodes_2:
            # using one chain
            alice = [self.nodes[0]]
            rep_nodes = self.nodes[1:-1]
            rep_nodes_2 = []
            bob = [self.nodes[-1]]
        else:
            # using two repeater chains
            alice = [self.nodes[0], self.nodes_2[0]]
            rep_nodes = self.nodes[1:-1]
            rep_nodes_2 = self.nodes_2[1:-1]
            bob = [self.nodes[-1], self.nodes_2[-1]]

        # get current round from end nodes
        current_round = alice[0].cdata["round_E"][-1]
        for a in alice:
            assert isinstance(a, EndNode)
            if a.cdata["round_E"][-1] != current_round:
                raise RuntimeError("Protocols on Alice triggered ChainCollector while in different rounds: "
                                   "{} vs. {}".format(current_round, a.cdata["round_E"][-1]))
        for b in bob:
            assert isinstance(b, EndNode)
            if b.cdata["round_E"][-1] != current_round:
                raise RuntimeError("Protocols on Alice and Bob triggered ChainCollector while in different rounds: "
                                   "{} vs. {}".format(current_round, b.cdata["round_E"][-1]))

        # only save state if all BSMs successful
        all_outcomes = []
        for node in alice + bob:
            all_outcomes.append(node.cdata["midpoint_outcomes_E"][-1][0])
        for node in rep_nodes:
            all_outcomes.append(node.cdata["midpoint_outcomes_L"][-1][0])
            all_outcomes.append(node.cdata["midpoint_outcomes_R"][-1][0])
            all_outcomes.append(node.cdata["swap_outcomes"][-1][0])
        if self.nodes_2:
            for node in rep_nodes_2:
                all_outcomes.append(node.cdata["midpoint_outcomes_L"][-1][0])
                all_outcomes.append(node.cdata["midpoint_outcomes_R"][-1][0])
                all_outcomes.append(node.cdata["swap_outcomes"][-1][0])

        if any([not outcome.success for outcome in all_outcomes]):
            # append nothing to dataframe
            if logger.isEnabledFor(logging.DEBUG):
                print("{}: ChainCollector discarding unsuccessful states.".format(ns.sim_time()))
            return
        else:
            if logger.isEnabledFor(logging.DEBUG):
                print("{}: ChainCollector collecting states".format(ns.sim_time()))
            # calculate number of attempts needed for this success
            num_attempts = current_round - self.current_round
            self.current_round = current_round
            self.num_successes += 1

            if self.num_successes % 100 == 0:
                print("--No. of entanglement successes so far: ", self.num_successes, "between",
                      alice[0].name, "and", bob[0].name, ".--")

            # get states from both end nodes (both are lists)
            if not self.nodes_2:
                if alice[0].cdata["states_E"][-1][0].qstate is None or bob[0].cdata["states_E"][-1][0].qstate is None:
                    # one of the sources produces vacuum, discard result
                    return {}
                combined_state = qapi.reduced_dm(alice[0].cdata["states_E"][-1] + bob[0].cdata["states_E"][-1])
            else:
                combined_state = qapi.reduced_dm(alice[0].cdata["states_E"][-1] + alice[1].cdata["states_E"][-1] +
                                                 bob[0].cdata["states_E"][-1] + bob[1].cdata["states_E"][-1])
            state = copy.deepcopy(combined_state)
            if len(state) > 4:
                # convert multi-photon state to sparse matrix to reduce memory requirement
                state = sparse.csr_matrix(state)

            # create dictionary
            # Note: we are only collecting successes so storing the bell_index for the outcomes is enough
            # Note: netsquid_simulationtools expects the name to be {}_outcome_{} when calculating the expected state
            data = {"state": state,
                    "number_of_rounds": num_attempts,
                    "round": current_round,
                    "midpoint_outcome_0": self.nodes[0].cdata["midpoint_outcomes_E"][-1][0].bell_index
                    }
            # add midpoint and swap results to dictionary
            i = 0
            if rep_nodes:
                for node in rep_nodes:
                    data["midpoint_outcome_{}".format(i)] = node.cdata["midpoint_outcomes_L"][-1][0].bell_index
                    data["swap_outcome_{}".format(i)] = node.cdata["swap_outcomes"][-1][0].bell_index
                    i += 1
                data["midpoint_outcome_{}".format(i)] = rep_nodes[-1].cdata["midpoint_outcomes_R"][-1][0].bell_index
                if self.nodes_2:
                    for node in rep_nodes_2:
                        data["chain_2_midpoint_{}".format(i)] = node.cdata["midpoint_outcomes_L"][-1][0].bell_index
                        data["chain_2_swap_{}".format(i)] = node.cdata["swap_outcomes"][-1][0].bell_index
                        i += 1
                    data["chain_2_midpoint_{}".format(i)] = rep_nodes[-1].cdata["midpoint_outcomes_R"][-1][0].bell_index

        # un-trigger magic protocol if needed
        if self.magic_protocols is not None:
            for mp in self.magic_protocols:
                mp._is_triggered = False
        if self.max_successes is not None:
            if self.num_successes >= self.max_successes:
                # stop simulation
                print("Stopping simulation after", self.num_successes, " rounds.")
                ns.sim_stop()

        return data


class MeasurementCollector:
    """Callable class for the DataCollector of a repeater chain.

    Collecting only the end-node measurement results (and not the state).
    These measurement outcomes must always be 0 and 1, following the convention
    which is also used in `netsquid-simulationtools`.

    This DataCollector does not cheat and should only be called once all relevant data has reached the Endnodes.
    For AE this should be triggered by the MessagingProtocol on the DetectorNode.

    Note: Data should be stored on nodes (not Protocols)

    Parameters
    ----------
    messaging_protocol_alice : instance of :class:`~.MessagingProtocol`
        MessagingProtocol running on one of the detector nodes (Alice) at the end of the chain.
    messaging_protocol_bob : instance of :class:`~.MessagingProtocol`
        MessagingProtocol running on the other detector node (Bob) at the end of the chain.

    Notes
    -------
    constructs:

    dataframe: instance of :class:`~pandas.DataFrame`
        Dataframe containing all midpoint and swap outcomes, the end node measurement results and basis, the number of
        attempts necessary to generate that state, and the round this state was generated in.
        Note: Since this collects the end-node measurement outcomes it does not contain the end-to-end state
        (as the measurement changes the state)

    """
    def __init__(self, messaging_protocol_alice, messaging_protocol_bob):
        self.current_round = 0
        self.num_successes = 0
        self.alice_proto = messaging_protocol_alice
        self.bob_proto = messaging_protocol_bob

    def __call__(self, evexpr):
        """Function that is called by the data collector.

        Collects the complete messages from the detector nodes.

        Returns
        -------
        data : dict
            Dictionary containing the measurement outcomes and simulation data for this successful round.

        """
        assert evexpr.first_term.triggered_time == evexpr.second_term.triggered_time
        assert self.alice_proto.node.cdata["final_message"]
        assert self.bob_proto.node.cdata["final_message"]

        if logger.isEnabledFor(logging.DEBUG):
            print("{}: MeasurementCollector collecting endnode measurement results".format(sim_time()))

        # process Message to extract relevant data
        final_message_alice = copy.deepcopy(self.alice_proto.node.cdata["final_message"][-1])
        final_message_bob = copy.deepcopy(self.bob_proto.node.cdata["final_message"][-1])
        list_alice = final_message_alice.items
        list_bob = final_message_bob.items
        list_alice.reverse()  # this results in format [Alice,..,Bob(,Alice,...Bob)]

        # double check if correct messages were picked up by DataCollector
        for msg in [final_message_alice, final_message_bob]:
            for item in msg.items:
                if isinstance(item, list):
                    item = item[0]
                if (isinstance(item, QKDOutcome) or isinstance(item, BSMOutcome)) and not item.success:
                    raise ValueError(f"Unsuccessful message {msg} accidentally picked up by DC.")

        # check if both messages came from the same attempt
        if final_message_alice.meta["round"] != final_message_bob.meta["round"]:
            raise ValueError("Datacollection was triggered by messages from different rounds. Alice: ",
                             final_message_alice.meta["round"], " Bob: ", final_message_bob.meta["round"])

        # two chains should be handled individually
        if self.alice_proto.detection_protocol.dual_chain:
            # cutting list in halves
            msg_chain_1 = list_alice[len(list_alice) // 2:]
            msg_chain_2 = list_alice[:len(list_alice) // 2]
            bsm_outcomes = msg_chain_1[2:-2]
            bsm_outcomes_chain_2 = msg_chain_2[2:-2]
            head_chain_1 = msg_chain_1[:2] + msg_chain_1[-2:]
            head_chain_2 = msg_chain_2[:2] + msg_chain_2[-2:]
            if head_chain_1 == head_chain_2:
                qkd_outcome_alice = msg_chain_1[1]
                qkd_outcome_bob = msg_chain_1[-2]
            else:
                raise ValueError("DataCollector has different message heads for the two chains: {} vs. {}"
                                 .format(head_chain_1, head_chain_2))
            # sanity check
            if msg_chain_1[0] != "det_alice":
                raise ValueError("Datacollector trying to save Alice's results as Bob's {}".format(msg_chain_1))
        # for single chain both messages should agree
        elif list_alice != list_bob:
            raise ValueError("Alice and Bob got different messages in round: ", final_message_alice.meta["round"])
        else:
            bsm_outcomes = list_alice[2:-2]
            qkd_outcome_alice = list_alice[1]
            qkd_outcome_bob = list_alice[-2]
            if list_alice[0] != "det_alice":
                raise ValueError("Datacollector trying to save Alice's results as Bob's {}".format(list_alice))

        # get current round from message meta
        current_round = final_message_alice.meta["round"]
        # calculate number of attempts needed for this success
        num_attempts = current_round - self.current_round
        # update class variable
        self.current_round = current_round
        assert len(qkd_outcome_alice) == 1

        # count successes
        if qkd_outcome_alice[0].success and qkd_outcome_bob[0].success:
            # both Alice and Bob had a successful BSM measurement
            self.num_successes += 1
        else:
            # postselection -> discard
            print("final message is:", final_message_alice, final_message_bob)
            raise ValueError("Postselecting unsuccessful endnode BSM in round", current_round)

        if self.num_successes % 10 == 0:
            print("--No. of successful measurements so far: ", self.num_successes, "between",
                  self.alice_proto.node.name, "and", self.bob_proto.node.name, ".--")
        # create dictionary
        data = {"basis_A": qkd_outcome_alice[0].measurement_basis,
                "basis_B": qkd_outcome_bob[0].measurement_basis,
                "outcome_A": qkd_outcome_alice[0].outcome,
                "outcome_B": qkd_outcome_bob[0].outcome,
                "number_of_attempts": num_attempts / len(qkd_outcome_alice),
                "current_round": current_round,
                }
        # add midpoint and swap results to dictionary
        # Note: since we only collect successful messages it is enough to just add the bell_index here
        mid = 0
        swap = 0
        for i in range(len(bsm_outcomes)):
            if (i + 1) % 2 == 1:
                data["midpoint_outcome_{}".format(mid)] = bsm_outcomes[i][0].bell_index
                mid += 1

            else:
                data["swap_outcome_{}".format(swap)] = bsm_outcomes[i][0].bell_index
                swap += 1
        # add midpoint and swap result from second chain as additional columns
        if self.alice_proto.detection_protocol.dual_chain:
            mid = 0
            swap = 0
            for i in range(len(bsm_outcomes_chain_2)):
                if (i + 1) % 2 == 1:
                    data["chain_2_midpoint_{}".format(mid)] = bsm_outcomes_chain_2[i][0].bell_index
                    mid += 1

                else:
                    data["chain_2_swap_{}".format(swap)] = bsm_outcomes_chain_2[i][0].bell_index
                    swap += 1

        return data


class DecisionMaker(Entity):
    """Entity that makes decisions on how to continue the simulation.

    The decision is based on the current number of successes.

    Parameters
    ----------
    min_successes : int
        Number of successes after which to stop the simulation. Set to -1 for infinite operation.
    messaging_protocol_alice : instance of :class:`~.MessagingProtocol`
        MessagingProtocol (with the DetectionProtocol as an attribute) on Alice's end node detector signalling
        successful reception of success message.
    messaging_protocol_bob : instance of :class:`~.MessagingProtocol`
        MessagingProtocol on Bob's end node detector signalling successful reception of success message.
    datacollector : instance of :class:`~netsquid.util.datacollector.DataCollector` (optional, mostly for debugging)
        DataCollector collecting the simulation data. Has to contain a callable class with attribute `num_successes`.
        Can be used to check if DataCollector and DecisionMaker have the same count.

    """
    def __init__(self, min_successes, messaging_protocol_alice, messaging_protocol_bob, datacollector=None):
        super().__init__()
        self.min_successes = min_successes
        self.num_successes = 0
        self.alice_proto = messaging_protocol_alice
        self.bob_proto = messaging_protocol_bob
        self.datacollector = datacollector

    def decide(self, evexpr):
        """Function that is called when chain signals success.

        Decides what to do next:
            a) do nothing / continue
            b) change measurement basis
            c) stop simulation after desired number of successes

        Note: To generate data more quickly the DecisionMaker always sets Alice and Bob to measure in the same basis.

        """
        self.num_successes += 1
        if logger.isEnabledFor(logging.DEBUG):
            print("########### DECISIONMAKER: ", self.num_successes, " of ", self.min_successes, "###########")

        # check if DataCollector has the same count
        if self.datacollector is not None:
            if self.num_successes != self.datacollector.get_data_function.num_successes:
                # only prints instead of raising an error, because this should not break the simulation
                print("DecisionMaker has {} successes while DataCollector counted {}"
                      .format(self.num_successes, self.datacollector.get_data_function.num_successes))

        if self.min_successes > 0:
            # decide what to do (continue, change basis, stop sim)
            if self.num_successes >= self.min_successes:
                # stop simulation
                print("Stopping simulation after", self.num_successes, " rounds.")
                ns.sim_stop()
            elif self.num_successes == self.min_successes / 2:
                print("Switching measurement basis.")
                if self.alice_proto.detection_protocol.qkd_det.measurement_basis == "X":
                    # assuming both always measure in the same basis
                    self.alice_proto.detection_protocol.qkd_det.measurement_basis = "Z"
                    self.bob_proto.detection_protocol.qkd_det.measurement_basis = "Z"
                elif self.alice_proto.detection_protocol.qkd_det.measurement_basis == "Z":
                    # assuming both always measure in the same basis
                    self.alice_proto.detection_protocol.qkd_det.measurement_basis = "X"
                    self.bob_proto.detection_protocol.qkd_det.measurement_basis = "X"
                else:
                    print("No measurement basis specified on detection protocol. DecisionMaker not switching.")
            else:
                pass
        else:
            # no minimum number of successes specified: choose measurement basis at random until sim stops.
            # assuming both always measure in the same basis
            meas_basis = get_random_state().choice(["Z", "X"])
            self.alice_proto.detection_protocol.qkd_det.measurement_basis = meas_basis
            self.bob_proto.detection_protocol.qkd_det.measurement_basis = meas_basis
