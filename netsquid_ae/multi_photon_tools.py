import pickle
import os.path as path
from scipy.linalg import sqrtm

from netsquid_ae.multi_photon_povms import convert_scalars

from qualg.operators import Operator, BaseOperator
from qualg.q_state import BaseQuditState
from qualg.scalars import ProductOfScalars, InnerProductFunction, SumOfScalars


def write_to_txt(filename, output_name="full_multiphoton_povms"):
    """Function that writes pre-generated files to a text file '<output_name>.txt'.

    Parameters
    ----------
    filename : str
        Name of the file containing the QuAlg operators dictionary.
    output_name : str (optional)
        Name of the output txt file as '<output_name>.txt'
    """
    with open(filename, "rb") as input_file:
        operator_dict = pickle.load(input_file)

    output_file = open(output_name + ".txt", "w")
    if isinstance(operator_dict, dict):
        for k in operator_dict.keys():
            output_file.write(f"{k} \n")
            output_file.write(repr(operator_dict[k]) + " \n")
    elif isinstance(operator_dict, list):
        for operator in operator_dict:
            output_file.write(repr(operator) + " \n")
    output_file.close()


def read_from_txt(filename):
    """Function that reads operators from a text file.

    Parameters
    ----------
    filename : str
        Name of the text file containing the operator representations.

    Returns
    -------
    operator_dict : dict
        Dictionary containing the Operators and their keys (n,m)

    """
    keys = []
    ops = []
    with open(filename) as fp:
        line = fp.readline()
        n = int(line.strip()[1])
        m = int(line.strip()[4])
        keys.append((n, m))
        cnt = 1
        while line:
            line = fp.readline()
            if cnt % 2 == 1:
                ops.append(line.strip())
            else:
                if len(line.strip()) != 0:
                    n = int(line.strip()[1])
                    m = int(line.strip()[4])
                    keys.append((n, m))
            cnt += 1
    operator_dict = {}
    for i in range(len(keys)):
        operator_dict[keys[i]] = eval(ops[i])

    return operator_dict


def get_multiphoton_projectors(hom_visibility):
    """Gets the projectors (POVMs) for photon-number-resolving detectors,
    where up to six photons can arrive simultaneously.

    Parameters
    ----------
    hom_visibility : float
        Hong-Ou-Mandel interference visibility (i.e. photon indistinguishability) at the beam splitter of the BSM.

    Returns
    -------
    projectors_num_res : dict
        Dictionary where the keys (n, m) represent n photons coming from the left and m photons coming from the right
        and the corresponding values are the 16 x 16 projectors based on the visibility.

    """
    # open text file
    operator_dict = read_from_txt(path.dirname(path.abspath(__file__)) + "/full_multiphoton_povms.txt")
    projectors_num_res = {}
    for k in operator_dict.keys():
        # convert dict to arrays with given visibility
        array = operator_dict[k].to_numpy_matrix(convert_scalars, visibility=hom_visibility)
        projectors_num_res[k] = array

    return projectors_num_res


if __name__ == '__main__':
    ops_dict = read_from_txt('netsquid_ae/full_multiphoton_povms.txt')
    visibility = 0
    for key in [(2, 0)]:
        print(ops_dict[key])
        arr = sqrtm(ops_dict[key].to_numpy_matrix(convert_scalars, visibility))
        print(arr)
    exit()
    # workaround to pass lint
    op = Operator
    bs = BaseQuditState
    bo = BaseOperator
    pos = ProductOfScalars
    ips = InnerProductFunction
    sos = SumOfScalars

    # combine subsets to generate full set
    with open("multiphoton_povms_raw_subset_leq.pkl", "rb") as file:
        leq_dict = pickle.load(file)
    with open("multiphoton_povms_raw_subset_g.pkl", "rb") as file2:
        g_dict = pickle.load(file2)
    leq_dict.update(g_dict)
    with open('full_multiphoton_povms.pkl', 'wb') as output:
        pickle.dump(leq_dict, output, pickle.HIGHEST_PROTOCOL)
    write_to_txt("full_multiphoton_povms.pkl")
    exit()
