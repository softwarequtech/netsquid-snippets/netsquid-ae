from netsquid.protocols.protocol import Protocol
from netsquid.util.simtools import get_random_state

from netsquid_ae.multi_photon_detectors import MultiPhotonQKDDetector
from netsquid_ae.protocol_event_types import EVTYPE_ERROR, EVTYPE_FAIL, EVTYPE_SUCCESS

from netsquid_physlayer.detectors import QKDDetector


class DetectionProtocol(Protocol):
    """Protocol for the end node detection nodes.

    Measure qubits received from one (double-click scheme) or two (single-click scheme) repeater chains in the
    "X", "Y" or "Z" basis.

    Uses two photon entanglement for measurements in X and Y bases.

    Parameters
    ----------
    node : instance of :class:`~netsquid.nodes.node.Node`
        Node to apply protocol to.
    measurement_basis : str in ["X", "Y", "Z"] (optional)
        Measurement basis for the incoming photons. Default value is "Z".
    qkd_protocol : str in ["BB84", "six_state"] or `None` (optional)
        Protocol for quantum-key distribution to be used.
        If set to "BB84" then the measurement basis is chosen uniformly at random between "X" and "Z".
        If set to "six_state", the measurement basis is chosen uniformly at random between "X", "Y" and "Z".
        Default value is `None`, which implies that the measurement basis must be set
        by the fixed `measurement_basis` parameter.

    Notes
    ------
    Operation:
        Passes the external qubits to the QKD Detector and logs the corresponding measurement outcome.

    Example

    .. code-block:: python​

        from netsquid_ae.application_protocols import DetectionProtocol​

        # initialize protocol
        # Note: det_node is a fully set up detector node with a QDetector capable of measuring in "X", "Y"
        # and "Z" basis.

        detection_prot = DetectionProtocol(det_node, measurement_basis_basis="X")

        # start protocol

        detection_prot.start()​

        # run simulation

        netsquid.sim_run()

    """
    def __init__(self, node, measurement_basis="Z", qkd_protocol=None):
        super().__init__()
        self.node = node
        self.protocol_type = "Detection"
        self.qkd_det = node.subcomponents["QDet"]
        if qkd_protocol is not None:
            if qkd_protocol == "BB84":
                self.qkd_det.measurement_basis = get_random_state().choice(["X", "Z"])
            elif qkd_protocol == "six_state":
                self.qkd_det.measurement_basis = get_random_state().choice(["X", "Y", "Z"])
            else:
                raise ValueError(f"QKD protocol must be either 'BB84' or 'six_state', not {qkd_protocol}.")
        else:
            self.qkd_det.measurement_basis = measurement_basis
        self.qkd_protocol = qkd_protocol
        self.current_round = 0
        # prepare dictionary on node to collect data in
        self.node.cdata["qkd_outcomes"] = []
        self.node.cdata["round"] = []
        self.node.cdata["measured_modes"] = []
        self.node.cdata["successful_modes"] = []
        # Check whether one or two chains are used by looking at the number of ports of the node
        self.dual_chain = True if len(self.node.ports) == 4 else False
        self.port_1 = self.node.ports["IN_Chain_1"]
        if self.dual_chain:
            self.port_2 = self.node.ports["IN_Chain_2"]

    def start(self):
        """Starts the protocol. Binds input handler to detector ports."""
        super().start()
        self.port_1.forward_input(self.qkd_det.ports["qin0"])
        if self.dual_chain:
            self.port_2.forward_input(self.qkd_det.ports["qin1"])
        self.qkd_det.ports["cout0"].bind_output_handler(self._handle_qkd_outcome)

    def stop(self):
        """Stops the protocol. Dismisses Handlers and callbacks."""
        if not self.is_connected:
            raise RuntimeError("Cannot stop a protocol that is not fully connected")
        super().stop()
        # unbind handlers
        self.port_1.disconnect()
        if self.dual_chain:
            self.port_2.disconnect()
        self.qkd_det.ports["cout0"].bind_output_handler(None)

    def _handle_qkd_outcome(self, message):
        """Handles Message coming from the QKD Detector. Records the result and schedules appropriate events.

        Parameters
        ----------
        message : :class:~`netsquid.components.component.Message`
            Message received from the QKD Detector that contains the QKDOutcome.
        """
        qkd_outcomes = message.items
        successful_outcomes = []
        successful_modes = []

        assert len(qkd_outcomes) == len(self.node.cdata["measured_modes"][-1])
        for i in range(len(qkd_outcomes)):
            if qkd_outcomes[i].success:
                if qkd_outcomes[i].measurement_basis not in ["X", "Y", "Z"] or qkd_outcomes[i].outcome not in [0, 1]:
                    self._schedule_now(EVTYPE_ERROR)
                    break
                else:
                    # TODO: this currently only takes into account single chains
                    successful_outcomes.append(qkd_outcomes[i])
                    successful_modes.append(self.node.cdata["measured_modes"][-1][i])
        if not successful_outcomes:
            self._schedule_now(EVTYPE_FAIL)
        else:
            self._schedule_now(EVTYPE_SUCCESS)
        # Record outcome data on the node
        self.node.cdata["qkd_outcomes"].append(successful_outcomes)
        self.node.cdata["successful_modes"].append(successful_modes)
        # If a QKD protocol is used, randomly set the measurement basis
        if self.qkd_protocol == "BB84":
            self.qkd_det.measurement_basis = get_random_state().choice(["X", "Z"])
        elif self.qkd_protocol == "six_state":
            self.qkd_det.measurement_basis = get_random_state().choice(["X", "Y", "Z"])
        if "magic_override" not in self.node.cdata:
            # Note: "magic_override" is set by the magic_protocol on the end-node
            # current round is not being injected by magic, thus counting up by 1
            self.current_round += 1
            self.node.cdata["round"].append(self.current_round)

    def is_connected(self):
        """ Returns `True` if the DetectionProtocol is correctly setup."""
        return isinstance(self.qkd_det, QKDDetector) or isinstance(self.qkd_det, MultiPhotonQKDDetector)
