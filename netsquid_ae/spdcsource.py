import numpy as np

import netsquid as ns
import netsquid.qubits.qubitapi as qapi
from netsquid.components.component import Message
from netsquid.components.qsource import QSource, SourceStatus, QSourceTriggerError
from netsquid.qubits.state_sampler import StateSampler

__all__ = [
    "SPDCSource",
]


class SPDCSource(QSource):
    """Entangled photon pair source based on Spontaneous Parametric Down Conversion (SPDC).

    Photon pairs are distributed according to the distribution of the two-mode squeezing Hamiltonian
    and can be either produced in a presence_absence (see https://doi.org/10.1103/PhysRevA.43.3854, eq. (2.19) )
    or time_bin encoding  (see https://arxiv.org/abs/quant-ph/0207117 , eq. (2)) and also support frequency
    multiplexing capabilities.

    Parameters
    ----------
    name : str
        Name of source which is also used to label qubits.
    status : instance of :class:`~netsquid.components.qsource.SourceStatus`, optional
        The status with which to initialize the source. This status will also be (re)set when calling the reset method.
    frequency : float, optional
        Frequency at which the source creates qubits in Hertz.
    spectral_modes : int, optional
        Number of spectral modes of the source
    encoding : str
        The encoding in which the qubits are created, should be either ``time_bin`` or ``presence_absence``
    multi_photon : bool
        Parameter that denotes whether states with up to 3 photons are used
    emission_probability : list or None
        The probability of emitting a certain number of photons. This must be a list of length 4, where each element
        denotes the probability of generating a photon pair with the number of photons equal to its list index.
        If multi_photon is not used (False) then this list is truncated after n=1 and a warning is printed if the
        discarded values were != 0.
        If this is None, ``mu`` must be supplied.
    mu : float or None
        The mean photon number for the thermal distribution of the SPDC source. Must be supplied when
        'emission_probability' is None.
    Rate: float, optinoal
        Measurement of the rate of generated photon pairs in [Hz].
        Used for the Number of modes calculation.
    g2: float, optinoal
        Measurement of the second order correlation function, g2, for the heralded light from the source.
        Used for the Number of Modes calculation
    properties : dict or None, optional
        Extra properties associated with this component.

    """

    def __init__(self, name, status=SourceStatus.OFF, frequency=20e6, spectral_modes=1, encoding=None,
                 multi_photon=False, emission_probability=None, mu=None, g2=None, Rate=None, properties=None):
        self.name = name
        if not isinstance(spectral_modes, int) or spectral_modes < 1:
            if g2 is None or Rate is None or frequency is None:
                raise ValueError("Either num_multiplexing_modes has to be specified or the parameters g2, Rate "
                                 "and frequency have to be specified.")
            else:
                spectral_modes = self._compute_nummodes(frequency=frequency, Rate=Rate, g2=g2)
        self.spectral_modes = spectral_modes
        if encoding not in ["time_bin", "presence_absence"]:
            raise ValueError("Encoding should be either presence absence or time bin")
        self.encoding = encoding
        if (mu is not None and emission_probability is not None) or (mu is None and emission_probability is None):
            raise ValueError(f"Both 'mu':{mu} and 'emission_probability':{emission_probability} are specified "
                             f"or both are set to None. Please specify exactly one parameter and set the other"
                             f" to None.")
        if emission_probability is None:
            if not multi_photon:
                raise ValueError("If multi_photon=False mu must be None, not {}".format(mu))
            # Set emission probability according to thermal distribution
            if not isinstance(mu, float) or mu < 0:
                raise ValueError("A positive real number must be supplied as mean photon number mu")
            self.mu = mu
            self._thermal_distribution(mu)
        else:
            self.mu = None
            # A list of emission probabilities must be supplied
            if not isinstance(emission_probability, list) or len(emission_probability) != 4:
                raise ValueError("If mu is None, emission probability must be a "
                                 "list with length 4 and not {}.".format(emission_probability))
            # make sure probabilities add up to 1
            assert np.isclose(sum(emission_probability), 1.)
            if not multi_photon:
                if emission_probability[2:] != [0, 0]:
                    # Warn that emission probability is about to get truncated
                    print("WARNING: Not using multi_photon emission but specified emission_probabilities for n=3,4."
                          "These will be truncated and added to single photon emission.")
                    emission_probability = (emission_probability[0], sum(emission_probability[1:]), 0., 0.)
            # Take the square root to get probability amplitudes
            self.probability_amplitudes = np.sqrt(emission_probability)
        self.multi_photon = multi_photon
        spdc_state_sampler = self._set_two_mode_squeezed_state()
        # Send first half of each photon pair to the first output port and the other half to the other port
        qubit_mapping = [0] * int(self.num_qubits / 2) + [1] * int(self.num_qubits / 2)
        # Call the constructor of the QSource with the correct state sampler and qubit mapping
        super().__init__(name=name, frequency=frequency, properties=properties, num_ports=2, status=status,
                         state_sampler=spdc_state_sampler, qubit_port_mapping=qubit_mapping)

    def _compute_nummodes(self, frequency, g2, Rate=3.5e3):
        """"Computes the number of modes available for multiplexing following Hellebek et al. (unpublished).
        The equation is N_modes = 4/g^2 * Rate * Time. Rate is given by Barcelona, Time is 1/source_frequency.

        Parameters
        ----------
        Source freuquency: The frequency with which the source emmits in [Hz].
        Rate: The generation of pairs pr. unit time in [Hz]. Rate pr. Power of source is 3.5*10^3 [Hz/mW].
        g2: A measurement of the second order correlation function, g2, for the heralded light source.
        """

        Nm = int(np.floor(4/g2*Rate*1/frequency))
        if Nm < 0:
            raise ValueError("Number of modes is calculated to be a negative number. Check the parameters used.")
        else:
            return max(Nm, 1)

    def _thermal_distribution(self, mu):
        """Generate multi photon states with up to 3 output photons per pulse.
        Distribution can be found in https://arxiv.org/abs/quant-ph/0207117 , eq. (2) for 'time_bin' encoding and
        https://doi.org/10.1103/PhysRevA.43.3854, eq. (2.19) for 'presence_absence' and inserting mu = sinh^2(r).

        Parameters
        ----------
        mu : float
            Mean photon number.
        """
        self.probability_amplitudes = [0] * 4
        for n in range(3):
            if self.encoding == 'time_bin':
                self.probability_amplitudes[n] = (n + 1) * mu ** n / ((mu + 1) ** (n + 2))
            else:
                self.probability_amplitudes[n] = mu ** n / ((mu + 1) ** (n + 1))
        # Truncate the higher order probabilities
        self.probability_amplitudes[3] = 1 - np.sum(self.probability_amplitudes)
        self.probability_amplitudes = np.sqrt(self.probability_amplitudes)

    def _set_two_mode_squeezed_state(self):
        """Sets the probability amplitudes of an SPDC source.
        Can be either a realistic source with up to 3 photons emitted, or
        a perfect photon source with a certain emission probability.
        """
        if self.multi_photon:
            if self.encoding == "presence_absence":
                # Send state p_0|00> + p_1|11> + p_2|22> + p_3|33>
                self.num_qubits = 4
                ampl = [0j] * (2 ** self.num_qubits)
                ampl[0] = self.probability_amplitudes[0]
                ampl[5] = self.probability_amplitudes[1]
                ampl[10] = self.probability_amplitudes[2]
                ampl[15] = self.probability_amplitudes[3]
            else:
                # Send state p_0|00,00;00,00> + p_1(|01,00;01,00> - |00,01;00,01>)
                # + p_2(|10,00;10,00> - |01,01;01,01> + |00,10;00,10>)
                # + p_3(|11,00;11,00> - |10,01;10,01> + |01,10;01,10> + |00,11;00,11>)
                # The ordering we use for the time bin encoding is [lE, lL; rE, rL], where l and r denote qubits that
                # are sent to the left and right, and E and L denote the early and late time windows
                self.num_qubits = 8
                ampl = [0j] * (2 ** self.num_qubits)
                ampl[0] = self.probability_amplitudes[0]  # 0 photon pairs
                ampl[68] = self.probability_amplitudes[1] / np.sqrt(2)  # 1 lE, 1 rE
                ampl[17] = -self.probability_amplitudes[1] / np.sqrt(2)  # 1 lL, 1 rL
                ampl[136] = self.probability_amplitudes[2] / np.sqrt(3)  # 2 lE, 2 rE
                ampl[85] = -self.probability_amplitudes[2] / np.sqrt(3)  # 1 lE, 1 lL, 1 rE, 1 rL
                ampl[34] = self.probability_amplitudes[2] / np.sqrt(3)  # 2 lL, 2 rL
                ampl[204] = self.probability_amplitudes[3] / 2  # 3 lE, 3 rE
                ampl[153] = -self.probability_amplitudes[3] / 2  # 2 lE, 1 lL, 2 rE, 1 rL
                ampl[102] = self.probability_amplitudes[3] / 2  # 1 lE, 2 lL, 1 rE, 2 rL
                ampl[51] = -self.probability_amplitudes[3] / 2  # 3 lL, 3 rL
        else:
            self.num_qubits = 2
            ampl = [0j] * (2 ** self.num_qubits)
            if self.encoding == "presence_absence":
                # Send out state (1-p)|00> + p|11>
                ampl[0] = self.probability_amplitudes[0]
                ampl[3] = self.probability_amplitudes[1]
            else:
                # NOTE: The interpretation of the pair probabilities is different in this case! We either send an
                # equal superposition state between the early and late time bins or we sent None, which is done in
                # _generate_new_pulse()
                ampl[0] = 1 / np.sqrt(2)
                ampl[3] = 1 / np.sqrt(2)

        return StateSampler(np.array([ampl]).T)

    def _generate_new_pulse(self):
        """"Overrides class method to support frequency multiplexed photon pair generation."""
        # Generate a new pulse.
        if self.status == SourceStatus.OFF:
            return
        curr_time = ns.sim_time()
        if not np.isclose(curr_time, self._busy_until) and curr_time < self._busy_until:
            raise QSourceTriggerError("QSource is triggered while busy generating a pulse (t_now = {}. busy until t = "
                                      "{}.".format(curr_time, self._busy_until))
        # Use a single call to prep_delay to make sure the same emission_delay is passed to the emission noise model:
        local_prep_delay = self.prep_delay
        self._busy_until = curr_time + local_prep_delay
        # Schedule emission.
        event = self._schedule_after(local_prep_delay, self._EVT_NEWPULSE)
        # Generate pulse messages.
        self._pulse_count += 1
        sysname = "{}-#{}-".format(self.name, self._pulse_count)
        qs_repr, _, _ = self._state_sampler.sample()
        qubits = []
        for m in range(self.spectral_modes):
            if (not self.multi_photon and self.encoding == "time_bin") and \
                    ns.util.simtools.get_random_state().rand() < self.probability_amplitudes[0]:
                # Send None to both output ports
                qubits.extend([None, None])
            else:
                qubits_per_mode = qapi.create_qubits(self._state_sampler.num_qubits, no_state=True,
                                                     system_name="{}mode-{}Q".format(sysname, m + 1))
                qapi.assign_qstate(qubits_per_mode, qs_repr)
                if not (self.encoding == "time_bin" and not self.multi_photon):
                    # Set the qubits to number states except for non-multi photon time-bin encoding
                    for q in qubits_per_mode:
                        q.is_number_state = True
                qubits.extend(qubits_per_mode)
        # Distribute qubits over ports according to qubit_port_mapping
        qubits_per_port = {}
        self._qubits_per_event[event] = qubits
        for qubit_idx, qubit in enumerate(qubits):
            # Each mode has the same qubit port mapping (hence the modulo)
            port_idx = self._qubit_port_mapping[qubit_idx % self.state_sampler.num_qubits]
            qubits_per_port.setdefault(self._output_port_names[port_idx], []).append(qubit)
        # Use only the emission delay part of the prep_delay to pass to emission noise model.
        emission_delay = local_prep_delay - self.properties["trigger_delay"]
        for port_name in self._output_port_names:
            self._port_messages[(event, port_name)] = Message(qubits_per_port[port_name],
                                                              emission_time=curr_time + local_prep_delay,
                                                              emission_delay=emission_delay)
