import logging
import numpy as np
from scipy.linalg import sqrtm

import netsquid
from netsquid.util.simlog import logger
from netsquid.qubits.qubit import Qubit
from netsquid.util.simtools import sim_time
from netsquid.qubits import operators as ops
from netsquid.qubits import qubitapi as qapi
from netsquid.qubits import opmath, qformalism
from netsquid.qubits.ketstates import BellIndex
from netsquid.components.qdetector import QuantumDetectorError

from netsquid_ae.multi_photon_tools import get_multiphoton_projectors

from netsquid_physlayer.detectors import BSMOutcome, BSMDetector, QKDDetector, ModeError

__all__ = [
    'MultiPhotonBSMDetector',
    'MultiPhotonQKDDetector',
]


class MultiPhotonBSMDetector(BSMDetector):
    r"""Component that performs a photon-based probabilistic Bell-state measurement (BSM) using linear optics.
    Supports only multi-photon states, where the photon number is encoded in two qubits in binary.
    For example, the state |01> represent one photon, while the state |11> represents three photons.

    Schematic of setup:

    .. code-block:: text

                --------------------
                |   /|             |
        cout0 : > A( |===     =====< : qin0
                |   \|   \\ //     |
                |       ------ BS  |
                |   /|   // \\     |
        cout1 : > B( |===     =====< : qin1
                |   \|             |
                --------------------

    Contains a single beam splitter (BS), which interferes incoming photons, and two photon detectors A and B.
    Two input ports `qin0` and `qin1` handle incoming qubits,
    after which a (possibly noisy) measurement is done.
    The measurement outcome, successful mode (if any) and optional additional information are transmitted
    using output ports `cout0` and `cout1`.
    Measurements are only possible for multi-qubit states in either presence-absence or time-bin encoding, and
    only performed when the two messages (containing the photon states) arrive simultaneously.
    The photon encoding is fixed and must be set in the constructor.
    The number of multiplexing modes is automatically identified by the number of qubits in the received message.

    Parameters
    ----------
    name : str
        Name of detector.
    p_dark : float, optional
        Dark-count probability, i.e. probability of measuring a photon while no photon was present, per detector.
        Default value 0.
    det_eff : float, optional
        Efficiency per detector, i.e. the probability of detecting an incoming photon. Default value 1.
    visibility : float, optional
        Visibility of the Hong-Ou-Mandel dip, also referred to as the photon indistinguishability. Default value 1.
    num_resolving : bool, optional
        If set to True, photon-number-resolving detectors will be used, otherwise threshold detectors.
        Default value False.
    encoding : str in ['presence_absence', 'time_bin'], optional
        The photon encoding of the source, which is used to determine how the qubits should be grouped for a
        measurement. Default value 'presence_absence'.
    force_povm_outcome : str or None, optional
        String representation of the forced measurement outcome or None if outcome should not be forced.
    allow_multiple_successful_modes : bool, optional
        If set to True, multiple modes can be successful otherwise the detector stops measuring after the first
        successful mode.
    dead_time : float, optional
        Time after the measurement in which the detectors can't be triggered.
        It is possible for qubits to propagate through the system during the dead time.
        Qubits that would arrive at the detectors during their dead_time are discarded when they enter the system.
    """

    def __init__(self, name, p_dark=0., det_eff=1., visibility=1., num_resolving=False, encoding="presence_absence",
                 force_povm_outcome=None, allow_multiple_successful_modes=False, dead_time=0):
        super().__init__(name, p_dark=p_dark, det_eff=det_eff, visibility=visibility, num_resolving=num_resolving,
                         allow_multiple_successful_modes=allow_multiple_successful_modes, dead_time=dead_time)
        self.encoding = encoding
        if self.encoding == 'presence_absence':
            self._grouping = 2
        elif self.encoding == 'time_bin':
            self._grouping = 4
        else:
            raise ValueError(f"Photon encoding must be either 'presence_absence' or 'time_bin', not {encoding}.")
        self.force_povm_outcome = force_povm_outcome
        # set container for probabilities
        # TODO: change this to not store information locally but attach probability to measurement outcome directly
        self._forced_probabilities = []

    @property
    def encoding(self):
        return self._encoding

    @encoding.setter
    def encoding(self, value):
        try:
            _ = self._encoding
            raise QuantumDetectorError("Cannot change encoding after initialization of "
                                       f"MultiPhotonBSMDetector {self.name}.")
        except AttributeError:
            if value not in ['presence_absence', 'time_bin']:
                raise ValueError(f"Encoding must be either 'presence_absence' or 'time_bin', not {value}")
            self._encoding = value

    @property
    def force_povm_outcome(self):
        """str: String describing the measurement outcome to be forced, otherwise None."""
        return self._force_povm_outcome

    @force_povm_outcome.setter
    def force_povm_outcome(self, label):
        """Setter for the forced POVM outcome label.

        Parameters
        ----------
        label : float
            New label to be used for forced magic."""
        # check DM formalism is set
        if label is not None \
                and qformalism.get_qstate_formalism() not in [qformalism.QFormalism.DM, qformalism.QFormalism.SPARSEDM]:
            raise ValueError(f"Trying to use unsupported qformalism {qformalism.get_qstate_formalism()} with forced "
                             f"magic. Please use either DM ot SPARSEDM formalism.")
        if label is not None:
            # check if provided label has correct length
            if self.encoding == "presence_absence" and len(label) != 2:
                raise ValueError(F"Trying to force POVM of incorrect length {len(label)}, should be "
                                 F"string of length 2 for {self.encoding} encoding.")
            elif self.encoding == "time_bin" and len(label) != 4:
                raise ValueError(F"Trying to force POVM of incorrect length {len(label)}, should be "
                                 F"string of length 4 for {self.encoding} encoding.")
        self._force_povm_outcome = label

    def _set_meas_operators_with_beamsplitter(self):
        """Sets the measurement (Kraus) operators for getting a certain click pattern with visibility,
        detector efficiency and dark counts included.
        First we get the projectors for having a certain number of photons arriving at one of the detectors,
        provided the Hong-Ou-Mandel dip visibility at hand.
        Then we include detection efficiency and dark counts to get a full set of POVMs, after which we find a
        representation in terms of Kraus operators by taking the matrix square root.
        """
        # Get the (28) multi-photon projectors, provided the Hong-Ou-Mandel interference visibility
        projectors = get_multiphoton_projectors(self.visibility)

        no_photons_at_both = np.zeros([16, 16], dtype=complex)
        one_photon_at_A_none_at_B = np.zeros([16, 16], dtype=complex)
        multiple_photons_at_A_none_at_B = np.zeros([16, 16], dtype=complex)
        one_photon_at_B_none_at_A = np.zeros([16, 16], dtype=complex)
        multiple_photons_at_B_none_at_A = np.zeros([16, 16], dtype=complex)
        at_least_one_photon_at_both = np.zeros([16, 16], dtype=complex)

        for m in range(7):
            for n in range(7 - m):
                proj = projectors[(m, n)]
                no_click_m, no_click_n = self._prob_no_photon_detected(m), self._prob_no_photon_detected(n)
                one_photon_m = self._prob_exactly_one_photon_detected(m)
                one_photon_n = self._prob_exactly_one_photon_detected(n)
                # No photons get detected at either detector. This can only occur if
                # m photons arrive at A, which all get lost and no dark count occurs, and
                # n photons arrive at B, which all get lost and no dark count occurs.
                no_photons_at_both += no_click_m * no_click_n * proj
                # Exactly one photon gets detected at A, while none are detected at B. This can only occur if
                # m photons arrive at A, of which exactly 1 gets detected, and
                # n photons arrive at B, after which no photons are detected
                one_photon_at_A_none_at_B += one_photon_m * no_click_n * proj
                # Multiple photon gets detected at A, while none are detected at B. This can only occur if
                # m photons arrive at A, after which two or more are detected, and
                # n photons arrive at B, after which no photons are detected
                # Here we use the fact that Pr(X >= 2) = 1 - Pr(X = 1) - Pr(X = 0).
                multiple_photons_at_A_none_at_B += (1 - no_click_m - one_photon_m) * no_click_n * proj
                # Due to symmetry, we can follow the same reasoning when A and B are switched
                one_photon_at_B_none_at_A += one_photon_n * no_click_m * proj
                multiple_photons_at_B_none_at_A += (1 - no_click_n - one_photon_n) * no_click_m * proj
                # Finally, we consider the case when at least one photon arrives at both detectors.
                # This can only occur if
                # m photons arrive at detector A, of which at least one gets detected, and
                # n photons arrive at detector B, of which at least one gets detected.
                # Here we use the fact that Pr(X >= 1) = 1 - Pr(X = 0).
                at_least_one_photon_at_both += (1 - no_click_m) * (1 - no_click_n) * proj

        if not self.num_resolving:
            # In this case we cannot distinguish between one or two photons getting detected
            n_00 = ops.Operator("n_00", sqrtm(no_photons_at_both))
            n_10 = ops.Operator("n_10", sqrtm(one_photon_at_A_none_at_B + multiple_photons_at_A_none_at_B))
            n_01 = ops.Operator("n_01", sqrtm(one_photon_at_B_none_at_A + multiple_photons_at_B_none_at_A))
            n_11 = ops.Operator("n_11", sqrtm(at_least_one_photon_at_both))

            self._meas_operators = [n_00, n_10, n_01, n_11]
        else:
            # We have two separate operators for one or multiple photons arriving at one the detectors, allowing us to
            # identify false positives during entanglement generation
            n_00 = ops.Operator("n_00", sqrtm(no_photons_at_both))
            n_10 = ops.Operator("n_10", sqrtm(one_photon_at_A_none_at_B))
            n_01 = ops.Operator("n_01", sqrtm(one_photon_at_B_none_at_A))
            n_11 = ops.Operator("n_11", sqrtm(at_least_one_photon_at_both))
            n_20 = ops.Operator("n_20", sqrtm(multiple_photons_at_A_none_at_B))
            n_02 = ops.Operator("n_02", sqrtm(multiple_photons_at_B_none_at_A))

            self._meas_operators = [n_00, n_10, n_01, n_11, n_20, n_02]

    def preprocess_inputs(self):
        """Functionality incorporated in `measure()`"""
        pass

    def postprocess_outputs(self, dict_port_outcomes):
        """Functionality incorporated in `measure()`"""
        pass

    def measure(self):
        """Perform a measurement on the received qubits.

        Applies preprocessing to the qubits before the measurement, and
        applies postprocessing to the measured classical outcomes.
        After the measurement all qubits are discarded.

        Override of superclass method to support multiplexed measurements and to select the correct group of qubits.
        If some of the parameters have changed, the measurement operators are reset.
        First the correct qubits per mode are selected.
        If the qubits are in presence-absence encoding, we can measure the qubits directly,
        but for time-bin encoding we perform the double-click scheme
        and measure the photons in each time window separately.
        All the raw measurement outcomes (int) are transformed to a BSMOutcome.
        Additionally, the successful mode (if any) is transmitted as the meta information
        of the classical output message.
        """
        if self._parameter_changed:
            self._set_meas_operators_with_beamsplitter()
            self._parameter_changed = False
        forced_label_1, forced_label_2 = None, None
        if self._force_povm_outcome is not None:
            # extract labels
            if self.encoding == "presence_absence":
                # presence_absence encoding
                forced_label_1 = self._force_povm_outcome
            else:
                # time_bin encoding
                forced_label_1 = self._force_povm_outcome[:2]
                forced_label_2 = self._force_povm_outcome[-2:]
        # Get all qubits per port.
        q_lists = [self._qubits_per_port[port_name] for port_name in self._input_port_names]
        num_modes = len(q_lists[0]) // self._grouping
        if num_modes != len(q_lists[1]) // self._grouping:
            raise ModeError(f"{netsquid.sim_time()} : Imbalanced number of modes. MultiPhotonBSMDetector {self.name} "
                            f"received {len(q_lists[0])} qubits from one side, while the other side received "
                            f"{len(q_lists[1])} qubits.")
        arrival_times, qubits, __ = zip(*[item for q_list in q_lists for item in q_list])
        # Override for multiplexed measurement
        outcomes = [BSMOutcome(success=False)]
        for mode in range(num_modes):
            # Only perform a measurement if the two arrival times are exactly equal
            arrival_time_left = arrival_times[self._grouping * mode: self._grouping * (mode + 1)]
            arrival_time_right = \
                arrival_times[self._grouping * (num_modes + mode):self._grouping * (num_modes + mode + 1)]
            if arrival_time_left != arrival_time_right:
                raise QuantumDetectorError(f"Arrival times of qubits not equal.\nLeft qubit arrived at "
                                           f"{arrival_time_left}, while right qubit arrived at {arrival_time_right}.")
            # Perform pair-wise measurement for a single mode
            qubits_left = qubits[self._grouping * mode:self._grouping * (mode + 1)]
            qubits_right = qubits[self._grouping * (num_modes + mode):self._grouping * (num_modes + mode + 1)]
            if self._encoding == "presence_absence":
                outcome = self._perform_measurement(qubits=qubits_left + qubits_right, force_qubit_label=forced_label_1)
            else:
                # Time-bin encoding, split the qubits up into the four individual ones
                [q_left1, q_left2, q_left3, q_left4] = qubits_left
                [q_right1, q_right2, q_right3, q_right4] = qubits_right
                # Now combine the correct qubits to measure in the early and late time windows
                outcome_early = self._perform_measurement(qubits=[q_left1, q_left2] + [q_right1, q_right2],
                                                          force_qubit_label=forced_label_1)
                outcome_late = self._perform_measurement(qubits=[q_left3, q_left4] + [q_right3, q_right4],
                                                         force_qubit_label=forced_label_2)
                if self._force_povm_outcome is not None:
                    # format probabilities and labels
                    probability = self._forced_probabilities[-2][1] * self._forced_probabilities[-1][1]
                    forced_label = self._forced_probabilities[-2][0] + self._forced_probabilities[-1][0]
                    del self._forced_probabilities[-1]
                    del self._forced_probabilities[-1]
                    self._forced_probabilities.append((forced_label, probability))
                # A measurement in the double-click scheme is successful if and only if there is a single detector click
                # in both modes
                if outcome_early.success and outcome_late.success:
                    # If the same detector clicked twice the state is presumed to be projected onto
                    # `BellIndex.PSI_PLUS`, otherwise it's `BellIndex.PSI_MINUS`.
                    if outcome_early.bell_index == outcome_late.bell_index:
                        outcome = BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)
                    else:
                        outcome = BSMOutcome(success=True, bell_index=BellIndex.PSI_MINUS)
                else:
                    # Not both modes are successful, so the overall outcome is unsuccessful
                    outcome = BSMOutcome(success=False)
            if outcome.success:
                # Store this outcome and the corresponding mode to be transmitted back in a classical message
                # and break to for-loop so that the rest of the qubits are not measured
                if not outcomes[0].success:
                    # first success remove the fail placeholder
                    outcomes = []
                outcomes.append(outcome)
                if self._meta["successful_modes"] == [None]:
                    self._meta["successful_modes"] = []
                self._meta["successful_modes"].append(mode)
                if logger.isEnabledFor(logging.DEBUG):
                    logger.debug(f"Successful BSM in BSMDetector {self.name} at time {sim_time()}"
                                 f"with outcome {outcome} and successful mode {mode}.")
                if not self._allow_multiple_successful_modes:
                    break
        # Discard all the qubits
        [qapi.discard(qubit) for qubit in qubits if qubit is not None]
        self._qubits_per_port.clear()
        # Take the measurement outcomes and put the outcomes on the ports
        outcomes_per_port = {port_name: outcomes[:] for port_name in self._output_port_names}
        self.inform(outcomes_per_port)
        # Reset the meta information
        self._meta["successful_modes"] = [None]

    def _perform_measurement(self, qubits, force_qubit_label=None):
        """Helper method that manually performs a measurement in case of forced magic.

        Parameters
        ----------
        qubits : list of :obj:`~netsquid.qubits.Qubit`
            List of qubits to be measured.
        force_qubit_label : str or None (optional)
            The qubit label to be used in forced magic.
            Default value is None, which corresponds to a regular qubit measurement without forced magic.

        Returns
        -------
            _ : BSMOutcome
            The measurement outcome transformed to a BSMOutcome.
        """
        if force_qubit_label is None:
            # Measure the qubits with the measurement operators that were set and discard them
            outcome = qapi.gmeasure(qubits, self._meas_operators)[0]
        else:
            # Force measurement outcome
            operators = self._meas_operators
            forced_operator = None
            for op in operators:
                if op.name[-2:] == force_qubit_label:
                    forced_operator = op
            if len(qubits) > 1:
                qapi.combine_qubits(qubits)
            qs = qubits[0].qstate
            qubit_indices = [qs.indices[qubit.name] for qubit in qubits]
            # act with forced operator
            if forced_operator is None:
                raise ValueError(f"No forced operator was selected for label {force_qubit_label}")
            if qformalism.get_qstate_formalism() == qformalism.QFormalism.DM:
                # DM formalism
                dm = opmath.operate_dm(qs.qrepr.dm, forced_operator, qubit_indices)
                # calculate probability
                p = np.real(np.trace(dm))
                # cast nan to zero
                p = np.nan_to_num(p)
                # replace dm if p is not zero
                if p != 0.:
                    qs.qrepr = qformalism.DenseDMRepr(dm / p)
            elif qformalism.get_qstate_formalism() == qformalism.QFormalism.SPARSEDM:
                # SPARSEDM formalism
                dm = opmath.operate_sparse_dm(qs.qrepr.sparse_dm, forced_operator, qubit_indices)
                # calculate probability
                p = np.real(dm.diagonal().sum())
                # cast nan to zero
                p = np.nan_to_num(p)
                # replace dm
                if p != 0.:
                    qs.qrepr = qformalism.SparseDMRepr(dm / p)
            else:
                raise ValueError(f"Wrong quantum formalism {qformalism.get_qstate_formalism()} encountered at"
                                 f"MultiPhotonBSMDetector {self.name}. Must be either 'DM' or 'SPARSEDM'.")

            # always set outcome to success so protocols herald success and state is picked up by datacollector
            outcome = 1
            # TODO: maybe store these as a dict?
            self._forced_probabilities.append((force_qubit_label, p))
        return self._measoutcome2bsmoutcome[outcome]


class MultiPhotonQKDDetector(QKDDetector):
    r"""Detector able to perform multi-photon-based qubit measurements in three bases (X, Y and Z).
     Outcomes can be used to extract a secret key from entangled states used for quantum key distribution (QKD).

    Schematic of setup:

    .. code-block:: text

        Without beam splitter (Z basis)   With beam splitter (BS) and phase shifter (PS)(X and Y bases)
                ----------------                      -----------------------
                |   /|         |                      |   /|           PS   |
                | A( |=========< : qin0               | A( |===     ==|xx|==< : qin0
                |   \|         |                      |   \|   \\ //        |
        cout0 : >              |              cout0 : >       ------ BS     |
                |   /|         |                      |   /|   // \\        |
                | B( |=========< : qin1               | B( |===     ========< : qin1
                |   \|         |                      |   \|                |
                ----------------                      -----------------------

    Able to perform measurements in both presence-absence and time-bin encodings.
    If time-bin encoding is used, only the `qin0` port must be used to transmit a single four-qubit state.
    These are then split into a pair of two-qubit states in presence-absence encoding.
    Otherwise, both output ports must be used to transmit two two-qubit states.
    Depending on the measurement basis, a different setup is used.
    Measurements in the Z basis are performed by directly measuring the incoming qubits.
    For measurements in the X basis, the incoming qubits are first interfered on a beam splitter.
    For measurements in the Y basis, a phase shift is additionally applied to one of the two qubits.
    The measurement outcome is transmitted using a single classical output port `cout0`.

    Parameters
    ----------
    name : str
        Name of detector.
    measurement_basis : str in ['X', 'Y', 'Z'], optional
        Measurement basis in which the incoming qubits are measured.
    p_dark : float, optional
        Dark-count probability, i.e. probability of measuring a photon while no photon was present, per detector.
    det_eff : float, optional
        Efficiency per detector, i.e. the probability of detecting an incoming photon.
    visibility : float, optional
        Visibility of the Hong-Ou-Mandel dip, also referred to as the photon indistinguishability.
    num_resolving : bool, optional
        If set to True, photon-number-resolving detectors will be used, otherwise threshold detectors.
    encoding : str in ['presence_absence', 'time_bin'], optional
        The photon encoding of the source, which is used to determine how the qubits should be grouped for a
        measurement. Default value 'presence_absence'.
    dead_time : float, optional
        Time after the measurement in which the detectors can't be triggered.
        It is possible for qubits to propagate through the system during the dead time.
        Qubits that would arrive at the detectors during their dead_time are discarded when they enter the system.

    """
    def __init__(self, name, measurement_basis="Z", p_dark=0., det_eff=1., visibility=1., num_resolving=False,
                 encoding="presence_absence", dead_time=0):
        super().__init__(name, p_dark=p_dark, det_eff=det_eff, visibility=visibility, num_resolving=num_resolving,
                         measurement_basis=measurement_basis, dead_time=dead_time)
        self.encoding = encoding
        if self.encoding == 'presence_absence':
            self._grouping = 2
        elif self.encoding == 'time_bin':
            self._grouping = 4
        else:
            raise ValueError(f"Photon encoding must be either 'presence_absence' or 'time_bin', not {encoding}.")
        # Set the phase shifter to be used for measurements in the Y basis.
        self._phase_shifter = ops.Operator("two_qubit_phase_shifter",
                                           np.array([[1, 0, 0, 0], [0, 1j, 0, 0], [0, 0, 1j, 0], [0, 0, 0, 1j]]))

    @property
    def encoding(self):
        return self._encoding

    @encoding.setter
    def encoding(self, value):
        try:
            _ = self._encoding
            raise QuantumDetectorError("Cannot change encoding after initialization of "
                                       f"MultiPhotonBSMDetector {self.name}.")
        except AttributeError:
            if value not in ['presence_absence', 'time_bin']:
                raise ValueError(f"Encoding must be either 'presence_absence' or 'time_bin', not {value}")
            self._encoding = value

    def preprocess_inputs(self):
        """Functionality incorporated in `measure()`"""
        pass

    def postprocess_outputs(self, dict_port_outcomes):
        """Functionality incorporated in `measure()`"""
        pass

    def _set_meas_operators_with_beamsplitter(self):
        """Sets the measurement (Kraus) operators for getting a certain click pattern with visibility,
        detector efficiency and dark counts included.
        First we get the projectors for having a certain number of photons arriving at one of the detectors,
        provided the Hong-Ou-Mandel dip visibility at hand.
        Then we include detection efficiency and dark counts to get a full set of POVMs, after which we find a
        representation in terms of Kraus operators by taking the matrix square root.
        """
        # Get the (28) multi-photon projectors, provided the Hong-Ou-Mandel interference visibility
        projectors = get_multiphoton_projectors(self.visibility)
        # Initialize POVMs
        no_photons_at_both = np.zeros([16, 16], dtype=complex)
        one_photon_at_A_none_at_B = np.zeros([16, 16], dtype=complex)
        multiple_photons_at_A_none_at_B = np.zeros([16, 16], dtype=complex)
        one_photon_at_B_none_at_A = np.zeros([16, 16], dtype=complex)
        multiple_photons_at_B_none_at_A = np.zeros([16, 16], dtype=complex)
        at_least_one_photon_at_both = np.zeros([16, 16], dtype=complex)
        for m in range(7):
            for n in range(7 - m):  # At most six photons can arrive simultaneously at a single detector
                proj = projectors[(m, n)]
                no_click_m, no_click_n = self._prob_no_photon_detected(m), self._prob_no_photon_detected(n)
                one_photon_m = self._prob_exactly_one_photon_detected(m)
                one_photon_n = self._prob_exactly_one_photon_detected(n)
                # No photons get detected at either detector. This can only occur if
                # m photons arrive at A, which all get lost and no dark count occurs, and
                # n photons arrive at B, which all get lost and no dark count occurs.
                no_photons_at_both += no_click_m * no_click_n * proj
                # Exactly one photon gets detected at A, while none are detected at B. This can only occur if
                # m photons arrive at A, of which exactly 1 gets detected, and
                # n photons arrive at B, after which no photons are detected
                one_photon_at_A_none_at_B += one_photon_m * no_click_n * proj
                # Multiple photon gets detected at A, while none are detected at B. This can only occur if
                # m photons arrive at A, after which two or more are detected, and
                # n photons arrive at B, after which no photons are detected
                # Here we use the fact that Pr(X >= 2) = 1 - Pr(X = 1) - Pr(X = 0).
                multiple_photons_at_A_none_at_B += (1 - no_click_m - one_photon_m) * no_click_n * proj
                # Due to symmetry, we can follow the same reasoning when A and B are switched
                one_photon_at_B_none_at_A += one_photon_n * no_click_m * proj
                multiple_photons_at_B_none_at_A += (1 - no_click_n - one_photon_n) * no_click_m * proj
                # Finally, we consider the case when at least one photon arrives at both detectors.
                # This can only occur if
                # m photons arrive at detector A, of which at least one gets detected, and
                # n photons arrive at detector B, of which at least one gets detected.
                # Here we use the fact that Pr(X >= 1) = 1 - Pr(X = 0).
                at_least_one_photon_at_both += (1 - no_click_m) * (1 - no_click_n) * proj

        if not self.num_resolving:
            # In this case we cannot distinguish between one or two photons getting detected
            n_00 = ops.Operator("n_00", sqrtm(no_photons_at_both))
            n_10 = ops.Operator("n_10", sqrtm(one_photon_at_A_none_at_B + multiple_photons_at_A_none_at_B))
            n_01 = ops.Operator("n_01", sqrtm(one_photon_at_B_none_at_A + multiple_photons_at_B_none_at_A))
            n_11 = ops.Operator("n_11", sqrtm(at_least_one_photon_at_both))

            self._meas_operators = [n_00, n_10, n_01, n_11]
        else:
            # We have two separate operators for one or multiple photons arriving at one the detectors, allowing us to
            # identify false positives during entanglement generation
            n_00 = ops.Operator("n_00", sqrtm(no_photons_at_both))
            n_10 = ops.Operator("n_10", sqrtm(one_photon_at_A_none_at_B))
            n_01 = ops.Operator("n_01", sqrtm(one_photon_at_B_none_at_A))
            n_11 = ops.Operator("n_11", sqrtm(at_least_one_photon_at_both))
            n_20 = ops.Operator("n_20", sqrtm(multiple_photons_at_A_none_at_B))
            n_02 = ops.Operator("n_02", sqrtm(multiple_photons_at_B_none_at_A))

            self._meas_operators = [n_00, n_10, n_01, n_11, n_20, n_02]

    def _set_meas_operators_without_beamsplitter(self):
        """Sets the measurement (Kraus) operators for getting a certain click pattern for a setup without a
        beam splitter, but with detector efficiency and dark counts included.
        First we get the projectors for having a certain number of photons arriving at one of the detectors.
        Then we include detection efficiency and dark counts to get a full set of POVMs, after which we find a
        representation in terms of Kraus operators by taking the matrix square root.
        """
        # Initialize projectors
        projectors = {(m, n): np.zeros((16, 16), dtype=complex) for m in range(4) for n in range(4)}
        # Now set the correct index for a number of incoming photons to 1
        projectors[(0, 0)][0, 0], projectors[(1, 0)][4, 4], projectors[(2, 0)][8, 8] = 1, 1, 1
        projectors[(3, 0)][12, 12], projectors[(0, 1)][1, 1], projectors[(1, 1)][5, 5] = 1, 1, 1
        projectors[(2, 1)][9, 9], projectors[(3, 1)][13, 13], projectors[(0, 2)][2, 2] = 1, 1, 1
        projectors[(1, 2)][6, 6], projectors[(2, 2)][10, 10], projectors[(3, 2)][14, 14] = 1, 1, 1
        projectors[(0, 3)][3, 3], projectors[(1, 3)][7, 7], projectors[(2, 3)][11, 11] = 1, 1, 1
        projectors[(3, 3)][15, 15] = 1
        # Initialize POVMs
        no_photons_at_both = np.zeros([16, 16], dtype=complex)
        one_photon_at_A_none_at_B = np.zeros([16, 16], dtype=complex)
        multiple_photons_at_A_none_at_B = np.zeros([16, 16], dtype=complex)
        one_photon_at_B_none_at_A = np.zeros([16, 16], dtype=complex)
        multiple_photons_at_B_none_at_A = np.zeros([16, 16], dtype=complex)
        at_least_one_photon_at_both = np.zeros([16, 16], dtype=complex)
        for m in range(4):
            for n in range(4):
                no_click_m = self._prob_no_photon_detected(m)
                no_click_n = self._prob_no_photon_detected(n)
                one_photon_m = self._prob_exactly_one_photon_detected(m)
                one_photon_n = self._prob_exactly_one_photon_detected(n)
                # No photons get detected at either detector. This can only occur if
                # m photons arrive at A, which all get lost and no dark count occurs, and
                # n photons arrive at B, which all get lost and no dark count occurs.
                no_photons_at_both += no_click_m * no_click_n * projectors[(m, n)]
                # Exactly one photon gets detected at A, while none are detected at B. This can only occur if
                # m photons arrive at A, of which exactly 1 gets detected, and
                # n photons arrive at B, after which no photons are detected
                one_photon_at_A_none_at_B += one_photon_m * no_click_n * projectors[(m, n)]
                # Multiple photon gets detected at A, while none are detected at B. This can only occur if
                # m photons arrive at A, after which two or more are detected, and
                # n photons arrive at B, after which no photons are detected
                # Here we use the fact that Pr(X >= 2) = 1 - Pr(X = 1) - Pr(X = 0).
                multiple_photons_at_A_none_at_B += (1 - no_click_m - one_photon_m) * no_click_n * projectors[(m, n)]
                # Due to symmetry, we can follow the same reasoning when A and B are switched
                one_photon_at_B_none_at_A += one_photon_n * no_click_m * projectors[(m, n)]
                multiple_photons_at_B_none_at_A += (1 - no_click_n - one_photon_n) * no_click_m * projectors[(m, n)]
                # Finally, we consider the case when at least one photon arrives at both detectors.
                # This can only occur if
                # m photons arrive at detector A, of which at least one gets detected, and
                # n photons arrive at detector B, of which at least one gets detected.
                # Here we again use the fact that Pr(X >= 1) = 1 - Pr(X = 0).
                at_least_one_photon_at_both += (1 - no_click_m) * (1 - no_click_n) * projectors[(m, n)]

        if not self.num_resolving:
            # In this case we cannot distinguish between one or two photons getting detected
            n_00 = ops.Operator("n_00_no_bs", sqrtm(no_photons_at_both))
            n_10 = ops.Operator("n_10_no_bs", sqrtm(one_photon_at_A_none_at_B + multiple_photons_at_A_none_at_B))
            n_01 = ops.Operator("n_01_no_bs", sqrtm(one_photon_at_B_none_at_A + multiple_photons_at_B_none_at_A))
            n_11 = ops.Operator("n_11_no_bs", sqrtm(at_least_one_photon_at_both))

            self._meas_operators = [n_00, n_10, n_01, n_11]
        else:
            # We have two separate operators for one or multiple photons arriving at one the detectors, allowing us to
            # identify false positives during entanglement generation
            n_00 = ops.Operator("n_00_no_bs", sqrtm(no_photons_at_both))
            n_10 = ops.Operator("n_10_no_bs", sqrtm(one_photon_at_A_none_at_B))
            n_01 = ops.Operator("n_01_no_bs", sqrtm(one_photon_at_B_none_at_A))
            n_11 = ops.Operator("n_11_no_bs", sqrtm(at_least_one_photon_at_both))
            n_20 = ops.Operator("n_20_no_bs", sqrtm(multiple_photons_at_A_none_at_B))
            n_02 = ops.Operator("n_02_no_bs", sqrtm(multiple_photons_at_B_none_at_A))

            self._meas_operators = [n_00, n_10, n_01, n_11, n_20, n_02]

    def measure(self):
        """Perform a measurement on the received qubits.

        Applies preprocessing to the qubits before the measurement, and
        applies postprocessing to the measured classical outcomes.
        After the measurement all qubits are discarded.

        Override of superclass method to support multiplexed measurements and dual-rail encoded qubit measurements.
        If some of the parameters have changed, the measurement operators are reset.
        First the correct qubit-pair per mode is selected.
        If the qubits are in presence-absence encoding, we can measure the qubits directly,
        but for dual-rail encoding we first split the different time-bins and then measure those.
        All the raw measurement outcomes (int) are transformed to a QKDOutcome.
        Additionally, the successful modes (if any) are transmitted as the meta information
        of the classical output message.

        Raises
        ------
        QuantumDetectorError
            If the `is_number_state` property is not the same for the pair of qubits.
        """
        if self._parameter_changed:
            # Reset the measurement operators based on which basis is used
            if self._measurement_basis == "Z":
                self._set_meas_operators_without_beamsplitter()
            else:
                self._set_meas_operators_with_beamsplitter()
            self._parameter_changed = False

        # Get all qubits per port.
        q_lists = [self._qubits_per_port[port_name] for port_name in self._input_port_names]
        arrival_times_raw, qubits_raw, metas = zip(*[item for q_list in q_lists for item in q_list])

        # handle None in list
        qubits, arrival_times = [], []
        for q in range(len(qubits_raw)):
            if isinstance(qubits_raw[q], Qubit):
                qubits.append(qubits_raw[q])
                arrival_times.append(arrival_times_raw[q])
            elif qubits_raw[q] is None:
                qubits.extend([None] * self._grouping)
                arrival_times.extend([arrival_times_raw[q]] * self._grouping)
            else:
                raise RuntimeError(f"QKDDetector {self.name} received input {q} of type {type(q)}.")

        # determine number of modes
        num_modes = len(qubits) // self._grouping if self.encoding == "time_bin"\
            else len(qubits) // (2 * self._grouping)

        outcomes = []
        for mode in range(num_modes):
            if self.encoding == "time_bin":
                # Photons are only received on the qin0 port, split them up into two photons and perform rotation
                qubits_time_bin = qubits[self._grouping * mode:self._grouping * (mode + 1)]
                q_early_or_Left = qubits_time_bin[2:]
                q_late_or_right = qubits_time_bin[:2]
            else:
                # presence absence
                # Only perform a measurement if the two arrival times are exactly equal
                arrival_time_left = arrival_times[self._grouping * mode: self._grouping * (mode + 1)]
                arrival_time_right = \
                    arrival_times[self._grouping * (num_modes + mode):self._grouping * (num_modes + mode + 1)]
                if arrival_time_left != arrival_time_right:
                    raise QuantumDetectorError(f"Arrival times of qubits not equal.\nLeft qubit arrived at "
                                               f"{arrival_time_left}, while right qubit arrived at "
                                               f"{arrival_time_right}.")
                # group for pair-wise measurement for a single mode
                q_early_or_Left = qubits[self._grouping * mode:self._grouping * (mode + 1)]
                q_late_or_right = qubits[self._grouping * (num_modes + mode):self._grouping * (num_modes + mode + 1)]

            # measure
            if all(x is None for x in q_early_or_Left + q_late_or_right):
                # all inputs were None, append fail
                outcome = 0
            else:
                if self._measurement_basis == "Y":
                    qapi.operate(q_early_or_Left, self._phase_shifter)
                outcome, _ = qapi.gmeasure(q_early_or_Left + q_late_or_right, meas_operators=self._meas_operators)
            outcome = self._measurement2qkdoutcome(outcome)
            outcomes.append(outcome)
        assert len(outcomes) == num_modes
        # Discard all the qubits
        [qapi.discard(qubit) for qubit in qubits if qubit is not None]
        self._qubits_per_port.clear()
        # Take the measurement outcomes and put the outcomes on the ports
        outcomes_per_port = {port_name: outcomes[:] for port_name in self._output_port_names}
        self.inform(outcomes_per_port)
        # Reset the meta information
        self._meta["successful_modes"] = None
