import numpy as np
from collections import defaultdict

import netsquid.qubits.qubitapi as qapi
from netsquid.qubits.qubit import Qubit
from netsquid.util.simtools import sim_time
from netsquid.qubits.operators import Operator
from netsquid.components.component import Component


class MultiPhotonBeamSplitter(Component):
    """Beam splitter component with one or two input and two output ports supporting multiplexed input.

    Note if using only one input port use "qin0" and leave "qin1" disconnected, the second input port will then input
    'vacuum'.

    Parameters
    ----------
    name : str
        Name of the beam splitter component.
    encoding : str
        Encoding of the input state, either "presence_absence" or "time_bin".

    """
    def __init__(self, name, encoding):
        super().__init__(name=name, port_names=["qin0", "qin1", "qout0", "qout1"])
        self.encoding = encoding
        self._single_input = True
        self._qubits_per_port = defaultdict(lambda: [])
        # bind input handler to port
        for port_name in ["qin0", "qin1"]:
            self.ports[port_name].bind_input_handler(self._handle_qinput, tag_meta=True)

    @property
    def encoding(self):
        """Encoding of the input state, either "presence_absence" or "time_bin"."""
        return self._encoding

    @encoding.setter
    def encoding(self, encoding):
        """Sets encoding of input state."""
        if encoding not in ['time_bin', 'presence_absence']:
            raise ValueError(f"Unknown input encoding {encoding}. Please specify either 'time_bin' or "
                             f"'presence_absence'")
        self._encoding = encoding

    def _handle_qinput(self, message=None):
        """Store qubits when they arrive, and apply the beam splitter operation only after qubits from all connected
        inputs have arrived."""
        meta = message.meta
        sender = meta["rx_port_name"]
        arrival_time = sim_time()
        for qubit in message.items:
            if not isinstance(qubit, Qubit):
                raise ValueError(f"A message should contain Qubits, not {qubit}")
            self._qubits_per_port[sender].append((arrival_time, qubit, meta))

        # check if all inputs have arrived
        if self.ports["qin1"].is_connected:
            self._single_input = False
            if self._qubits_per_port["qin1"] and self._qubits_per_port["qin0"]:
                self._apply_beam_splitter_operation()
        else:
            self._apply_beam_splitter_operation()

    def _apply_beam_splitter_operation(self, event=None):
        """Apply the operation of the beam splitter to one or two inputs.
        """
        q_lists = [self._qubits_per_port[port_name] for port_name in ["qin0", "qin1"]]
        arrival_times, qubit_list, __ = zip(*[item for q_list in q_lists for item in q_list])
        bs_unitary = self._beam_splitter_unitary()
        qubits_out0, qubits_out1 = [], []

        if self.encoding == "presence_absence":
            grouping = 2
        else:
            grouping = 4
        num_modes = int(len(qubit_list) / grouping) if self._single_input else int(len(qubit_list) / (2 * grouping))

        for mode in range(num_modes):
            if self._single_input:
                qubits = list(qubit_list[grouping * mode:grouping * (mode + 1)])
                # create virtual qubits in 00 for second input
                if self._encoding == "presence_absence":
                    virtual_qubits = qapi.create_qubits(2)
                    all_qubits = qubits + virtual_qubits
                else:
                    virtual_qubits = qapi.create_qubits(4)
                    # [0, 1] : physical photons in the early time window
                    # [2, 3] : virtual photons in the early time window
                    # [4, 5] : physical photons in the late time window
                    # [6, 7] : virtual photons in the late time window
                    all_qubits = qubits[2:] + virtual_qubits[:2] + qubits[:2] + virtual_qubits[2:]

            else:
                # Only perform a operation if the two arrival times are exactly equal
                arrival_time_left = arrival_times[grouping * mode: grouping * (mode + 1)]
                arrival_time_right = arrival_times[grouping * (num_modes + mode):grouping * (num_modes + mode + 1)]
                if arrival_time_left != arrival_time_right:
                    raise RuntimeError(f"Arrival times of qubits not equal.\nLeft qubit arrived at "
                                       f"{arrival_time_left}, while right qubit arrived at {arrival_time_right}.")
                # Perform pair-wise operation for a single mode
                qubits_0 = qubit_list[grouping * mode:grouping * (mode + 1)]
                qubits_1 = qubit_list[grouping * (num_modes + mode):grouping * (num_modes + mode + 1)]
                all_qubits = list(qubits_0 + qubits_1)

            assert len(all_qubits) == 2 * grouping
            # apply beam splitter unitary
            qapi.operate(all_qubits, bs_unitary)
            # add to correct output list
            if self.encoding == "presence_absence":
                qubits_out0.extend(all_qubits[:2])
                qubits_out1.extend(all_qubits[2:])
            else:
                # [0, 1] : output0 in the early time window
                # [2, 3] : output1 in the early time window
                # [4, 5] : output0 in the late time window
                # [6, 7] : output1 in the late time window
                qubits_out0.extend(all_qubits[0:2])
                qubits_out1.extend(all_qubits[2:4])
                qubits_out0.extend(all_qubits[4:6])
                qubits_out1.extend(all_qubits[6:8])

        # transmit qubits to output ports
        self.ports["qout0"].tx_output(qubits_out0)
        self.ports["qout1"].tx_output(qubits_out1)

        # reset qubit input dict
        self._qubits_per_port = defaultdict(lambda: [])

    def _beam_splitter_unitary(self):
        """Set beam splitter unitary."""
        # Four qubit I/O beam splitter
        u_bs = np.zeros([16, 16], dtype=complex)
        # 00,00 -> 00,00
        u_bs[0, 0] = 1
        # 01,00 -> 01,00 + 00,01
        u_bs[4, 4] = 1 / np.sqrt(2)
        u_bs[4, 1] = 1 / np.sqrt(2)
        # 00,01 -> 01,00 - 00,01
        u_bs[1, 4] = 1 / np.sqrt(2)
        u_bs[1, 1] = -1 / np.sqrt(2)
        # 10,00 -> 10,00 + 01,01 + 00,10
        u_bs[8, 8] = 1 / 2
        u_bs[8, 5] = 1 / np.sqrt(2)
        u_bs[8, 2] = 1 / 2
        # 00,10 -> 10,00 - 01,01 + 00,10
        u_bs[2, 8] = 1 / 2
        u_bs[2, 5] = -1 / np.sqrt(2)
        u_bs[2, 2] = 1 / 2
        # 11,00 -> 11,00 + 10,01 + 01,10 + 00,11
        u_bs[12, 12] = 1 / (2 * np.sqrt(2))
        u_bs[12, 9] = np.sqrt(3) / (2 * np.sqrt(2))
        u_bs[12, 6] = np.sqrt(3) / (2 * np.sqrt(2))
        u_bs[12, 3] = 1 / (2 * np.sqrt(2))
        # 00,11 -> 11,00 - 10,01 + 01,10 - 00,11
        u_bs[3, 12] = 1 / (2 * np.sqrt(2))
        u_bs[3, 9] = -np.sqrt(3) / (2 * np.sqrt(2))
        u_bs[3, 6] = np.sqrt(3) / (2 * np.sqrt(2))
        u_bs[3, 3] = -1 / (2 * np.sqrt(2))
        # 01,01 -> 10,00 - 00,10
        u_bs[5, 8] = 1 / np.sqrt(2)
        u_bs[5, 2] = -1 / np.sqrt(2)
        # 10,01 -> 11,00 + 10,01 - 01,10 - 00,11
        u_bs[9, 12] = np.sqrt(3) / (2 * np.sqrt(2))
        u_bs[9, 9] = 1 / (2 * np.sqrt(2))
        u_bs[9, 6] = -1 / (2 * np.sqrt(2))
        u_bs[9, 3] = -np.sqrt(3) / (2 * np.sqrt(2))
        # 01,10 -> 11,00 - 10,01 - 01,10 + 00,11
        u_bs[6, 12] = np.sqrt(3) / (2 * np.sqrt(2))
        u_bs[6, 9] = -1 / (2 * np.sqrt(2))
        u_bs[6, 6] = -1 / (2 * np.sqrt(2))
        u_bs[6, 3] = np.sqrt(3) / (2 * np.sqrt(2))
        for i in [7, 10, 11, 13, 14, 15]:  # Add diagonal entries to make it a valid unitary
            u_bs[i, i] = 1

        if self.encoding == "time_bin":
            return Operator(name="BS", matrix=np.kron(u_bs, u_bs))
        else:
            return Operator(name="BS", matrix=u_bs)
