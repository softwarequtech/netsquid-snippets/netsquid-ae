import numpy as np

from netsquid_simulationtools.parameter_set import Parameter, ParameterSet
from netsquid_ae.ae_chain_setup import _check_and_process_length_params


# Functions to convert parameters into probabilities of no-error/success (and back)
# coherence time to prob
def _coherence_time_prob_fn(x):
    """Convert coherence time to loss probability."""
    return np.exp(-1. / x)


# prob to coherence time
def _inverse_coherence_time_prob_fn(x):
    """Convert loss probability to coherence time."""
    if x == 1.:
        return np.inf
    elif x == 0.:
        return 0
    else:
        return - 1. / np.log(x)


class AEParameterSet(ParameterSet):
    """Set of Parameters for atomic ensemble simulations."""

    _REQUIRED_PARAMETERS = [

        # SOURCE

        # encoding/scheme used, e.g.: "presence_absence" or "time_bin"
        # Note: this is a tunable parameter, maybe this should be list of possible encodings?
        Parameter(name="source_encoding",
                  units=None,
                  perfect_value=None,
                  type=str,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # frequency of the photon source
        # Note: this is a tunable parameter
        Parameter(name="source_frequency",
                  units="Hz",
                  perfect_value=None,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # number of spectral modes the source can produce
        # TODO: how to convert num_modes to error probability?
        Parameter(name="source_spectral_modes",
                  units=None,
                  perfect_value=np.inf,
                  type=int,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # number of spatial modes the source can produce
        Parameter(name="source_spatial_modes",
                  units=None,
                  perfect_value=np.inf,
                  type=int,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # number of temporal modes the source can produce
        Parameter(name="source_temporal_modes",
                  units=None,
                  perfect_value=np.inf,
                  type=int,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # photon number distribution
        # e.g. "thermal"
        Parameter(name="photon_num_distribution",
                  units=None,
                  perfect_value=None,
                  type=str,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # mean photon / photon pair number
        # for single photon / photon pair source
        # for presence_absence encoding this is the probability of emitting a photon
        # Note: this is a tunable parameter and needs to be optimized.
        Parameter(name="mean_photon_number",
                  units=None,
                  perfect_value=None,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # emission probabilities
        # should be used alternatively to an actual distribution and mean
        Parameter(name="emission_probabilities",
                  units=None,
                  perfect_value=[0, 1., 0., 0.],
                  type=list,
                  convert_to_prob_fn=lambda x: 1 - x[2],
                  # Note: for [a,b,c,d] the probability of success  = b, therefore p(error) = 1-b
                  convert_from_prob_fn=None),

        # Parameters for number of multiplexing modes calculation
        # Is used alternatively to directly writing number of modes.
        # Rate of generated photon pairs. Based on measurement.
        Parameter(name="Rate",
                  units="Hz",
                  perfect_value=np.inf,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # Measurement of the second order correlation function, g2, for the heralded light source
        Parameter(name="g2",
                  units=None,
                  perfect_value=0.,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # MEMORY

        # Maximum efficiency of the memory if photon would be retrieved immediately (t_{storage} = 0)
        # Overall efficiency of the memory mem_eff depends on storage time t as:
        # mem_eff(t) = "max_memory_efficiency" * np.exp(- t / "memory_coherence_time")
        Parameter(name="max_memory_efficiency",
                  units=None,
                  perfect_value=1.,
                  type=float,
                  convert_to_prob_fn=lambda x: x,
                  convert_from_prob_fn=lambda x: x),

        # Memory coherence time T1
        Parameter(name="memory_T1",
                  units="nanoseconds",
                  perfect_value=np.inf,
                  type=float,
                  convert_to_prob_fn=_coherence_time_prob_fn,
                  convert_from_prob_fn=_inverse_coherence_time_prob_fn),

        # Memory coherence time T2
        Parameter(name="memory_T2",
                  units="nanoseconds",
                  perfect_value=np.inf,
                  type=float,
                  convert_to_prob_fn=_coherence_time_prob_fn,
                  convert_from_prob_fn=_inverse_coherence_time_prob_fn),

        # number of spectral modes the memory can store
        Parameter(name="memory_spectral_modes",
                  units=None,
                  perfect_value=np.inf,
                  type=int,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # number of spatial modes the memory can store
        Parameter(name="memory_spatial_modes",
                  units=None,
                  perfect_value=np.inf,
                  type=int,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # number of temporal modes the memory can store
        Parameter(name="memory_temporal_modes",
                  units=None,
                  perfect_value=np.inf,
                  type=int,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # additional noise on the memory
        # e.g. "multi-photon emission"
        # Note: this is a rather vague "parameter" and needs to come with additional information and params
        Parameter(name="memory_additional_noise",
                  units=None,
                  perfect_value=None,
                  type=str,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # supported encodings for the memory
        # Note: maybe should be list of allowed encodings instead?
        Parameter(name="memory_encoding",
                  units=None,
                  perfect_value=None,
                  type=str,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # time-dependence of the memory
        # Note: currently implemented "gaussian" and "exponential" decay
        Parameter(name="memory_time_dependence",
                  units=None,
                  perfect_value=None,
                  type=str,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),


        # FIBER

        # initial loss probability
        Parameter(name="coupling_loss_fibre",
                  units=None,
                  perfect_value=0.,
                  type=float,
                  # TODO: add conversion func
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # MIDPOINT DETECTOR

        # photon indistinguishability / visibility at the midpoint detector
        Parameter(name="det_visibility",
                  units=None,
                  perfect_value=1.,
                  type=float,
                  convert_to_prob_fn=lambda x: x,
                  convert_from_prob_fn=lambda x: x),

        # midpoint detector detection time window
        Parameter(name="det_time_window",
                  units="nanoseconds",
                  perfect_value=None,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # midpoint detector dead time
        Parameter(name="det_dead_time",
                  units="nanoseconds",
                  perfect_value=None,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # midpoint detector after-pulsing
        # increased probability of false-positive after detection
        Parameter(name="det_after_pulse",
                  units=None,
                  perfect_value=0.,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # dark count probability at the midpoint detectors (per detector)
        # The probability of no dark counts at all
        # is computed as `exp(-1. * dark_count_rate * time_window)`
        Parameter(name="det_dark_count_rate",
                  units="Hz",
                  perfect_value=0.,
                  type=float,
                  convert_to_prob_fn=lambda x: np.exp(-1. * x),
                  convert_from_prob_fn=lambda x: -1. * np.log(x)),

        # Detection efficiency of the midpoint detectors
        Parameter(name="det_efficiency",
                  units=None,
                  perfect_value=1.,
                  type=float,
                  convert_to_prob_fn=lambda x: x,
                  convert_from_prob_fn=lambda x: x),

        # Number or non-number resolving photon detectors, `True` if detector is number resolving
        # Note: this is a tunable parameter
        Parameter(name="det_num_resolving",
                  units=None,
                  perfect_value=None,
                  type=bool,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # mode limit of midpoint detector
        # int : limit that detector puts on the multiplexing capability of the system
        # -1 indicates unlimited
        Parameter(name="det_mode_limit",
                  units=None,
                  perfect_value=False,
                  type=int,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # linear optics used in midpoint detector
        # bool: whether detector uses linear optics
        # Note: if False, needs more specification about actual setup
        Parameter(name="det_lin_opt",
                  units=None,
                  perfect_value=None,
                  type=bool,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),


        # SWAP DETECTOR

        # photon indistinguishability / visibility at the swap detector
        # this is determined by the visibility of photons released by the memories
        Parameter(name="swap_det_visibility",
                  units=None,
                  perfect_value=1.,
                  type=float,
                  convert_to_prob_fn=lambda x: x,
                  convert_from_prob_fn=lambda x: x),

        # swap detector detection time window
        Parameter(name="swap_det_time_window",
                  units="nanoseconds",
                  perfect_value=None,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # swap detector dead time
        Parameter(name="swap_det_dead_time",
                  units="nanoseconds",
                  perfect_value=None,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # swap detector after-pulsing
        # increased probability of false-positive after detection
        Parameter(name="swap_det_after_pulse",
                  units=None,
                  perfect_value=0.,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # dark count probability at the swap detectors (per detector)
        # The probability of no dark counts at all
        # is computed as `exp(-1. * dark_count_rate * time_window)`
        Parameter(name="swap_det_dark_count_rate",
                  units="Hz",
                  perfect_value=0.,
                  type=float,
                  convert_to_prob_fn=lambda x: np.exp(-1. * x),
                  convert_from_prob_fn=lambda x: -1. * np.log(x)),

        # Detection efficiency of the swap detectors
        Parameter(name="swap_det_efficiency",
                  units=None,
                  perfect_value=1.,
                  type=float,
                  convert_to_prob_fn=lambda x: x,
                  convert_from_prob_fn=lambda x: x),

        # Number or non-number resolving photon detectors at swap, `True` if detector is number resolving
        # Note: this is a tunable parameter
        Parameter(name="swap_det_num_resolving",
                  units=None,
                  perfect_value=None,
                  type=bool,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # mode limit of swap detector
        # bool: whether detector limits the multiplexing capability of the system
        # Note: if True, needs more specification
        # TODO: this parameter is unnecessary as swapping is not multiplexed anyway
        Parameter(name="swap_det_mode_limit",
                  units=None,
                  perfect_value=False,
                  type=bool,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # linear optics used in swap detector
        # bool: whether detector uses linear optics
        # Note: if False, needs more specification about actual setup
        Parameter(name="swap_det_lin_opt",
                  units=None,
                  perfect_value=None,
                  type=bool,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),


        # ABSTRACT PARAMETERS

        # elementary link fidelity
        # fidelity of target state
        Parameter(name="el_link_fidelity",
                  units=None,
                  perfect_value=1.,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # elementary link generation success probability
        Parameter(name="el_link_succ_prob",
                  units=None,
                  perfect_value=1.,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # swap quality
        # probability of applying depolarizing channel after perfect swap
        Parameter(name="swap_quality",
                  units=None,
                  perfect_value=0.,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None),

        # swap probability
        # probability of a successful swap
        Parameter(name="swap_probability",
                  units=None,
                  perfect_value=1.,
                  type=float,
                  convert_to_prob_fn=None,
                  convert_from_prob_fn=None)]


def convert_parameter_set(sim_params, param_set_dict):
    """Converts a dict of on AEParameterSet and combines with sim_params to create single parameter dictionary.
    Also performs checks on the ParameterSet dictionary

    Parameters
    ----------
    sim_params : dict
        Dictionary containing simulation parameters.
    param_set_dict : dict (optional)
        AEParameterSet dictionary containing simulation parameters. If None, only the checks are performed

    Returns
    -------
    sim_params : dict
        Combined dictionary containing all simulation parameters.
    """
    if not isinstance(param_set_dict, dict):
        raise ValueError(f"param_set_dict must be of type dict not {type(param_set_dict)}.")

    # convert length and node distance as necessary
    sim_params = _check_and_process_length_params(**sim_params)

    # Check photon number of given parameter set
    if param_set_dict['photon_num_distribution'] != "thermal":
        if param_set_dict['mean_photon_number'] != ParameterSet._NOT_SPECIFIED:
            raise ValueError(f"Mean photon pair number currently not implemented for "
                             f"{param_set_dict['photon_num_distribution']} distribution. Please use"
                             f"'emission_probabilities' instead.")
    else:
        if param_set_dict['mean_photon_number'] == ParameterSet._NOT_SPECIFIED:
            raise ValueError("Photo Distribution set to 'thermal' but no mean photon number specified.")
        elif param_set_dict['emission_probabilities'] != ParameterSet._NOT_SPECIFIED:
            param_set_dict['emission_probabilities'] = ParameterSet._NOT_SPECIFIED
            print(f"Using thermal distribution with mean photon (pair) number = "
                  f"{param_set_dict['mean_photon_number']}"
                  f" and ignoring emission probabilities {param_set_dict['emission_probabilities']}.")
    sim_params['mean_photon_number_l'] = param_set_dict['mean_photon_number']
    sim_params['mean_photon_number_r'] = param_set_dict['mean_photon_number']
    sim_params['emission_probabilities_l'] = param_set_dict['emission_probabilities']
    sim_params['emission_probabilities_r'] = param_set_dict['emission_probabilities']

    # check if any parameters that should be the same for the whole simulation differ by component

    # encoding
    if param_set_dict["source_encoding"] != param_set_dict["memory_encoding"]:
        raise ValueError(f"Source encoding was {param_set_dict['source_encoding']}, while memory was specified "
                         f"with {param_set_dict['memory_encoding']}.")
    sim_params["encoding"] = param_set_dict["source_encoding"]

    # spectral modes
    if param_set_dict["source_spectral_modes"] != param_set_dict["memory_spectral_modes"]:
        print(f"Source spectral modes was {param_set_dict['source_spectral_modes']}, while memory was specified "
              f"with {param_set_dict['memory_spectral_modes']}.")
    spectral_modes = min(param_set_dict["source_spectral_modes"], param_set_dict["memory_spectral_modes"])

    # temporal modes
    if param_set_dict["source_temporal_modes"] != param_set_dict["memory_temporal_modes"]:
        print(f"Source temporal modes was {param_set_dict['source_temporal_modes']}, while memory was specified "
              f"with {param_set_dict['memory_temporal_modes']}.")
    temporal_modes = min(param_set_dict["source_temporal_modes"], param_set_dict["memory_temporal_modes"])

    # spatial modes
    if param_set_dict["source_spatial_modes"] != param_set_dict["memory_spatial_modes"]:
        print(f"Source spatial modes was {param_set_dict['source_spatial_modes']}, while memory was specified with "
              f"{param_set_dict['memory_spatial_modes']}.")
    spatial_modes = min(param_set_dict["source_spatial_modes"], param_set_dict["memory_spatial_modes"])

    # check if detector limits multiplexing
    total_modes = spatial_modes * temporal_modes * spatial_modes
    if 0 < param_set_dict['det_mode_limit'] < total_modes:
        print(f"Total number of modes {total_modes} larger than multiplexing limit of midpoint detector"
              f"{param_set_dict['det_mode_limit']}")
        total_modes = param_set_dict['det_mode_limit']

    print(f"Setting total number of multiplexing modes used in simulation to {spectral_modes}.")
    sim_params["num_multiplexing_modes"] = total_modes

    # Convert ParameterSet params to parameters used in Simulation
    # calculate dark count probability (change once time windows are implemented on detectors)
    # rate is in [Hz] and time window in [ns] therefore use conversion
    sim_params["det_dark_count_prob"] = \
        float(1 - np.exp(- param_set_dict['det_dark_count_rate'] * param_set_dict['det_time_window'] * 1e-9))
    sim_params["swap_det_dark_count_prob"] = \
        float(1 - np.exp(- param_set_dict['swap_det_dark_count_rate'] * param_set_dict['swap_det_time_window'] * 1e-9))

    # Warn about not implemented parameters
    if not param_set_dict["det_lin_opt"] or not param_set_dict["swap_det_lin_opt"]:
        raise NotImplementedError("Only linear optics implemented.")
    print("After-pulsing not implemented yet.")
    print("Detection dead-time not implemented yet.")
    # TODO: determine what to do with T1,T2
    print("T1/T2 noise not implemented yet.")
    print("USING T2 as memory coherence time.")
    sim_params["memory_coherence_time"] = param_set_dict["memory_T2"]

    if param_set_dict["memory_additional_noise"] != "No":
        print(f"Additional memory noise {param_set_dict['memory_additional_noise']} not implemented yet.")

    # copy params
    for key in ["source_frequency", "mean_photon_number", "emission_probabilities",         # source
                "max_memory_efficiency", "memory_time_dependence",                          # memory
                "coupling_loss_fibre",                                                      # fiber
                "det_visibility", "det_efficiency", "det_num_resolving",                    # mid & swap det
                "swap_det_visibility", "swap_det_efficiency", "swap_det_num_resolving"]:
        sim_params[key] = param_set_dict[key]

    return sim_params
