from netsquid.nodes import Node, Connection
from netsquid_physlayer.classical_connection import ClassicalConnectionWithLength

from netsquid_ae.ae_component_setup import setup_end_node, setup_repeater_node, setup_heralded_connection, \
    setup_qkd_node

__all__ = [
    "EndNode",
    "RepeaterNode",
    "QKDNode",
    "HeraldedConnection",
    "MessagingConnection"
]


class EndNode(Node):
    """End node that generates entangled photon states, stores one half in its memory and sends the other half to
    the midpoint station for entanglement generation.

    Parameters
    ----------
    name : str
        Name of the end node.
    params : dict
        Dictionary of simulation parameters which are used to set up the component.
    mpn_index : int
        Index of the mean photon number for the source.
        See :func:`~.ae_chain_setup._get_mpn_indices` for details on how this index is defined and used.
        Default is None. In that case, the mean photon number is the same for all nodes in the chain.
    """
    def __init__(self, name, mpn_index=None, **kwargs):
        super().__init__(name)
        try:
            setup_end_node(node=self, mpn_index=mpn_index, **kwargs)
        except KeyError:
            raise ValueError("EndNode cannot be set up properly, not all necessary params are available")

    def unique_mem_pos_to_real_mem_pos(self, unique_mem_pos):
        """Map unique mem_pos to a memory and its respective real memory positions.

        Note: Useful if node contains multiple memories.

        Parameters:
        -----------
        unique_mem_pos : list
            List describing a unique memory position in range(0, total_num_modes * num_memories).

        Returns
        -------
        qmemory:
            Quantum memory this unique memory position belongs to.
        positions : list
            List of actual memory positions that correspond to the unique memory position.
        """

        return self.subcomponents["QMem"], unique_mem_pos
        # Note: can easily be extended to more memories


class RepeaterNode(Node):
    """Repeater node which contains two sources, two memories and a BSM detector for performing the
    entanglement swap. Tries to generate entanglement on either side while the local states are stored in memory,
    then afterwards performs a Bell-state measurement with the detector.

    Parameters
    ----------
    name : str
        Name of the repeater node.
    params : dict
        Dictionary of simulation parameters which are used to set up the component.
    mpn_index : int
        Index of the mean photon number for the source.
        See :func:`~.ae_chain_setup._get_mpn_indices` for details on how this index is defined and used.
        Default is None. In that case, the mean photon number is the same for all nodes in the chain.
    """
    def __init__(self, name, mpn_index=None, **kwargs):
        super().__init__(name)
        try:
            setup_repeater_node(node=self, mpn_index=mpn_index, **kwargs)
        except KeyError:
            raise ValueError(f"Repeater Node '{name}' cannot be set up properly, "
                             f"not all necessary kwargs are available.")


class QKDNode(Node):
    """Node that contains a detection setup capable of performing measurements in multiple bases for quantum-key
    distribution.

    Parameters
    ----------
    name : str
        Name of the QKD node.
    params : dict
        Dictionary of simulation parameters which are used to set up the component.
    """
    def __init__(self, name, params):
        super().__init__(name)
        setup_qkd_node(node=self, sim_params=params)


class HeraldedConnection(Connection):
    """Connection that contains two quantum channels for receiving the qubits on two sides, a detector capable of
    performing a Bell-state measurement for entanglement swapping and two classical channel for sending back the
    measurement result to either side.

    Parameters
    ----------
    name : str
        Name of the heralded connection.
    """
    def __init__(self, name, **kwargs):
        super().__init__(name)
        try:
            setup_heralded_connection(connection=self, **kwargs)
            # TODO: add speed_of_light_l/r?
        except KeyError:
            raise ValueError(f"Heralded connection '{name}' cannot be set up properly, "
                             f"not all necessary kwargs are available")


class MessagingConnection(ClassicalConnectionWithLength):
    """Connection required for sending classical messages back and forth, informing the end nodes about the measurement
    results of the repeater nodes (if any), which are needed for classical post-processing in case of QKD.

    Parameters
    ----------
    name : str
        Name of the messaging connection.
    params : dict
        Dictionary of simulation parameters which are used to set up the component.
    """
    def __init__(self, name, c=200000, **kwargs):
        node_distance = kwargs["channel_length_l"] + kwargs["channel_length_r"]
        super().__init__(name, length=node_distance, c=c)
