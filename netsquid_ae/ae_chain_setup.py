import logging

from netsquid.nodes.network import Network
from netsquid.util.simlog import logger

from netsquid_ae.ae_protocols import EmissionProtocol, SwapProtocol, ExtractionProtocol, MessagingProtocol, \
    MagicProtocol
from netsquid_ae.application_protocols import DetectionProtocol
from netsquid_ae.ae_magic_distributor import AEMagicDistributor
from netsquid_ae.ae_classes import EndNode, RepeaterNode, HeraldedConnection, MessagingConnection, QKDNode

from netsquid_simulationtools.parameter_set import ParameterSet


def _create_physical_network(name, sim_params, num_repeaters=0, id_str="1"):
    """Create a physical network for a repeater chain with variable number of repeaters.
    Contains end nodes, repeater nodes, heralded connections for entanglement generation
    and messaging connections for classical communication between nodes.

    Note that all the (possibly asymmetric) elementary links in this repeater chain will be identical.

    Parameters
    ----------
    name : str
        Name of the Network.
    sim_params : dict
        Dictionary of simulation parameters, used to set up the physical network.
    num_repeaters : int
        Number of repeaters in the repeater chain.
        Default value is 0, which corresponds to a single elementary link
    id_str : str
        String identifier for the chain number.
        Only relevant when `presence_absence` encoding is used,
        in which we have two independent identical chains in parallel
        so that we can perform QKD.

    Returns
    -------
    network : :class:`~netsquid.nodes.network.Network`
        Physical network representing the repeater chain.
    """
    network = Network(name=name)
    mpn_indices = _get_mpn_indices(sim_params)  # retrieve mpn indices
    # First add the Nodes, starting with EndNode Alice, then all RepeaterNodes (if any) and finally EndNode Bob
    network.add_node(EndNode(name=f"A_{id_str}", mpn_index=mpn_indices[0], **sim_params))
    for r in range(1, num_repeaters + 1):
        network.add_node(RepeaterNode(name=f"R_{id_str}_{r}", mpn_index=mpn_indices[r], **sim_params))
    network.add_node(EndNode(name=f"B_{id_str}", mpn_index=mpn_indices[-1], **sim_params))
    # Then add all the connections used for heralded entanglement generation and messaging from left to right
    node_list = list(network.nodes.values())
    for n in range(num_repeaters + 1):
        connection = HeraldedConnection("Conn_{}_{}".format(id_str, n + 1), **sim_params)
        messaging_conn = MessagingConnection("MessagingConn_{}_{}".format(id_str, n + 1), **sim_params)
        left_node = node_list[n]
        right_node = node_list[n + 1]
        # End node ports intentionally named differently, in turn this requires connecting them separately
        # Left Node of first Connection is Alice, for any other it is a RepeaterNode
        left_port_name = "IO_Connection_E" if n == 0 else "IO_Connection_R"
        # Right Node of last Connection is Bob, for any other it is a RepeaterNode
        right_port_name = "IO_Connection_E" if n == num_repeaters else "IO_Connection_L"
        network.add_connection(node1=left_node, node2=right_node, connection=connection,
                               port_name_node1=left_port_name, port_name_node2=right_port_name, label="heralding")
        network.add_connection(node1=left_node, node2=right_node, connection=messaging_conn,
                               port_name_node1="Messaging_IO_R", port_name_node2="Messaging_IO_L", label="messaging")

    return network


def _add_protocols_and_magic(network, sim_params, extraction_delay=0., repetition_delay=0.):
    """Adds all the protocols to the nodes and introduces magic if required.

    Supports midpoint asymmetry in (1) repeater chains with magic, (2) elementary links with magic, (3) elementary
    links without magic. Does NOT support asymmetry in repeater chains without magic.

    Parameters
    ----------
    network : :class:`~netsquid.nodes.network.Network`
        Physical network representing the repeater chain.
    sim_params : dict
        Dictionary of simulation parameters, which are only used to set up magic if it is used.
    extraction_delay : float (optional)
        Virtual time delay between receiving heralding message from midpoint and extracting the qubit from the memory
        in the :class:`~netsquid_ae.ae_protocols.ExtractionProtocol`. Only set to non-zero values if full post-swap
        end-to-end quantum states need to be extracted to ensure all swaps have been performed before extraction.

    Returns
    -------
    protocols : list of :class:`~netsquid.protocols.Protocol`
        List of protocols (emission, swap, extraction and messaging) that are added to the network.
    magic_protocols : list of :class:`~netsquid.protocols.Protocol`
        List of magic protocols that are added to the network.
        If magic is not used, returns an empty list.
    magic_distributor : :class:`~netsquid_magic.magic_distributor.MagicDistributor`
        Merged MagicDistributor that contains all MagicDistributor containing the components that are added to
        the network.
        This replaces the heralded connections in terms of functionality but they are not deleted from the network.
        If magic is not used, return an empty list.
    """

    protocols = []
    magic_protocols = []
    magic_distributors = []
    connection_list = [conn for conn in list(network.connections.values()) if isinstance(conn, HeraldedConnection)]
    node_list = list(network.nodes.values())
    if len(node_list) - len(connection_list) == 2:
        # two chains present , splitting
        con_list1 = connection_list[:len(connection_list) // 2]
        con_list2 = connection_list[len(connection_list) // 2:]
        node_list1 = node_list[:len(node_list) // 2]
        node_list2 = node_list[len(node_list) // 2:]
        connection_lists = [con_list1, con_list2]
        node_lists = [node_list1, node_list2]
        chains = 2
    else:
        connection_lists = [connection_list]
        node_lists = [node_list]
        chains = 1

    for c in range(chains):
        for idx, node in enumerate(node_lists[c]):
            if isinstance(node, EndNode):
                if sim_params["magic"]:
                    # Using MagicProtocol instead of EmissionProtocol
                    if "A" in node.name:
                        # We are on the left side of an elementary link, so create new MagicDistributor
                        magic_distributor = AEMagicDistributor(nodes=node_lists[c][idx:idx + 2],
                                                               heralded_connection=connection_lists[c][idx],
                                                               repetition_delay=repetition_delay)
                        magic_conn = connection_lists[c][idx]
                        magic_distributors.append(magic_distributor)
                    ep = MagicProtocol(node, memory=node.subcomponents["QMem"], component=magic_conn)
                    magic_protocols.append(ep)
                    protocols.append(ep)
                    protocols.append(ExtractionProtocol(node, ep, delay=extraction_delay))
                else:
                    # Running full simulation with EmissionProtocol
                    if "A" in node.name:
                        delay = max(0, 1e9 * sim_params["channel_length_r"] / 200000 -
                                    1e9 * sim_params["channel_length_l"] / 200000)
                    else:
                        delay = max(0, 1e9 * sim_params["channel_length_l"] / 200000 -
                                    1e9 * sim_params["channel_length_r"] / 200000)
                    ep = EmissionProtocol(node, memory=node.subcomponents["QMem"],
                                          source=node.subcomponents["PPS"],
                                          delay=delay,
                                          max_attempts=sim_params["num_attempts_proto"])
                    protocols.append(ep)
                    protocols.append(ExtractionProtocol(node, ep, delay=extraction_delay))
                protocols.append(MessagingProtocol(node, emission_protocol_1=ep))
            elif isinstance(node, RepeaterNode):
                # This is a repeater node
                if sim_params["magic"]:
                    # Using MagicProtocols instead of EmissionProtocols
                    # Create a MagicDistributor for the elementary link on the right side of this repeater node
                    new_magic_distributor = AEMagicDistributor(nodes=node_lists[c][idx:idx + 2],
                                                               heralded_connection=connection_lists[c][idx],
                                                               repetition_delay=repetition_delay)
                    new_magic_conn = connection_lists[c][idx]
                    magic_distributors.append(new_magic_distributor)
                    # Use the component of the previously created MagicDistributor on the left side
                    ep_l = MagicProtocol(node, memory=node.subcomponents["QMem_L"], component=magic_conn)
                    # Use the component of the the newly created MagicDistributor on the right side
                    ep_r = MagicProtocol(node, memory=node.subcomponents["QMem_R"], component=new_magic_conn)
                    magic_protocols.append(ep_l)
                    magic_protocols.append(ep_r)
                    # The new magic distributor now becomes the previous for future reference
                    magic_distributor = new_magic_distributor
                    magic_conn = new_magic_conn
                else:
                    # Running full simulation with EmissionProtocols
                    if sim_params["channel_length_l"] != sim_params["channel_length_r"]:
                        raise NotImplementedError("Running full simulations (without magic) of repeater chains with "
                                                  "midpoint asymmetry is not supported yet.")
                    ep_l = EmissionProtocol(node, memory=node.subcomponents["QMem_L"],
                                            source=node.subcomponents["PPS_L"],
                                            max_attempts=sim_params["num_attempts_proto"])
                    ep_r = EmissionProtocol(node, memory=node.subcomponents["QMem_R"],
                                            source=node.subcomponents["PPS_R"],
                                            max_attempts=sim_params["num_attempts_proto"])
                sp = SwapProtocol(node, ep_l, ep_r)
                protocols.append(ep_l)
                protocols.append(ep_r)
                protocols.append(sp)
                protocols.append(MessagingProtocol(node, emission_protocol_1=ep_l, emission_protocol_2=ep_r,
                                                   swap_protocol=sp))
            else:
                raise TypeError(f"Invalid node type {type(node)}. Must be {type(EndNode)} or {type(RepeaterNode)}.")

    # merge magic_distributors
    if sim_params["magic"]:
        magic_distributor = magic_distributors[0]
        for md in magic_distributors[1:]:
            magic_distributor.merge_magic_distributor(md)
    else:
        magic_distributor = magic_distributors

    return protocols, magic_protocols, magic_distributor


def create_elementary_link(memory_coherence_time, max_memory_efficiency, memory_time_dependence, mem_num_modes,
                           source_num_modes, Rate, g2, magic, multi_photon, encoding, mean_photon_number,
                           emission_probabilities, source_frequency, time_step, num_attempts, channel_length_l,
                           channel_length_r, attenuation_l, attenuation_r, coupling_loss_fibre, det_dark_count_prob,
                           det_efficiency, det_visibility, det_num_resolving, fibre_phase_stdv_l, fibre_phase_stdv_r,
                           length, num_attempts_proto, state_files_directory, multiple_link_successes,
                           repetition_delay=0, **kwargs):
    """Sets up an elementary link using atomic ensembles.

    This should be used to generate sampled magic for an elementary link.
    This method now supports asymmetric channel lengths and sets up corresponding delays for emission protocols.

     Schematics of the setup:

     .. code-block:: text

        "Alice"            "HeraldedConnection"         "Bob"

        ----------                                      ----------
        o        o   <-  messaging ports (unused) ->    o        o
        | |Clk|  |                                      | |Clk|  |
        |        |                                      |        |
        o-|QMem| | ------------------------------------ | |QMem|-o
        |    |   | | <<<<<CC<<<<<o------o>>>>>CC>>>>> | |   |    |
        |   (PPS)-o              | QDet |               o-(PPS)  |
        ---------- | >>>>>QC>>>>>o------o<<<<<QC<<<<< | ----------
                   ------------------------------------

        Where:
        o               is a port
        >>CC/QC>>       is a one way classical/quantum channel
        |QMem|          is a quantum memory
        (PPS)           is a source of entangled photon pairs
        |Clk|           is a clock
        |QDet|          is a detector performing a linear optical BSM
        ----            boundaries of a node / component

    Parameters
    ----------
    memory_coherence_time : float
        Memory coherence time in [ns].
    max_memory_efficiency : float
        Memory efficiency at t=0 (between 0 and 1).
    memory_time_dependence : str
        Time dependence of the memory efficiency. Default is "exponential".
    memory_num_modes : int
        Number of multiplexing modes for memory.
    source_num_modes : int
        Number of multiplexing modes for source.
    Rate: float, optinoal
        Measurement of the rate of generated photon pairs in [Hz].
        Used for the Number of modes calculation.
        Should be specified if num_multiplexing_modes is not specified.
    g2: float, optinoal
        Measurement of the second order correlation function, g2, for the heralded light from the source.
        Used for the Number of Modes calculation
        Should be specified if num_multiplexing_modes is not specified.
    source_frequency : float, optional
        Frequency at which the source creates qubits in [Hz].
    magic : bool
        Whether magic is used or not. This determines if a memory position is created for every multiplexing mode or
        only a single memory position (magic=True) is created to save resources.
    multi_photon : bool
        Whether the simulation is using multi photon emission or not.
    encoding : str
        Encoding of the photons. Either "time_bin" or "presence_absence".
    mean_photon_number : list or None
        List of mean photon numbers for the thermal distribution of the SPDC sources along the chain.
        The `mpn_index` (see below) then determines which mean photon number is assigned to which source. This allows
        for different mean photon numbers along the chain.
        Must be supplied when `emission_probabilities` is None.
    emission_probabilities: list or None
        List of lists of probabilities of emitting a certain number of photons along the chain.
        The `mpn_index` (see below) then determines which list of emission_probabilities is assigned to which source.
        This allows for different emission_probabilities along the chain.
        This emission_probabilities must be a list of length 4, where each element denotes the probability of
        generating a photon pair with the number of photons equal to its list index.
    time_step : float
        Time step at which the clock on this node ticks in [ns]. Only used if magic=False.
    num_attempts : int
        Number of ticks after which the clock stops, effectively stopping attempts of entanglement generation. Set to
        -1 for unlimited operation.
    channel_length_l : float
        Length of the channel left of the detectors [in km].
    channel_length_r : float
        Length of the channel right of the detectors [in km].
    attenuation_l : float
        Attenuation of the channel left of the detectors [in dB/km].
    attenuation_r: float
        Attenuation of the channel right of the detectors [in dB/km].
    coupling_loss_fibre : float
        Coupling loss/ length independent loss of the channel to the midpoint, in [0,1].
    det_dark_count_prob : float
        Dark count probability of the midpoint detectors.
    det_efficiency : float
        Detection efficiency of the midpoint detectors.
    det_visibility : float
        Photon indistinguishability at the midpoint BSM.
    det_num_resolving : bool
        Whether or not midpoint detectors are number-resolving.
    fibre_phase_stdv_l : float
        Standard deviation of the gaussian distribution of the phase for PhaseLossModel on the channel left of
        the detectors. Set to 0.0 in order to not include `PhaseLossModel` in the simulation.
    fibre_phase_stdv_r : float
        Standard deviation of the gaussian distribution of the phase for PhaseLossModel on the channel right of
        the detectors. Set to 0.0 in order to not include `PhaseLossModel` in the simulation.
    num_attempts_proto : int
        Number of entanglement attempts to run the entanglement generation protocol for.
        Set to -1 to run protocol until explicitly stopped.
    state_files_directory : str
        Name of directory containing elementary link state files saved as
        :obj:`.netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder` in *.pickle format following
        the naming convention defined in `forced_magic_state_generator.py`.
        These should have beengenerated using this specific heralded connection.
        This is only needed when using magic ('forced' or 'sampled') and will be handled and checked by
        the `~.netsquid_ae.ae_magic_distributor.AEMagicDistributor`.
    multiple_link_successes : bool
        If set to True, multiple modes can be successful otherwise the detector stops measuring after the first
        successful mode.
    length : float
        Total length of the elementary link. Can be specified instead of `channel_length_l` and `channel_length_r` to
        generate a symmetric elementary link of `channel_length_l = channel_length_r = length / 2`.
    repetition_delay : float
        Delay between automated deliveries by the `~.netsquid_ae.ae_magic_distributor.AEMagicDistributor.
        Set to non-zero to avoid concurrence issues.

    Returns
    -------
    protocols : list
        List of all protocols (:class:`~netsquid.protocols.Protocol`) in the simulation. (here only Alice's and Bob's)
    network : :class:`~netsquid.nodes.network.Network`
        Network component that contains all the :class:`~netsquid.nodes.Node`s and
        :class:`~netsquid.connections.Connection`s that represent the physical network.
    magic : list
        List with the merged MagicDistributor (:class:`~netsquid_magic.magic_distributor.MagicDistributor`) as first
        item and a list of all magic protocols as the second item.
        Empty list when no magic is used.

    """
    params = {"memory_coherence_time": memory_coherence_time, "max_memory_efficiency": max_memory_efficiency,
              "memory_time_dependence": memory_time_dependence, "mem_num_modes": mem_num_modes,
              "source_num_modes": source_num_modes, "Rate": Rate, "g2": g2,
              "magic": magic, "multi_photon": multi_photon, "encoding": encoding,
              "mean_photon_number": mean_photon_number, "emission_probabilities": emission_probabilities,
              "time_step": time_step, "num_attempts": num_attempts,
              "channel_length_l": channel_length_l, "channel_length_r": channel_length_r,
              "attenuation_l": attenuation_l, "attenuation_r": attenuation_r,
              "source_frequency": source_frequency, "coupling_loss_fibre": coupling_loss_fibre,
              "det_dark_count_prob": det_dark_count_prob,
              "det_efficiency": det_efficiency, "det_visibility": det_visibility,
              "det_num_resolving": det_num_resolving, "fibre_phase_stdv_l": fibre_phase_stdv_l,
              "fibre_phase_stdv_r": fibre_phase_stdv_r, "num_repeaters": 0, "length": length,
              "num_attempts_proto": num_attempts_proto, "state_files_directory": state_files_directory,
              "multiple_link_successes": multiple_link_successes}
    # check certain sim_params for sanity
    sim_params = _check_sim_params(**params)
    mpn_index_alice, mpn_index_bob = _get_mpn_indices(sim_params)  # retrieve mpn index for both nodes

    # Create physical network
    network = Network(name="Elementary Link Setup")
    # Two end nodes Alice and Bob
    alice = EndNode("Alice", mpn_index=mpn_index_alice, **sim_params)
    bob = EndNode("Bob", mpn_index=mpn_index_bob, **sim_params)
    # Add them to the Network
    network.add_nodes([alice, bob])
    # Create a heralded connection and add it to the Network by connecting it to Alice and Bob
    heralded_connection = HeraldedConnection("HeraldedConnection", **sim_params)
    network.add_connection(node1=alice, node2=bob, connection=heralded_connection, label="heralding",
                           port_name_node1="IO_Connection_E", port_name_node2="IO_Connection_E")

    # Alice
    if sim_params["magic"]:
        # "Magic" , therefore using MagicProtocol instead of EmissionProtocol
        # We are on the left side of an elementary link, so create new MagicDistributor
        magic_distributor = AEMagicDistributor(nodes=[alice, bob], heralded_connection=heralded_connection,
                                               repetition_delay=repetition_delay)
        # Using MagicProtocol instead of actual EmissionProtocol
        emission_prot_a = MagicProtocol(alice, memory=alice.subcomponents["QMem"], component=heralded_connection)
    else:
        # running full simulation, adding regular EmissionProtocol
        delay = max(0, 1e9 * sim_params["channel_length_r"] / 200000 - 1e9 * sim_params["channel_length_l"] / 200000)
        emission_prot_a = EmissionProtocol(alice, memory=alice.subcomponents["QMem"],
                                           source=alice.subcomponents["PPS"],
                                           delay=delay,
                                           max_attempts=sim_params["num_attempts_proto"])
    # Bob
    if sim_params["magic"]:
        # "Magic" , therefore using MagicProtocol instead of EmissionProtocol
        # We are on the right side of an elementary link, so not creating new MagicDistributor
        emission_prot_b = MagicProtocol(bob, memory=bob.subcomponents["QMem"], component=heralded_connection)
    else:
        # running full simulation, adding regular EmissionProtocol
        delay = max(0, 1e9 * sim_params["channel_length_l"] / 200000 - 1e9 * sim_params["channel_length_r"] / 200000)
        emission_prot_b = EmissionProtocol(bob, memory=bob.subcomponents["QMem"],
                                           source=bob.subcomponents["PPS"],
                                           delay=delay,
                                           max_attempts=sim_params["num_attempts_proto"])

    protocols = [emission_prot_a, emission_prot_b]

    if sim_params["magic"]:
        # if using magic start the automatic delivery of states
        magic = [magic_distributor, [emission_prot_a, emission_prot_b]]
    else:
        magic = []

    return protocols, network, magic


def create_single_repeater(memory_coherence_time, max_memory_efficiency, memory_time_dependence, mem_num_modes,
                           source_num_modes, Rate, g2, magic, multi_photon, encoding, mean_photon_number,
                           emission_probabilities, source_frequency, time_step, num_attempts, channel_length_l,
                           channel_length_r, attenuation_l, attenuation_r, coupling_loss_fibre, det_dark_count_prob,
                           det_efficiency, det_visibility, det_num_resolving,
                           swap_det_dark_count_prob, swap_det_efficiency, swap_det_visibility, swap_det_num_resolving,
                           fibre_phase_stdv_l, fibre_phase_stdv_r, length, num_attempts_proto, state_files_directory,
                           multiple_link_successes, repetition_delay=0, extraction_delay=0., **kwargs):
    """Set up a 3-node repeater chain using atomic ensembles including an end-node detection setup for QKD.
    This function sets up a single repeater chain made up of identical (possibly asymmetric) elementary links.

    Schematics of the setup:

     .. code-block:: text

        ---------o--------o==messaging==o----------o==messaging==o--------o---------
        | det_a | | alice |             | repeater |             |  bob  | | det_b |
        ---------o--------o====|BSM|====o----------o====|BSM|====o--------o---------

        Where:
        o       is a port
        =       is a connection
        |BSM|   is an optical Bell state measurement
        ----    boundaries of a node

    Parameters
    ----------
    memory_coherence_time : float
        Memory coherence time in [ns].
    max_memory_efficiency : float
        Memory efficiency at t=0 (between 0 and 1).
    memory_time_dependence : str
        Time dependence of the memory efficiency. Default is "exponential".
    memory_num_modes : int
        Number of multiplexing modes for memory.
    source_num_modes : int
        Number of multiplexing modes for source.
    Rate: float, optinoal
        Measurement of the rate of generated photon pairs in [Hz].
        Used for the Number of modes calculation.
        Should be specified if num_multiplexing_modes is not specified.
    g2: float, optinoal
        Measurement of the second order correlation function, g2, for the heralded light from the source.
        Used for the Number of Modes calculation
        Should be specified if num_multiplexing_modes is not specified.
    source_frequency : float, optional
        Frequency at which the source creates qubits in [Hz].
    magic : bool
        Whether magic is used or not. This determines if a memory position is created for every multiplexing mode or
        only a single memory position (magic=True) is created to save resources.
    multi_photon : bool
        Whether the simulation is using multi photon emission or not.
    encoding : str
        Encoding of the photons. Either "time_bin" or "presence_absence".
    mean_photon_number : list or None
        List of mean photon numbers for the thermal distribution of the SPDC sources along the chain.
        The `mpn_index` (see below) then determines which mean photon number is assigned to which source. This allows
        for different mean photon numbers along the chain.
        Must be supplied when `emission_probabilities` is None.
    emission_probabilities: list or None
        List of lists of probabilities of emitting a certain number of photons along the chain.
        The `mpn_index` (see below) then determines which list of emission_probabilities is assigned to which source.
        This allows for different emission_probabilities along the chain.
        This emission_probabilities must be a list of length 4, where each element denotes the probability of
        generating a photon pair with the number of photons equal to its list index.
    time_step : float
        Time step at which the clock on this node ticks in [ns]. Only used if magic=False.
    num_attempts : int
        Number of ticks after which the clock stops, effectively stopping attempts of entanglement generation. Set to
        -1 for unlimited operation.
    channel_length_l : float
        Length of the channel left of the detectors [in km].
    channel_length_r : float
        Length of the channel right of the detectors [in km].
    attenuation_l : float
        Attenuation of the channel left of the detectors [in dB/km].
    attenuation_r: float
        Attenuation of the channel right of the detectors [in dB/km].
    coupling_loss_fibre : float
        Coupling loss/ length independent loss of the channel to the midpoint, in [0,1].
    det_dark_count_prob : float
        Dark count probability of the midpoint detectors.
    det_efficiency : float
        Detection efficiency of the midpoint detectors.
    det_visibility : float
        Photon indistinguishability at the midpoint BSM.
    det_num_resolving : bool
        Whether midpoint detectors are number-resolving.
    swap_det_dark_count_prob : float
        Dark count probability of the swap and end_node detectors.
    swap_det_efficiency : float
        Detection efficiency of the swap and end_node detectors.
    swap_det_visibility : float
        Photon indistinguishability at the swap BSM. Note: for the end_node detectors this is hardcoded to 1 (perfect).
    swap_det_num_resolving : bool
        Whether swap and end_node detectors are number-resolving.
    fibre_phase_stdv_l : float
        Standard deviation of the gaussian distribution of the phase for PhaseLossModel on the channel left of
        the detectors. Set to 0.0 in order to not include `PhaseLossModel` in the simulation.
    fibre_phase_stdv_r : float
        Standard deviation of the gaussian distribution of the phase for PhaseLossModel on the channel right of
        the detectors. Set to 0.0 in order to not include `PhaseLossModel` in the simulation.
    num_attempts_proto : int
        Number of entanglement attempts to run the entanglement generation protocol for.
        Set to -1 to run protocol until explicitly stopped.
    state_files_directory : str
        Name of directory containing elementary link state files saved as
        :obj:`.netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder` in *.pickle format following
        the naming convention defined in `forced_magic_state_generator.py`.
        These should have been generated using this specific heralded connection.
        This is only needed when using magic ('forced' or 'sampled') and will be handled and checked by
        the `~.netsquid_ae.ae_magic_distributor.AEMagicDistributor`.
    multiple_link_successes : bool
        If set to True, multiple modes can be successful otherwise the detector stops measuring after the first
        successful mode.
    length : float
        Total length of the elementary link. Can be specified instead of `channel_length_l` and `channel_length_r` to
        generate a symmetric elementary link of `channel_length_l = channel_length_r = length / 2`.
    repetition_delay : float
        Delay between automated deliveries by the `~.netsquid_ae.ae_magic_distributor.AEMagicDistributor.
        Set to non-zero in order to avoid concurrence issues.
    extraction_delay : float (optional)
        Virtual time delay between receiving heralding message from midpoint and extracting the qubit from the memory
        in the :class:`~netsquid_ae.ae_protocols.ExtractionProtocol`. Only set to non-zero values if full post-swap
        end-to-end quantum states need to be extracted to ensure all swaps have been performed before extraction.

    Returns
    -------
    protocols : list
        List of all protocols (:class:`~netsquid.protocols.Protocol`) in the simulation.
    network : :class:`~netsquid.nodes.network.Network`
        Network component that contains all the :class:`~netsquid.nodes.Node`s and
        :class:`~netsquid.connections.Connection`s that represent the physical network.
    magic : list
        List with the merged MagicDistributor (:class:`~netsquid_magic.magic_distributor.MagicDistributor`) as first
        item and a list of all magic protocols as the second item.
        Empty list when no magic is used.

    """
    params = {"memory_coherence_time": memory_coherence_time, "max_memory_efficiency": max_memory_efficiency,
              "memory_time_dependence": memory_time_dependence, "mem_num_modes": mem_num_modes,
              "source_num_modes": source_num_modes, "Rate": Rate, "g2": g2,
              "magic": magic, "multi_photon": multi_photon, "encoding": encoding,
              "mean_photon_number": mean_photon_number, "emission_probabilities": emission_probabilities,
              "time_step": time_step, "num_attempts": num_attempts,
              "channel_length_l": channel_length_l, "channel_length_r": channel_length_r,
              "attenuation_l": attenuation_l, "attenuation_r": attenuation_r,
              "source_frequency": source_frequency, "coupling_loss_fibre": coupling_loss_fibre,
              "det_dark_count_prob": det_dark_count_prob,
              "det_efficiency": det_efficiency, "det_visibility": det_visibility,
              "det_num_resolving": det_num_resolving,
              "swap_det_dark_count_prob": swap_det_dark_count_prob,
              "swap_det_efficiency": swap_det_efficiency, "swap_det_visibility": swap_det_visibility,
              "swap_det_num_resolving": swap_det_num_resolving,
              "fibre_phase_stdv_l": fibre_phase_stdv_l, "fibre_phase_stdv_r": fibre_phase_stdv_r,
              "num_repeaters": 1, "length": length,
              "num_attempts_proto": num_attempts_proto, "state_files_directory": state_files_directory,
              "multiple_link_successes": multiple_link_successes}
    # check certain sim_params for sanity
    sim_params = _check_sim_params(**params)
    mpn_index_alice, mpn_index_rep, mpn_index_bob = _get_mpn_indices(sim_params)  # retrieve mpn index for all nodes

    # Set up physical network with specific naming of the nodes
    alice = EndNode("A_1", mpn_index=mpn_index_alice, **sim_params)
    bob = EndNode("B_1", mpn_index=mpn_index_bob, **sim_params)
    repeater = RepeaterNode("R_1_1", mpn_index=mpn_index_rep, **sim_params)
    # Create network and add nodes in a list from left to right
    network = Network("Single Repeater Setup", nodes=[alice, repeater, bob])
    # Create two connections for each pair of nodes, used for either entanglement generation or
    # sending classical messages
    heralded_connection_a_r = HeraldedConnection("Conn_A_R", **sim_params)
    heralded_connection_r_b = HeraldedConnection("Conn_R_B", **sim_params)
    messaging_connection_a_r = MessagingConnection("MessagingConn_A_R", **sim_params)
    messaging_connection_r_b = MessagingConnection("MessagingConn_R_B", **sim_params)
    # Add them all to the network in the correct order and by referring to the correct ports
    network.add_connection(node1=alice, node2=repeater, connection=heralded_connection_a_r, label="heralding",
                           port_name_node1="IO_Connection_E", port_name_node2="IO_Connection_L")
    network.add_connection(node1=repeater, node2=bob, connection=heralded_connection_r_b, label="heralding",
                           port_name_node1="IO_Connection_R", port_name_node2="IO_Connection_E")
    network.add_connection(node1=alice, node2=repeater, connection=messaging_connection_a_r, label="messaging",
                           port_name_node1="Messaging_IO_R", port_name_node2="Messaging_IO_L")
    network.add_connection(node1=repeater, node2=bob, connection=messaging_connection_r_b, label="messaging",
                           port_name_node1="Messaging_IO_R", port_name_node2="Messaging_IO_L")

    # Set up all the protocols
    protocols = []
    magic_distributors = []
    magic_protocols = []
    # Alice
    if sim_params["magic"]:
        # "Magic" , therefore using MagicProtocol instead of EmissionProtocol
        # We are on the left side of an elementary link, so create new MagicDistributor
        magic_distributor = AEMagicDistributor(nodes=[alice, repeater],
                                               heralded_connection=heralded_connection_a_r,
                                               repetition_delay=repetition_delay)
        magic_distributors.append(magic_distributor)
        # Note that we use this variable name for its later use in the ExtractionProtocol
        emission_prot = MagicProtocol(alice, memory=alice.subcomponents["QMem"], component=heralded_connection_a_r)
        magic_protocols.append(emission_prot)
    else:
        # running full simulation, adding regular EmissionProtocol
        emission_prot = EmissionProtocol(alice, memory=alice.subcomponents["QMem"],
                                         source=alice.subcomponents["PPS"],
                                         max_attempts=sim_params["num_attempts_proto"])

    protocols.append(emission_prot)
    protocols.append(ExtractionProtocol(alice, emission_prot, delay=extraction_delay))
    protocols.append(MessagingProtocol(alice, emission_protocol_1=emission_prot))

    # Repeater
    if sim_params["magic"]:
        # "Magic" , therefore using MagicProtocol instead of EmissionProtocol
        # Create a new MagicDistributor for the elementary link on the right side of this repeater node
        new_magic_distributor = AEMagicDistributor(nodes=[repeater, bob],
                                                   heralded_connection=heralded_connection_r_b,
                                                   repetition_delay=repetition_delay)
        magic_distributors.append(new_magic_distributor)
        # Use the component of the previously created MagicDistributor on the left side
        emission_prot_l = MagicProtocol(repeater, memory=repeater.subcomponents["QMem_L"],
                                        component=heralded_connection_a_r)
        # Use the component of the MagicDistributor on the right side
        emission_prot_r = MagicProtocol(repeater, memory=repeater.subcomponents["QMem_R"],
                                        component=heralded_connection_r_b)
        magic_protocols.append(emission_prot_l)
        magic_protocols.append(emission_prot_r)
    else:
        # running full simulation, adding regular EmissionProtocol
        emission_prot_l = EmissionProtocol(repeater, memory=repeater.subcomponents["QMem_L"],
                                           source=repeater.subcomponents["PPS_L"],
                                           max_attempts=sim_params["num_attempts_proto"])
        emission_prot_r = EmissionProtocol(repeater, memory=repeater.subcomponents["QMem_R"],
                                           source=repeater.subcomponents["PPS_R"],
                                           max_attempts=sim_params["num_attempts_proto"])
    swap_prot = SwapProtocol(repeater, emission_prot_l, emission_prot_r)
    protocols.append(emission_prot_l)
    protocols.append(emission_prot_r)
    protocols.append(swap_prot)
    protocols.append(MessagingProtocol(repeater, emission_protocol_1=emission_prot_l,
                                       emission_protocol_2=emission_prot_r,
                                       swap_protocol=swap_prot))
    # Bob
    if sim_params["magic"]:
        # "Magic" , therefore using MagicProtocol instead of EmissionProtocol
        # We are on the right side of an elementary link, so not creating new MagicDistributor
        emission_prot = MagicProtocol(bob, memory=bob.subcomponents["QMem"], component=heralded_connection_r_b)
        magic_protocols.append(emission_prot)
    else:
        # running full simulation, adding regular EmissionProtocol
        emission_prot = EmissionProtocol(bob, memory=bob.subcomponents["QMem"],
                                         source=bob.subcomponents["PPS"],
                                         max_attempts=sim_params["num_attempts_proto"])
    protocols.append(emission_prot)
    protocols.append(ExtractionProtocol(bob, emission_prot, delay=extraction_delay))
    protocols.append(MessagingProtocol(bob, emission_protocol_1=emission_prot))

    # QKD application
    ##########################################################
    # This application can be removed/replaced and the Repeater Chain still remains functional.
    # Data collection should be adjusted accordingly (needs to be called by the correct Protocols)
    # Depending on the application, changes might be necessary to the messaging protocol.

    # Here we are interested in generating measurement data for extraction of secret-key-rate as efficiently as
    # possible. Therefore Alice and Bob always measure in the same basis here.
    det_protos = create_qkd_application(network=network, sim_params=sim_params, measure_directly=True,
                                        initial_measurement_basis="X")
    protocols.extend(det_protos)

    # add merge MagicDistributors
    if sim_params["magic"]:
        # for implementation of magic merge and start MagicDistributor
        magic_dist = magic_distributors[0]
        magic_dist.merge_magic_distributor(magic_distributors[1])
        magic = [magic_dist, magic_protocols]
    else:
        magic = []

    return protocols, network, magic


def create_repeater_chain(memory_coherence_time, max_memory_efficiency, memory_time_dependence, mem_num_modes,
                          source_num_modes, Rate, g2, magic, multi_photon, encoding, mean_photon_number,
                          emission_probabilities, source_frequency, time_step, num_attempts, channel_length_l,
                          channel_length_r, attenuation_l, attenuation_r, coupling_loss_fibre, num_repeaters,
                          det_dark_count_prob, det_efficiency, det_visibility, det_num_resolving,
                          swap_det_dark_count_prob, swap_det_efficiency, swap_det_visibility, swap_det_num_resolving,
                          fibre_phase_stdv_l, fibre_phase_stdv_r, length, num_attempts_proto, state_files_directory,
                          multiple_link_successes, repetition_delay=0., id_str="1", **kwargs):
    """Set up a n-node repeater chain using atomic ensembles.
    This function sets up a repeater chain made up of identical (possibly asymmetric) elementary links.

     Schematics of the setup:

     .. code-block:: text

        End Node              <--------- n Repeater Nodes --------->              End Node

        o-------o==messaging==o------------o== . . . ==o------------o==messaging==o-------o
        | alice |             | repeater_1 |           | repeater_n |             |  bob  |
        o-------o====|BSM|====o------------o== . . . ==o------------o====|BSM|====o-------o

        Protocols:

        Emission                            Emission, Emission                      Emission
        Extraction                          Swap                                    Extraction
        Messaging                           Messaging                               Messaging

        Where:
        o       is a port
        =       is a connection
        |BSM|   is an optical Bell state measurement
        ----    boundaries of a node

    Note: :class:`.EmissionProtocol` can be substituted by :class:`.MagicProtocol`

    Parameters
    ----------
    memory_coherence_time : float
        Memory coherence time in [ns].
    max_memory_efficiency : float
        Memory efficiency at t=0 (between 0 and 1).
    memory_time_dependence : str
        Time dependence of the memory efficiency. Default is "exponential".
    memory_num_modes : int
        Number of multiplexing modes for memory.
    source_num_modes : int
        Number of multiplexing modes for source.
    Rate: float, optinoal
        Measurement of the rate of generated photon pairs in [Hz].
        Used for the Number of modes calculation.
        Should be specified if num_multiplexing_modes is not specified.
    g2: float, optinoal
        Measurement of the second order correlation function, g2, for the heralded light from the source.
        Used for the Number of Modes calculation
        Should be specified if num_multiplexing_modes is not specified.
    source_frequency : float, optional
        Frequency at which the source creates qubits in [Hz].
    magic : bool
        Whether magic is used or not. This determines if a memory position is created for every multiplexing mode or
        only a single memory position (magic=True) is created to save resources.
    multi_photon : bool
        Whether the simulation is using multi photon emission or not.
    encoding : str
        Encoding of the photons. Either "time_bin" or "presence_absence".
    mean_photon_number : list or None
        List of mean photon numbers for the thermal distribution of the SPDC sources along the chain.
        The `mpn_index` (see below) then determines which mean photon number is assigned to which source. This allows
        for different mean photon numbers along the chain.
        Must be supplied when `emission_probabilities` is None.
    emission_probabilities: list or None
        List of lists of probabilities of emitting a certain number of photons along the chain.
        The `mpn_index` (see below) then determines which list of emission_probabilities is assigned to which source.
        This allows for different emission_probabilities along the chain.
        This emission_probabilities must be a list of length 4, where each element denotes the probability of
        generating a photon pair with the number of photons equal to its list index.
    time_step : float
        Time step at which the clock on this node ticks in [ns]. Only used if magic=False.
    num_attempts : int
        Number of ticks after which the clock stops, effectively stopping attempts of entanglement generation. Set to
        -1 for unlimited operation.
    channel_length_l : float
        Length of the channel left of the detectors [in km].
    channel_length_r : float
        Length of the channel right of the detectors [in km].
    attenuation_l : float
        Attenuation of the channel left of the detectors [in dB/km].
    attenuation_r: float
        Attenuation of the channel right of the detectors [in dB/km].
    coupling_loss_fibre : float
        Coupling loss/ length independent loss of the channel to the midpoint, in [0,1].
    num_repeaters : int
        Number of repeaters in the chain.
    det_dark_count_prob : float
        Dark count probability of the midpoint detectors.
    det_efficiency : float
        Detection efficiency of the midpoint detectors.
    det_visibility : float
        Photon indistinguishability at the midpoint BSM.
    det_num_resolving : bool
        Whether midpoint detectors are number-resolving.
    swap_det_dark_count_prob : float
        Dark count probability of the swap detectors.
    swap_det_efficiency : float
        Detection efficiency of the swap detectors.
    swap_det_visibility : float
        Photon indistinguishability at the swap BSM.
    swap_det_num_resolving : bool
        Whether swap and detectors are number-resolving.
    fibre_phase_stdv_l : float
        Standard deviation of the gaussian distribution of the phase for PhaseLossModel on the channel left of
        the detectors. Set to 0.0 in order to not include `PhaseLossModel` in the simulation.
    fibre_phase_stdv_r : float
        Standard deviation of the gaussian distribution of the phase for PhaseLossModel on the channel right of
        the detectors. Set to 0.0 in order to not include `PhaseLossModel` in the simulation.
    num_attempts_proto : int
        Number of entanglement attempts to run the entanglement generation protocol for.
        Set to -1 to run protocol until explicitly stopped.
    state_files_directory : str
        Name of directory containing elementary link state files saved as
        :obj:`.netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder` in *.pickle format following
        the naming convention defined in `forced_magic_state_generator.py`.
        These should have been generated using this specific heralded connection.
        This is only needed when using magic ('forced' or 'sampled') and will be handled and checked by
        the `~.netsquid_ae.ae_magic_distributor.AEMagicDistributor`.
    multiple_link_successes : bool
        If set to True, multiple modes can be successful otherwise the detector stops measuring after the first
        successful mode.
    length : float
        Total length of the elementary link. Can be specified instead of `channel_length_l` and `channel_length_r` to
        generate a symmetric elementary link of `channel_length_l = channel_length_r = length / 2`.
    repetition_delay : float
        Delay between automated deliveries by the `~.netsquid_ae.ae_magic_distributor.AEMagicDistributor.
        Set to non-zero in order to avoid concurrence issues.
    id_str : str
        Identification string of the chain. Will be added to component names for easier identification.

    Returns
    -------
    protocols : list
        List of all protocols (:class:`~netsquid.protocols.Protocol`) in the chain.
    network : :class:`~netsquid.nodes.network.Network`
        Network component that contains all the :class:`~netsquid.nodes.Node`s and
        :class:`~netsquid.connections.Connection`s that represent the physical network.
    magic : list
        List with the merged MagicDistributor (:class:`~netsquid_magic.magic_distributor.MagicDistributor`) as first
        item and a list of all magic protocols as the second item.
        Empty list when no magic is used.


    Note
    ------
    All connecting is done from left to right (Alice->Bob).


    Example

    .. code-block:: python​

        from netsquid_ae.ae_chain_setup import create_repeater_chain

        # setting up a QKD experiment in presence-absence encoding
        # create two repeater chains
        protocols, nodes = create_repeater_chain(self.sim_params, id_str="1")
        protocols_2, nodes_2 = create_repeater_chain(self.sim_params, id_str="2")
        # combine both networks into a single large one
        network1.combine(network2)

        # create and connect QKD end-node detection setup
        det_protocols = create_qkd_application(network=network1, sim_params=self.sim_params)

        # start protocols and clock triggering the protocol
        for proto in protocols.extend(protocols_2.extend(det_protocols)):
            proto.start()
        for node in list(network.nodes.values())[:-2]:
            node.subcomponents["Clock"].start()

        # run simulation

        netsquid.sim_run()

    """

    params = {"memory_coherence_time": memory_coherence_time, "max_memory_efficiency": max_memory_efficiency,
              "memory_time_dependence": memory_time_dependence, "mem_num_modes": mem_num_modes,
              "source_num_modes": source_num_modes, "Rate": Rate, "g2": g2,
              "magic": magic, "multi_photon": multi_photon, "encoding": encoding,
              "mean_photon_number": mean_photon_number, "emission_probabilities": emission_probabilities,
              "time_step": time_step, "num_attempts": num_attempts,
              "channel_length_l": channel_length_l, "channel_length_r": channel_length_r,
              "attenuation_l": attenuation_l, "attenuation_r": attenuation_r,
              "source_frequency": source_frequency,
              "coupling_loss_fibre": coupling_loss_fibre,
              "det_dark_count_prob": det_dark_count_prob,
              "det_efficiency": det_efficiency, "det_visibility": det_visibility,
              "det_num_resolving": det_num_resolving,
              "swap_det_dark_count_prob": swap_det_dark_count_prob,
              "swap_det_efficiency": swap_det_efficiency, "swap_det_visibility": swap_det_visibility,
              "swap_det_num_resolving": swap_det_num_resolving,
              "fibre_phase_stdv_l": fibre_phase_stdv_l, "fibre_phase_stdv_r": fibre_phase_stdv_r,
              "num_repeaters": num_repeaters, "length": length,
              "num_attempts_proto": num_attempts_proto, "state_files_directory": state_files_directory,
              "multiple_link_successes": multiple_link_successes}
    # check certain sim_params for sanity
    sim_params = _check_sim_params(**params)
    # Set up network
    network = _create_physical_network(name=f"{num_repeaters}-Repeater Chain {id_str}", sim_params=sim_params,
                                       num_repeaters=num_repeaters, id_str=id_str)
    # Then set up all the protocols and add magic if required
    protocols, magic_protocols, magic_distributor = _add_protocols_and_magic(network, sim_params,
                                                                             repetition_delay=repetition_delay)
    magic = [magic_distributor, magic_protocols]

    # debugging
    if logger.isEnabledFor(logging.DEBUG):
        # print out of all ports and what they are connected to
        for node in list(network.nodes.values()):
            print("Checking Ports on: ", node.name)
            for port in node.ports:
                print(port, ": connected to :", node.ports[port].connected_port)

    return protocols, network, magic


def create_qkd_application(network, sim_params, measure_directly=True, initial_measurement_basis="Z",
                           qkd_protocol=None):
    """Function that creates an end node detection setup and automatically connects it to one or two repeater chains.

    Parameters
    ----------
    network: `~netsquid.nodes.network.Network`
        Network that contains the repeater chain and thus the end nodes.
    sim_params : dict
        Simulation parameters.
    measure_directly : bool (optional)
        Whether photons at the end node should be measured directly (True) or only after being stored in the memory.
    initial_measurement_basis : str in ["X", "Y", "Z"] (optional)
        The initial common measurement basis. To generate more data an initial measurement basis can be specified to
        avoid the factor of 1/2 due to Alice and Bob choosing basis at random. The common measurement basis then needs
        to be changed manually after half the simulation.
        The default value is `None`, which corresponds to a random basis selection.
    qkd_protocol : str in ["BB84", "six_state"] (optional)
        The quantum-key distribution protocol to be used. Can either be 'BB84' or 'six_state', which corresponds to
        random basis selection of "X" or "Z" or "X", "Y" or "Z" respectively.
        Default value is `None`, which implies that the measurement basis is set manually.


    Returns
    -------
    protocols : list
        List of the protocols (:class:`~netsquid.protocols.Protocol`) on the detector nodes.

    """
    protocols = []
    # extract the end nodes from the Network
    end_nodes = [node for node in network.nodes.values() if isinstance(node, EndNode)]

    if measure_directly:
        # photons are measured directly, therefore removing noise on the end node memory
        for node in end_nodes:
            node.subcomponents["QMem"].models["qout_noise_model"] = None

    # create detector nodes
    det_a, det_b = QKDNode("det_alice", sim_params), QKDNode("det_bob", sim_params)
    network.add_nodes([det_a, det_b])
    # connect detector nodes to repeater chain(s)
    for end_node in end_nodes:
        name, id_str = end_node.name.split("_")
        if name == "A":
            # connect Det to Alice (Qbit and Messaging)
            det_a.ports[f"IN_Chain_{id_str}"].connect(end_node.ports["Qbit_out"])
            det_a.ports[f"Message_IO_Chain_{id_str}"].connect(end_node.ports["Messaging_IO_L"])
        elif name == "B":
            # connect Bob to Det (Qbit and Messaging)
            end_node.ports["Qbit_out"].connect(det_b.ports[f"IN_Chain_{id_str}"])
            end_node.ports["Messaging_IO_R"].connect(det_b.ports[f"Message_IO_Chain_{id_str}"])
        else:
            raise RuntimeError(f"Invalid end node name {name}, must either be 'A' or 'B'.")

    # set up protocols (set initial measurement basis (will be changed after half the successes by DecisionMaker))
    #
    # Note: in a real QKD experiment Alice and Bob would choose their measurement basis randomly (security requirement).
    # Here we are interested in generating measurement data for extraction of secret-key-rate as efficiently as
    # possible. Therefore Alice and Bob always measure in the same basis here.
    #
    det_proto_a = DetectionProtocol(det_a, measurement_basis=initial_measurement_basis, qkd_protocol=qkd_protocol)
    det_proto_b = DetectionProtocol(det_b, measurement_basis=initial_measurement_basis, qkd_protocol=qkd_protocol)
    protocols.append(det_proto_a)
    protocols.append(det_proto_b)
    protocols.append(MessagingProtocol(det_a, detection_protocol=det_proto_a))
    protocols.append(MessagingProtocol(det_b, detection_protocol=det_proto_b))

    return protocols


def _check_sim_params(**kwargs):
    """Check if all given simulation parameters are in order.

    Checks:
    * num_repeaters
    * channel_length_l/r, length
    * time_step
    * multi_photon, mean_photon_number, emission_probabilities
    """
    # check number of repeaters
    if kwargs["num_repeaters"] < 0:
        raise ValueError(f"Number of repeaters must be a non-negative integer not {kwargs['num_repeaters']}")

    kwargs = _check_and_process_length_params(**kwargs)

    # Check if time_step of rounds is set large enough
    speed_of_light_in_fiber = 200000    # default value from `~.netsquid.components.models.delaymodels.FibreDelayModel`
    cycle_time = ((kwargs["channel_length_l"] + kwargs["channel_length_r"]) / speed_of_light_in_fiber) * 1e9
    if kwargs["time_step"] <= 1.20 * cycle_time:
        raise ValueError(f"Time step of rounds t = {kwargs['time_step']} smaller than or within 20% of the"
                         f" cycle time t_c = {cycle_time} [* 1.2 = {1.20 * cycle_time}] Please increase time_step to"
                         f" ensure correct behavior of protocols (memory reset).")

    # inform about usage of mean photon number and emission probability
    if kwargs["multi_photon"] and kwargs["mean_photon_number"] != ParameterSet._NOT_SPECIFIED and \
            isinstance(kwargs["emission_probabilities"], tuple):
        print("Warning: emission_probabilities are not used since mu has been specified.")

    return kwargs


def _check_and_process_length_params(**kwargs):
    """Check if all given simulation parameters regarding length are in order.

    Note: If 'channel_length_l' and 'channel_length_r' are not defined, the 'length' will be divided in symmetric links
    according to the 'num_repeaters'.
    """
    # check that either total length or both channel lengths are set up
    if kwargs["length"] < 0 and (kwargs["channel_length_l"] < 0 and kwargs["channel_length_r"] < 0):
        raise ValueError("Either length or both channel lengths need to be defined but length, left channel length, "
                         f"and right channel length are: {kwargs['length']}, {kwargs['channel_length_l']}, "
                         f"{kwargs['channel_length_l']}.")

    # check that if all length params are defined, they are defined correctly
    if kwargs["length"] >= 0 and kwargs["channel_length_l"] >= 0 and kwargs["channel_length_r"] >= 0:
        node_distance = kwargs["channel_length_l"] + kwargs["channel_length_r"]
        if kwargs["length"] != node_distance * (kwargs["num_repeaters"] + 1):
            raise ValueError(f"Total length {kwargs['length']} and sum of node distances "
                             f"{node_distance * (kwargs['num_repeaters'] + 1)} not the same.")

    # convert length and channel_lengths as necessary
    if kwargs["channel_length_l"] < 0 and kwargs["channel_length_r"] < 0:
        kwargs["channel_length_l"] = kwargs["length"] / (kwargs["num_repeaters"] + 1) / 2
        kwargs["channel_length_r"] = kwargs["length"] / (kwargs["num_repeaters"] + 1) / 2
        print(f"Set symmetric channel lengths: {kwargs['channel_length_l']}, "
              f"from total length: {kwargs['length']}.")
    elif kwargs["length"] < 0:
        kwargs["length"] = (kwargs["channel_length_l"] + kwargs["channel_length_r"]) * (kwargs["num_repeaters"] + 1)
        print(f"Set total length: {kwargs['length']}, from channel lengths: "
              f"{kwargs['channel_length_l']} and {kwargs['channel_length_r']}.")

    return kwargs


def _get_mpn_indices(sim_params):
    """
    Retrieve mean photon number indices for all nodes in the chain. This index should be defined when the mean photon
    number is defined individually for each source and should be None if all sources use the same value.

    Note that a repeater node has two sources but gets assigned only the smaller mpn index out of the two.

    Example for illustrating how the indices work with 1 repeater node and mean photon number defined in `sim_params`
    as [0.1, 0.2, 0.3, 0.4]:

    .. code-block:: text

        +-------+           +-------+           +-------+
        | Alice |           |  Rep  |           |  Bob  |
        +-------+           +-------+           +-------+
        mpn_index = 0       mpn_index = 1       mpn_index = 3

    The mean photon number for all sources is then extracted as `sim_params["mean_photon_number"][mpn_index]`. For the
    repeater node, the second source then has `sim_params["mean_photon_number"][mpn_index + 1]`.

    Parameters
    ----------
    sim_params : dict
        Dictionary containing simulation parameters.
    """
    num_sources = 2 + 2 * sim_params["num_repeaters"]
    if sim_params["multi_photon"] \
            and sim_params["mean_photon_number"] is not None \
            and type(sim_params["mean_photon_number"]) is list:
        if len(sim_params["mean_photon_number"]) != num_sources:
            raise ValueError(f"There should be {num_sources} individually defined mean photon numbers for a repeater "
                             f"chain of {sim_params['num_repeaters']} repeaters "
                             f"but instead there are {len(sim_params['mean_photon_number'])}")
        mpn_indices = [1 + 2 * r for r in range(sim_params["num_repeaters"])]
        return [0] + mpn_indices + [num_sources - 1]
    else:
        return [None] * (2 + sim_params["num_repeaters"])
