import numpy as np

from netsquid_ae.ae_parameter_set import AEParameterSet

from netsquid_simulationtools.parameter_set import ParameterSet

NA = ParameterSet._NOT_SPECIFIED


class Guha2015Base(AEParameterSet):
    """Baseclass for the parameters used to investigate the effect of multi-photon emission in
     S. Guha, PRA 92, 022357 (2015).
     Note: Subclass for different emission probabilities.
     """
    # SOURCE
    source_encoding = "time_bin"
    source_frequency = 1 / 50e-9
    source_spectral_modes = 1000
    source_spatial_modes = 1
    source_temporal_modes = 1
    photon_num_distribution = "custom_guha"
    mean_photon_number = NA                     # not used
    Rate = NA                                   # not used
    g2 = NA                                     # not used

    # MEMORY
    max_memory_efficiency = 10 ** (-1 / 10)     # 1dB memory loss
    memory_T1 = NA
    memory_T2 = np.inf
    memory_time_dependence = "exponential"
    memory_spectral_modes = 1000
    memory_spatial_modes = 1
    memory_temporal_modes = 1
    memory_additional_noise = "No"
    memory_encoding = "time_bin"

    # FIBER (telecom)
    # attenuation = 0.15
    coupling_loss_fibre = 0.

    # MIDPOINT DETECTORS
    det_visibility = 1.
    det_time_window = 1.
    det_dead_time = 0.
    det_after_pulse = NA
    det_dark_count_rate = 1e3
    det_efficiency = 0.9
    det_num_resolving = False
    det_mode_limit = False
    det_lin_opt = True

    # SWAP DETECTORS
    swap_det_visibility = 1.
    swap_det_time_window = 1.
    swap_det_dead_time = 0.
    swap_det_after_pulse = NA
    swap_det_dark_count_rate = 1e3
    swap_det_efficiency = 0.9
    swap_det_num_resolving = False
    swap_det_mode_limit = False
    swap_det_lin_opt = True

    # ABSTRACT PARAMS
    el_link_fidelity = NA
    el_link_succ_prob = NA
    swap_quality = NA
    swap_probability = NA


class Guha2015p000(Guha2015Base):
    """Perfect single pair source with no multi-pair emission."""
    emission_probabilities = [1 - .9, .9, 0., 0]


class Guha2015p013(Guha2015Base):
    """Source with probability of emitting two pairs p(2) = 0.013."""
    emission_probabilities = [1 - .9 - .013, .9, .013, 0]


class Guha2015p019(Guha2015Base):
    """Source with probability of emitting two pairs p(2) = 0.019."""
    emission_probabilities = [1 - .9 - .019, .9, .019, 0]


class Guha2015p035(Guha2015Base):
    """Source with probability of emitting two pairs p(2) = 0.035."""
    emission_probabilities = [1 - .9 - .035, .9, .035, 0]


class Guha2015p041(Guha2015Base):
    """Source with probability of emitting two pairs p(2) = 0.041."""
    emission_probabilities = [1 - .9 - .041, .9, .041, 0]


class Guha2015p000_v9(Guha2015Base):
    """Perfect single pair source with no multi-pair emission and imperfect photon visibility=0.9 at the detectors."""
    emission_probabilities = [1 - .9, .9, 0., 0]
    det_visibility = .9
    swap_det_visibility = .9


class Guha2015p013_td_v9(Guha2015Base):
    """Source with probability of emitting two pairs p(2) = 0.013, imperfect photon visibility=0.9 and finite memory
    lifetime t=1e5ns."""
    emission_probabilities = [1 - .9 - .013, .9, .013, 0]
    memory_T2 = 1e5
    det_visibility = .9
    swap_det_visibility = .9


class PerfectPhotonPairTest(AEParameterSet):
    """Test parameter set with a source that deterministically emits single photons."""
    # SOURCE
    source_encoding = "time_bin"
    source_frequency = 10e3
    source_spectral_modes = 100
    source_spatial_modes = 1
    source_temporal_modes = 1
    photon_num_distribution = "perfect"
    mean_photon_number = NA
    emission_probabilities = [0., 1., 0., 0.]

    # MEMORY
    max_memory_efficiency = 1.
    memory_T1 = NA
    memory_T2 = np.inf
    memory_time_dependence = "exponential"
    memory_spectral_modes = 100
    memory_spatial_modes = 1
    memory_temporal_modes = 1
    memory_additional_noise = "No"
    memory_encoding = "time_bin"

    # FIBER (telecom)
    # attenuation = 0.2
    coupling_loss_fibre = 0.

    # MIDPOINT DETECTORS
    det_visibility = 1.
    det_time_window = 0.
    det_dead_time = 0.
    det_after_pulse = NA
    det_dark_count_rate = 0.
    det_efficiency = 1.
    det_num_resolving = False
    det_mode_limit = 100
    det_lin_opt = True

    # SWAP DETECTORS
    swap_det_visibility = 1.
    swap_det_time_window = 0.
    swap_det_dead_time = 0.
    swap_det_after_pulse = NA
    swap_det_dark_count_rate = 0.
    swap_det_efficiency = 1.
    swap_det_num_resolving = False
    swap_det_mode_limit = False
    swap_det_lin_opt = True

    # ABSTRACT PARAMS
    el_link_fidelity = 1.
    el_link_succ_prob = 1.
    swap_quality = 1.
    swap_probability = 1.


if __name__ == "__main__":
    # test if parameter sets are specified correctly
    test1 = PerfectPhotonPairTest()
    test2 = Guha2015p000()
    test3 = Guha2015p035()
    test4 = Guha2015p041()
