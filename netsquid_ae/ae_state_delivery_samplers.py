import os
import numpy as np
import pickle
from scipy.sparse import csr_matrix

from netsquid.qubits.ketutil import ket2dm
from netsquid.qubits.ketstates import BellIndex
from netsquid.qubits.state_sampler import StateSampler

from netsquid_magic.state_delivery_sampler import HeraldedStateDeliverySamplerFactory, SingleClickDeliverySamplerFactory

from netsquid_physlayer.detectors import BSMOutcome

from netsquid_simulationtools.repchain_sampler import RepchainSampler

from netsquid_ae.forced_magic_state_generator import generate_forced_magic, set_magic_file_name


class AEStateDeliverySamplerFactory(HeraldedStateDeliverySamplerFactory):
    """Baseclass for a multiplexed HeraldedStateDeliverySamplerFactory that works with the AEMagicDistributor
    to deliver states along a chain.

    Subclassed for "analytical", "sampled" and "forced" magic.

    """
    def __init__(self, func_delivery):
        self._single_mode_success_prob = None
        super().__init__(func_delivery=func_delivery)

    @property
    def single_mode_success_prob(self):
        return self._single_mode_success_prob

    @staticmethod
    def _multiplexed_success_probability(single_mode_success_prob, num_multiplexing_modes):
        """ Probability of successfully establishing link with at least one of the multiplexing modes

        Parameters
        ----------
        num_multiplexing_modes : int
            Total number of multiplexing modes used.
        single_mode_success_prob : float
            Probability to generate heralded entanglement for a single mode.

        """
        return 1 - np.power((1 - single_mode_success_prob), num_multiplexing_modes)

    def magic_distributor_probability_override(self):
        """Function used by the MagicDistributor to overwrite the success probability."""
        self.p_1 = 1.

    @staticmethod
    def _check_baseline_parameters(dataframe_holder, **kwargs):
        """Check if baseline parameters of saved states agree with simulation parameters.

        This is a helper function for sampled and forced magic.

        Parameters
        ----------
        dataframe_holder : :obj:`~netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder`
            RepchainDataFrameHolder with dataframe containing all possible elementary link outcomes as labels and
            corresponding elementary link states and probabilities and the parameters they were generated with.
        kwargs : dict
            Simulation parameters to compare against.

        """
        baseline_params = dataframe_holder.baseline_parameters

        # make sure states were single mode
        if baseline_params["num_multiplexing_modes"] != 1:
            raise ValueError(f"Proper implementation of multiplexing only if sample is generated from"
                             f" single mode! Here sample had {baseline_params['num_multiplexing_modes']} modes.")
        # Compare baseline parameters to simulation parameters
        for key in baseline_params:
            if key in ['encoding', 'mean_photon_number_l', 'mean_photon_number_r',
                       'emission_probabilities_l', 'emission_probabilities_r'
                       'channel_length_l', 'channel_length_r', 'attenuation_l', 'attenuation_r',
                       'coupling_loss_l', 'coupling_loss_r',
                       'det_dark_count_prob', 'det_efficiency', 'det_visibility', 'det_num_resolving']:
                if isinstance(kwargs[key], list):
                    # dataframe holder saves lists as tuples while simulation works with lists
                    assert baseline_params[key] == tuple(kwargs[key])
                elif baseline_params[key] != kwargs[key]:
                    raise ValueError("Baseline parameter {} has value {} in sample, but value {} in simulation."
                                     .format(key, baseline_params[key], kwargs[key]))


class AEAnalyticalSDSFactory(AEStateDeliverySamplerFactory):
    """HeraldedStateDeliverySamplerFactory that produces DeliverySamples based on the analytical model described in
    https://arxiv.org/pdf/1404.7183.pdf , Appendix A.1 for time_bin encoding

   For presence_absence a simplified version of the
   `netsquid_magic.state_delivery_sampler.SingleClickDeliverySamplerFactory` is used that is equivalent.

   Both models thus assume perfect emission probability p_em = 1. and det_visibility = 1.

    """
    def __init__(self):
        super().__init__(func_delivery=self._create_analytical_state_sampler)

    def _create_analytical_state_sampler(self, det_dark_count_prob, det_efficiency, attenuation_l,
                                         attenuation_r, channel_length_l, channel_length_r, encoding, multi_photon,
                                         num_multiplexing_modes, **kwargs):
        """
        Parameters
        ----------
        det_dark_count_prob : float
            Midpoint detector dark count probability.
        det_efficiency : float
            Midpoint detector detection efficiency.
        attenuation_l : float
            Attenuation of the fiber to the left of the detectors.
        attenuation_r : float
            Attenuation of the fiber to the right of the detectors.
        channel_length_l : float
            Length of the channel left of the detectors in km.
        channel_length_r : float
            Length of the channel right of the detectors in km.
        num_multiplexing_modes : int
            Number of multiplexing modes.
        """
        assert encoding in ["time_bin", "presence_absence"]

        if encoding == "time_bin":
            if not (kwargs["emission_probabilities_l"] == [0., 1., 0., 0.] and
                    kwargs["emission_probabilities_r"] == [0., 1., 0., 0.]):
                raise NotImplementedError("Multi photon analytical magic only implemented for perfect photon pair "
                                          f"sources, not for probabilities {kwargs['emission_probabilities_l']} and "
                                          f"{kwargs['emission_probabilities_r']}")

            a_e, b_e, c_e = self._compute_coefficients(det_dark_count_prob, det_efficiency, attenuation_l,
                                                       attenuation_r, channel_length_l, channel_length_r)
            s_1 = a_e + b_e + 2 * c_e
            if multi_photon:
                psi_0 = np.zeros([2 ** 8, 1], dtype=complex)
                psi_0[17] = 1  # |00,01;00,01>
                psi_1 = np.zeros([2 ** 8, 1], dtype=complex)
                psi_1[20] = 1  # |00,01;01,00>
                psi_2 = np.zeros([2 ** 8, 1], dtype=complex)
                psi_2[65] = 1  # |01,00;00,01>
                psi_3 = np.zeros([2 ** 8, 1], dtype=complex)
                psi_3[68] = 1  # |01,00;01,00>
                m_plus = (psi_1 + psi_2) / np.sqrt(2)
                m_minus = (psi_1 - psi_2) / np.sqrt(2)
            else:
                psi_0 = np.array([[1., 0., 0., 0.]], dtype=complex).T  # |00>
                psi_3 = np.array([[0., 0., 0., 1.]], dtype=complex).T  # |11>
                # (|01> + |10>)/sqrt(2)
                m_plus = np.array([[0., 1. / np.sqrt(2), 1. / np.sqrt(2), 0.]], dtype=complex).T
                # (|01> - |10>)/sqrt(2)
                m_minus = np.array([[0., 1. / np.sqrt(2), - 1. / np.sqrt(2), 0.]], dtype=complex).T
            # Density matrix for when the midpoint yields outcome `1`
            dm_mp1 = (a_e * ket2dm(m_minus) + b_e * ket2dm(m_plus) + c_e * ket2dm(psi_0) + c_e * ket2dm(psi_3)) / s_1
            # Density matrix for when the midpoint yields outcome `2`
            dm_mp2 = (a_e * ket2dm(m_plus) + b_e * ket2dm(m_minus) + c_e * ket2dm(psi_0) + c_e * ket2dm(psi_3)) / s_1

            dm_states = [dm_mp1, dm_mp2]

            self._single_mode_success_prob = 4 * s_1

        else:
            # presence-absence, use simplified version of SingleClickDeliverySamplerFactory and extend to multi-photon
            p_det_a, p_det_b = SingleClickDeliverySamplerFactory._compute_total_detection_probability(
                length_A=channel_length_l, length_B=channel_length_r, p_loss_length_A=attenuation_l,
                p_loss_length_B=attenuation_r, p_loss_init_A=0., p_loss_init_B=0.,
                detector_efficiency=det_efficiency
            )

            dm_states, traces = [], []
            # simplifications to be equivalent to model for time-bin
            visibility = 1
            p_fail_class_corr = 0
            coherent_phase = 0
            [p_up_up, p_up_down, p_down_up, p_down_down] = \
                SingleClickDeliverySamplerFactory._compute_detection_probabilities(
                    alpha_A=kwargs["emission_probabilities_l"][1],
                    alpha_B=kwargs["emission_probabilities_r"][1],
                    p_dc=det_dark_count_prob,
                    p_det_A=p_det_a,
                    p_det_B=p_det_b,
                    visibility=visibility,
                    num_resolving=kwargs["det_num_resolving"])
            for detector_outcome in [1, -1]:
                # Define the un-normalized matrix of Psi^{\pm}
                s = np.sqrt(visibility * p_up_down * p_down_up) * detector_outcome
                if not multi_photon:
                    dm_psi = np.array(
                        [[0.5 * p_fail_class_corr * (p_up_down + p_down_up) + p_up_up, 0., 0., 0.],
                         [0., (1 - p_fail_class_corr) * p_up_down,
                          (1 - p_fail_class_corr) * np.exp(coherent_phase * 1j) * s, 0.],
                         [0., (1 - p_fail_class_corr) * np.exp(-1 * coherent_phase * 1j) * s,
                          (1 - p_fail_class_corr) * p_down_up, 0.],
                         [0., 0., 0., 0.5 * p_fail_class_corr * (p_up_down + p_down_up) + p_down_down]],
                        dtype=complex)
                else:
                    dm_psi = np.zeros([16, 16], dtype=complex)
                    dm_psi[0, 0] = 0.5 * p_fail_class_corr * (p_up_down + p_down_up) + p_up_up
                    dm_psi[1, 1] = (1 - p_fail_class_corr) * p_up_down
                    dm_psi[1, 4] = (1 - p_fail_class_corr) * np.exp(coherent_phase * 1j) * s
                    dm_psi[4, 1] = (1 - p_fail_class_corr) * np.exp(-1 * coherent_phase * 1j) * s
                    dm_psi[4, 4] = (1 - p_fail_class_corr) * p_down_up
                    dm_psi[5, 5] = 0.5 * p_fail_class_corr * (p_up_down + p_down_up) + p_down_down
                # normalization
                traces.append(np.trace(dm_psi))
                dm_states.append(dm_psi / np.trace(dm_psi))

            self._single_mode_success_prob = sum([p_up_up, p_up_down, p_down_up, p_down_down])

        state_sampler = StateSampler(dm_states,
                                     probabilities=[.5, .5],
                                     labels=[BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS),
                                             BSMOutcome(success=True, bell_index=BellIndex.PSI_MINUS)])

        multiplexed_success_prob = self._multiplexed_success_probability(self._single_mode_success_prob,
                                                                         num_multiplexing_modes)
        self._success_probability = multiplexed_success_prob

        return state_sampler, multiplexed_success_prob

    @staticmethod
    def _compute_coefficients(qdet_pdark, qdet_efficiency, attenuation_l, attenuation_r, length_l, length_r):
        """Compute the coefficients according to Guha et al., 2015."""
        P_e = qdet_pdark
        eta_e = qdet_efficiency
        lambda_t_l = np.power(10, - (attenuation_l / 10) * (length_l / 2))
        lambda_t_r = np.power(10, - (attenuation_r / 10) * (length_r / 2))
        A_e_l = eta_e * lambda_t_l + P_e * (1 - eta_e * lambda_t_l)
        A_e_r = eta_e * lambda_t_r + P_e * (1 - eta_e * lambda_t_r)
        B_e = 1 - (1 - P_e) * (1 - eta_e * lambda_t_l) * (1 - eta_e * lambda_t_r)
        # Probability of getting the correct single clicks in both time windows
        a_e = (1 / 8) * (P_e ** 2 * (1 - A_e_l) * (1 - A_e_r) + A_e_l * A_e_r * (1 - P_e) ** 2)
        # Probability of getting a single click in both time windows, but in the wrong combination, such that
        # the midpoint outcome and the corresponding Bell state do not coincide
        b_e = (1 / 8) * (A_e_l * P_e * (1 - A_e_l) * (1 - P_e) + A_e_r * P_e * (1 - A_e_r) * (1 - P_e))
        # Probability of getting a single click in both time windows without there being actual entanglement
        c_e = (1 / 8) * P_e * (1 - P_e) * (P_e * (1 - B_e) + B_e * (1 - P_e))
        return a_e, b_e, c_e


class AESampledSDSFactory(AEStateDeliverySamplerFactory):
    """HeraldedStateDeliverySamplerFactory that produces DeliverySamples based on the sampled data of a single mode.
    """
    def __init__(self):
        self._state_dataframe_holder = None
        super().__init__(func_delivery=None)

    @staticmethod
    def load_sampled_magic(**kwargs):

        if kwargs['state_files_directory'] is None:
            raise ValueError("For sampled magic state_files_directory must not be None.")
        print(kwargs)
        filename = set_magic_file_name(unique_str=None, **kwargs)

        if os.path.isfile(filename):
            print(f"State Sample File {filename} found on disk.")
            sample_holder = pickle.load(open(filename, "rb"))

        else:
            raise RuntimeError(f"No sampled state file with name {filename} found on disk. Can not initialize sampled "
                               f"magic.")

        return sample_holder

    def create_state_delivery_sampler(self, num_multiplexing_modes, **kwargs):
        """
        Parameters
        ----------
        num_multiplexing_modes : int
            Total number of multiplexing modes.
        """
        if self._state_dataframe_holder is None:
            self._state_dataframe_holder = self.load_sampled_magic(**kwargs)

        # check parameters in dataframe holder versus input parameters
        self._check_baseline_parameters(self._state_dataframe_holder, **kwargs)

        channel_length_l = kwargs['channel_length_l']   # define this because the query command otherwise bugs out in
        channel_length_r = kwargs['channel_length_r']   # python<3.8

        node_distance = int(channel_length_l + channel_length_r)
        repchain_sampler = RepchainSampler(self._state_dataframe_holder, node_distance)

        # Get the subdataframe corresponding to the node distance of this simulation
        sample_df = \
            self._state_dataframe_holder.dataframe.\
            query("channel_length_l == @channel_length_l and channel_length_r == @channel_length_r").\
            reset_index(drop=True)
        if self._state_dataframe_holder.baseline_parameters["generation_duration_unit"] == "rounds":
            total_attempts = sample_df["generation_duration"].sum()
        else:
            raise ValueError("For sampled magic please use a data with generation_duration_unit = 'rounds' and not "
                             f"{self._state_dataframe_holder.baseline_parameters['generation_duration_unit']}")
        num_successes = len(sample_df.index)
        # Probability of successfully establishing link for a single spectral mode
        self._single_mode_success_prob = num_successes / total_attempts

        multiplexed_success_prob = self._multiplexed_success_probability(self._single_mode_success_prob,
                                                                         num_multiplexing_modes)
        self._success_probability = multiplexed_success_prob
        return repchain_sampler, multiplexed_success_prob


class AEForcedSDSFactory(AEStateDeliverySamplerFactory):
    """AESampledSDSFactory that uses forced midpoint outcomes to create the full (successful) quantum state of the
    elementary link.

    """
    def __init__(self):
        self._state_dataframe_holder = None
        super().__init__(func_delivery=self._create_state_sampler_from_forced_data)

    def _create_state_sampler_from_forced_data(self, encoding, num_multiplexing_modes, **kwargs):
        """
        Parameters
        ----------
        dataframe_holder : :obj:`~netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder`
            RepchainDataFrameHolder with dataframe containing all possible elementary link outcomes as labels and
            corresponding elementary link states and probabilities.
        encoding : str in ["presence_absence", "time_bin"]
            Encoding to be used. This influences the number of possible detector outcomes.
        num_multiplexing_modes : int
            Number of multiplexing modes to be used.

        """
        kwargs["encoding"] = encoding
        kwargs["num_multiplexing_modes"] = num_multiplexing_modes
        if self._state_dataframe_holder is None:
            self._state_dataframe_holder = generate_forced_magic(**kwargs)
        forced_states_df = self._state_dataframe_holder.dataframe
        # check parameters in dataframe holder versus input parameters
        self._check_baseline_parameters(self._state_dataframe_holder, **kwargs)
        # extract states and success probabilities from dataframe
        if encoding == "presence_absence":
            # presence_absence encoding -> two successful labels
            prob_success_a = forced_states_df.loc[forced_states_df["forced_label"] == "10"] \
                .success_probability.values[0]
            state_a = forced_states_df.loc[forced_states_df["forced_label"] == "10"].state.values[0]

            prob_success_b = forced_states_df.loc[forced_states_df["forced_label"] == "01"] \
                .success_probability.values[0]
            state_b = forced_states_df.loc[forced_states_df["forced_label"] == "01"].state.values[0]

        else:
            # time_bin encoding -> 2 possible outcomes per time-bin -> four successful labels in total
            # same detector clicked in both bins, BellIndex.PSI_PLUS
            prob_success_1 = forced_states_df.loc[forced_states_df["forced_label"] == "1010"] \
                .success_probability.values[0]
            state_1 = forced_states_df.loc[forced_states_df["forced_label"] == "1010"].state.values[0]
            prob_success_2 = forced_states_df.loc[forced_states_df["forced_label"] == "0101"] \
                .success_probability.values[0]
            state_2 = forced_states_df.loc[forced_states_df["forced_label"] == "0101"].state.values[0]
            if not np.isclose(prob_success_1, prob_success_2):
                raise ValueError(f"Probabilities for both states should be the same not {prob_success_1}"
                                 f"and {prob_success_2} with difference {prob_success_2-prob_success_1}")

            # combine state for BellIndex.PSI_PLUS
            # Note: both of the above measurement outcomes herald the projection onto PSI_PLUS, thus we can combine
            # their respective DMs and add their probabilities to the total probability of getting PSI_PLUS.
            state_a = (state_1 + state_2) / 2.
            prob_success_a = prob_success_1 + prob_success_2

            # different detectors clicked, BellIndex.PSI_MINUS
            prob_success_3 = forced_states_df.loc[forced_states_df["forced_label"] == "0110"] \
                .success_probability.values[0]
            state_3 = forced_states_df.loc[forced_states_df["forced_label"] == "0110"].state.values[0]
            prob_success_4 = forced_states_df.loc[forced_states_df["forced_label"] == "1001"] \
                .success_probability.values[0]
            state_4 = forced_states_df.loc[forced_states_df["forced_label"] == "1001"].state.values[0]
            if not np.isclose(prob_success_3, prob_success_4):
                raise ValueError(f"Probabilities for both states should be the same not {prob_success_3}"
                                 f"and {prob_success_4} with difference {prob_success_4-prob_success_3}")

            # combine state for BellIndex.PSI_MINUS , see above for explanation
            state_b = (state_3 + state_4) / 2.
            prob_success_b = prob_success_3 + prob_success_4

        # create state sampler
        if isinstance(state_a, csr_matrix) or isinstance(state_b, csr_matrix):
            # Convert to dense matrix and then to array
            state_a = np.squeeze(np.asarray(csr_matrix.todense(state_a)))
            state_b = np.squeeze(np.asarray(csr_matrix.todense(state_b)))
        if not np.isclose(prob_success_a, prob_success_b):
            raise ValueError(f"Probabilities for both states should be the same not {prob_success_a}"
                             f"and {prob_success_b} with difference {prob_success_b-prob_success_a}")

        state_sampler = StateSampler([state_a, state_b],
                                     probabilities=[.5, .5],
                                     labels=[BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS),
                                             BSMOutcome(success=True, bell_index=BellIndex.PSI_MINUS)])
        self._single_mode_success_prob = prob_success_a + prob_success_b

        multiplexed_success_prob = self._multiplexed_success_probability(self._single_mode_success_prob,
                                                                         num_multiplexing_modes)
        self._success_probability = multiplexed_success_prob

        return state_sampler, multiplexed_success_prob
