import abc
import math
import numpy as np
from netsquid.qubits.qubit import Qubit
from netsquid.qubits import qubitapi as qapi
from netsquid.qubits import operators as ops
from netsquid.util import simtools as simtools
from netsquid.components.models.qerrormodels import QuantumErrorModel
from netsquid.util.constrainedmap import ValueConstraint, nonnegative_constr


class QuantumLossModel(QuantumErrorModel):
    """Base class for exponential photon loss.

        Parameters
        ----------
        rng : instance of :class:`~numpy.random.RandomState` or None, optional
            Random number generator to use. If ``None`` then `~netsquid.util.simtools.get_random_state` is used.
        'mp_encoding' : str (property)
            Multi-photon encoding, either:
            'presence_absence': simple number state (2 qubits)
            'time_bin': time-bin encoding (4 qubits)
            None: no multi-photon encoding (1 qubit) > using regular amplitude damping


        """
    def __init__(self, **kwargs):
        super().__init__()
        # specify multi-photon encoding used
        # presence_absence -> 2 qubits encode 1 qubit
        # time_bin -> 4 qubits encode 1 qubit
        # None if multi-photon is not used
        self.required_properties.append('mp_encoding')

        self.properties['rng'] = simtools.get_random_state()
        self.properties.update(kwargs)

    def error_operation(self, qubits, delta_time=0, **kwargs):
        """Noisy quantum operation to apply to qubits.

        Here it is loss.

        Parameters
        ----------
        qubits : list of :class:`~netsquid.qubits.qubit.Qubit`
            Qubits to apply noise to.
        delta_time : float, optional
            Time qubit has spent on component [ns].

        """
        # calculate loss probability
        prob_loss = self.prob_item_lost(delta_time=delta_time, **kwargs)
        assert 0 <= prob_loss <= 1
        # check encoding
        if kwargs['mp_encoding'] not in [None, 'presence_absence', 'time_bin']:
            raise ValueError("LossModel received unknown encoding.")

        if kwargs['mp_encoding'] is None:
            # old code if not using multiphoton
            for qubit in qubits:
                if qubit is None:
                    continue
                if not isinstance(qubit, Qubit):
                    raise ValueError("Trying to apply noise to qubit {} which is type {} instead."
                                     .format(qubit, type(qubit)))
                elif qubit.is_number_state:
                    # If qubit is a number state, then we want to amplitude dampen
                    # towards |0> but not physically lose it.
                    qapi.amplitude_dampen(qubit, gamma=prob_loss, prob=1.)
                elif math.isclose(prob_loss, 1.) or self.properties["rng"].random_sample() <= prob_loss:
                    if qubit.qstate is not None:
                        qapi.discard(qubit)
        else:
            if kwargs["mp_encoding"] == 'presence_absence':
                group = 2
            else:
                group = 4
            self.set_operators(gamma=prob_loss, encoding=kwargs['mp_encoding'])
            for i in range(0, len(qubits), group):
                q = qubits[i:i + group]
                # TODO check if weights should be set
                qapi.multi_operate(q, self._meas_operators)

    @abc.abstractmethod
    def prob_item_lost(self, item, delta_time=0., **kwargs):
        """Probability that the item is lost (to be overriden).

        Parameters
        ----------
        item : Any or instance of:class:`~netsquid.qubits.qubit.Qubit`
            Item that may be lost.
        delta_time : float, optional
            Time item has spent on channel [ns].

        Returns
        -------
        float
            Probability that item is lost.

        """
        # Should be overwritten
        raise NotImplementedError

    def set_operators(self, gamma, encoding):
        """Set Kraus operators for generalized amplitude damping for multi-photon events.

        Here we follow eq. (2.4) of 'Bosonic quantum codes for amplitude damping', Chuang et al., 1997,
        https://link.aps.org/doi/10.1103/PhysRevA.56.1114

        Parameters
        ----------
        gamma : float
            Damping parameter.
        encoding: str in ['presence_absence', 'time_bin']
            Photon encoding used.

        """
        g = math.sqrt(gamma)  # SQRT(probability of losing a photon)
        h = math.sqrt(1 - gamma)
        # Kraus operators A_k for losing exactly k photons
        A_0 = np.array([[1, 0, 0, 0],
                        [0, h, 0, 0],
                        [0, 0, h ** 2, 0],
                        [0, 0, 0, h ** 3]])

        A_1 = np.array([[0, g, 0, 0],
                        [0, 0, math.sqrt(2) * g * h, 0],
                        [0, 0, 0, math.sqrt(3) * g * (h ** 2)],
                        [0, 0, 0, 0]])

        A_2 = np.array([[0, 0, g ** 2, 0],
                        [0, 0, 0, math.sqrt(3) * h * (g ** 2)],
                        [0, 0, 0, 0],
                        [0, 0, 0, 0]])

        A_3 = np.array([[0, 0, 0, g ** 3],
                        [0, 0, 0, 0],
                        [0, 0, 0, 0],
                        [0, 0, 0, 0]])
        n_0 = ops.Operator("n_0", A_0)
        n_1 = ops.Operator("n_1", A_1)
        n_2 = ops.Operator("n_2", A_2)
        n_3 = ops.Operator("n_3", A_3)

        if encoding == 'presence_absence':
            self._meas_operators = [n_0, n_1, n_2, n_3]
        if encoding == 'time_bin':
            self._meas_operators = []
            m = 0       # indexes for naming the generated operators (there must be a nicer way)
            for a in [A_0, A_1, A_2, A_3]:
                m += 1
                n = 0
                for b in [A_0, A_1, A_2, A_3]:
                    n += 1
                    self._meas_operators.append(ops.Operator('n_{}x{}'.format(m, n), np.kron(a, b)))


class MemoryLossModel(QuantumLossModel):
    """Loss model on the memory.

    Loss depends on the time spent on the memory.

    Parameters
    ----------
    time_dependence : str (optional)
        Type of time-dependence as a string: currently implemented only 'exponential' and 'gaussian' decay.
        'exponential' calculates p_loss from delta_time='time qubit spent on memory' as
            1 - (max_efficiency * np.exp(- delta_time / coherence_time)) ,
        'gaussian' as
            1 - (max_efficiency * np.exp(- .5 * (delta_time / coherence_time)**2)) .

    """

    def __init__(self, time_dependence, **kwargs):
        super().__init__(**kwargs)
        self.required_properties.append('max_efficiency')
        self.required_properties.append('coherence_time')
        self.required_properties.append('time_dependence')
        # self.required_properties.append('delta_time')
        if time_dependence not in ['exponential', 'gaussian']:
            raise ValueError(f"Unknown memory time-dependence '{time_dependence}'. Please choose 'gaussian' or"
                             f"'exponential'")
        else:
            self.time_dependence = time_dependence

    def prob_item_lost(self, delta_time, **kwargs):
        """Calculates probability that qubit is lost in memory depending on the time it spent there.

        Note: Delta_time must now be added as a property before applying the noise !

        Properties
        ----------
        delta_time : float, optional
            Time qubit has spent on component [ns].

        """

        if self.time_dependence == "exponential":
            return 1 - (kwargs['max_efficiency'] * np.exp(- self.properties['delta_time'] / kwargs['coherence_time']))
        else:
            return 1 - (kwargs['max_efficiency'] * np.exp(- .5 * (self.properties['delta_time'] /
                                                                  kwargs['coherence_time'])**2))


class MemoryLossModelOnMemory(MemoryLossModel):

    def prob_item_lost(self, delta_time, **kwargs):
        """Calculates probability that qubit is lost in memory depending on the time it spent there.

        Note: Delta_time must now be added as a property before applying the noise !

        Properties
        ----------
        delta_time : float, optional
            Time qubit has spent on component [ns].

        """

        if self.time_dependence == "exponential":
            return 1 - (kwargs['max_efficiency'] * np.exp(- delta_time / kwargs['coherence_time']))
        else:
            return 1 - (kwargs['max_efficiency'] * np.exp(- .5 * (delta_time /
                                                                  kwargs['coherence_time'])**2))


class FibreLossModel(QuantumLossModel):
    """Loss model on the Fibre."""

    def __init__(self, p_loss_init=0.2, p_loss_length=0.25, rng=None):
        super().__init__()
        self.add_property('p_loss_init', p_loss_init,
                          value_constraints=ValueConstraint(lambda x: 0 <= x <= 1))
        self.add_property('p_loss_length', p_loss_length,
                          value_constraints=nonnegative_constr)
        self.add_property('rng', rng, value_type=np.random.RandomState)
        self.rng = rng if rng else simtools.get_random_state()
        self.required_properties = ["length"]

    @property
    def rng(self):
        """ :obj:`~numpy.random.RandomState`: Random number generator."""
        return self.properties['rng']

    @rng.setter
    def rng(self, value):
        self.properties['rng'] = value

    @property
    def p_loss_init(self):
        """float: initial probability of losing a photon when it enters channel."""
        return self.properties['p_loss_init']

    @p_loss_init.setter
    def p_loss_init(self, value):
        self.properties['p_loss_init'] = value

    @property
    def p_loss_length(self):
        """float: photon survival probability per channel length [dB/km]."""
        return self.properties['p_loss_length']

    @p_loss_length.setter
    def p_loss_length(self, value):
        self.properties['p_loss_length'] = value

    def prob_item_lost(self, **kwargs):
        """Calculates probability that a Qubit is lost on the channel depending on the length of the channel.

        """
        return 1 - (1 - self.p_loss_init) * np.power(10, - kwargs['length'] * self.p_loss_length / 10)


class NoNoiseModel(QuantumErrorModel):
    """Empty QuantumNoiseModel that does not do anything."""

    def error_operation(self, qubits, delta_time=0, **kwargs):
        """Noisy quantum operation to apply to qubits.

        Parameters
        ----------
        qubits : tuple of :obj:`~netsquid.qubits.qubit.Qubit`
            Qubits to apply noise to.
        delta_time : float, optional
            Time qubit has spent on component [ns].

        """
        pass


class FixedLossModel(QuantumLossModel):
    """Model that applies fixed Loss with probability p = (1 - max_efficiency)."""

    def __init__(self, **kwargs):
        super().__init__()
        self.properties.update(kwargs)
        self.required_properties.append('max_efficiency')

    @property
    def p_loss(self):
        """float: probability of losing a photon in the component."""
        return 1 - self.properties['max_efficiency']

    def prob_item_lost(self, delta_time=0, **kwargs):
        """Calculates probability that Qubit is lost in memory depending on the time it spent there.

        Parameters
        ----------
        delta_time : float, optional
            Time Qubit has spent on component [ns]."""
        self.properties.update(kwargs)

        return 1 - self.properties['max_efficiency']


class FibrePhaseModel(QuantumErrorModel):
    """In presence-absence encoding photons travelling pick up a phase:
    1. fixed phase of the pump laser (DLCZ like schemes), Phi (with mean=0 and standard_deviation as parameter).
    2. random gaussian phase on the fibre, Theta.

    See e.g. https://arxiv.org/pdf/0906.2699.pdf equation (7)."""

    def __init__(self, multi_photon, standard_deviation, fixed_phase, **kwargs):
        super().__init__(**kwargs)
        self.properties.update({'multi_photon': multi_photon})
        self.properties.update({'standard_deviation': standard_deviation})
        self.properties.update({'fixed_phase': fixed_phase})
        self.properties['rng'] = simtools.get_random_state()

    @property
    def multi_photon(self):
        """bool: Whether multi-photon simulation is used."""
        return self.properties['multi_photon']

    @multi_photon.setter
    def multi_photon(self, value):
        self.properties['multi_photon'] = value

    @property
    def standard_deviation(self):
        """float: Standard deviation of the gaussian distribution of the phase."""
        return self.properties['standard_deviation']

    @standard_deviation.setter
    def standard_deviation(self, value):
        self.properties['standard_deviation'] = value

    @property
    def fixed_phase(self):
        """float: Fixed phase acquired on the channel."""
        return self.properties['fixed_phase']

    @fixed_phase.setter
    def fixed_phase(self, value):
        self.properties['fixed_phase'] = value

    def error_operation(self, qubits, **kwargs):
        """Noisy quantum operation to apply to qubits.

        Here it is an additional phase with mean=0 and standard deviation as parameter.

        Parameters
        ----------
        qubits : tuple of :class:`~netsquid.qubits.qubit.Qubit`
            Qubits to apply noise to.
        """
        # calculate random phase
        gaussian_phase = self.properties["rng"].normal(loc=0, scale=self.properties["standard_deviation"])
        # Note: Here we don't bring the phase back to the interval [0, np.pi] because the periodicity is already
        # accounted for in the exponential and the code is simpler without adding an additional modulo pi

        p = np.exp(1j * (self.properties['fixed_phase'] + gaussian_phase))

        if not self.properties['multi_photon']:
            # Kraus operator for up to 1 total photons
            phase = ops.Operator("phase", np.array([[1, 0],
                                                    [0, p]]))
            for qubit in qubits:
                if not isinstance(qubit, Qubit):
                    raise ValueError("Trying to apply noise to qubit {} which is type {} instead."
                                     .format(qubit, type(qubit)))
                elif qubit.is_number_state:
                    # If qubit is a number state, then we want to apply phase
                    qapi.operate(qubit, phase)
        else:
            # Kraus operator for up to 3 total photons
            phase = ops.Operator("phase", np.array([[1, 0, 0, 0],
                                                    [0, p, 0, 0],
                                                    [0, 0, p, 0],
                                                    [0, 0, 0, p]]))

            for i in range(0, len(qubits), 2):
                q = qubits[i:i + 2]
                qapi.operate(q, phase)
