from pydynaa.core import EventType
__all__ = [
    "EVTYPE_SUCCESS",
    "EVTYPE_FAIL",
    "EVTYPE_ERROR",
]
#: Event for successful execution of the protocol
EVTYPE_SUCCESS = EventType("SUCCESS", "Protocol finished with SUCCESS.")
#: Event for failed execution of the protocol
EVTYPE_FAIL = EventType("FAIL", "Protocol finished with FAIL.")
#: Event for error during execution of the protocol
EVTYPE_ERROR = EventType("ERROR", "Protocol finished with an ERROR.")
