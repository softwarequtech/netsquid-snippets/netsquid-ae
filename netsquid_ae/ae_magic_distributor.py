import logging
from copy import deepcopy


from pydynaa.core import EventHandler

import netsquid as ns
from netsquid.util.simlog import logger
from netsquid.components.component import Message
from netsquid.util.simtools import sim_time, get_random_state
from netsquid.qubits.qubitapi import create_qubits, assign_qstate

from netsquid_magic.state_delivery_sampler import DeliverySample
from netsquid_magic.magic_distributor import MagicDistributor, _Delivery

from netsquid_physlayer.detectors import BSMOutcome

from netsquid_ae.spdcsource import SPDCSource
from netsquid_ae.qlossmodel import FibrePhaseModel
from netsquid_ae.ae_classes import HeraldedConnection
from netsquid_ae.ae_state_delivery_samplers import AEAnalyticalSDSFactory, AEForcedSDSFactory, AESampledSDSFactory


class AEMagicDistributor(MagicDistributor):
    """MagicDistributor for atomic ensembles.

    This subclass is necessary for the fundamentally different protocol used for atomic ensemble architectures (e.g.
    two memories per repeater node, different number of qubits depending on encoding, etc.).

    Supports sampled, analytical and forced magic for both time_bin and presence_absence encoding.

    Parameters
    ----------
    nodes : list
        List of the two nodes this MagicDistributor should deliver to.
        The MagicDistributor takes the source parameters from the properties of the nodes.
    heralded_connection : :class:`~netsquid_ae.ae_classes.HeraldedConnection`
        HeraldedConnection the MagicDistributor is replacing.
        The MagicDistributor takes the detector and fibre parameters from the properties of the connection.
    repetition_delay : float, (optional)
        Delay [ns] between automated repetitions of deliveries. This can be used to give other protocols time to finish
        before the next delivery is scheduled.

    """

    COINCIDENCE = ns.EventType("SUCCESS", "Successful elementary link entanglement in all links at the same time.")

    DEFAULT_VALUES = {"coupling_loss_fibre": 0,
                      "attenuation_l": .25,
                      "speed_of_light_l": 200000,
                      "attenuation_r": .25,
                      "speed_of_light_r": 200000,
                      "det_dark_count_prob": 0,
                      "det_efficiency": 1,
                      "det_num_resolving": False,
                      "det_visibility": 1,
                      "num_multiplexing_modes": 1}

    SIDES = ["A", "B"]  # Names of ports of heralded connection, used to keep the two sides apart

    def __init__(self, nodes, heralded_connection=None,
                 channel_length_l=None, channel_length_r=None,
                 coupling_loss_fibre=None,
                 attenuation_l=None, attenuation_r=None,
                 speed_of_light_l=None, speed_of_light_r=None,
                 fibre_phase_stdv_l=None, fibre_phase_stdv_r=None,
                 det_dark_count_prob=None, det_efficiency=None,
                 det_visibility=None, det_num_resolving=None,
                 num_multiplexing_modes=None,
                 encoding=None,
                 magic=None, state_dataframe_holder=None,
                 multi_photon=None,
                 mean_photon_number_l=None, mean_photon_number_r=None,
                 emission_probabilities_l=None, emission_probabilities_r=None,
                 source_frequency=None,
                 state_files_directory=None,
                 repetition_delay=0,
                 multiple_link_successes=None):

        if heralded_connection is not None and not isinstance(heralded_connection, HeraldedConnection):
            raise ValueError("Magic distributor must read properties from a HeraldedConnection")

        try:
            node_dict_from_connections = \
                {side: heralded_connection.ports[side].connected_port.component for side in self.SIDES}
            if node_dict_from_connections != self.node_dict:
                raise RuntimeError("Node that is connected to 'A' port of heralded connection was not listed first"
                                   "in 'nodes' argument.")
        except AttributeError:
            pass

        kwargs = {"channel_length_l": channel_length_l, "channel_length_r": channel_length_r,
                  "coupling_loss_fibre": coupling_loss_fibre,
                  "attenuation_l": attenuation_l, "attenuation_r": attenuation_r,
                  "speed_of_light_l": speed_of_light_l, "speed_of_light_r": speed_of_light_r,
                  "fibre_phase_stdv_l": fibre_phase_stdv_l, "fibre_phase_stdv_r": fibre_phase_stdv_r,
                  "det_dark_count_prob": det_dark_count_prob,
                  "det_efficiency": det_efficiency,
                  "det_visibility": det_visibility,
                  "det_num_resolving": det_num_resolving,
                  "num_multiplexing_modes": num_multiplexing_modes,
                  "multi_photon": multi_photon,
                  "encoding": encoding,
                  "magic": magic, "state_dataframe_holder": state_dataframe_holder,
                  "mean_photon_number_l": mean_photon_number_l, "mean_photon_number_r": mean_photon_number_r,
                  "emission_probabilities_l": emission_probabilities_l,
                  "emission_probabilities_r": emission_probabilities_r,
                  "source_frequency": source_frequency,
                  "state_files_directory": state_files_directory,
                  "multiple_link_successes": multiple_link_successes
                  }
        # dict with not-none input parameters for initialization of super-class
        kwargs_not_none = {key: value for (key, value) in kwargs.items() if value is not None}

        # update entire list of kwargs with parameters from components in order to do extras here that are not in init
        # of super class.
        kwargs_not_none.update(self._get_parameters_from_nodes(nodes))
        kwargs.update(self._get_parameters_from_nodes(nodes))

        # Note: this gets only num_multiplexing_modes, multi_photon, magic, multiple_link_successes
        if heralded_connection is None and (channel_length_l is None or channel_length_r is None):
            raise ValueError("If heralded_connection is None, "
                             "both channel_length_l and channel_length_r need to be given a numerical value.")

        # set factory
        if kwargs["magic"] == "analytical":
            delivery_sampler_factory = AEAnalyticalSDSFactory()
        elif kwargs["magic"] == "forced":
            delivery_sampler_factory = AEForcedSDSFactory()
        else:  # Sampled magic
            delivery_sampler_factory = AESampledSDSFactory()
        # Calculate state delay for both nodes and set the max cycle time as label delay
        channel_length_l = heralded_connection.subcomponents["QCh_L"].properties["length"]
        channel_length_r = heralded_connection.subcomponents["QCh_R"].properties["length"]
        speed_of_light_l = heralded_connection.subcomponents["QCh_L"].models["delay_model"].properties["c"]
        speed_of_light_r = heralded_connection.subcomponents["QCh_R"].models["delay_model"].properties["c"]
        cycle_time_l = (channel_length_l * 2) / speed_of_light_l * 1E9
        cycle_time_r = (channel_length_r * 2) / speed_of_light_r * 1E9
        state_delay = {nodes[0].ID: cycle_time_l, nodes[1].ID: cycle_time_r}
        max_link_cycle_time = max(cycle_time_l, cycle_time_r)
        kwargs_not_none["cycle_time"] = max_link_cycle_time

        # current round and total success probability for chain delivery
        self._p_total = 1
        # optional delay between automated repetitions of deliveries
        self._repetition_delay = repetition_delay

        # set up handler that auto-repeats chain delivery
        self._request_handler = EventHandler(self.add_chain_delivery)

        # set rng
        self._rng = get_random_state()

        self._max_chain_cycle_time = -1  # holds maximum value of cycle time for the entire chain

        super().__init__(delivery_sampler_factory, nodes, component=heralded_connection, state_delay=state_delay,
                         label_delay=max_link_cycle_time, custom_labels=None, skip_rounds=False, **kwargs_not_none)
        # determine number of qubits per memory
        if self.fixed_delivery_parameters[0]["multi_photon"]:
            if self.fixed_delivery_parameters[0]["encoding"] == "time_bin":
                self._num_qubits_per_memory = [4]
            elif self.fixed_delivery_parameters[0]["encoding"] == "presence_absence":
                self._num_qubits_per_memory = [2]
            else:
                raise ValueError(f"Unknown multi-photon encoding {self.fixed_delivery_parameters[0]['encoding']}"
                                 f" on AEMagicDistributor.")
        else:
            self._num_qubits_per_memory = [1]

        # set memory positions
        self._memory_positions = list(range(self._num_qubits_per_memory[0]))

        # set phase noise if necessary
        self._set_phase_presence_absence()

        # create state delivery sampler such that chain_delivery can pick up initial probabilities
        self.delivery_sampler_factory[0].create_state_delivery_sampler(**self.fixed_delivery_parameters[0])
        # set source frequency to calculate the minimal repetition rate for the chain_delivery
        self._source_frequency = self.fixed_delivery_parameters[0]["source_frequency"]
        self._first_delivery = True

        # set counter to uniquely identify deliveries
        self._delivery_id = 0

    def _set_phase_presence_absence(self):
        self._apply_phase = False
        if self.fixed_delivery_parameters[0]['encoding'] == 'presence_absence':
            if self.fixed_delivery_parameters[0]['fibre_phase_stdv_l'] not in [None, 0.]:
                self._fibre_phase_model_l = \
                    FibrePhaseModel(multi_photon=self.fixed_delivery_parameters[0]['multi_photon'],
                                    standard_deviation=self.fixed_delivery_parameters[0]['fibre_phase_stdv_l'],
                                    fixed_phase=0)
            else:
                self._fibre_phase_model_l = None
            if self.fixed_delivery_parameters[0]['fibre_phase_stdv_r'] not in [None, 0.]:
                self._fibre_phase_model_r = \
                    FibrePhaseModel(multi_photon=self.fixed_delivery_parameters[0]['multi_photon'],
                                    standard_deviation=self.fixed_delivery_parameters[0]['fibre_phase_stdv_r'],
                                    fixed_phase=0)
            else:
                self._fibre_phase_model_r = None
            if not (self._fibre_phase_model_l is None and self._fibre_phase_model_r is None):
                self._apply_phase = True

    @property
    def node_dict(self):
        return dict(zip(self.SIDES, self.nodes[0]))

    @staticmethod
    def _get_parameters_from_heralded_connection(heralded_connection):
        """Read parameters from a heralded connection component.

        Parameters
        ----------
        heralded_connection : :class:`netsquid-physlayer.heraldedGeneration.HeraldedConnection

        Returns
        -------
        dictionary with parameter values

        """
        if heralded_connection is None:
            return {}

        assert isinstance(heralded_connection, HeraldedConnection)
        bsmdet = heralded_connection.subcomponents["QDet"]
        quantum_channel_l = heralded_connection.subcomponents["QCh_L"]
        quantum_channel_r = heralded_connection.subcomponents["QCh_R"]
        channel_length_l = quantum_channel_l.properties["length"]
        channel_length_r = quantum_channel_r.properties["length"]
        speed_of_light_l = heralded_connection.subcomponents["QCh_L"].models["delay_model"].properties["c"]
        speed_of_light_r = heralded_connection.subcomponents["QCh_R"].models["delay_model"].properties["c"]
        attenuation_l = quantum_channel_l.models["quantum_noise_model"].p_loss_length
        attenuation_r = quantum_channel_r.models["quantum_noise_model"].p_loss_length
        coupling_loss_fibre = quantum_channel_l.models["quantum_noise_model"].p_loss_init
        coupling_loss__other_fibre = quantum_channel_r.models["quantum_noise_model"].p_loss_init
        assert coupling_loss_fibre == coupling_loss__other_fibre
        fibre_phase_stdv_l = quantum_channel_l.properties["fibre_phase_stdv"]
        fibre_phase_stdv_r = quantum_channel_r.properties["fibre_phase_stdv"]
        state_files_directory = heralded_connection.properties["state_files_directory"]
        # determine sources that are connected to this heralded connection
        # determine nodes the connection is conneted to
        node_l = heralded_connection.ports["A"].connected_port.component
        node_r = heralded_connection.ports["B"].connected_port.component
        # determine target ports
        # from list of sources try which one is forwarded to the port that is connected to this connection
        for subcomponent in node_l.subcomponents.filter_by_type(SPDCSource):
            if (node_l.subcomponents[subcomponent].ports["qout0"].forwarded_ports["output"].connected_port ==
                    heralded_connection.ports["A"]):
                source_l = node_l.subcomponents[subcomponent]
            else:
                pass
        for subcomponent in node_r.subcomponents.filter_by_type(SPDCSource):
            if (node_r.subcomponents[subcomponent].ports["qout0"].forwarded_ports["output"].connected_port ==
                    heralded_connection.ports["B"]):
                source_r = node_r.subcomponents[subcomponent]
            else:
                pass
        encoding = source_l.encoding
        assert encoding == source_r.encoding
        source_frequency = source_l.frequency
        assert source_frequency == source_r.frequency
        emission_probabilities_l = [q**2 for q in source_l.probability_amplitudes]
        emission_probabilities_r = [q**2 for q in source_r.probability_amplitudes]
        mean_photon_number_l = source_l.mu
        mean_photon_number_r = source_r.mu

        params = {
            "channel_length_l": channel_length_l,
            "channel_length_r": channel_length_r,
            "attenuation_l": attenuation_l,
            "attenuation_r": attenuation_r,
            "coupling_loss_fibre": coupling_loss_fibre,
            "speed_of_light_l": speed_of_light_l,
            "speed_of_light_r": speed_of_light_r,
            "fibre_phase_stdv_l": fibre_phase_stdv_l,
            "fibre_phase_stdv_r": fibre_phase_stdv_r,
            "det_dark_count_prob": bsmdet.p_dark,
            "det_efficiency": bsmdet.det_eff,
            "det_visibility": bsmdet.visibility,
            "det_num_resolving": bsmdet.num_resolving,
            "encoding": encoding,
            "mean_photon_number_l": mean_photon_number_l,
            "mean_photon_number_r": mean_photon_number_r,
            "emission_probabilities_l": emission_probabilities_l,
            "emission_probabilities_r": emission_probabilities_r,
            "source_frequency": source_frequency,
            "state_files_directory": state_files_directory
        }

        return params

    @staticmethod
    def _get_parameters_from_nodes(nodes):
        if len(nodes) == 1:
            # after merging the MD this becomes necessary
            nodes = nodes[0]
        magic = nodes[0].properties["magic"]
        assert magic == nodes[1].properties["magic"]
        num_multiplexing_modes = min(nodes[0].properties["num_multiplexing_modes"],
                                     nodes[1].properties["num_multiplexing_modes"])
        multi_photon = nodes[0].properties["multi_photon"]
        assert multi_photon == nodes[1].properties["multi_photon"]
        multiple_link_successes = nodes[0].properties["multiple_link_successes"]
        assert multiple_link_successes == nodes[1].properties["multiple_link_successes"]
        return {"num_multiplexing_modes": num_multiplexing_modes,
                "multi_photon": multi_photon,
                "magic": magic,
                "multiple_link_successes": multiple_link_successes}

    def _read_params_from_nodes_or_component(self):
        params = deepcopy(self.DEFAULT_VALUES)
        [heralded_connection] = self._component

        params.update(self._get_parameters_from_heralded_connection(heralded_connection))
        params.update(self._get_parameters_from_nodes(self.nodes))

        return params

    def start_auto_delivery(self):
        """Start the automated generation of chain deliveries."""
        # current round and total success probability for chain delivery

        self._p_total = 1
        self._first_delivery = True
        # calculate total success probability over all the links
        for dsf in self.delivery_sampler_factory:
            self._p_total *= dsf._success_probability
            dsf.magic_distributor_probability_override()
        # check if success probability is within reason (np.random.geometric is buggy for very small values)
        if self._p_total <= 1e-16:
            raise ValueError(f"Success probability of this setup is {self._p_total}. Please consider using improved "
                             f"parameters.")
        # wait for coincidence event
        self._wait(self._request_handler, event_type=self.COINCIDENCE, entity=self)
        # initiate first request
        self._max_chain_cycle_time = max([x for delays in self._label_delay for x in list(delays.values())])
        first_success_duration = self._calculate_next_successful_round()
        self._schedule_after(first_success_duration, self.COINCIDENCE)

    def stop_auto_delivery(self):
        """Stop the automated generation of chain deliveries."""
        # dismiss the handler
        self._dismiss(self._request_handler, event_type=self.COINCIDENCE, entity=self)

    def _calculate_next_successful_round(self):
        """Calculate round when success in all elementary links happens and add it to current round.

        Returns
        -------
        generation_duration : float
            Duration until the next successful round.
        """
        number_of_attempts = self._rng.geometric(self._p_total)
        assert number_of_attempts != 0

        self.repetition_duration = max(1 / self._source_frequency, self._max_chain_cycle_time)
        if (1 / self._source_frequency) > self._max_chain_cycle_time:
            print("1/source_frequency larger than cycle time of ", self._max_chain_cycle_time,
                  ". Repetition rate therefore limited by source.")

        # add optional delay to the next success duration
        return self.repetition_duration * number_of_attempts + self._repetition_delay

    def add_chain_delivery(self, event, **kwargs):
        """Add chain delivery across all nodes in the merged MagicDistributor. This is done by looping over all
        MagicDistributors contained in the merged MagicDistributor that calls this function.
        Additionally the total success probability is calculated and the delivery sampler factories are overwritten
        to always return success.

        If this is the first chain delivery, state and label delays are set. Labels should arrive at the same time
        (max cycle time of all links) in the entire chain, such that all qubits are extracted from memory at the
        same time for swapping. State delays are different for varying lengths, their derivation works as follows:
        1. calculate when state should be at midpoint of link n:
            midpoint_arrival_n = max_chain_cycle_time - (max_link_n_cycle_time / 2)
        2. calculate when state needs to be sent:
            state_delay_l = midpoint_arrival_n - (cycle_time_l / 2)
            state_delay_r = midpoint_arrival_n - (cycle_time_r / 2)
        Both states need to arrive at the same time but have to travel potentially different times to the midpoint.
        To gain more intuition, see :class:`~.TestMagicDistributor` for schematics of specific setups.

        Parameters
        ----------
        kwargs : additional args that will be passed on to
            :obj:`~netsquid_magic.state_delivery_sampler.DeliverySampler`.sample

        """
        if logger.isEnabledFor(logging.DEBUG):
            print("{:.2e} ++ {}: MagicDistributor has coincident success. Adding chain delivery.".format(
                sim_time(), self.nodes[0][0].name))

        # reset all memories
        # TODO: test this
        for memory_dict in self._qmemories:
            for memory in memory_dict.values():
                memory.reset()

        # for each original part of magic distributor, call add delivery
        for i in range(len(self.delivery_sampler_factory)):
            # get ids of nodes in each part of the magic distributor
            corresponding_node_ids = [node.ID for node in self.nodes[i]]

            memory_pos_dict = {}
            for ni in corresponding_node_ids:
                memory_pos_dict[ni] = self._memory_positions

            node_ports = {
                self.nodes[i][0].ID: self.component[i].ports["A"].connected_port.name,
                self.nodes[i][1].ID: self.component[i].ports["B"].connected_port.name,
            }

            if self._first_delivery:  # update state and label delays if this is the first chain delivery

                updated_state_delays = {node_id: self._max_chain_cycle_time - (self._label_delay[i][node_id] / 2) -
                                        (self._state_delay[i][node_id] / 2) for node_id in corresponding_node_ids}
                self._state_delay[i] = updated_state_delays

                updated_label_delays = {node_id: self._max_chain_cycle_time - self._state_delay[i][node_id]
                                        for node_id in corresponding_node_ids}
                self._label_delay[i] = updated_label_delays

            self.add_delivery(memory_pos_dict, node_ports, md_index=i,
                              cycle_time=self._max_chain_cycle_time, **kwargs)
        self._first_delivery = False
        # auto repeat
        next_success_duration = self._calculate_next_successful_round()
        self._schedule_after(next_success_duration, self.COINCIDENCE)

    def add_delivery(self, memory_positions, ports=None, md_index=0, **kwargs):
        """This function overwrites its parent to allow for delivering multiple states in a single delivery."""
        if logger.isEnabledFor(logging.DEBUG):
            print("{:.2e} ++ {}: AEMagicDistributor adding delivery.".format(sim_time(), self.nodes[0][0].name))
        self._delivery_id += 1

        if self.fixed_delivery_parameters[0]['multiple_link_successes']:
            # copied the parent function to change what's in the delivery, with otherwise same functionality

            # Check all nodes originate from the same magic distributor
            for nodeID in memory_positions.keys():
                if not any(node.ID == nodeID for node in self.nodes[md_index]):
                    raise ValueError("Not all nodes refer to the same parameters, they have varying md_index")

            # Check that delivery is added to all quantum memories in the magic distributor
            if len(memory_positions) != len(self._qmemories[md_index]):
                raise ValueError("More memory positions_of_connections provided than QMemories found in the connection")

            for nodeID in memory_positions.keys():
                if isinstance(memory_positions[nodeID], int):
                    # Wrap in a list in order to check its length and later assign the qstate consistently
                    memory_positions[nodeID] = [memory_positions[nodeID]]
                if len(memory_positions[nodeID]) != self._num_qubits_per_memory[md_index]:
                    raise ValueError("Number of given memory positions and specified number of qubits do not match")

            # Make sure the nodes have the specified ports
            self._assert_nodes_have_ports(ports, md_index=md_index)

            # add the delivery
            parameters = {}
            parameters.update(self.fixed_delivery_parameters[md_index])
            parameters.update(kwargs)
            parameters['md_index'] = md_index
            delivery_sampler = self.delivery_sampler_factory[md_index].create_state_delivery_sampler(**parameters)

            # now we want to actually deliver multiple states in a single delivery
            # determine number of successes for this link
            n_successes_this_link = \
                self._rng.binomial(n=self.fixed_delivery_parameters[0]["num_multiplexing_modes"],
                                   p=self.delivery_sampler_factory[md_index].single_mode_success_prob)
            if n_successes_this_link == 0:
                n_successes_this_link = 1  # always have at least one success
            # draw random integers to determine successful modes
            successful_modes = self._rng.choice(range(self.fixed_delivery_parameters[0]["num_multiplexing_modes"]),
                                                size=n_successes_this_link,
                                                replace=False)
            assert len(successful_modes) == n_successes_this_link
            successful_modes.sort()

            delivery_states, delivery_labels = [], []
            for key in memory_positions.keys():
                memory_positions[key] = []

            for mode in range(self.fixed_delivery_parameters[0]["num_multiplexing_modes"]):
                if mode in successful_modes:
                    # for each successful mode sample and then combine to one sample of multiple states
                    delivery_sample = delivery_sampler.sample(skip_rounds=self._skip_rounds)
                    delivery_states.append(delivery_sample.state)
                    delivery_labels.append(delivery_sample.label)
                    for key in memory_positions.keys():
                        # determine memory position of this mode
                        memory_positions[key].extend(
                            list(range(mode * self._num_qubits_per_memory[0],
                                       mode * self._num_qubits_per_memory[0] + self._num_qubits_per_memory[0])))
                else:
                    # not a successful mode, append label for failure such that modes are handled correctly by protocols
                    delivery_labels.append(None)
            assert len(delivery_labels) == self.fixed_delivery_parameters[0]["num_multiplexing_modes"]
            # since the random number of modes we are sampling are for a specific time of deliver (now) set duration=0
            delivery_duration = 0
            delivery = _Delivery(
                sample=DeliverySample(delivery_states, delivery_duration, delivery_labels),
                memory_positions=memory_positions,
                ports=ports,
                parameters=parameters,
            )

            event = self._schedule_state_delivery_events(delivery)
            return event

        else:
            # we ignore all possible other successes and only deliver one successful mode which will be swapped
            for key in memory_positions.keys():
                # TODO: The memory position the MD gets when called is ignored and the state is always delivered to the
                #  first memory position
                memory_positions[key] = list(range(self._num_qubits_per_memory[0]))
            super().add_delivery(memory_positions, ports=ports, md_index=md_index, **kwargs)

    def _create_entangled_qubits(self, delivery):
        """Function of subclass needs to be overwritten to allow for multiple states being delivered"""
        md_index = delivery.parameters['md_index']  # extract md_index from delivery information

        qubits = {}

        if isinstance(delivery.sample.state, list):
            # multiple states should be delivered
            for node in self.nodes[md_index]:
                qubits[node.ID] = []
            for single_state in delivery.sample.state:
                single_state_qubits = {}
                for node in self.nodes[md_index]:
                    single_state_qubits[node.ID] = create_qubits(int(single_state.num_qubits / 2))
                ordered_qubits = []
                [ordered_qubits.extend(single_state_qubits[node.ID]) for node in self.nodes[md_index]]
                if delivery.sample.state is not None:
                    assign_qstate(ordered_qubits, single_state)
                for node in self.nodes[md_index]:
                    qubits[node.ID].extend(single_state_qubits[node.ID])
        else:
            # delivery containing a single state
            for node in self.nodes[md_index]:
                qubits[node.ID] = create_qubits(self._num_qubits_per_memory[md_index])

            # put the state onto the qubits
            ordered_qubits = []
            [ordered_qubits.extend(qubits[node.ID]) for node in self.nodes[md_index]]
            if delivery.sample.state is not None:
                assign_qstate(ordered_qubits, delivery.sample.state)

        # TODO: decide what to put on memory if failed / state=None
        return qubits

    def _create_label_message(self, label):
        """Overwrite method in order to ensure classical message always has BSMOutcome as item and has the correct
        metadata attached, e.g. the successful modes."""
        if isinstance(label, list):
            # delivering multiple states
            # reconstruct successful modes from labels
            successful_modes = []
            successful_labels = []
            for i in range(len(label)):
                if label[i] is not None:
                    successful_modes.append(i)
                    successful_labels.append(BSMOutcome(success=True, bell_index=label[i][1]))
            assert isinstance(successful_labels[0], BSMOutcome)
            message = Message(items=successful_labels)
        else:
            # when delivering single state, label is of form ('success', BSMOutcome)
            assert isinstance(label[1], BSMOutcome)
            message = Message(items=label[1])
            successful_modes = [0]

        current_round = int(ns.sim_time() / self._max_chain_cycle_time) if self._max_chain_cycle_time != 0 else 0
        message.meta["round"] = current_round
        message.meta["id"] = "qdetector"
        message.meta["delivery_id"] = f"{self.uid}_{self._delivery_id}"
        message.meta["successful_modes"] = successful_modes

        return message

    def get_qmemories_from_nodes(self):
        """Create a dictionary of quantum memories that the MagicDistributor uses, based on the first character of the
        name of the nodes.
        """
        qmemories = {}

        node_l = self._nodes[0][0]
        node_r = self._nodes[0][1]
        heralded_connection = self._component[0]

        for subcomponent in node_l.subcomponents.filter_by_type(SPDCSource):
            if (node_l.subcomponents[subcomponent].ports["qout0"].forwarded_ports["output"].connected_port ==
                    heralded_connection.ports["A"]):
                source_l = node_l.subcomponents[subcomponent]
            else:
                pass
        for subcomponent in node_r.subcomponents.filter_by_type(SPDCSource):
            if (node_r.subcomponents[subcomponent].ports["qout0"].forwarded_ports["output"].connected_port ==
                    heralded_connection.ports["B"]):
                source_r = node_r.subcomponents[subcomponent]
            else:
                pass

        qmemories[node_l.ID] = source_l.ports["qout1"].connected_port.component
        qmemories[node_r.ID] = source_r.ports["qout1"].connected_port.component

        return qmemories

    def _apply_noise(self, delivery, quantum_memory, positions):
        """
        Applies phase noise to delivered qubits. This is a workaround to implement random/fixed phase picked up on the
        fibre.
        """
        if self._apply_phase:
            # figure our which quantum memory triggered this function and match it with the correct phase model
            # TODO: find a better way to do this, that doesn't hard-code naming conventions
            if quantum_memory.name[-1] == "L":
                phase_model = self._fibre_phase_model_l
            elif quantum_memory.name[-1] == "R":
                phase_model = self._fibre_phase_model_r
            elif quantum_memory.name[-1] == "E":
                if quantum_memory.name[-3] == "b":
                    # bob > right
                    phase_model = self._fibre_phase_model_r
                elif quantum_memory.name[-3] == "e":
                    # alice > left
                    phase_model = self._fibre_phase_model_l
                else:
                    raise ValueError
            elif quantum_memory.name[:8] == "end_node":
                phase_model = self._fibre_phase_model_r
            elif quantum_memory.name[:13] == "repeater_node":
                if quantum_memory.name[-3] == "A":
                    phase_model = self._fibre_phase_model_r
                elif quantum_memory.name[-3] == "B":
                    phase_model = self._fibre_phase_model_l
                else:
                    raise ValueError
            elif quantum_memory.name[:10] == "start_node":
                phase_model = self._fibre_phase_model_l
            else:
                raise ValueError(f"Unable to process name of quantum memory {quantum_memory.name}.")

            if phase_model is not None:
                step = 2 if phase_model.multi_photon else 1
                for pos in positions[::step]:
                    qubits = quantum_memory.peek([pos, pos + 1]) if phase_model.multi_photon \
                        else quantum_memory.peek(pos)
                    if qubits is not None:
                        # The magic distributor passes all memory positions to this function, not only the ones that
                        # got a delivery, so check if there is qubits there first:
                        if None not in qubits:
                            phase_model.error_operation(qubits)
        else:
            pass
