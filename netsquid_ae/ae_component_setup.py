# Script setting up the different components of a atomic ensemble repeater chain

import logging


from netsquid.util.simlog import logger
from netsquid.components.clock import Clock
from netsquid.components.qsource import SourceStatus
from netsquid.components.models.delaymodels import FibreDelayModel
from netsquid.components import QuantumMemory, ClassicalChannel, QuantumChannel

from netsquid_ae.spdcsource import SPDCSource
from netsquid_ae.qlossmodel import MemoryLossModel, FibreLossModel, FibrePhaseModel, MemoryLossModelOnMemory
from netsquid_ae.multi_photon_detectors import MultiPhotonBSMDetector, MultiPhotonQKDDetector

from netsquid_physlayer.detectors import BSMDetector, QKDDetector

from netsquid_simulationtools.parameter_set import ParameterSet


def setup_memory(name, memory_coherence_time, max_memory_efficiency, memory_time_dependence, num_multiplexing_modes,
                 magic, multi_photon, encoding, multiple_link_successes, **kwargs):
    """Set up standard quantum memory used on all nodes.

    Parameters
    ----------
    name : str
        Name of the quantum memory.
    memory_coherence_time : float
        Memory coherence time in [ns].
    max_memory_efficiency : float
        Memory efficiency at t=0 (between 0 and 1).
    memory_time_dependence : str
        Time dependence of the memory efficiency. Default is "exponential".
    num_multiplexing_modes : int
        Total number of multiplexing modes the memory can store.
    magic : str or None
        Type of magic that is used. This, together with multiple_link_successes, determines if a memory position is
        created for every multiplexing mode or only a single memory position (magic!=None and
        multiple_link_successes=False) is created to save resources.
    multi_photon : bool
        Whether the simulation is using multi photon emission or not.
    encoding : str
        Encoding of the photons. Either "time_bin" or "presence_absence".
    multiple_link_successes : bool
        Whether the simulation allows to have multiple successes per elementary link.


    Returns
    -------
    qmemory : instance of :class:`~netsquid.components.qmemory`
        Memory set up using the simulation parameters.

    """
    # get relevant properties from sim_params
    properties = {
        'coherence_time': memory_coherence_time,
        'max_efficiency': max_memory_efficiency,
        'mp_encoding': None,
        'num_multiplexing_modes': num_multiplexing_modes,
    }
    # determine grouping for implementation of multi-photon events
    if multi_photon:
        properties["mp_encoding"] = encoding
        if encoding == "presence_absence":
            grouping = 2
        else:
            grouping = 4
        memory_noise_model = None
    else:
        grouping = 1
        memory_noise_model = MemoryLossModelOnMemory(time_dependence=memory_time_dependence)

    if magic and not multiple_link_successes:
        num_memory_positions = grouping  # Qubits are always distributed at a fixed memory position
    else:
        num_memory_positions = grouping * num_multiplexing_modes
    if num_memory_positions > 1e4:
        raise ValueError("Creating more than 1e4 memory positions leads to a memory spike in netsquid and is therefore"
                         f" not encouraged. Here {num_memory_positions} were caught.")
    qmemory = QuantumMemory(name, num_positions=num_memory_positions, memory_noise_models=memory_noise_model,
                            properties=properties)

    if multi_photon:
        # set memory loss model with time-dependence, for multiphoton we use work-around in order to apply noise
        # manually to multiple qubits at once
        qmemory.models['qout_noise_model'] = MemoryLossModel(time_dependence=memory_time_dependence)

    # Note that AFC memories have no noise, only loss
    return qmemory


def setup_source(name, multi_photon, encoding, mean_photon_number, emission_probabilities, num_multiplexing_modes,
                 source_frequency, Rate, g2, mpn_index=None, **kwargs):
    """Set up standard photon pair source used on all nodes.

    Parameters
    ----------
    name : str
        Name of the photon pair source.
    multi_photon : bool
        Whether the simulation is using multi photon emission or not.
    encoding : str
        Encoding of the photons. Either "time_bin" or "presence_absence".
    mean_photon_number : list or None
        List of mean photon numbers for the thermal distribution of the SPDC sources along the chain.
        The `mpn_index` (see below) then determines which mean photon number is assigned to which source. This allows
        for different mean photon numbers along the chain.
        Must be supplied when `emission_probabilities` is None.
    emission_probabilities: list or None
        List of lists of probabilities of emitting a certain number of photons along the chain.
        The `mpn_index` (see below) then determines which list of emission_probabilities is assigned to which source.
        This allows for different emission_probabilities along the chain.
        This emission_probabilities must be a list of length 4, where each element denotes the probability of
        generating a photon pair with the number of photons equal to its list index.
        If multi_photon is not used (False) then this list is truncated after n=1.
        If this is None, `mean_photon_number` must be supplied.
    num_multiplexing_modes : int, optional
        Total number of multiplexing modes the source is emitting.
        Should be specified if source_frequency, Rate and g2 are not specified.
    source_frequency : float, optional
        Frequency at which the source creates qubits in [Hz].
        Should be specified if num_multiplexing_modes is not specified.
    Rate: float, optinoal
        Measurement of the rate of generated photon pairs in [Hz].
        Used for the Number of modes calculation.
        Should be specified if num_multiplexing_modes is not specified.
    g2: float, optinoal
        Measurement of the second order correlation function, g2, for the heralded light from the source.
        Used for the Number of Modes calculation
        Should be specified if num_multiplexing_modes is not specified.
    mpn_index : int
        Index of the mean photon number for the source.
        Default is None. In that case, the mean photon number is the same for all nodes in the chain.

    Returns
    -------
    source : instance of :class:`~.spdcsource`
        Photon pair source set up using the simulation parameters.

    """
    if multi_photon and mean_photon_number not in [ParameterSet._NOT_SPECIFIED, None, [None, None],
                                                   [ParameterSet._NOT_SPECIFIED, ParameterSet._NOT_SPECIFIED]]:
        if emission_probabilities is not None:
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug("Warning: using non-zero value of mean_photon_number ({}) for SPDC source instead of "
                             "emission probabilities ({}).".format(mean_photon_number, emission_probabilities))
        mu = mean_photon_number if mpn_index is None else mean_photon_number[mpn_index]
        emission_probability = None
    else:
        mu = None
        emission_probability = emission_probabilities if mpn_index is None else emission_probabilities[mpn_index]

    source = SPDCSource(name,
                        spectral_modes=num_multiplexing_modes,
                        mu=mu,
                        emission_probability=emission_probability,
                        multi_photon=multi_photon,
                        encoding=encoding,
                        status=SourceStatus.EXTERNAL,  # using external trigger for source, frequency becomes irrelevant
                        frequency=source_frequency,
                        Rate=Rate,
                        g2=g2)

    return source


def setup_end_node(node, memory_coherence_time, max_memory_efficiency, memory_time_dependence, source_num_modes,
                   mem_num_modes, magic, multi_photon, encoding, mean_photon_number, emission_probabilities,
                   time_step, num_attempts, source_frequency, multiple_link_successes, Rate, g2, mpn_index=None,
                   start_delay=.0, **kwargs):
    """Set up an end node.

    Schematic of the end node:

    .. code-block:: text

                                    ----------------------------
               Messaging I/O left : >         |Clock|          < : Messaging I/O right
                                    |   --------    --------   |
                Qubit output port : > > | Qmem | <> | SPDC | < < : I/O for the heralded connection
                                    |   --------    --------   |
                                    ----------------------------

    Where ``Qmem`` is a quantum memory and ``SPDC`` is a spontaneous parametric down conversion photon pair source.

    Parameters
    ----------
    node : instance of :class:`~netsquid.nodes.node.Node`
        Node to set up as end node.
    memory_coherence_time : float
        Memory coherence time in [ns].
    max_memory_efficiency : float
        Memory efficiency at t=0 (between 0 and 1).
    mem_time_dependence : str
        Time dependence of the memory efficiency. Default is "exponential".
    memory_num_modes : int
        Number of multiplexing modes for memory.
    source_num_modes : int
        Number of multiplexing modes for source.
    magic : str or None
        Type of magic that is used. This determines if a memory position is created for every multiplexing mode or
        only a single memory position (magic!=None) is created to save resources.
    multi_photon : bool
        Whether the simulation is using multi photon emission or not.
    encoding : str
        Encoding of the photons. Either "time_bin" or "presence_absence".
    mean_photon_number : list or None
        List of mean photon numbers for the thermal distribution of the SPDC sources along the chain.
        The `mpn_index` (see below) then determines which mean photon number is assigned to which source. This allows
        for different mean photon numbers along the chain.
        Must be supplied when `emission_probabilities` is None.
    emission_probabilities : list or None
        List of lists of probabilities of emitting a certain number of photons along the chain.
        The `mpn_index` (see below) then determines which list of emission_probabilities is assigned to which source.
        This allows for different emission_probabilities along the chain.
        This emission_probabilities must be a list of length 4, where each element denotes the probability of
        generating a photon pair with the number of photons equal to its list index.
        If multi_photon is not used (False) then this list is truncated after n=1.
        If this is None, `mean_photon_number` must be supplied.
    time_step : float
        Time step at which the clock on this node ticks in [ns]. Only used if magic=False.
    num_attempts : int
        Number of ticks after which the clock stops, effectively stopping attempts of entanglement generation. Set to
        -1 for unlimited operation.
    source_frequency : float, optional
        Frequency at which the source creates qubits in [Hz].
    multiple_link_successes : bool
        Whether the simulation allows to have multiple successes per elementary link.
    Rate: float, optinoal
        Measurement of the rate of generated photon pairs in [Hz].
        Used for the Number of modes calculation.
        Should be specified if num_multiplexing_modes is not specified.
    g2: float, optinoal
        Measurement of the second order correlation function, g2, for the heralded light from the source.
        Used for the Number of Modes calculation
        Should be specified if num_multiplexing_modes is not specified.
    mpn_index : int (optional)
        Index of the mean photon number for the source.
        See :func:`~.ae_chain_setup._get_mpn_indices` for details on how this index is defined and used.
        Default is None. In that case, the mean photon number is the same for all nodes in the chain.
    start_delay : float (optional)
        Time after which the node's clock starts ticking. Default is 0.0.

    Returns
    -------
    node : instance of :class:`~netsquid.nodes.node.Node`
        Fully set up End node.

    """
    if multiple_link_successes and encoding == "presence_absence":
        raise NotImplementedError("Correct handling of multiple_link_successes not implemented for dual chain "
                                  "presence_absence encoding yet.")
    # Create subcomponents
    qmemory = setup_memory(f"QMem_{node.name}_E", memory_coherence_time=memory_coherence_time,
                           max_memory_efficiency=max_memory_efficiency,
                           memory_time_dependence=memory_time_dependence,
                           num_multiplexing_modes=mem_num_modes,
                           magic=magic, multi_photon=multi_photon, encoding=encoding,
                           multiple_link_successes=multiple_link_successes)
    photon_pair_source = setup_source(f"SPDCSource_{node.name}_E", multi_photon=multi_photon,
                                      encoding=encoding, mean_photon_number=mean_photon_number,
                                      emission_probabilities=emission_probabilities,
                                      num_multiplexing_modes=source_num_modes,
                                      source_frequency=source_frequency,
                                      mpn_index=mpn_index,
                                      Rate=Rate,
                                      g2=g2)
    # Set up a clock based on the 'time_step' parameter in the sim_params
    clock = Clock(f"Clock_{node.name}", frequency=1e9 / time_step,
                  start_delay=start_delay, max_ticks=num_attempts)

    # Find number of modes for the node
    num_multiplexing_modes = min(photon_pair_source.spectral_modes, mem_num_modes)

    # Add subcomponents to node
    node.add_subcomponent(photon_pair_source, "PPS")
    node.add_subcomponent(qmemory, "QMem")
    node.add_subcomponent(clock, "Clock")
    # Add outer ports to the node
    node.add_ports(["IO_Connection_E", "Qbit_out", "Messaging_IO_L", "Messaging_IO_R"])
    # Connect one output of the PPS to the input of the memory
    node.subcomponents["PPS"].ports["qout1"].connect(node.subcomponents["QMem"].ports["qin"])
    # Forward outputs to the outer node ports
    node.subcomponents["PPS"].ports["qout0"].forward_output(node.ports["IO_Connection_E"])
    node.subcomponents["QMem"].ports["qout"].forward_output(node.ports["Qbit_out"])
    node.properties.update({"magic": magic,
                            "num_multiplexing_modes": num_multiplexing_modes,
                            "multi_photon": multi_photon,
                            "multiple_link_successes": multiple_link_successes})

    return node


def setup_repeater_node(node, memory_coherence_time, max_memory_efficiency, memory_time_dependence,
                        source_num_modes, mem_num_modes, magic, multi_photon, encoding, mean_photon_number,
                        emission_probabilities, time_step, num_attempts, source_frequency,
                        swap_det_dark_count_prob, swap_det_efficiency, swap_det_visibility, swap_det_num_resolving,
                        multiple_link_successes, Rate, g2, mpn_index=None, start_delay=.0, **kwargs):
    """Set up a repeater node.

    Schematic of the repeater node:

    .. code-block:: text

                                  --------------------------------------------------------------
                                  |                                                            |
            Messaging I/O left : >                                                              < : Messaging I/O right
                                  |                        > -------- <                        |
                                  |                          | QDet |                          |
                                  |  --------    --------    |      |    --------    --------  |
              I/O for heralded : > < | SPDC | >> | Qmem | >> -------- << | Qmem | << | SPDC | > < : I/O for heralded
              connection          |  --------    --------                --------    --------  |    connection
                                  |                                                            |
                                  |                           |Clock|                          |
                                  --------------------------------------------------------------

    Where ``Qmem`` is a quantum memory, ``SPDC`` is a spontaneous parametric down conversion
    photon pair source and ``QDet`` is a detector capable of doing Bell state measurements.

    Parameters
    ----------
    node : instance of :class:`~netsquid.nodes.node.Node`
        Node to set up as end node.
    memory_coherence_time : float
        Memory coherence time in [ns].
    max_memory_efficiency : float
        Memory efficiency at t=0 (between 0 and 1).
    memory_time_dependence : str
        Time dependence of the memory efficiency. Default is "exponential".
    mem_num_modes : int
        Number of multiplexing modes for memory.
    source_num_modes : int
        Number of multiplexing modes for source.
    magic : str or None
        Type of magic that is used. This determines if a memory position is created for every multiplexing mode or
        only a single memory position (magic!=None) is created to save resources.
    multi_photon : bool
        Whether the simulation is using multi photon emission or not.
    encoding : str
        Encoding of the photons. Either "time_bin" or "presence_absence".
    mean_photon_number : list or None
        List of mean photon numbers for the thermal distribution of the SPDC sources along the chain.
        The `mpn_index` (see below) then determines which mean photon number is assigned to which source. This allows
        for different mean photon numbers along the chain.
        Must be supplied when `emission_probabilities` is None.
    emission_probabilities: list or None
        List of lists of probabilities of emitting a certain number of photons along the chain.
        The `mpn_index` (see below) then determines which list of emission_probabilities is assigned to which source.
        This allows for different emission_probabilities along the chain.
        This emission_probabilities must be a list of length 4, where each element denotes the probability of
        generating a photon pair with the number of photons equal to its list index.
        If multi_photon is not used (False) then this list is truncated after n=1.
        If this is None, `mean_photon_number` must be supplied.
    time_step : float
        Time step at which the clock on this node ticks in [ns]. Only used if magic=False.
    num_attempts : int
        Number of ticks after which the clock stops, effectively stopping attempts of entanglement generation. Set to
        -1 for unlimited operation.
    source_frequency : float, optional
        Frequency at which the source creates qubits in [Hz].
    swap_det_dark_count_prob : float
        Dark count probability of the swap BSM detectors.
    swap_det_efficiency : float
        Detection efficiency of the swap BSM detectors.
    swap_det_visibility : float
        Photon indistinguishability at the swap BSM (aka after the memories).
    swap_det_num_resolving : bool
        Whether or not swap BSM detectors are number-resolving.
    multiple_link_successes : bool
        Whether the simulation allows to have multiple successes per elementary link.
        If set to True, multiple modes can be successful otherwise the detector stops measuring after the first
        successful mode.
    Rate: float, optinoal
        Measurement of the rate of generated photon pairs in [Hz].
        Used for the Number of modes calculation.
        Should be specified if source_num_modes is not specified.
    g2: float, optinoal
        Measurement of the second order correlation function, g2, for the heralded light from the source.
        Used for the Number of Modes calculation
        Should be specified if source_num_modes is not specified.
    mpn_index : int
        Index of the mean photon number for the source.
        See :func:`~.ae_chain_setup._get_mpn_indices` for details on how this index is defined and used.
        Default is None. In that case, the mean photon number is the same for all nodes in the chain.
    start_delay : float (optional)
        Time after which the node's clock starts ticking. Default is 0.0.

    Returns
    -------
    node : instance of :class:`~netsquid.nodes.node.Node`
        Fully set up Repeater node.

    """
    if multiple_link_successes and encoding == "presence_absence":
        raise NotImplementedError("Correct handling of multiple_link_successes not implemented for dual chain "
                                  "presence_absence encoding yet.")
    # define mpn index for the right source (index of left source is simply mpn_index)
    mpn_index_r = None if mpn_index is None else mpn_index + 1  # either None or the next mpn index

    # Create subcomponents
    qmemory_l = setup_memory(f"QMem_Rep{node.name}_L",
                             memory_coherence_time=memory_coherence_time,
                             max_memory_efficiency=max_memory_efficiency,
                             memory_time_dependence=memory_time_dependence,
                             num_multiplexing_modes=mem_num_modes,
                             magic=magic, multi_photon=multi_photon, encoding=encoding,
                             multiple_link_successes=multiple_link_successes)
    qmemory_r = setup_memory(f"QMem_Rep{node.name}_R",
                             memory_coherence_time=memory_coherence_time,
                             max_memory_efficiency=max_memory_efficiency,
                             memory_time_dependence=memory_time_dependence,
                             num_multiplexing_modes=mem_num_modes,
                             magic=magic, multi_photon=multi_photon, encoding=encoding,
                             multiple_link_successes=multiple_link_successes)
    photon_pair_source_l = setup_source(f"SPDCSource_Rep{node.name}_L", multi_photon=multi_photon,
                                        encoding=encoding, mean_photon_number=mean_photon_number,
                                        emission_probabilities=emission_probabilities,
                                        num_multiplexing_modes=source_num_modes,
                                        source_frequency=source_frequency,
                                        mpn_index=mpn_index,
                                        Rate=Rate, g2=g2)
    photon_pair_source_r = setup_source(f"SPDCSource_Rep{node.name}_R", multi_photon=multi_photon,
                                        encoding=encoding, mean_photon_number=mean_photon_number,
                                        emission_probabilities=emission_probabilities,
                                        num_multiplexing_modes=source_num_modes,
                                        source_frequency=source_frequency,
                                        mpn_index=mpn_index_r,
                                        Rate=Rate, g2=g2)
    if not multi_photon:
        bsm_detector = BSMDetector(f"BSMDetector_Rep{node.name}", p_dark=swap_det_dark_count_prob,
                                   det_eff=swap_det_efficiency,
                                   visibility=swap_det_visibility,
                                   num_resolving=swap_det_num_resolving,
                                   allow_multiple_successful_modes=multiple_link_successes)
    else:
        bsm_detector = MultiPhotonBSMDetector(f"MultiPhotonBSMDetector_Rep{node.name}",
                                              p_dark=swap_det_dark_count_prob,
                                              det_eff=swap_det_efficiency,
                                              visibility=swap_det_visibility,
                                              num_resolving=swap_det_num_resolving,
                                              encoding=encoding,
                                              allow_multiple_successful_modes=multiple_link_successes)
    # Setup a clock based on the 'time_step' parameter
    clock = Clock(f"Clock_{node.name}", frequency=1e9 / time_step, start_delay=start_delay, max_ticks=num_attempts)

    # Find number of modes for node
    num_multiplexing_modes = min(
        photon_pair_source_l.spectral_modes, photon_pair_source_r.spectral_modes, mem_num_modes
    )

    # Add subcomponents to node
    node.add_subcomponent(photon_pair_source_l, "PPS_L")
    node.add_subcomponent(photon_pair_source_r, "PPS_R")
    node.add_subcomponent(qmemory_l, "QMem_L")
    node.add_subcomponent(qmemory_r, "QMem_R")
    node.add_subcomponent(bsm_detector, "QDet")
    node.add_subcomponent(clock, "Clock")
    # Add ports for node
    node.add_ports(["IO_Connection_L", "IO_Connection_R", "Messaging_IO_L", "Messaging_IO_R"])
    # Forward one photon from each SPDC source to outer ports on the left and right
    node.subcomponents["PPS_L"].ports["qout0"].forward_output(node.ports["IO_Connection_L"])
    node.subcomponents["PPS_R"].ports["qout0"].forward_output(node.ports["IO_Connection_R"])
    # Connect the other photon of each SPDC source to its respective memory
    node.subcomponents["PPS_L"].ports["qout1"].connect(node.subcomponents["QMem_L"].ports["qin"])
    node.subcomponents["PPS_R"].ports["qout1"].connect(node.subcomponents["QMem_R"].ports["qin"])
    # Connect memory output to respective Qdetector ports
    node.subcomponents["QMem_L"].ports["qout"].connect(node.subcomponents["QDet"].ports["qin0"])
    node.subcomponents["QMem_R"].ports["qout"].connect(node.subcomponents["QDet"].ports["qin1"])
    # Note that the output of the detector is handled by the protocol!
    node.properties.update({"magic": magic,
                            "num_multiplexing_modes": num_multiplexing_modes,
                            "multi_photon": multi_photon,
                            "multiple_link_successes": multiple_link_successes})

    return node


def setup_heralded_connection(connection, channel_length_l, channel_length_r, attenuation_l, attenuation_r,
                              coupling_loss_fibre, det_dark_count_prob,
                              det_efficiency, det_visibility, det_num_resolving, encoding, multi_photon,
                              magic, fibre_phase_stdv_l, fibre_phase_stdv_r, state_files_directory,
                              multiple_link_successes, speed_of_light_l=2e5, speed_of_light_r=2e5, **kwargs):
    """Create a heralded connection component.

    Schematic of the heralded connection:

    .. code-block:: text

          ------------------------------
          | > CL <> ---------- <> CR < |
        > |         |  QDet  |         | < : incoming qubits and outgoing classical messages
          | > QL <> ---------- <> QR < |
          ------------------------------

    Where ``Q{L, R}`` are quantum channels and ``C{L, R}`` classical channels.

    Parameters
    ----------
    connection : instance of :class:`~netsquid.nodes.connections.Connection`
        Connection to set up as heralded connection.
    channel_length_l : float
        Length of the channel left of the detectors [in km].
    channel_length_r : float
        Length of the channel right of the detectors [in km].
    attenuation_l : float
        Attenuation of the channel left of the detectors [in dB/km].
    attenuation_r: float
        Attenuation of the channel right of the detectors [in dB/km].
    coupling_loss_fibre : float
        Coupling loss/ length independent loss of the channel to the midpoint, in [0,1].
    det_dark_count_prob : float
        Dark count probability of the midpoint detectors.
    det_efficiency : float
        Detection efficiency of the midpoint detectors.
    det_visibility : float
        Photon indistinguishability at the midpoint BSM.
    det_num_resolving : bool
        Whether or not midpoint detectors are number-resolving.
    encoding : str
        Encoding of the photons. 'presence_absence' for single-click scheme/ 'time_bin' for double-click scheme.
    multi_photon : bool
        Whether to include multi-photon emission in the simulation.
    magic : str or None
        Whether to use magic to create elementary link states. This sets the number of memory positions to the size of
        a single qubit to safe resources.
    fibre_phase_stdv_l : float
        Standard deviation of the gaussian distribution of the phase for PhaseLossModel on the channel left of
        the detectors. Set to 0.0 in order to not include `PhaseLossModel` in the simulation.
    fibre_phase_stdv_r : float
        Standard deviation of the gaussian distribution of the phase for PhaseLossModel on the channel right of
        the detectors. Set to 0.0 in order to not include `PhaseLossModel` in the simulation.
    state_files_directory : str
        Name of directory containing elementary link state files saved as
        :obj:`.netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder` in *.pickle format following
        the naming convention defined in `forced_magic_state_generator.py`.
        These should have beengenerated using this specific heralded connection.
        This is only needed when using magic ('forced' or 'sampled') and will be handled and checked by
        the `~.netsquid_ae.ae_magic_distributor.AEMagicDistributor`.
    multiple_link_successes : bool
        If set to True, multiple modes can be successful otherwise the detector stops measuring after the first
        successful mode.
    speed_of_light_l : float, optional
        Speed of light in the fibre in [m/s] of the channel to the left of the detectors. Default c=2e5.
    speed_of_light_r : float, optional
        Speed of light in the fibre in [m/s] of the channel to the right of the detectors. Default c=2e5.

    Returns
    -------
    connection : instance of :class:`~netsquid.nodes.connections.Connection`
        Fully set up Heralded connection.

    """
    # Delay model with speed of light in fibre
    delay_model_l = FibreDelayModel(c=speed_of_light_l)
    delay_model_r = FibreDelayModel(c=speed_of_light_r)
    # Loss model with initial loss and loss over distance
    loss_model_l = FibreLossModel(p_loss_init=coupling_loss_fibre, p_loss_length=attenuation_l)
    loss_model_r = FibreLossModel(p_loss_init=coupling_loss_fibre, p_loss_length=attenuation_r)
    # Classical channels for connection
    cchannel_to_l = ClassicalChannel("CChannel_to_L_{}".format(connection.name), length=channel_length_l,
                                     models={"delay_model": delay_model_l})
    cchannel_to_r = ClassicalChannel("CChannel_to_R_{}".format(connection.name), length=channel_length_r,
                                     models={"delay_model": delay_model_r})
    # Quantum channels for connection
    if encoding == "presence_absence" and magic is None:
        # Note for magic the fibre phase is applied by the magic distributor
        phase_model_l = FibrePhaseModel(multi_photon=multi_photon, standard_deviation=fibre_phase_stdv_l, fixed_phase=0)
        phase_model_r = FibrePhaseModel(multi_photon=multi_photon, standard_deviation=fibre_phase_stdv_r, fixed_phase=0)
        qchannel_from_l = QuantumChannel("QChannel_from_L_{}".format(connection.name), length=channel_length_l,
                                         models={"delay_model": delay_model_l,
                                                 "quantum_noise_model": loss_model_l + phase_model_l},
                                         transmit_empty_items=True, properties={'mp_encoding': None,
                                                                                'fibre_phase_stdv': fibre_phase_stdv_l})
        qchannel_from_r = QuantumChannel("QChannel_from_R_{}".format(connection.name), length=channel_length_r,
                                         models={"delay_model": delay_model_r,
                                                 "quantum_noise_model": loss_model_r + phase_model_r},
                                         transmit_empty_items=True, properties={'mp_encoding': None,
                                                                                'fibre_phase_stdv': fibre_phase_stdv_r})
    else:
        qchannel_from_l = QuantumChannel("QChannel_from_L_{}".format(connection.name), length=channel_length_l,
                                         models={"delay_model": delay_model_l,
                                                 "quantum_noise_model": loss_model_l},
                                         transmit_empty_items=True, properties={'mp_encoding': None,
                                                                                'fibre_phase_stdv': fibre_phase_stdv_l})
        qchannel_from_r = QuantumChannel("QChannel_from_R_{}".format(connection.name), length=channel_length_r,
                                         models={"delay_model": delay_model_r,
                                                 "quantum_noise_model": loss_model_r},
                                         transmit_empty_items=True, properties={'mp_encoding': None,
                                                                                'fibre_phase_stdv': fibre_phase_stdv_r})
    # QDetector for the connection to perform measurements at the midpoint
    if not multi_photon:
        qdetector = BSMDetector("BSMDetector_midpoint_{}".format(connection.name),
                                p_dark=det_dark_count_prob,
                                det_eff=det_efficiency,
                                visibility=det_visibility,
                                num_resolving=det_num_resolving)
    else:
        # update encoding on qchannels
        qchannel_from_l.properties['mp_encoding'] = encoding
        qchannel_from_r.properties['mp_encoding'] = encoding

        qdetector = MultiPhotonBSMDetector(f"MultiPhotonBSMDetector_midpoint_{connection.name}",
                                           p_dark=det_dark_count_prob,
                                           det_eff=det_efficiency,
                                           visibility=det_visibility,
                                           encoding=encoding,
                                           num_resolving=det_num_resolving,
                                           allow_multiple_successful_modes=multiple_link_successes)
    # Add all components to the node for further reference
    # Note: left and right are defined relative to the detector
    connection.add_subcomponent(qchannel_from_l, "QCh_L")
    connection.add_subcomponent(qchannel_from_r, "QCh_R")
    connection.add_subcomponent(cchannel_to_l, "CCh_L")
    connection.add_subcomponent(cchannel_to_r, "CCh_R")
    connection.add_subcomponent(qdetector, "QDet")
    # Note that the Connection constructor by default adds two ports "A" and "B"
    # Connect output of of the quantum channel to the input of the detector for receiving
    connection.subcomponents["QCh_L"].ports["recv"].connect(connection.subcomponents["QDet"].ports["qin0"])
    connection.subcomponents["QCh_R"].ports["recv"].connect(connection.subcomponents["QDet"].ports["qin1"])
    # Connect input of the classical channel to output of detector for sending
    connection.subcomponents["CCh_L"].ports["send"].connect(connection.subcomponents["QDet"].ports["cout0"])
    connection.subcomponents["CCh_R"].ports["send"].connect(connection.subcomponents["QDet"].ports["cout1"])
    # Forward quantum inputs from A and B onto quantum channel for sending
    connection.ports["A"].forward_input(connection.subcomponents["QCh_L"].ports["send"])
    connection.ports["B"].forward_input(connection.subcomponents["QCh_R"].ports["send"])
    # Forward classical output from A and B to outer nodes for receiving
    connection.subcomponents["CCh_L"].ports["recv"].forward_output(connection.ports["A"])
    connection.subcomponents["CCh_R"].ports["recv"].forward_output(connection.ports["B"])

    # for magic add elementary link state files directory as property (states should be saved as dataframe holders
    # in *.pickle format in this directory following the naming convention in `forced_magic_state_generator.py`
    connection.properties.update({"state_files_directory": state_files_directory})

    return connection


def setup_qkd_node(node, sim_params):
    """Set up a detector node.

    Schematic of setup:

        .. code-block:: text

          -----------------------------------
          |                                 |
          |    ----------      Proj. meas.  < : Message I/O chain 1
          |    >        <                   < : incoming qubits chain 1     (main port to use)
          |    |  QDet  |     (Threshold)   |
          |    >        <                   < :  incoming qubits chain 2    (only if second
          |    ----------      (Detector)   < :  Message I/O chain 2         rail is used)
          -----------------------------------

    Parameters
    ----------
    node : instance of :class:`~netsquid.nodes.node.Node`
        Node to set up as end node.
    sim_params : dict
        Tunable simulation parameters.

    Returns
    -------
    detector_node : instance of :class:`~netsquid.nodes.node.Node`
        Repeater node to use.

    """
    if not sim_params["multi_photon"]:
        qkd_detector = QKDDetector(f"QKDDet_{node.name}", p_dark=sim_params["swap_det_dark_count_prob"],
                                   det_eff=sim_params["swap_det_efficiency"],
                                   visibility=1., num_resolving=sim_params["swap_det_num_resolving"])
    else:
        qkd_detector = MultiPhotonQKDDetector(f"MultiPhotonQKDDet_{node.name}",
                                              p_dark=sim_params["swap_det_dark_count_prob"],
                                              det_eff=sim_params["swap_det_efficiency"],
                                              visibility=1.,
                                              encoding=sim_params["encoding"],
                                              num_resolving=sim_params["swap_det_num_resolving"])
    node.add_subcomponent(qkd_detector, "QDet")
    # Add either two or four ports based on the number of repeater chains (hence the photon encoding)
    node.add_ports(["IN_Chain_1", "Message_IO_Chain_1"]) if sim_params["encoding"] == "time_bin" else \
        node.add_ports(["IN_Chain_1", "Message_IO_Chain_1", "IN_Chain_2", "Message_IO_Chain_2"])

    return node
