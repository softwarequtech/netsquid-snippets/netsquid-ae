import copy
import logging
from itertools import chain

from pydynaa.core import EventType, EventHandler, EventExpression, ExpressionHandler

from netsquid.protocols.protocol import Protocol
from netsquid.qubits import qubitapi as qapi
from netsquid.qubits.ketstates import BellIndex
from netsquid.util.simlog import logger
from netsquid.util.simtools import sim_time
from netsquid.components.component import Message
from netsquid.components.qsource import SourceStatus

from netsquid_ae.ae_classes import EndNode, RepeaterNode, QKDNode
from netsquid_ae.protocol_event_types import EVTYPE_SUCCESS, EVTYPE_FAIL, EVTYPE_ERROR

from netsquid_physlayer.detectors import BSMOutcome, QKDOutcome


class EmissionProtocol(Protocol):
    """Protocol responsible for attempting entanglement generation in an elementary link using atomic ensembles.

    Runs with a specific quantum memory and a specific source of entangles photon pairs.

    This protocol supports multiple successful modes on the elementary link.

    Parameters
    ----------
    node : instance of :class:`~netsquid.nodes.node.Node`
        Node to apply protocol to.
    memory : instance of :class:`~netsquid.components.qmemory`
        Memory the protocol runs on.
    source : instance of :class:`~.spdcsource`
        Source of entangled photon pairs the protocol triggers.
    delay : int
        Delay (in nanoseconds) that determines when the source should be triggered. This delay should be set such
        that the photons arrive at the midpoint detector at the same time. It can be calculated as follows:
            delay = difference between channel lengths * factor for ns (1e9) / speed of photons in fibre
        Default is 0 (case of no delay).
    max_attempts : int, optional
        Maximum number of entanglement attempts to run for. If negative (default) will
        run until explicitly stopped.

    Notes
    -----
    Operation:

        #. triggered by evtype_tick from clock
           -> triggers source to emit a single round of photons after a specified delay
        #. waits for results from midpoint of the elementary link and records results and modes on the node.
        #. schedules appropriate event (SUCC, FAIL, ERR) depending on heralding signal.

    Example

    .. code-block:: python​

        from netsquid_ae.ae_protocols import EmissionProtocol​

        # initialize protocol
        # Note: alice is a fully set up end node with a quantum memory (QMem), a photon pair source
        # (PPS) and a clock

        emission_prot = EmissionProtocol(alice, memory=alice.subcomponents["QMem"],
                                  source=alice.subcomponents["PPS"], max_attempts=100)

        # start protocol and clock triggering the protocol

        emission_prot.start()​
        alice.subcomponents["Clock"].start()

        # run simulation

        netsquid.sim_run()

    """
    evtype_emit = EventType("Schedule emission", "Delay over, schedule the source to be triggered now.")

    def __init__(self, node, memory, source, delay=0, max_attempts=-1):
        super().__init__()
        self.node = node
        self.protocol_type = "Emission"
        self.delay = delay
        # register necessary components
        self.source = source
        self.memory = memory
        self.clock = node.subcomponents["Clock"]
        # initialize counters
        self.num_attempts = 0
        self.num_successes = 0
        self.max_attempts = max_attempts
        self._is_triggered = False
        self._is_waiting = False

        # set grouping size of qubits
        # Note: only required to put correct qubits on node for pick up by StateCollector
        if self.memory.properties["mp_encoding"] == 'presence_absence':
            self.grouping = 2
        elif self.memory.properties["mp_encoding"] == 'time_bin':
            self.grouping = 4
        else:
            self.grouping = 1

        # record ports (get port through source this protocol runs on, as rep node has 2 ports)
        self.port_link = self.source.ports["qout0"].forwarded_ports["output"]
        # event handlers
        self._delay_handler = EventHandler(self._process_delay)
        self._trigger_handler = EventHandler(self._trigger)

        # prepare dictionary on node to collect data in
        self.node.cdata["successful_modes_{}".format(self.source.name[-1])] = []
        self.node.cdata["midpoint_outcomes_{}".format(self.source.name[-1])] = []
        self.node.cdata["round_{}".format(self.source.name[-1])] = []

    def start(self):
        """Starts the protocol.

        Waits for clock, binds input handler to port and sets protocol to running.

        Protocol should be fully connected.

        """
        if self.is_running:
            # If we are running already, do nothing
            return
        super().start()
        # set up handler waiting for clicks of the clock
        self._wait(self._delay_handler, event_type=self.clock.evtype_tick, entity=self.clock)
        self._wait(self._trigger_handler, event_type=self.evtype_emit, entity=self)
        # bind handlers to ports
        self.port_link.bind_input_handler(self._handle_link_result)

    def stop(self):
        """Stops the protocol.

        Protocol should be fully connected. Does not clear internal state.

        Dismisses EventHandlers and callbacks, sets source status to OFF.

        """
        if not self.is_connected:
            raise RuntimeError("Cannot stop a protocol that is not fully connected")
        self.source.status = SourceStatus.OFF
        super().stop()
        self._dismiss(self._delay_handler, event_type=self.clock.evtype_tick, entity=self.clock)
        self._dismiss(self._trigger_handler, event_type=self.evtype_emit, entity=self)
        # unbind handlers to ports
        self.port_link.bind_input_handler(None)

    def _process_delay(self, event):
        """Schedules triggering of the source after a delay specified for this protocol."""
        self._schedule_after(self.delay, self.evtype_emit)

    def _trigger(self, event):
        """ Triggers the protocol.

        Resets memory, then triggers source.

        """

        if self._is_triggered:
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug("{}: WARNING: Triggering source on {} before data was collected by Messenger.".format(
                    sim_time(), self.node.name))
        if self._is_waiting:
            print("{}: WARNING: Triggering source on {} before receiving heralding signal.".format(sim_time(),
                                                                                                   self.node.name))
        self.memory.reset()
        self._is_triggered = True   # indicator that protocol was triggered, reset when messaging protocols collects
        self._is_waiting = True     # indicator that protocol is waiting for heralding signal
        # count attempts
        self.num_attempts += 1

        # triggering source
        self.source.trigger()
        # log round
        self.node.cdata["round_{}".format(self.source.name[-1])].append(self.num_attempts)

        if self.num_attempts >= self.max_attempts >= 0:
            self.stop()

    def _handle_link_result(self, message):
        """Function handling incoming messages from the midpoint of the elementary link.

        Stores midpoint result and successful mode on the node, then schedules appropriate event.

        Parameters
        ----------
        message : :obj:`netsquid.components.component.Message`
            Incoming message containing the midpoint result as the item and the successful mode as meta.

        """

        if not self.is_running:
            raise RuntimeError("Received message {} while protocol was not running."
                               .format(message))

        # Separate message content and meta
        midpoint_results = message.items
        successful_modes = message.meta['successful_modes']
        assert midpoint_results is not None
        self.node.cdata["successful_modes_{}".format(self.source.name[-1])].append(successful_modes)
        self.node.cdata["midpoint_outcomes_{}".format(self.source.name[-1])].append(midpoint_results)

        # TODO: decide whether this should be done in extraction protocol?
        if successful_modes != [None]:
            # put successful qubits in cdata for StateCollector to pick up
            successful_qubits = []
            for mode in successful_modes:
                positions = list(range(mode * self.grouping,
                                       (mode * self.grouping) + self.grouping))
                # Note: Noise is only applied on re-emission from the memory
                for pos in positions:
                    successful_qubits.append(self.memory.mem_positions[pos]._qubit)
                # should this append or replace?
            self.node.cdata["states_{}".format(self.source.name[-1])] = [successful_qubits]
        MagicProtocol.write_data_to_detector_node(self=self, successful_modes=successful_modes)

        # schedule appropriate event
        if midpoint_results[0].success:  # if midpoint had no successes the message is only one item with success=False
            bell_indices = [mr.bell_index for mr in midpoint_results]
            if set(bell_indices).issubset([BellIndex.PSI_PLUS, BellIndex.PSI_MINUS]):
                # midpoint heralded success in all successful modes
                self._schedule_now(EVTYPE_SUCCESS)
            else:
                # at least one unknown bellstate was heralded
                self._schedule_now(EVTYPE_ERROR)
        else:
            # midpoint heralded failure
            self._schedule_now(EVTYPE_FAIL)

        self._is_waiting = False    # reset indicator that protocol is waiting for heralding signal

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("{:.2e} ++ {}: success for mode {} with result {}".format(
                sim_time(), self.node.name, self.node.cdata["successful_modes_{}".format(self.source.name[-1])][-1],
                midpoint_results))

    @property
    def is_connected(self):
        """ Returns True iff protocol is correctly connected."""
        return self.source.ports["qout0"].forwarded_ports['output'].is_connected and self.source is not None and \
            self.memory is not None and self.memory.ports["qin"].connected_port.component == self.source


class SwapProtocol(Protocol):
    """Protocol responsible for swapping entanglement between two quantum memories.

    This protocol is designed to run in an automated fashion every time after the two EmissionProtocols on the memories
    are done.

    This protocol (currently) only supports swapping a single successful mode per link.

    Parameters
    ----------
    node : instance of :class:`~netsquid.nodes.node.Node`
        Node to apply protocol to.
    proto_l : instance of :class:`~.EmissionProtocol`
        EmissionProtocol running on the left memory.
    proto_r : instance of :class:`~.EmissionProtocol`
        EmissionProtocol running on the right memory.
    swap_single_pair_only : bool (optional)
        Whether only a single pair should be swapped per attempt. If set to False, all possible pairs are swapped
        simultaneously.

    Notes
    -----
    Operation:

        1. waits for relevant events from the two EmissionProtocols running on the node.
        2. pops (first) successful qubits from the memories connected to the detector ports.
        ::
            Note: As a workaround for simulating multi-photon emissions with multiple qubits
            representing photons the noise on the memory is applied only on emission.
        3. records swap results and mode received from detector.
        4. schedules appropriate event (SUCC, FAIL, ERR).

    Example:

    .. code-block:: python​

        from netsquid_ae.ae_protocols import EmissionProtocol​, SwapProtocol​

        # initialize protocol
        # Note: repeater is a fully set up repeater node with two quantum memories (QMem), two photon
        # pair sources (PPS), a clock and a Qdetector

        ep_left = EmissionProtocol(repeater, memory=repeater.subcomponents["QMem_L"],
                                        source=repeater.subcomponents["PPS_L"], max_attempts=100)
        ep_right = EmissionProtocol(repeater, memory=repeater.subcomponents["QMem_R"],
                                        source=repeater.subcomponents["PPS_R"], max_attempts=100)
        swap_proto = SwapProtocol(repeater, ep_l, ep_r)

        # start protocols and clock triggering the emission protocols

        for proto in [ep_left, ep_right, swap_proto]:
            proto.start()

        repeater.subcomponents["Clock"].start()

        # run simulation

        netsquid.sim_run()

    """

    def __init__(self, node, proto_l, proto_r, swap_single_pair_only=True):
        super().__init__()
        self.node = node
        # check if running on an actual repeater node
        if not isinstance(node, RepeaterNode):
            raise ValueError(f"Swap protocol must be attached to RepeaterNode not {type(node)}")
        # verify presence of all necessary components
        for comp in ['PPS_L', 'PPS_R', 'QMem_L', 'QMem_R', 'QDet', 'Clock']:
            if comp not in self.node.subcomponents:
                raise ValueError("SwapProtocol running on node missing some components.")
        self.protocol_type = "Swap"
        self.proto_l = proto_l
        self.proto_r = proto_r
        self._swap_single_pair_only = swap_single_pair_only
        # initialize necessary components
        self.detector = node.subcomponents["QDet"]
        self.memory_L = node.subcomponents["QMem_L"]
        self.memory_R = node.subcomponents["QMem_R"]

        # set grouping size of qubits
        if self.memory_L.properties["mp_encoding"] == 'presence_absence':
            # in presence-absence encoding (single-click scheme) two qubits represent one state
            self.grouping = 2
        elif self.memory_L.properties["mp_encoding"] == "time_bin":
            # in time-bin encoding (double-click scheme) four qubits (2 qubits * 2 time d.o.f.) represent one state
            self.grouping = 4
        else:
            self.grouping = 1
        # event expressions
        self.evexpr_proto_l = EventExpression(source=self.proto_l, event_type=EVTYPE_SUCCESS) | \
            EventExpression(source=self.proto_l, event_type=EVTYPE_ERROR) | \
            EventExpression(source=self.proto_l, event_type=EVTYPE_FAIL)
        self.evexpr_proto_r = EventExpression(source=self.proto_r, event_type=EVTYPE_SUCCESS) | \
            EventExpression(source=self.proto_r, event_type=EVTYPE_ERROR) | \
            EventExpression(source=self.proto_r, event_type=EVTYPE_FAIL)
        self.evexpr_trigger = self.evexpr_proto_l & self.evexpr_proto_r

        self.trigger_handler = ExpressionHandler(self._trigger)

        # prepare dictionary on node to collect data in
        self.node.cdata["swap_outcomes"] = []
        self.node.cdata["swapped_modes_L"] = []
        self.node.cdata["swapped_modes_R"] = []

    def start(self):
        """Starts the protocol.

        Waits for EmissionProtocols, sets protocol to running and binds output handler to port of detector.

        """
        super().start()
        # wait for restart expression
        self._wait(self.trigger_handler, expression=self.evexpr_trigger)
        # bind handlers
        self.detector.ports["cout0"].bind_output_handler(self._handle_swap_result, tag_meta=True)

    def stop(self):
        """Stops the protocol.

        Protocol should be fully connected. Does not clear internal state.

        Dismisses EventHandlers and callbacks.

        """
        if not self.is_connected:
            raise RuntimeError("Cannot stop a protocol that is not fully connected")
        super().stop()
        # unbind handlers
        self.detector.ports["cout0"].bind_output_handler(None)

    def _trigger(self, evexpr):
        """ Triggers the protocol.

        Called when both EmissionProtocols are done.

            #. pop successful Qubits from both memories
                Note: if at least one EmissionProtocol was unsuccessful, immediately schedules FAIL
            #. Noise is applied as EmissionNoise (as a workaround the time spent on the memory (delta_time) is passed to
            the emission noise model as a property).

            Note: the Qubits will be automatically put on the detector through the connected ports.

        """
        # get successful mode (left and right) from node
        successful_modes_l = self.node.cdata["successful_modes_L"][-1]
        successful_modes_r = self.node.cdata["successful_modes_R"][-1]
        assert isinstance(successful_modes_l, list)
        assert isinstance(successful_modes_r, list)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("{:.2e} >< Triggering SwapProtocol on {}. Successful modes were [L:{}, R:{}].".format(
                sim_time(), self.node.name, successful_modes_l, successful_modes_r))

        modes_to_be_swapped_l, modes_to_be_swapped_r, meta_l, meta_r = self._select_modes_to_swap(successful_modes_l,
                                                                                                  successful_modes_r)
        if self._swap_single_pair_only:
            # only swap a single mode
            modes_to_be_swapped_r = [modes_to_be_swapped_r[0]]
            modes_to_be_swapped_l = [modes_to_be_swapped_l[0]]
        # write swapped modes to node to be picked up by messaging
        self.node.cdata["swapped_modes_L"].append(modes_to_be_swapped_l)
        self.node.cdata["swapped_modes_R"].append(modes_to_be_swapped_r)

        if modes_to_be_swapped_l[0] is None or modes_to_be_swapped_r[0] is None:
            # if at least one successful mode is None immediately schedule failure
            self._handle_swap_result(Message(BSMOutcome(success=False)))
        else:
            if modes_to_be_swapped_l and modes_to_be_swapped_r:
                positions_left = []
                positions_right = []
                # determine positions of qubits to be popped
                for mode in modes_to_be_swapped_l:
                    assert mode is not None
                    positions_left += list(range(mode * self.grouping, (mode * self.grouping) + self.grouping))
                for mode in modes_to_be_swapped_r:
                    assert mode is not None
                    positions_right += list(range(mode * self.grouping, (mode * self.grouping) + self.grouping))

                # if memory used qout_noise_model manually apply noise
                if self.memory_L.models["qout_noise_model"] is not None or \
                        self.memory_R.models["qout_noise_model"] is not None:
                    # get delta_time for emission noise model
                    delta_times_l = self.memory_L.delta_time(positions=positions_left)
                    delta_times_r = self.memory_R.delta_time(positions=positions_left)
                    if (len(set(delta_times_l)) == 1) and (len(set(delta_times_r)) == 1):
                        # delta times are equal within each set
                        self.memory_L.models["qout_noise_model"].properties.update({"delta_time": delta_times_l[0]})
                        self.memory_R.models["qout_noise_model"].properties.update({"delta_time": delta_times_r[0]})
                        # pop successful qubits (memory loss will be applied as emission noise)
                        qubits_l = self.memory_L.pop(positions_left, meta_data=meta_l)
                        qubits_r = self.memory_R.pop(positions_right, meta_data=meta_r)
                        if logger.isEnabledFor(logging.DEBUG):
                            logger.debug("{:.2e} >< SwapProtocol on {} popping qubits [L:{}, R:{}].".format(
                                sim_time(), self.node.name, qubits_l, qubits_r))
                    else:
                        # error occurred, schedule ERR event
                        print("Qubits to be popped spent different time on the memory.")
                        self._handle_swap_result(Message("Error while popping."))
                else:
                    # pop successful qubits (memory loss will be applied correctly by memory itself)
                    qubits_l = self.memory_L.pop(positions_left, meta_data=meta_l)
                    qubits_r = self.memory_R.pop(positions_right, meta_data=meta_r)
                    if logger.isEnabledFor(logging.DEBUG):
                        logger.debug("{:.2e} >< SwapProtocol on {} popping qubits [L:{}, R:{}] without emission "
                                     "noise.".format(sim_time(), self.node.name, qubits_l, qubits_r))
            else:
                raise RuntimeError()

    def _select_modes_to_swap(self, successful_modes_l, successful_modes_r):
        """Function that selects which of the successful elementary links to swap.

        In this very simple implementation only the first successful mode from each side is swapped.

        This function should be overwritten for more complicated protocols e.g. pairing off multiple modes to swap each
        round.

        Parameters
        ----------
        successful_modes_l : list
            List of all successful modes from the elementary link to the left.
        successful_modes_r : list
            List of all successful modes from the elementary link to the right.

        Returns
        -------
        modes_to_be_swapped_l : list of int
            List of the modes from the left side that should be swapped.
        modes_to_be_swapped_r : list of int
            List of the modes from the right side that should be swapped.
        meta_l : dict
            Dictionary of meta information that should be appended to the popped qubits from the left link.
            This should contain all necessary information that the swap component connected to the memories requires.
        meta_r : dict
            Dictionary of meta information that should be appended to the popped qubits from the left link.
            This should contain all necessary information that the swap component connected to the memories requires.
        """
        modes_to_be_swapped_l = [successful_modes_l[0]]
        modes_to_be_swapped_r = [successful_modes_r[0]]
        meta_l = {"modes": [successful_modes_l[0]]}
        meta_r = {"modes": [successful_modes_r[0]]}
        return modes_to_be_swapped_l, modes_to_be_swapped_r, meta_l, meta_r

    def _handle_swap_result(self, message):
        """ Handles messages from classical output ports of the detector.

        Records result on node and schedules appropriate event.

        This function only supports BellIndex.PSI_PLUS, BellIndex.PSI_MINUS. IF the detector used for the swapping BSM
        can project on more/other states this function should be overwritten.


        Parameters
        ----------
        message : :obj:`netsquid.components.component.Message`
            Message from the swap detector containing the swap result as the item.

        """
        swap_result, = message.items
        # store data on node
        self.node.cdata["swap_outcomes"].append([swap_result])
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("{:.2e} >< SwapProtocol on {} scheduling outcome events.   ".format(
                sim_time(), self.node.name))
        # schedule appropriate event
        if swap_result.success:
            if swap_result.bell_index not in [BellIndex.PSI_PLUS, BellIndex.PSI_MINUS]:
                # unknown heralding state
                self._schedule_now(EVTYPE_ERROR)
            else:
                # successful swap
                self._schedule_now(EVTYPE_SUCCESS)
        else:
            # fail (either in emission protocols or in swap)
            self._schedule_now(EVTYPE_FAIL)

    @property
    def is_connected(self):
        """ Returns True iff protocol is correctly connected."""
        return self.memory_L is not None and self.memory_R is not None and self.detector is not None and \
            self.memory_L.ports["qout"].connected_port.component == self.detector and \
            self.memory_R.ports["qout"].connected_port.component == self.detector


class ExtractionProtocol(Protocol):
    """Protocol for extracting successful Qubits from the memory (after successful entanglement generation)
    and putting them on the output port.

    To be used on EndNodes.

    Parameters
    ----------
    node : instance of :class:`~netsquid.nodes.node.Node`
        Node to apply protocol to.
    proto : instance of :class:`~.EmissionProtocol`
        EmissionProtocol running on the memory.
    delay : float , optional
        Time to wait after receiving event from EmissionProtocol. (e.g. to ensure there was enough time to swap)

    Notes
    -----
    Operation:

        1. waits for events from emission protocol running on the memory
        ::
            Note: ExtractionProtocol is only concerned about the events regarding handling messages from midpoint
        2. waits for t = delay
        3. pops successful qubit from memory (applies noise if needed) and puts it on the port.
        ::
            Note: if no mode was successful puts None on port.
        4. schedules appropriate event.

    Example

    .. code-block:: python​

        from netsquid_ae.ae_protocols import EmissionProtocol​, ExtractionProtocol

        # initialize protocol
        # Note: alice is a fully set up end node with a quantum memory (QMem) and photon pair source
        # (PPS) and a clock

        emission_prot = EmissionProtocol(alice, memory=alice.subcomponents["QMem"],
                                  source=alice.subcomponents["PPS"], max_attempts=100)
        extraction_proto = ExtractionProtocol(alice, emission_prot)

        # start protocols and clock triggering the emission protocol

        emission_prot.start()​
        extraction_prot.start()​
        alice.subcomponents["Clock"].start()

        # run simulation

        netsquid.sim_run()

    """
    evtype_extract = EventType("EX", "Delay over, extract now.")

    def __init__(self, node, proto, delay=0):
        super().__init__()
        self.node = node
        self.protocol_type = "Extraction"
        self.proto = proto
        self.delay = delay
        # initialize necessary components
        self.memory = node.subcomponents["QMem"]
        self.port = node.ports["Qbit_out"]
        # initialize counters
        self.successful_mode = None

        # set grouping size of qubits
        if self.memory.properties["mp_encoding"] == 'presence_absence':
            self.grouping = 2
        elif self.memory.properties["mp_encoding"] == 'time_bin':
            self.grouping = 4
        else:
            self.grouping = 1
        # event handlers
        self.evexpr_proto = EventExpression(source=self.proto, event_type=EVTYPE_SUCCESS) | \
            EventExpression(source=self.proto, event_type=EVTYPE_ERROR) | \
            EventExpression(source=self.proto, event_type=EVTYPE_FAIL)
        self._trigger_handler = ExpressionHandler(self._trigger)
        self._extraction_handler = EventHandler(self._extract)

    def start(self):
        """Starts the protocol.

        Waits for EmissionProtocol and extraction event after delay and sets protocol to running.

        """
        super().start()
        # wait once for restart expression
        self._wait(self._trigger_handler, expression=self.evexpr_proto)
        self._wait(self._extraction_handler, entity=self, event_type=self.evtype_extract)

    def stop(self):
        """Stops the protocol.

        Protocol should be fully connected. Does not clear internal state.

        Dismisses EventHandlers and callbacks.

        """
        if not self.is_connected:
            raise RuntimeError("Cannot stop a protocol that is not fully connected")
        super().stop()
        self._dismiss(self._trigger_handler, entity=self.proto)
        self._dismiss(self._extraction_handler, entity=self, event_type=self.evtype_extract)

    def _trigger(self, evexpr):
        """Triggered by event from EmissionProtocol.

        Waits then schedules event for extraction.

        """
        successful_modes = self.node.cdata["successful_modes_E"][-1]
        qubits = []
        for successful_mode in successful_modes:
            if successful_mode is not None:
                positions = list(range(successful_mode * self.grouping,
                                       (successful_mode * self.grouping) + self.grouping))
                qubits.append(self.memory.peek(positions=positions, skip_noise=True))
                # qubits.append([self.memory.mem_positions[pos]._qubit for pos in positions])
        self.node.cdata["states_E"] = qubits

        self._schedule_after(self.delay, self.evtype_extract)

    def _extract(self, event):
        """Extracts successful Qubit from memory.

            #. pops successful Qubit(s)
                Note: if no mode was successful puts None on port
            #. Noise is applied as EmissionNoise (as a workaround the time spent on the memory (delta_time) is passed to
            the emission noise model as a property).
            #. schedules appropriate event (SUCC, FAIL, ERR)

            Note: the Qubits will be automatically put on the output port of the node as the memory output port is
            forwarded to it.

        """
        successful_modes = self.node.cdata["successful_modes_E"][-1]
        if successful_modes[0] is None:
            # no mode succeeded this round
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug("{:.2e} >< Extraction protocol {} scheduling fail for mode {}".format(
                    sim_time(), self.node.name, successful_modes))
            # Transmit vacuum to port to ensure failed QKDOutcome
            self.port.tx_output(qapi.create_qubits(self.grouping))
            # schedule event
            self._schedule_now(EVTYPE_FAIL)
        else:
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug("{:.2e} >< Extracting successful qubit on {}".format(
                    sim_time(), self.node.name))
            for successful_mode in successful_modes:
                positions = list(range(successful_mode * self.grouping,
                                       (successful_mode * self.grouping) + self.grouping))
                # get delta_time for applying noise model manually
                delta_times = self.memory.delta_time(positions=positions)
                if len(set(delta_times)) == 1:
                    # delta times are all equal
                    # the delay is only introduced to be able to access the end-to-end state, in reality the state is
                    # measured instantly, therefore we subtract the delay here:
                    delta_time = delta_times[0] - self.delay
                    if self.memory.models["qout_noise_model"] is not None:
                        self.memory.models["qout_noise_model"].properties.update({"delta_time": delta_time})
                    # pop successful qubits (memory loss will be applied as emission noise)
                    new_meta = {"round": self.node.cdata["round_E"][-1],  # additional meta for end node
                                "successful_modes": successful_modes}
                    self.memory.pop(positions, meta_data=new_meta)
                else:
                    print("Qubits to be popped spent different time on the memory.")  # this should never happen
                    self._schedule_now(EVTYPE_ERROR)
            self._schedule_now(EVTYPE_SUCCESS)

    @property
    def is_connected(self):
        """ Returns True iff protocol is correctly connected."""
        return self.memory is not None and self.port is not None


class MessagingProtocol(Protocol):
    """Protocol for sending messages between the different nodes.

    Parameters
    ----------
    node : instance of :class:`~netsquid.nodes.node.Node`
        Node the protocol runs on.
    emission_protocol_1 : instance of :class:`~.EmissionProtocol`
        EmissionProtocol running on the first memory.
    emission_protocol_2 : instance of :class:`~.EmissionProtocol`
        EmissionProtocol running on the second memory. Only used if running on a RepeaterNode.
    swap_protocol : instance of :class:`~.SwapProtocol`
        SwapProtocol running on the node. Only used if running on a RepeaterNode.
    detection_protocol : instance of :class:`~.DetectionProtocol`
        DetectionProtocol running on the node. Only used if running on a DetectionNode.


    Notes
    ------
    Operation:
        1. gets triggered by last protocol on node (EndNode: Extraction, RepeaterNode: Swap, DetectionNode: Detection)
        ::
            if on Detection node: create message and send through (both) ports
            else: check if message from last node already arrived
                no: idle
                yes: append data and send through opposite port
        2. gets triggered by arriving message
        ::
            if on Detection: (check if both chains worked), pop event for data collector
            else: see if already triggered by protocol
                no: idle
                yes: append data and send through opposite port

    Example

    .. code-block:: python​

        from netsquid_ae.ae_protocols import EmissionProtocol​, ExtractionProtocol, MessagingProtocol

        # initialize protocol
        # Note: alice is a fully set up end node with a quantum memory (QMem) and photon pair source
        # (PPS) and a clock

        protocols = []
        emission_prot = EmissionProtocol(alice, memory=alice.subcomponents["QMem"],
                                  source=alice.subcomponents["PPS"], max_attempts=100))
        protocols.append(emission_prot)
        protocols.append(ExtractionProtocol(alice, emission_prot))
        protocols.append(MessagingProtocol(alice, emission_protocol_1=emission_prot))

        # start protocols and clock triggering the emission protocol

        for proto in protocols:
            proto.start()​
        alice.subcomponents["Clock"].start()

        # run simulation

        netsquid.sim_run()

    """

    # TODO: decide whether node should decide if not sending message further when failed.
    # TODO: consider subclassing messaging protocol.
    node_type_detection = "Detection Node"
    node_type_end = "End Node"
    node_type_repeater = "Repeater Node"

    def __init__(self, node, emission_protocol_1=None, emission_protocol_2=None, swap_protocol=None,
                 detection_protocol=None):
        super().__init__()
        self.node = node
        self.protocol_type = "Messaging"
        if detection_protocol is None:
            self.clock = node.subcomponents["Clock"]
        self.emission_protocol_1 = emission_protocol_1
        self.emission_protocol_2 = emission_protocol_2
        self.swap_protocol = swap_protocol
        self.detection_protocol = detection_protocol
        self._app_connected = True
        # determine what kind of node this protocol is running on
        if isinstance(self.node, QKDNode):
            self.node_type = self.node_type_detection
            # initialize data storage
            self.node.cdata["final_message"] = []
            if self.detection_protocol.dual_chain:
                # initialize container for both chains
                self.dual_chain_container = {"Message_IO_Chain_1": None,
                                             "Message_IO_Chain_2": None,
                                             }
        elif isinstance(self.node, EndNode):
            self.node_type = self.node_type_end
            # initialize data storage
            self.node.cdata["final_message"] = []
            self.node.cdata["application_data"] = []
            self.node.cdata["sent_LR"] = []
        elif isinstance(self.node, RepeaterNode):
            self.node.cdata["sent_LR"] = []
            self.node_type = self.node_type_repeater
            # initialize data storage
            # no additional data added by messaging protocol
        else:
            raise ValueError(f"Unknown node type {type(self.node)} with name {self.node.name} for MessagingProtocol.")

        # initialize message storage
        self.message_storage = []

        # event handlers
        self._protocol_handler = EventHandler(self._collect_protocol_data)

    def start(self):
        """Starts the protocol.

        Waits for correct event from protocol, checks if an application (e.g. detector node for QKD-application) is
        connected, binds Handlers to ports used for messaging, sets protocol to running.

        Protocol should be fully connected.

        """
        super().start()
        # set up handler waiting for correct protocol event
        if self.node_type == self.node_type_detection:
            self._wait(self._protocol_handler, entity=self.detection_protocol)
        elif self.node_type == self.node_type_end:
            self._protocol_handler = ExpressionHandler(self._collect_protocol_data)
            evexpr_proto = EventExpression(source=self.emission_protocol_1, event_type=EVTYPE_SUCCESS) | \
                EventExpression(source=self.emission_protocol_1, event_type=EVTYPE_ERROR) | \
                EventExpression(source=self.emission_protocol_1, event_type=EVTYPE_FAIL)
            self._wait(self._protocol_handler, expression=evexpr_proto)
        else:
            self._wait(self._protocol_handler, entity=self.swap_protocol)

        # check if application is connected to chain / or bare repeater chain, default set in init is True
        if (self.node_type == self.node_type_end) and\
                (not self.node.ports["Messaging_IO_L"].is_connected or
                 not self.node.ports["Messaging_IO_R"].is_connected):
            self._app_connected = False

        # bind handlers to ports
        if self.node_type == self.node_type_detection:
            self.node.ports["Message_IO_Chain_1"].bind_input_handler(self._handle_incoming_message, tag_meta=True)
            if self.detection_protocol.dual_chain:
                self.node.ports["Message_IO_Chain_2"].bind_input_handler(self._handle_incoming_message, tag_meta=True)
        else:
            self.node.ports["Messaging_IO_L"].bind_input_handler(self._handle_incoming_message)
            self.node.ports["Messaging_IO_R"].bind_input_handler(self._handle_incoming_message)

    def stop(self):
        """Stops the protocol.

        Protocol should be fully connected. Does not clear internal state.

        Dismisses Handlers and callbacks.

        """
        if not self.is_connected:
            raise RuntimeError("Cannot stop a protocol that is not fully connected")
        super().stop()
        self._dismiss(self._protocol_handler)
        # unbind handlers
        if self.node_type == self.node_type_detection:
            self.node.ports["Message_IO_Chain_1"].bind_input_handler(None)
            if self.detection_protocol.dual_chain:
                self.node.ports["Message_IO_Chain_2"].bind_input_handler(None)
        else:
            self.node.ports["Messaging_IO_L"].bind_input_handler(None)
            self.node.ports["Messaging_IO_R"].bind_input_handler(None)

    def _collect_protocol_data(self, event):
        """Function triggered by protocol event.

        Collects correct data from protocols and saves them with corresponding time tag/round.

        On Detection node starts the message with a format of:
            [sender, [QKDOutcome(basis, outcome),...], [end_node_midpoint_results], [swap_results],
            [midpoint_results_r]]
        and meta: round, chain, successful_modes

        """
        if self.node_type == self.node_type_detection:
            assert len(self.node.cdata["qkd_outcomes"]) == len(self.node.cdata["round"])
            assert len(self.node.cdata["qkd_outcomes"]) == len(self.node.cdata["successful_modes"])
            qkd_outcomes = self.node.cdata["qkd_outcomes"][-1]
            successful_modes = self.node.cdata["successful_modes"][-1]
            current_round = self.node.cdata["round"][-1]
            # starting chain of messages
            det_message = Message(items=[self.node.name, qkd_outcomes],
                                  round=current_round,
                                  successful_modes=successful_modes)
            self._send_message(det_message)
            return
        elif self.node_type == self.node_type_end:
            self.node.cdata["sent_LR"].append(0)            # set counter for sent messages to 0
            self.emission_protocol_1._is_triggered = False
            if not self._app_connected:
                # no "application" is connected, start message on end node
                mo = self.node.cdata["midpoint_outcomes_E"][-1]
                chain_message = Message(items=[self.node.name, mo],
                                        round=self.node.cdata["round_E"][-1],
                                        successful_modes=self.node.cdata["successful_modes_E"][-1])
                chain_message.meta['chain'] = 1
                if self.node.ports["Messaging_IO_R"].is_connected:
                    self.node.ports["Messaging_IO_R"].tx_output(chain_message)
                elif self.node.ports["Messaging_IO_L"].is_connected:
                    self.node.ports["Messaging_IO_L"].tx_output(chain_message)
                else:
                    raise RuntimeError("No Application connected but also none of the ports is connected.")
                return
        else:
            # repeater node
            self.node.cdata["sent_LR"].append(0)            # set counter for sent messages to 0
            self.emission_protocol_1._is_triggered = False
            self.emission_protocol_2._is_triggered = False

        # check if message with corresponding time tag has arrived
        for message in self.message_storage:
            if message.meta["round"] == self.clock.num_ticks:
                self._send_message(message)

    def _handle_incoming_message(self, message):
        """Function triggered by incoming message.

        Checks if data of corresponding round is already available.

        Parameters
        ----------
        message : :obj:`netsquid.components.component.Message`
            Incoming message containing a list of outcomes and node names as the item and the current round as meta.
            Note for the precise format of the message see :meth:`~.ae_protocols.MessagingProtocol._send_message`.

        """
        if self.node_type == self.node_type_detection:
            # append correct measurement to message
            current_round = message.meta["round"]
            current_qkd_outcomes = self.node.cdata["qkd_outcomes"][self.node.cdata["round"].index(current_round)]
            current_measured_modes = self.node.cdata["successful_modes"][self.node.cdata["round"].index(current_round)]
            message, local_index = self._check_if_modes_match(message=message, local_modes=current_measured_modes)
            current_qkd_outcomes = self._filter_data(current_qkd_outcomes, local_index)
            message.items.append(current_qkd_outcomes)
            message.items.append(self.node.name)

            if self.detection_protocol.dual_chain:
                # dual chain
                chain_name = message.meta["rx_port_name"]
                self.dual_chain_container[chain_name] = message
                if not (None in self.dual_chain_container.values()):
                    combined_message_items = self.dual_chain_container["Message_IO_Chain_1"].items \
                        + self.dual_chain_container["Message_IO_Chain_2"].items
                    combined_message = Message(combined_message_items)
                    combined_message.meta = message.meta

                    self.node.cdata["final_message"].append(combined_message)
                    # ##### Scheduling events ######
                    if self.dual_chain_container["Message_IO_Chain_1"].meta["round"] != \
                            self.dual_chain_container["Message_IO_Chain_2"].meta["round"]:
                        self._schedule_now(EVTYPE_ERROR)
                    else:
                        self._schedule_events(combined_message)
                    # reset dual chain container
                    self.dual_chain_container = {"Message_IO_Chain_1": None,
                                                 "Message_IO_Chain_2": None,
                                                 }
            else:
                if len(message.items[1]) == 0:
                    self.node.cdata["final_message"].append(message)
                    self._schedule_events(message)
                else:
                    # scheduling events for multiple successes in a single message
                    for n in range(len(message.items[1])):
                        single_item = []
                        single_item.append(message.items[0])
                        for item in message.items[1:-1]:
                            single_item.append([item[n]])
                        single_item.append(message.items[-1])
                        single_outcome_message = Message(items=single_item, **message.meta)

                        self.node.cdata["final_message"].append(single_outcome_message)
                        self._schedule_events(single_outcome_message, delay=n)
            # delete data
            index = self.node.cdata["round"].index(current_round)
            del self.node.cdata["qkd_outcomes"][:index]
            del self.node.cdata["measured_modes"][:index]
            del self.node.cdata["successful_modes"][:index]
            del self.node.cdata["round"][:index]
            for key in self.node.cdata.keys():
                if key != "final_message":  # final message storage is cleared by datacollector
                    assert len(self.node.cdata["round"]) == len(self.node.cdata[key])

        elif not self._app_connected:
            # end node and no application is connected: schedule events
            self.node.cdata["final_message"].append(message)
            self._schedule_events(message)

        else:
            # repeater or end node
            # check if data with corresponding time tag exists already
            if self.node_type == self.node_type_end:
                data_round_l = self.node.cdata["round_E"]
                data_round_r = self.node.cdata["round_E"]
                keys = ['successful_modes_E', 'midpoint_outcomes_E']
            else:
                data_round_l = self.node.cdata["round_L"]
                data_round_r = self.node.cdata["round_R"]
                keys = ['successful_modes_L', 'successful_modes_R', 'midpoint_outcomes_L', 'midpoint_outcomes_R',
                        'swap_outcomes', 'swapped_modes_L', 'swapped_modes_R', 'sent_LR']
            if (message.meta["round"] in data_round_l) and (message.meta["round"] in data_round_r) and\
                    all(len(self.node.cdata[i]) == len(self.node.cdata[[*self.node.cdata][0]]) for i in keys):
                # all corresponding data is available, send message
                self._send_message(message)
            else:
                # store message until corresponding data is collected
                self.message_storage.append(message)

    def _send_message(self, message):
        """Function that compiles the new message and sends it on to the next node.

        Parameters
        ----------
        message : :obj:`netsquid.components.component.Message`
            Message containing a list of outcomes and node names as the item and the current round as meta.
            The precise format is given in the note below.

        Note
        ----
        Message.items: list
            Format [sender, QKDOutcome(basis, outcome), endnode_midpoint_result, swap_result, midpoint_result_r..]

        """
        current_round = message.meta["round"]
        # get sender from message to determine direction of sending
        sender = message.items[0]

        if self.node_type == self.node_type_repeater:
            # Get the two midpoint outcomes and single swap outcome
            index_left = self.node.cdata["round_L"].index(current_round)
            index_right = self.node.cdata["round_R"].index(current_round)
            if index_left != index_right:
                raise RuntimeError(f"Mismatch in rounds for midpoint outcome at {self.node.name}."
                                   f"Left side has {self.node.cdata['round_L'][index_left]}, while"
                                   f"right side has {self.node.cdata['round_R'][index_right]}.")
            mo_l = self.node.cdata["midpoint_outcomes_L"][index_left]
            mo_r = self.node.cdata["midpoint_outcomes_R"][index_right]
            modes_l = self.node.cdata["swapped_modes_L"][index_left]
            modes_r = self.node.cdata["swapped_modes_R"][index_right]
            so = self.node.cdata["swap_outcomes"][index_left]
            # select modes
            if sender in ["det_alice", "A_1", "A_2"]:  # -->
                swapped_modes_incoming_side = modes_l
                swapped_modes_other_side = modes_r
            elif sender in ["det_bob", "B_1", "B_2"]:  # <--
                swapped_modes_incoming_side = modes_r
                swapped_modes_other_side = modes_l
            else:
                raise ValueError("Unknown sender:", sender, " in message at node", self.node.name)
            message, swap_index = self._check_if_modes_match(message=message,
                                                             local_modes=swapped_modes_incoming_side,
                                                             local_modes_other_side=swapped_modes_other_side)
            midpoint_index_l = self._swapped_mode_index_to_midpoint_mode_index(
                local_modes_index=swap_index,
                swapped_modes=modes_l,
                local_midpoint_modes=self.node.cdata["successful_modes_L"][index_left])
            midpoint_index_r = self._swapped_mode_index_to_midpoint_mode_index(
                local_modes_index=swap_index,
                swapped_modes=modes_r,
                local_midpoint_modes=self.node.cdata["successful_modes_R"][index_right])
            mo_l = self._filter_data(mo_l, midpoint_index_l)
            mo_r = self._filter_data(mo_r, midpoint_index_r)
            so = self._filter_data(so, swap_index)
            # check if last midpoint result of message == first midpoint result of this protocol, then send
            if sender in ["det_alice", "A_1", "A_2"]:  # -->
                if message.items[-1] == mo_l:
                    message.items.append(so)
                    message.items.append(mo_r)
                    # --> sending to the right
                    self.node.ports["Messaging_IO_R"].tx_output(message)
                else:
                    # some error has occured in the messaging
                    raise RuntimeError("Mismatch of midpoint result in link left of node:", self.node.name)
            else:
                if message.items[-1] == mo_r:
                    message.items.append(so)
                    message.items.append(mo_l)

                    self.node.ports["Messaging_IO_L"].tx_output(message)
                else:
                    # some error has occured in the messaging
                    raise RuntimeError("Mismatch of midpoint results in link right of node:", self.node.name)

            # count how many messages have been sent for this entry
            self.node.cdata["sent_LR"][index_left] += 1
            # delete sent entries
            if self.node.cdata["sent_LR"][index_left] >= 2:
                # counter for this entry is 2, therefore messages to both sides have been sent for this entry
                del self.node.cdata["midpoint_outcomes_L"][:index_left + 1]
                del self.node.cdata["midpoint_outcomes_R"][:index_right + 1]
                del self.node.cdata["swap_outcomes"][:index_left + 1]
                del self.node.cdata["round_L"][:index_left + 1]
                del self.node.cdata["round_R"][:index_right + 1]
                del self.node.cdata["successful_modes_L"][:index_left + 1]
                del self.node.cdata["successful_modes_R"][:index_right + 1]
                del self.node.cdata["sent_LR"][:index_left + 1]
                del self.node.cdata["swapped_modes_L"][:index_left + 1]
                del self.node.cdata["swapped_modes_R"][:index_right + 1]

        elif self.node_type == self.node_type_end:
            current_index = self.node.cdata["round_E"].index(current_round)
            mo = self.node.cdata["midpoint_outcomes_E"][current_index]
            message, local_index = \
                self._check_if_modes_match(message=message,
                                           local_modes=self.node.cdata["successful_modes_E"][current_index])
            mo = self._filter_data(mo, local_index)
            modes = self._filter_data(self.node.cdata["successful_modes_E"][current_index], local_index)
            assert modes == message.meta["successful_modes"]
            if sender in ["det_alice", "A_1", "A_2"]:
                if self.node.name in ["A_1", "A_2"]:
                    # only append at beginning of chain (to avoid appending last midpoint twice)
                    message.items.append(mo)
                else:
                    # do not append at end of chain, sanity check:
                    if message.items[-1] != mo:
                        raise RuntimeError("Mismatch of midpoint results in link left of node:", self.node.name)

                self.node.ports["Messaging_IO_R"].tx_output(message)

            elif sender in ["det_bob", "B_1", "B_2"]:
                if self.node.name in ["B_1", "B_2"]:
                    # only append at beginning of chain (to avoid appending last midpoint twice)
                    message.items.append(mo)
                else:
                    # do not append at end of chain, sanity check:
                    if message.items[-1] != mo:
                        raise RuntimeError("Mismatch of midpoint results in link right of node:", self.node.name)

                self.node.ports["Messaging_IO_L"].tx_output(message)
            else:
                raise ValueError("Unknown sender:", sender, ", in message at node", self.node.name)
            # count how many messages have been sent for this entry
            current_index = self.node.cdata["round_E"].index(current_round)
            self.node.cdata["sent_LR"][current_index] += 1
            # deleted sent entries
            if self.node.cdata["sent_LR"][current_index] >= 2:
                # counter for this entry is 2 > messages to both sides have been sent for this entry
                del self.node.cdata["midpoint_outcomes_E"][:current_index + 1]
                self.node.cdata["states_E"] = []
                del self.node.cdata["successful_modes_E"][:current_index + 1]
                del self.node.cdata["sent_LR"][:current_index + 1]
                del self.node.cdata["round_E"][:current_index + 1]
        else:
            # detection protocol starting chain, send message to (both) chains
            if self.detection_protocol.dual_chain:
                if logger.isEnabledFor(logging.DEBUG):
                    logger.debug("{:.2e} ^^^ MessagingProtocol on {} sending message {} to both chains."
                                 .format(sim_time(), self.node.name, message))
                message_2 = copy.deepcopy(message)
                message_2.meta['chain'] = 2
                self.node.ports["Message_IO_Chain_2"].tx_output(message_2)
            message.meta['chain'] = 1
            self.node.ports["Message_IO_Chain_1"].tx_output(message)

    def _schedule_events(self, message, delay=0):
        """Function to analyse final message and schedule appropriate event.

        Input should be Message with items and meta "round", the current round.

        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("{:.2e} ^^^ MessagingProtocol: {} heralding end-to-end entanglement outcome events"
                         "for message {}.".format(sim_time(), self.node.name, message))
        # flatten all possible lists in the message, order of message.items does not matter here
        flattened_message = [item for item in message.items if not isinstance(item, list)] + \
            list(chain(*[items for items in message.items if isinstance(items, list)]))
        if any([(isinstance(item, BSMOutcome) or isinstance(item, QKDOutcome)) and
                not item.success for item in flattened_message]):
            self._schedule_after(delay, EVTYPE_FAIL)
        elif flattened_message in [['det_bob', 'det_alice'], ['det_alice', 'det_bob']]:
            # no end-to-end entanglement
            self._schedule_after(delay, EVTYPE_FAIL)
        else:
            # sanity check
            if not self.node.cdata["final_message"]:
                raise ValueError("final message can't be empty.")
            self._schedule_after(delay, EVTYPE_SUCCESS)
        # delete old messages that have been processed
        del self.node.cdata["final_message"][:-1]

    @staticmethod
    def _check_if_modes_match(message, local_modes, local_modes_other_side=None):
        """See if locally successful modes (e.g. from measurement or swap) match with the modes that are communicated
        by the message from the next node, to ensure that we pick up the right end-to-end measurement results for
        the created entangled state.

        Parameters
        ----------
        message : :obj:`netsquid.components.component.Message`
            Message containing a list of outcomes and node names as the item and the current round and modes as meta.
        local_modes : list
            List of locally successful modes either from midpoint measurements (end_node), swap measurements
            (repeater_node) or end node detector measurements (end_node).
        local_modes_other_side: list
            List of same length as `local_modes` containing the modes that `local_modes` were swapped with.

        Returns
        -------
        new_filtered_message: :obj:`netsquid.components.component.Message`
            New message where all non-matching results have been deleted and the successful modes updated.
        local_modes_index: list
            List of indices of the local modes that were successful, that can then be used to filter local results.
        """
        # get list of modes
        successful_modes_from_message = message.meta["successful_modes"]
        meta = message.meta

        # select which modes match and which list index they correspond to
        matching_modes_index = []
        new_successful_modes = []
        local_modes_index = []
        for j in range(len(local_modes)):
            for k in range(len(successful_modes_from_message)):
                if local_modes[j] == successful_modes_from_message[k]:
                    matching_modes_index.append(k)
                    new_successful_modes.append(local_modes[j])
                    local_modes_index.append(j)
        # select successful modes in existing message and filter out the rest
        new_items = [message.items[0]]
        for item in message.items[1:]:
            new_item = []
            for index in matching_modes_index:
                new_item.append(item[index])
            new_items.append(new_item)

        if local_modes_other_side is not None:
            # we are on a repeater node so the new successful modes are the ones we swapped with
            new_successful_modes = []
            for index in local_modes_index:
                new_successful_modes.append(local_modes_other_side[index])
        assert len(local_modes_index) == len(new_successful_modes)

        # update meta
        new_meta = {"successful_modes": new_successful_modes}
        meta.update(new_meta)
        new_filtered_message = Message(items=new_items, **meta)

        return new_filtered_message, local_modes_index

    @staticmethod
    def _swapped_mode_index_to_midpoint_mode_index(local_modes_index, swapped_modes, local_midpoint_modes):
        midpoint_index = []
        for i in local_modes_index:
            for j in range(len(local_midpoint_modes)):
                if swapped_modes[i] == local_midpoint_modes[j]:
                    midpoint_index.append(j)
        return midpoint_index

    @staticmethod
    def _filter_data(data_list, index_to_filter):
        new_data_list = []
        for index in index_to_filter:
            new_data_list.append(data_list[index])
        return new_data_list

    @property
    def is_connected(self):
        """ Returns True iff protocol is correctly connected."""
        if self.node_type == self.node_type_detection:
            if not self.detection_protocol.dual_chain and self.node.ports["Message_IO_Chain_1"].is_connected:
                return True
            elif self.detection_protocol.dual_chain and self.node.ports["Message_IO_Chain_1"].is_connected and \
                    self.node.ports["Message_IO_Chain_2"].is_connected:
                return True
            else:
                raise RuntimeError(f"MessagingProtocol is not connected properly on Detection Node {self.node.name}")
        elif self.node_type == self.node_type_repeater:
            if self.node.ports["Messaging_IO_L"].is_connected and self.node.ports["Messaging_IO_R"].is_connected:
                return True
            else:
                raise RuntimeError(f"MessagingProtocol is not connected properly on Repeater Node {self.node.name}")
        else:
            if self.node.ports["Messaging_IO_L"].is_connected or self.node.ports["Messaging_IO_R"].is_connected:
                return True
            else:
                raise RuntimeError(f"MessagingProtocol is not connected properly on End Node {self.node.name}")


class MagicProtocol(Protocol):
    """Protocol for distributing elementary link states with magic.

    Can use magic based on the analytical model described in https://arxiv.org/pdf/1404.7183.pdf ("analytical"), from
    sampled elementary link states ("sampled") or from reconstructed full elementary link states ("forced").

    In a repeater chain setup, this would replace the EmissionProtocol while leaving the other protocols intact.

    Parameters
    ----------
    node : instance of :class:`~netsquid.nodes.Node`
        Node on which this protocol lives.
    memory : instance of :class:`~netsquid.components.qmemory.QuantumMemory`
        Quantum memory in which the quantum state is magically generated.
    max_attempts: int, optional
        Maximum number of clock ticks that will be handled by this protocol.

    Notes
    -----
    The magic distributor is assumed to run on 'auto-pilot', i.e. it starts itself and continuously generates new
    successes.

    Operation:
    The MagicDistributor automatically generates deliveries.
    The Distributor delivers the appropriate message to the port, while the state is saved in the memory.
    The protocol is waiting for a message on the port and then schedules the corresponding (SUCCESS) event while
    writing the round, outcome and successful mode to the node.
    It also writes the current round to the detector node in order to have the right round associated with the end node
    measurements.

    Note that the successful mode is fixed to 0, since we do not need any other memory positions to store qubits.

    Example:

    .. code-block:: python​

        from netsquid_ae.ae_protocols import MagicProtocol
        from netsquid_ae.ae_magic import AEMagicDistributor

        # Initialize protocols
        # Note: alice and bob are fully set up end nodes with at least a quantum memory (QMem),
        # and they are connected by a HeraldedConnection, which contains both classical and quantum
        # channels and a detector.
        magic_distributor = AEMagicDistributor(nodes=[alice, bob],
                                               component=HeraldedConnection, **sim_params)
        # Create two MagicProtocols that use the same component as the MagicDistributor
        alice_mp = MagicProtocol(alice, memory=alice.subcomponents["QMem"],
                                 component=HeraldedConnection,
                                 max_attempts=10)
        bob_mp = MagicProtocol(bob, memory=bob.subcomponents["QMem"],
                               component=HeraldedConnection,
                               max_attempts=10)
        # Start both protocols and clocks that trigger the protocols
        alice_mp.start()
        bob_mp.start()
        magic_distributor.start_auto_delivery()
        # Run simulation
        netsquid.sim_run()

    """

    def __init__(self, node, memory, component, max_attempts=-1):
        super().__init__()
        self.node = node
        self.protocol_type = "Magic"
        self._component = component
        self.memory = memory
        self.clock = node.subcomponents["Clock"]
        # initialize counters
        self.current_round = 0
        self.max_attempts = max_attempts
        self._is_triggered = False
        self._is_waiting = False        # for the magic protocol this is always false

        # get sender port
        connected_ports = [p.connected_port for p in component.ports.values()]
        for cp in connected_ports:
            if cp in self.node.ports.values():
                self.port_sender = cp

        # Prepare dictionary to collect data in
        self.node.cdata["successful_modes_{}".format(self.memory.name[-1])] = []
        self.node.cdata["midpoint_outcomes_{}".format(self.memory.name[-1])] = []
        self.node.cdata["round_{}".format(self.memory.name[-1])] = []

    def start(self):
        """Starts waiting for messages on the port if protocol is fully connected.

        Note that the magic distributor runs on 'auto-pilot' such that it starts itself.

        """
        if self.is_running:
            # If we are running already, do nothing
            return

        super().start()
        self.port_sender.bind_input_handler(self._handle_link_result)

    def stop(self):
        """Stop the protocol and the magic distributor, dismiss all callbacks and EventHandlers."""
        if not self.is_connected:
            raise RuntimeError("Cannot stop a protocol that is not fully connected")
        super().stop()
        self.port_sender.bind_input_handler(None)

    def _handle_link_result(self, message):
        """Function handling incoming messages from the magic distributor.

        Stores the fixed successful mode, midpoint outcome and round on the node and schedules the correct
        event to be picked up by another protocol.

        Parameters
        ----------
        message : :obj:`netsquid.components.component.Message`
            Incoming message from the magic distributor.

        """
        if self._is_triggered:
            print(f"{sim_time()}: WARNING: Triggering magic distributor on "
                  f"{self.node.name} before data was collected by Messenger.")
            # something has gone wrong reset data storage and continue
            # TODO: figure out what goes wrong
            self.node.cdata["successful_modes_{}".format(self.memory.name[-1])] = []
            self.node.cdata["midpoint_outcomes_{}".format(self.memory.name[-1])] = []
            self.node.cdata["round_{}".format(self.memory.name[-1])] = []
        self._is_triggered = True   # indicator that protocol was triggered, reset when messaging protocols collects

        if not self.is_running:
            raise RuntimeError("Received message {} while protocol was not running."
                               .format(message))
        if isinstance(message.items[0], BSMOutcome):
            midpoint_results = message.items
            successful_modes = message.meta["successful_modes"]
        elif isinstance(message.items[0], str):
            # fail
            midpoint_results = [0]
            successful_modes = None
        else:
            raise ValueError(f"unknown message type {message} on node {self.node.name}. ")

        # update attempts
        self.current_round = message.meta["round"]
        # log round
        self.node.cdata["round_{}".format(self.memory.name[-1])].append(self.current_round)
        self.node.cdata["successful_modes_{}".format(self.memory.name[-1])].append(successful_modes)
        self.node.cdata["midpoint_outcomes_{}".format(self.memory.name[-1])].append(midpoint_results)

        self.write_data_to_detector_node(successful_modes=successful_modes, current_round=self.current_round)

        # schedule appropriate event
        if midpoint_results[0].success:
            for midpoint_result in midpoint_results:
                if midpoint_result.bell_index not in [BellIndex.PSI_PLUS, BellIndex.PSI_MINUS]:
                    # unknown heralding state
                    self._schedule_now(EVTYPE_ERROR)
                    break
            else:
                # midpoint heralded success
                self._schedule_now(EVTYPE_SUCCESS)
        else:
            # midpoint heralded failure
            self._schedule_now(EVTYPE_FAIL)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("{:.2e} ++ {}: success for mode {} with result {}".format(
                sim_time(), self.node.name, self.node.cdata["successful_modes_{}".format(self.memory.name[-1])][-1],
                midpoint_results))

    def write_data_to_detector_node(self, successful_modes, current_round=None):
        # write round to detector node
        if self.memory.ports["qout"].forwarded_ports:
            # memory port is forwarded, the memory is on an end node
            output_port = self.memory.ports["qout"].forwarded_ports["output"]
            if output_port.connected_port:
                # the end node is connected to a detector node
                detector_node = output_port.connected_port.component
                if current_round is not None:
                    # magic
                    if current_round not in detector_node.cdata["round"]:
                        # check if round is already there (dual chain will try to write twice)
                        detector_node.cdata["round"].append(current_round)
                        detector_node.cdata["measured_modes"].append(successful_modes)
                    detector_node.cdata["magic_override"] = [True]
                else:
                    detector_node.cdata["measured_modes"].append(successful_modes)

    @property
    def is_connected(self):
        """ Returns True iff protocol is correctly connected."""
        return self.memory is not None and self._component is not None and self.clock is not None
