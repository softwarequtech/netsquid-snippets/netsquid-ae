# Atomic Ensembles Forced Magic Factory - David Maier, 2021
# ==========================================================
#
# Script to generate end-to-end quantum states of elementary links by forcing midpoint outcomes
import os
import copy
import time
import logging
import numpy as np
import pickle
from scipy import sparse
from filelock import FileLock

import netsquid as ns
from netsquid.util import DataCollector
import netsquid.qubits.qubitapi as qapi
from pydynaa.core import EventExpression
from netsquid.util.simtools import logger
from netsquid.qubits.ketstates import BellIndex
from netsquid.nodes.network import Network

from netsquid_ae.protocol_event_types import EVTYPE_SUCCESS
from netsquid_ae.ae_protocols import EmissionProtocol
from netsquid_ae.ae_classes import EndNode, HeraldedConnection

from netsquid_physlayer.detectors import BSMOutcome

from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder


class ForcedLinkStateCollector:
    """Callable class for the DataCollector of an elementary link.

    Collecting the end-to-end state, the label of the forced midpoint outcome and the probability of that outcome.

    The resulting Dataframe can be used to recreate the full elementary link quantum state.

    For AE this should be triggered by EmissionProtocol.

    States should be stored on the nodes that the protocols, whose events trigger this DataCollector, live on.
    Measurement labels and probabilities are assumed to be stored in a list on the midpoint detector.

    Note: States including more than two qubits (aka. size > 4x4) will be saved as sparse instead of dense matrices.

    Parameters
    ----------
    alice_proto : instance of :class:`~.ae_protocols.EmissionProtocol`
        EmissionProtocol running on one end of the elementary link.
    bob_proto : instance of :class:`~.ae_protocols.EmissionProtocol`
        EmissionProtocol running on the other end of the elementary link.
    connection : instance of :class:~.ae_classes.HeraldedConnection'
        Connection containing the BSM detector in the middle of the elementary link.


    Notes
    -------
    constructs:

    dataframe: instance of :class:`~pandas.DataFrame`
        Dataframe containing the the end-to-end state and the label and probability of the forced detector outcome.
        Note: Since this collects the state it does not contain any end-node measurement results (as the measurement
        changes the state)

    """
    def __init__(self, alice_proto, bob_proto, connection):
        self.alice_proto = alice_proto
        self.bob_proto = bob_proto
        self.qdet = connection.subcomponents["QDet"]

    def __call__(self, event):
        """Function that is called by the data collector.

        Collects the end-to-end state from the nodes of the elementary link and the midpoint label and probability from
        the midpoint BSMDetector.

        Returns
        -------
        data : dict
            Dictionary containing the end-to-end state and the label and probability of the forced detector outcome
             of this simulation round.

        """
        if logger.isEnabledFor(logging.DEBUG):
            print("{}: ForcedLinkStateCollector collecting states".format(ns.sim_time()))

        # get states from both end nodes (both are lists)
        successful_qubits_a = self.alice_proto.node.cdata["states_E"][0]
        successful_qubits_b = self.bob_proto.node.cdata["states_E"][0]

        # take reduced density matrix
        combined_state = qapi.reduced_dm(successful_qubits_a + successful_qubits_b)
        state = copy.deepcopy(combined_state)
        if len(state) > 4:
            # TODO: change this once SparseDM (netsquid 0.10) is out
            # convert multi-photon state to sparse matrix to reduce memory requirement
            state = sparse.csr_matrix(state)

        # access probability and label stored on detector
        probs_and_label = self.qdet._forced_probabilities
        if len(probs_and_label) == 1:
            probability = probs_and_label[0][1]
            forced_label = probs_and_label[0][0]
        else:
            raise ValueError(f"List of forced probabilities on Qdet {probs_and_label} had unknown length.")
        self.qdet._forced_probabilities = []

        # assign correct BSMOutcomes to satisfy RepChainDataFrameHolder
        if forced_label in ["1010", "0101", "10"]:
            midpoint_outcome = BSMOutcome(success=True, bell_index=BellIndex.PSI_PLUS)
        elif forced_label in ["0110", "1001", "01"]:
            midpoint_outcome = BSMOutcome(success=True, bell_index=BellIndex.PSI_MINUS)
        else:
            midpoint_outcome = BSMOutcome(success=False)

        # create dictionary with relevant simulation data
        data = {"state": state,
                "success_probability": probability,
                "forced_label": forced_label,
                "midpoint_outcome_0": midpoint_outcome.bell_index,
                }

        return data


def run_simulation(encoding, mean_photon_number_l, mean_photon_number_r, emission_probabilities_l,
                   emission_probabilities_r, channel_length_l, channel_length_r, attenuation_l, attenuation_r,
                   coupling_loss_fibre, det_dark_count_prob, det_efficiency, det_visibility, det_num_resolving,
                   **kwargs):
    """Setup and run a simulation for fixed set of parameters.

    Sets up an elementary link and then runs the experiment exactly once for every possible (successful) midpoint
    detector outcome.

    Parameters
    ----------
    encoding : str
        Encoding of the photons. Either "time_bin" or "presence_absence".
    mean_photon_number_l : float or None
        Mean photon number for the thermal distribution of the SPDC sources on the left side of the elementary link.
        Must be supplied when `emission_probabilities_l` is None.
    mean_photon_number_r : float or None
        Mean photon number for the thermal distribution of the SPDC sources on the left side of the elementary link.
        Must be supplied when `emission_probabilities_r` is None.
    emission_probabilities_l: list or None
        List of probabilities of emitting a certain number of photons from the source on the left side of the
        elementary link. This must be a list of length 4, where each element denotes the probability of
        generating the number of photons pairs equal to its list index.
        If multi_photon is not used (False) then this list is truncated after n=1.
        If this is None, `mean_photon_number_l` must be supplied.
    emission_probabilities_r: list or None
        List of probabilities of emitting a certain number of photons from the source on the left side of the
        elementary link. This must be a list of length 4, where each element denotes the probability of
        generating the number of photons pairs equal to its list index.
        If multi_photon is not used (False) then this list is truncated after n=1.
        If this is None, `mean_photon_number_r` must be supplied.
    channel_length_l : float
        Length of the channel left of the detectors [in km].
    channel_length_r : float
        Length of the channel right of the detectors [in km].
    attenuation_l : float
        Attenuation of the channel left of the detectors [in dB/km].
    attenuation_r: float
        Attenuation of the channel right of the detectors [in dB/km].
    coupling_loss_fibre : float
        Coupling loss/ length independent loss of the channel to the midpoint, in [0,1].
    det_dark_count_prob : float
        Dark count probability of the midpoint detectors.
    det_efficiency : float
        Detection efficiency of the midpoint detectors.
    det_visibility : float
        Photon indistinguishability at the midpoint BSM.
    det_num_resolving : bool
        Whether or not midpoint detectors are number-resolving.

    Returns
    -------
    :obj:`~pandas.DataFrame`
        Dataframe containing elementary link state, midpoint outcome and corresponding probability.

    """
    start_time_simulation = time.time()
    protocols, network, forced_params = \
        create_el_link_for_forced_generation(encoding=encoding,
                                             mean_photon_number=[mean_photon_number_l, mean_photon_number_r],
                                             emission_probabilities=[emission_probabilities_l,
                                                                     emission_probabilities_r],
                                             channel_length_l=channel_length_l, channel_length_r=channel_length_r,
                                             attenuation_l=attenuation_l, attenuation_r=attenuation_r,
                                             coupling_loss_fibre=coupling_loss_fibre,
                                             det_dark_count_prob=det_dark_count_prob,
                                             det_efficiency=det_efficiency,
                                             det_visibility=det_visibility,
                                             det_num_resolving=det_num_resolving)
    connection = network.connections['conn|Alice<->Bob|heralding']
    nodes = network.nodes.values()

    # Start Protocols
    # =================
    for proto in protocols:
        proto.start()
    # Start Clocks
    # ================
    for node in nodes:
        node.subcomponents["Clock"].start()

    # Setup data collection
    # =====================
    em_proto = []
    for proto in protocols:
        if proto.protocol_type == "Emission":
            em_proto.append(proto)
    # setting up EventExpressions
    evexpr_succ_a = EventExpression(source=em_proto[0], event_type=EVTYPE_SUCCESS)
    evexpr_succ_b = EventExpression(source=em_proto[1], event_type=EVTYPE_SUCCESS)
    evexpr_success = evexpr_succ_a & evexpr_succ_b

    # set up state collector to pick up end-to-end state of elementary link
    sc = DataCollector(ForcedLinkStateCollector(alice_proto=em_proto[0], bob_proto=em_proto[1], connection=connection))
    sc.collect_on(evexpr_success)

    # determine labels to loop over
    if det_num_resolving:
        # number resolving: 6 ops ( no-clicks , 2x exactly-one, 2x more-than-1, at-least-one-in-each)
        # labels = ["00", "01", "10", "11", "02", "20"] # complete state
        labels = ["01", "10"]   # only successes
    else:
        # non-number = 4 operators
        # labels = ["00", "01", "10", "11"] # complete state
        labels = ["01", "10"]   # only successes

    if encoding == "time_bin":
        # compute all combinations of labels between the two bins
        dual_labels = []
        for l1 in labels:
            for l2 in labels:
                dual_labels.append(l1 + l2)
        labels = dual_labels

    for forced_label in labels:
        # force label in detector
        connection.subcomponents["QDet"]._force_povm_outcome = forced_label

        # Run simulation
        # ==============
        print(f"{ns.sim_time()} : Forcing label {forced_label}.")
        ns.sim_run()
        # reset clocks
        for node in nodes:
            node.subcomponents["Clock"].reset()
            node.subcomponents["Clock"].start()

    for proto in protocols:
        proto.stop()

    for node in nodes:
        node.reset()
    # make all lists (required by safe netconf loader) into tuples (required by RDFHolder)
    for key in forced_params.keys():
        if isinstance(forced_params[key], list):
            forced_params[key] = tuple(forced_params[key])
    forced_holder = RepchainDataFrameHolder(data=sc.dataframe, number_of_nodes=2,
                                            baseline_parameters=forced_params,
                                            description="Holder with forced elementary link states.")
    simulation_time = time.time() - start_time_simulation
    print(f"Generated states for labels: {labels} in {simulation_time:.2e} s")
    return forced_holder


def generate_forced_magic(unique_str=None, **kwargs):
    """Function to generate forced magic for an elementary link.

    The resulting data is then wrapped in a RDFH and saved as a pickle file for the magic factory to pick up.

    Parameters
    ----------
    kwargs : dict
        Simulation parameters.
    unique_str : str or None
        Unique string to add to the filename to avoid trouble with e.g. multiprocessing.

    Returns
    -------
    forced_holder :
        RDFH containing dataframe with states and dict of baseline parameters.
    """
    # set formalism (either DM or SparseDM)
    if kwargs["encoding"] == "presence_absence":
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.DM)
    else:
        # for time-bin dense DMs become too large therefore running `SPARSEDM` to drastically reduce memory req.
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.SPARSEDM)

    if kwargs['state_files_directory'] is None:
        raise ValueError("For forced magic state_files_directory must not be None.")
    # set filename to save sampled state as DataframeHolder, the MagicDistributor will check if parameters agree
    filename = set_magic_file_name(unique_str, **kwargs)

    if not os.path.exists(str(kwargs['state_files_directory'])):
        try:
            os.mkdir(str(kwargs['state_files_directory']))
        except FileExistsError:
            # sometimes this happens with multiple slurm jobs executing at the same time
            pass

    if os.path.isfile(filename):
        print(f"File {filename} already found on disk. Reusing and skipping generation of magic states.")
        with open(filename, "rb") as input_file:
            forced_holder = pickle.load(input_file)

    else:
        if not kwargs["multi_photon"]:
            raise NotImplementedError("Generating Forced Magic currently only implemented for multi-photon states.")
        # Run simulation for fixed parameters:
        forced_holder = run_simulation(**kwargs)

        if logger.isEnabledFor(logging.DEBUG):
            print("{}: Elementary Link Quantum State fully reproduced."
                  "Now generating StateSampler".format(ns.sim_time()))

        with FileLock(filename):
            with open(filename, 'wb') as handle:
                pickle.dump(forced_holder, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return forced_holder


def set_magic_file_name(unique_str=None, **kwargs):
    """Set the file name for the forced magic file.

    This function can be used by other entities to import the correct file.

    Parameters
    ----------
    kwargs : dict
        Simulation parameters.
    unique_str : str or None
        Unique string to add to the filename to avoid trouble with e.g. multiprocessing.

    Returns
    -------
    filename : str
        File name of the magic state file from the `state_files_directory` specified in the kwargs.
    """
    filename = (str(kwargs['state_files_directory']) + '/' + str(kwargs['magic']) + '_magic_state' +
                '_ENC_' + str(kwargs['encoding']) +
                '_MPN_' + str(kwargs['mean_photon_number_l']) + '_' + str(kwargs['mean_photon_number_r']) +
                '_EP_' + str(list(map(lambda a: f'{a:.5f}', kwargs['emission_probabilities_l']))) + '_' +
                str(list(map(lambda a: f'{a:.5f}', kwargs['emission_probabilities_r']))) +
                '_CL_' + str(kwargs['channel_length_l'])[:7] + '_' + str(kwargs['channel_length_r'])[:7] +
                '_ATT_' + str(kwargs['attenuation_l']) + '_' + str(kwargs['attenuation_r']) +
                '_LO_' + str(kwargs['coupling_loss_fibre']) +
                '_DC_' + str(kwargs['det_dark_count_prob']) +
                '_EF_' + str(kwargs['det_efficiency']) +
                '_V_' + str(kwargs['det_visibility']) +
                '_NR_' + str(kwargs['det_num_resolving']) +
                '.pickle')
    if unique_str is not None:
        filename = filename[:-7] + "_" + unique_str + '.pickle'

    return filename


def create_el_link_for_forced_generation(encoding, mean_photon_number, emission_probabilities, channel_length_l,
                                         channel_length_r, attenuation_l, attenuation_r, coupling_loss_fibre,
                                         det_dark_count_prob, det_efficiency, det_visibility, det_num_resolving):
    """Sets up an elementary link using atomic ensembles.

    This should be used to generate sampled magic for an elementary link.
    This method now supports asymmetric channel lengths and sets up corresponding delays for emission protocols.

     Schematics of the setup:

     .. code-block:: text

        "Alice"            "HeraldedConnection"         "Bob"

        ----------                                      ----------
        o        o   <-  messaging ports (unused) ->    o        o
        | |Clk|  |                                      | |Clk|  |
        |        |                                      |        |
        o-|QMem| | ------------------------------------ | |QMem|-o
        |    |   | | <<<<<CC<<<<<o------o>>>>>CC>>>>> | |   |    |
        |   (PPS)-o              | QDet |               o-(PPS)  |
        ---------- | >>>>>QC>>>>>o------o<<<<<QC<<<<< | ----------
                   ------------------------------------

        Where:
        o               is a port
        >>CC/QC>>       is a one way classical/quantum channel
        |QMem|          is a quantum memory
        (PPS)           is a source of entangled photon pairs
        |Clk|           is a clock
        |QDet|          is a detector performing a linear optical BSM
        ----            boundaries of a node / component

    Parameters
    ----------
    encoding : str
        Encoding of the photons. Either "time_bin" or "presence_absence".
    mean_photon_number : list or None
        List of mean photon numbers for the thermal distribution of the SPDC sources along the chain.
        The `mpn_index` (see below) then determines which mean photon number is assigned to which source. This allows
        for different mean photon numbers along the chain.
        Must be supplied when `emission_probabilities` is None.
    emission_probabilities: list or None
        List of lists of probabilities of emitting a certain number of photons along the chain.
        The `mpn_index` (see below) then determines which list of emission_probabilities is assigned to which source.
        This allows for different emission_probabilities along the chain.
        This emission_probabilities must be a list of length 4, where each element denotes the probability of
        generating a photon pair with the number of photons equal to its list index.
    channel_length_l : float
        Length of the channel left of the detectors [in km].
    channel_length_r : float
        Length of the channel right of the detectors [in km].
    attenuation_l : float
        Attenuation of the channel left of the detectors [in dB/km].
    attenuation_r: float
        Attenuation of the channel right of the detectors [in dB/km].
    coupling_loss_fibre : float
        Coupling loss/ length independent loss of the channel to the midpoint, in [0,1].
    det_dark_count_prob : float
        Dark count probability of the midpoint detectors.
    det_efficiency : float
        Detection efficiency of the midpoint detectors.
    det_visibility : float
        Photon indistinguishability at the midpoint BSM.
    det_num_resolving : bool
        Whether or not midpoint detectors are number-resolving.

    Returns
    -------
    protocols : list
        List of all protocols (:class:`~netsquid.protocols.Protocol`) in the simulation. (here only Alice's and Bob's)
    network : :class:`~netsquid.nodes.network.Network`
        Network component that contains all the :class:`~netsquid.nodes.Node`s and
        :class:`~netsquid.connections.Connection`s that represent the physical network.
    params : dict
        Full dictionary of simulation parameters used to generate the forced magic state.

    """
    params = {"encoding": encoding,
              "mean_photon_number": mean_photon_number, "emission_probabilities": emission_probabilities,
              "channel_length_l": channel_length_l, "channel_length_r": channel_length_r,
              "attenuation_l": attenuation_l, "attenuation_r": attenuation_r,
              "coupling_loss_fibre": coupling_loss_fibre,
              "det_dark_count_prob": det_dark_count_prob,
              "det_efficiency": det_efficiency, "det_visibility": det_visibility,
              "det_num_resolving": det_num_resolving,
              # fixed params for generation of states
              "memory_coherence_time": np.inf, "max_memory_efficiency": 1.,
              "memory_time_dependence": "exponential", "mem_num_modes": 1,
              "source_num_modes": 1, "Rate": 1, "g2": 1, "magic": None, "multi_photon": True, "num_attempts": 1,
              "time_step": 1.25 * (1 + channel_length_l + channel_length_r) / 2e5,
              "fibre_phase_stdv_l": 0, "fibre_phase_stdv_r": 0, "num_repeaters": 0,
              "state_files_directory": None, "multiple_link_successes": False, "num_attempts_proto": -1,
              "source_frequency": 1,
              }
    mpn_index_alice, mpn_index_bob = 0, 1

    # Create physical network
    network = Network(name="Elementary Link Setup")
    # Two end nodes Alice and Bob
    alice = EndNode("Alice", mpn_index=mpn_index_alice, **params)
    bob = EndNode("Bob", mpn_index=mpn_index_bob, **params)
    # Add them to the Network
    network.add_nodes([alice, bob])
    # Create a heralded connection and add it to the Network by connecting it to Alice and Bob
    heralded_connection = HeraldedConnection("HeraldedConnection", **params)
    network.add_connection(node1=alice, node2=bob, connection=heralded_connection, label="heralding",
                           port_name_node1="IO_Connection_E", port_name_node2="IO_Connection_E")
    # Finds number of multiplexing modes for simulation
    params["num_multiplexing_modes"] = min(alice.properties["num_multiplexing_modes"],
                                           bob.properties["num_multiplexing_modes"])
    # Alice
    if params["magic"]:
        raise ValueError("no magic")
    else:
        # running full simulation, adding regular EmissionProtocol
        # calculate delays in fibre to midpoint to assure asymmetric arrivals from both sides at the same time
        delay_a = max(0, 1e9 * params["channel_length_r"] / 200000 - 1e9 * params["channel_length_l"] / 200000)
        emission_prot_a = EmissionProtocol(alice, memory=alice.subcomponents["QMem"],
                                           source=alice.subcomponents["PPS"],
                                           delay=delay_a,
                                           max_attempts=params["num_attempts_proto"])
        # Bob
        delay_b = max(0, 1e9 * params["channel_length_l"] / 200000 - 1e9 * params["channel_length_r"] / 200000)
        emission_prot_b = EmissionProtocol(bob, memory=bob.subcomponents["QMem"],
                                           source=bob.subcomponents["PPS"],
                                           delay=delay_b,
                                           max_attempts=params["num_attempts_proto"])

    protocols = [emission_prot_a, emission_prot_b]

    return protocols, network, params
